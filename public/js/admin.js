window.addEventListener('load', function () {

    function sendXhrRequest(params) {
        let xhr = new XMLHttpRequest();
        if ("withCredentials" in xhr) {
            xhr.open(params.method, params.url, true);
        } else if (typeof XDomainRequest !== "undefined") {
            xhr = new XDomainRequest();
            xhr.open(params.method, params.url + ((/\?/).test(url) ? "&" : "?") + (new Date()).getTime(), true);
        } else {
            xhr = null;
        }
        if (!xhr) alert('XHR not supported!');
        if (params.data) {
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        }
        if (params.onload) {
            xhr.onload = function () {
                params.onload(xhr);
            };
        }
        if (params.data) {
            if (params.data.indexOf('_token') < 0) {
                params.data += '&_token=' + document.querySelector('[name="csrf-token"]').getAttribute('content');
            }
            xhr.send(params.data);
        } else {
            xhr.send();
        }
    }

    addEventListeners();

    function addEventListeners()
    {
        addSearchCityEventListener();
        addSearchWarehouseEventListener();
        addChangeCityDeliveryIdEventListener();
        addCustomSelectEventListener();
    }

    dependedComponentTrigger();

    function dependedComponentTrigger()
    {
        let element = document.querySelector('[data-depended-change-trigger]');
        if(element == undefined){
            return;
        }
        element.addEventListener('change',function (){
            setTimeout(function (){
                addEventListeners();
            },500)
        })
    }

    let delayed = null;

    function addSearchCityEventListener(){
        let block = document.querySelector('[data-delivery-city-select]');
        if(block == undefined) return;
        let element = block.querySelector('[data-city-search-input]');
        if(element == undefined) return;
        let optionsBlock = block.querySelector('[data-options]');
        if(optionsBlock == undefined) return;
        element.addEventListener('input',function (e){
            clearTimeout(delayed);
            delayed = setTimeout(function () {
                let needle = e.target.value;
                if(needle.length < 3) {
                    block.querySelector('[data-selected-title]').value = null;
                    block.querySelector('[data-selected-delivery-id]').value = null;

                    let refBlock = block.querySelector('[data-selected-ref]');
                    refBlock.value = null;
                    let evt = document.createEvent("HTMLEvents");
                    evt.initEvent("change", false, true);
                    refBlock.dispatchEvent(evt);

                    let warehouseContainer = document.querySelector('[data-delivery-warehouse-container]');
                    let inputs = warehouseContainer.querySelectorAll('input');
                    inputs.forEach(function (item){
                        item.value = null;
                    });
                    return;
                }
                let data = 'needle=' + needle;
                sendXhrRequest({
                    url: '/services/novaposhta/city',
                    method: 'POST',
                    onload: function (data) {
                        let res = JSON.parse(data.responseText);
                        optionsBlock.innerHTML = res.options;
                        block.querySelector('[data-delivery-select]').classList.add('open')
                        addCustomSelectEventListener();
                    },
                    data: data
                });
            }, 500);
        });
    }

    function addSearchWarehouseEventListener(){

        let block = document.querySelector('[data-delivery-warehouse-select]');
        if(block == undefined) return;
        let element = block.querySelector('[data-warehouse-search-input]');
        if(element == undefined) return;
        let optionsBlock = block.querySelector('[data-options]');
        if(optionsBlock == undefined) return;

        element.addEventListener('input',function (e){document.execCommand("undo");});
        element.addEventListener('click',function (e){
            if(optionsBlock.dataset.hasNext == 'false') {
                block.querySelector('[data-delivery-select]').classList.add('open');
                return;
            }
            let blockCity = document.querySelector('[data-delivery-city-select]');
            if(blockCity == undefined) {
                return;
            }
            let cityInput = blockCity.querySelector('[data-selected-delivery-id]');
            if(cityInput == undefined) {
                return;
            }
            let cityDeliveryId = cityInput.value;
            if(!cityDeliveryId || cityDeliveryId.length < 15) return;
            let pageValue = optionsBlock.dataset.page;
            let page = parseInt(pageValue);
            clearTimeout(delayed);
            delayed = setTimeout(function () {
                let data = 'cityDeliveryId=' + cityDeliveryId + '&page='+page;
                sendXhrRequest({
                    url: '/services/novaposhta/warehouse',
                    method: 'POST',
                    onload: function (data) {
                        let res = JSON.parse(data.responseText);
                        optionsBlock.dataset.page = ++page;
                        optionsBlock.insertAdjacentHTML('beforeend', res.options);
                        block.querySelector('[data-delivery-select]').classList.add('open');
                        addCustomSelectEventListener();
                        optionsBlock.dataset.hasNext = res.hasNext;
                        optionsBlock.addEventListener('scroll',function (e){
                            scrollWarehouse(e,cityDeliveryId,optionsBlock);
                        });
                    },
                    data: data
                });
            }, 500);
        });
    }

    function scrollWarehouse(event,cityDeliveryId,optionsBlock){
        let element = event.target;
        if (element.scrollHeight - element.scrollTop === element.clientHeight)
        {
            if(optionsBlock.dataset.hasNext == 'false') return;
            let pageValue = optionsBlock.dataset.page;
            let page = parseInt(pageValue);
            let data = 'cityDeliveryId=' + cityDeliveryId + '&page=' + page;
            sendXhrRequest({
                url: '/services/novaposhta/warehouse',
                method: 'POST',
                onload: function (data) {
                    let res = JSON.parse(data.responseText);
                    optionsBlock.dataset.page = ++page;
                    optionsBlock.dataset.hasNext = res.hasNext;
                    if(res.hasItems){
                        optionsBlock.insertAdjacentHTML('beforeend', res.options);
                        addCustomSelectEventListener();
                    }
                },
                data: data
            });
        }
    }

    function addChangeCityDeliveryIdEventListener(){
        let container = document.querySelector('[data-delivery-container]');
        if (container == undefined) return;
        let element = container.querySelector('[data-selected-delivery-id]');
        if(element == undefined) return;
        element.addEventListener('change',function (e){
            let warehouseContainer = container.querySelector('[data-delivery-warehouse-container]');
            if(warehouseContainer == undefined) return;
            let optionsBlock = warehouseContainer.querySelector('[data-options]');
            optionsBlock.dataset.hasNext = 'true';
            optionsBlock.dataset.page = 1;
            optionsBlock.innerHTML = '';
            let inputs = warehouseContainer.querySelectorAll('input');
            inputs.forEach(function (item){
                item.value = null;
            });
        });
    }

    function addCustomSelectEventListener(){
        let elements = document.querySelectorAll('[data-delivery-select]');
        if(elements == undefined) return;
        elements.forEach(function (element){
            element.addEventListener('click', function() {
                this.classList.toggle('open');
            });
            for (const option of element.querySelectorAll("[data-option]")) {
                option.addEventListener('click', function() {
                    if (!this.classList.contains('selected')) {
                        let selected = this.parentNode.querySelector('[data-option].selected');
                        if(selected != undefined) selected.classList.remove('selected');
                        this.classList.add('selected');
                        let customSelect = this.closest('[data-delivery-select]');
                        let deliveryId = this.dataset.deliveryId;
                        let ref = this.dataset.ref;

                        customSelect.querySelector('[data-custom-select-component-triger]').value = this.textContent;

                        let title = customSelect.querySelector('[data-selected-title]');
                        if(title != undefined){
                            title.value = this.textContent;
                        }

                        let delId = customSelect.querySelector('[data-selected-delivery-id]');
                        if(delId != undefined){
                            delId.value = deliveryId;
                            let evt = document.createEvent("HTMLEvents");
                            evt.initEvent("change", false, true);
                            delId.dispatchEvent(evt);
                        }

                        let refId = customSelect.querySelector('[data-selected-ref]');
                        if(refId != undefined){
                            refId.value = ref;
                            let evt = document.createEvent("HTMLEvents");
                            evt.initEvent("change", false, true);
                            refId.dispatchEvent(evt);
                        }

                    }
                })
            }
        })

    }

    window.addEventListener('click', function(e) {
        let elements = document.querySelectorAll('[data-delivery-select]');
        elements.forEach(function (element){
            if (!element.contains(e.target)) {
                element.classList.remove('open');
            }
        });
    });
});
function aclCheckGroup(element)
{
    let checked = element.checked;
    let scope = element.dataset.scope;
    let targetElements = document.querySelectorAll('[data-acl-' + scope +']');
    if(targetElements == undefined) return;
    targetElements.forEach(function (aclElement)
    {
        aclElement.checked = checked == true;
    });
}
