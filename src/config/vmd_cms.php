<?php

return [
    'frontend_app_url' => env('API_APP_URL','https://site.com'),
    "url_prefix" => "dashboard",
    "url_multi_languages" => true,
    "use_queue_jobs" => false,
    "use_group_translates" => false,
    "base_lang" => [
        "key" => "ru",
        "symbol" => "Рус",
        "title" => "Русский",
    ],
    "dashboard_base_lang" => [
        "key" => "ru",
        "symbol" => "Рус",
        "title" => "Русский",
    ],
    "navigation_drawer" =>
    [
        "logo_path" => "/files/vendor/vmd_cms/svg/client/logo.svg",
        "color" => "#192A3E",
        "color_active" => "#152231",
    ]
];
