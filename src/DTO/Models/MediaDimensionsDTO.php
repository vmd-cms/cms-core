<?php

namespace VmdCms\CoreCms\DTO\Models;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use VmdCms\CoreCms\Models\Media\MediaDimension;
use VmdCms\CoreCms\Services\Files\StorageAdapter;

class MediaDimensionsDTO implements Arrayable
{
    /**
     * @var array
     */
    protected $dimensionsArr;

    public function __construct(Collection $dimensions)
    {
        $this->dimensionsArr = [];
        $this->mapDimensionsArr($dimensions);
    }

    protected function mapDimensionsArr(Collection $dimensions){

       if(is_countable($dimensions)){
           $storageAdapter = StorageAdapter::getInstance();
           foreach ($dimensions as $dimension)
           {
               if($dimension instanceof MediaDimension)
               {
                   $this->dimensionsArr[$dimension->key] = $storageAdapter->getAbsolutePath($dimension->path);
               }
           }
       }
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->dimensionsArr;
    }
}
