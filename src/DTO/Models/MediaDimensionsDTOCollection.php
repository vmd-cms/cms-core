<?php

namespace VmdCms\CoreCms\DTO\Models;

use VmdCms\CoreCms\Collections\CoreCollectionAbstract;

class MediaDimensionsDTOCollection extends CoreCollectionAbstract
{
    /**
     * @param MediaDimensionsDTO $dto
     */
    public function append(MediaDimensionsDTO $dto)
    {
        $this->collection->add($dto);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function toArray()
    {
        return $this->collection->map(function (MediaDimensionsDTO $item){
            return $item->toArray();
        });
    }
}
