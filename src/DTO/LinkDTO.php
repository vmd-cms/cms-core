<?php

namespace VmdCms\CoreCms\DTO;

use VmdCms\CoreCms\Contracts\DTO\LinkDtoInterface;

class LinkDTO implements LinkDtoInterface
{
    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $link;

    /**
     * LinkDTO constructor.
     * @param string $title
     * @param string $link
     */
    public function __construct(string $title = '', string $link = '')
    {
        $this->title = $title;
        $this->link = $link;
    }

    public function toArray()
    {
        return [
            'title' => $this->title,
            'link' => $this->link,
        ];
    }
}
