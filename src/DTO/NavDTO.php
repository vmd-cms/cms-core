<?php

namespace VmdCms\CoreCms\DTO;

use VmdCms\CoreCms\Contracts\Collections\NavsCollectionInterface;
use VmdCms\CoreCms\Contracts\DTO\NavDtoInterface;

class NavDTO implements NavDtoInterface
{
    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $link;

    /**
     * @var NavsCollectionInterface
     */
    public $children;

    /**
     * @var string
     */
    public $icon;

    /**
     * NavDTO constructor.
     * @param string $title
     * @param string $link
     */
    public function __construct(string $title = null, string $link = null)
    {
        $this->title = $title;
        $this->link = $link;
    }

    public function toArray()
    {
        return [
            'title' => $this->title,
            'link' => $this->link,
            'icon' => $this->icon,
            'children' => $this->children instanceof NavsCollectionInterface ? $this->children->toArray() : null,
        ];
    }

    public function setChildren(NavsCollectionInterface $children): NavDtoInterface
    {
        $this->children = $children;
        return $this;
    }

    /**
     * @param string $path
     * @return $this|NavDtoInterface
     */
    public function setIcon(string $path = null): NavDtoInterface
    {
        $this->icon = $path;
        return $this;
    }
}
