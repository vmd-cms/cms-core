<?php

namespace VmdCms\CoreCms\DTO;

use VmdCms\CoreCms\Contracts\DTO\SectionRepositoryDtoInterface;
use VmdCms\CoreCms\CoreModules\Modules\Models\CoreModule;

class SectionRepositoryDto implements SectionRepositoryDtoInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $adminSectionClass;

    /**
     * @var string
     */
    private $coreModuleClass;

    /**
     * @var CoreModule|null
     */
    private $coreModule;

    /**
     * @var string
     */
    private $title;

    /**
     * SectionRepositoryDto constructor.
     * @param int $id
     * @param string $adminSectionClass
     * @param string $title
     */
    public function __construct(int $id, string $adminSectionClass, string $title)
    {
        $this->id = $id;
        $this->adminSectionClass = $adminSectionClass;
        $this->title = $title;
    }

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @inheritDoc
     */
    public function getAdminSectionClass(): string
    {
        return $this->adminSectionClass;
    }

    /**
     * @inheritDoc
     */
    public function getKey(): string
    {
        return ' ';
    }

    /**
     * @param null|CoreModule $coreModule
     * @return $this
     */
    public function setCoreModule(?CoreModule $coreModule): self
    {
        $this->coreModule = $coreModule;
        return $this;
    }

    /**
     * @return null|CoreModule
     */
    public function getCoreModule(): ?CoreModule
    {
        return $this->coreModule;
    }
}
