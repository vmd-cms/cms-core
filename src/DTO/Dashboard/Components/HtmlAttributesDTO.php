<?php

namespace VmdCms\CoreCms\DTO\Dashboard\Components;

use VmdCms\CoreCms\Contracts\DTO\HtmlAttributesDTOInterface;

class HtmlAttributesDTO implements HtmlAttributesDTOInterface
{
    /**
     * @var array
     */
    protected $classes;

    public function __construct()
    {
        $this->classes = [];
    }

    /**
     * @param string $element
     * @param string $class
     * @return $this
     */
    public function setClass(string $element, string $class): HtmlAttributesDTOInterface
    {
        $this->classes[$element] = [$class];
        return $this;
    }

    /**
     * @param string $element
     * @param string $class
     * @return $this
     */
    public function appendClass(string $element, string $class): HtmlAttributesDTOInterface
    {
        $this->classes[$element][] = $class;
        return $this;
    }

    protected function renderClasses(): array
    {
        $result = [];
        foreach ($this->classes as $element=>$classArr){
            $result[$element] = implode(' ',$classArr);
        }
        return $result;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'classes' => $this->renderClasses()
        ];
    }
}
