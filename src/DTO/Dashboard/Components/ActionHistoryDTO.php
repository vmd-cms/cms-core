<?php

namespace VmdCms\CoreCms\DTO\Dashboard\Components;

use Illuminate\Contracts\Support\Arrayable;

class ActionHistoryDTO implements Arrayable
{
    /**
     * @var string
     */
    protected $dateStr;

    /**
     * @var string
     */
    protected $iconClass;

    /**
     * @var string
     */
    protected $photoPath;

    /**
     * @var string
     */
    protected $titleStr;

    public function __construct($titleStr,$dateStr,$photoPath = null,$iconClass = null)
    {
        $this->titleStr = $titleStr;
        $this->dateStr = $dateStr;
        $this->photoPath = $photoPath;
        $this->iconClass = $iconClass;
    }

    /**
     * @param string $dateStr
     * @return ActionHistoryDTO
     */
    public function setDateStr(string $dateStr): ActionHistoryDTO
    {
        $this->dateStr = $dateStr;
        return $this;
    }

    /**
     * @param string $iconClass
     * @return ActionHistoryDTO
     */
    public function setIconClass(?string $iconClass): ActionHistoryDTO
    {
        $this->iconClass = $iconClass;
        return $this;
    }

    /**
     * @param string $photoPath
     * @return ActionHistoryDTO
     */
    public function setPhotoPath(?string $photoPath): ActionHistoryDTO
    {
        $this->photoPath = $photoPath;
        return $this;
    }

    /**
     * @param string $titleStr
     * @return ActionHistoryDTO
     */
    public function setTitleStr(string $titleStr): ActionHistoryDTO
    {
        $this->titleStr = $titleStr;
        return $this;
    }

    public function toArray()
    {
        return [
            'title' => $this->titleStr,
            'date' => $this->dateStr,
            'photo' => $this->photoPath,
            'icon' => $this->iconClass
        ];
    }
}
