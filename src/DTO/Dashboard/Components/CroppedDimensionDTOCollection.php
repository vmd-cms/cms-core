<?php

namespace VmdCms\CoreCms\DTO\Dashboard\Components;

use VmdCms\CoreCms\Collections\CoreCollectionAbstract;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\CroppedDimensionDTOCollectionInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\CroppedDimensionDTOInterface;

class CroppedDimensionDTOCollection extends CoreCollectionAbstract implements CroppedDimensionDTOCollectionInterface
{
    /**
     * @inheritDoc
     */
    public function appendItem(CroppedDimensionDTOInterface $dto): void
    {
        $this->collection->add($dto);
    }
}
