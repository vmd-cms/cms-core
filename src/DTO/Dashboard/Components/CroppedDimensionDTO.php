<?php

namespace VmdCms\CoreCms\DTO\Dashboard\Components;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\CroppedDimensionDTOInterface;

class CroppedDimensionDTO implements CroppedDimensionDTOInterface
{
    /**
     * @var string
     */
    protected $key;

    /**
     * @var int|null
     */
    protected $width;

    /**
     * @var int|null
     */
    protected $height;

    /**
     * @var int
     */
    protected $quality;

    /**
     * @param string $key
     * @param int|null $width
     * @param int|null $height
     * @param int $quality
     */
    public function __construct(string $key, int $width = null, int $height = null, int $quality = 90)
    {
        $this->key = $key;
        $this->width = $width;
        $this->height = $height;
        $this->setQuality($quality);
    }

    /**
     * @param int $quality
     * @return CroppedDimensionDTOInterface
     */
    public function setQuality(int $quality): CroppedDimensionDTOInterface
    {
        $this->quality = 0 < $quality && $quality<= 100 ? $quality : 90;
        return $this;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @return int|null
     */
    public function getWidth(): ?int
    {
        return $this->width;
    }

    /**
     * @return int|null
     */
    public function getHeight(): ?int
    {
        return $this->height;
    }

    /**
     * @return int
     */
    public function getQuality(): int
    {
        return $this->quality;
    }

    public function toArray()
    {
        return [
           'key' => $this->key,
           'width' => $this->width,
           'height' => $this->height,
           'quality' => $this->quality,
        ];
    }
}
