<?php

namespace VmdCms\CoreCms\DTO\Dashboard;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormComponentInterface;
use VmdCms\CoreCms\Contracts\DTO\DependedComponentDtoInterface;

class DependComponentDto implements DependedComponentDtoInterface
{
    /**
     * @var string
     */
    private $key;

    /**
     * @var FormComponentInterface
     */
    private $component;

    public function __construct(string $key, FormComponentInterface $component)
    {
        $this->key = $key;
        $this->component = $component;
    }

    /**
     * @return string
     */
    public function getKey() : string
    {
        return $this->key;
    }

    /**
     * @return FormComponentInterface
     */
    public function getFormComponent() : FormComponentInterface
    {
        return $this->component;
    }

    public function toArray()
    {
        return [
          'key' => $this->key,
          'component' => $this->component->toArray()
        ];
    }
}
