<?php

namespace VmdCms\CoreCms\DTO\Dashboard;

use VmdCms\CoreCms\Contracts\DTO\ModeratorPermissionDtoInterface;
use VmdCms\CoreCms\CoreModules\Moderators\Models\ModeratorRolePermission;

class ModeratorPermissionDto implements ModeratorPermissionDtoInterface
{

    /**
     * @var ModeratorRolePermission
     */
    private $item;

    public function __construct(ModeratorRolePermission $item)
    {
        $this->item = $item;
    }

    public function getModeratorRoleId(): ?int
    {
        return $this->item->moderator_role_id ?? null;
    }

    public function getCoreSectionId(): int
    {
        return $this->item->core_section_id;
    }

    public function isDisplayable(): bool
    {
        return $this->item->display_perm ?? false;
    }

    public function isCreatable(): bool
    {
        return $this->item->create_perm ?? false;
    }

    public function isViewable(): bool
    {
        return $this->item->view_perm ?? false;
    }

    public function isEditable(): bool
    {
        return $this->item->edit_perm ?? false;
    }

    public function isDeletable(): bool
    {
        return $this->item->delete_perm ?? false;
    }

}
