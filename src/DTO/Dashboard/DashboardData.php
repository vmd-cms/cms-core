<?php

namespace VmdCms\CoreCms\DTO\Dashboard;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use VmdCms\CoreCms\Contracts\Navigation\NavInterface;
use VmdCms\CoreCms\Contracts\Navigation\NavItemInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Entity\CoreTranslatesEntity;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\CoreModules\Moderators\Entity\AuthEntity;
use VmdCms\CoreCms\CoreModules\Moderators\Models\ModeratorRolePermission;
use VmdCms\CoreCms\CoreModules\Navigations\Models\CoreNav;
use VmdCms\CoreCms\CoreModules\Navigations\Models\CoreNavDashboard;
use VmdCms\CoreCms\CoreModules\Navigations\Models\CoreNavSettings;
use VmdCms\CoreCms\DTO\Dashboard\Navs\NavBar;
use VmdCms\CoreCms\DTO\Dashboard\Navs\NavItem;
use VmdCms\CoreCms\DTO\Dashboard\Navs\NavItemDto;
use VmdCms\CoreCms\Exceptions\Sections\SectionNotAvailableException;
use VmdCms\CoreCms\Initializers\AdminInitializer;
use VmdCms\CoreCms\Services\Config;

class DashboardData
{
    public $appBarData;

    /**
     * @var string
     */
    public $navBar;

    /**
     * @var string
     */
    public $navBarAdmin;

    private $repository;

    private $displayPermissions;

    private $authModerator;

    /**
     * @var AdminInitializer
     */
    private $initializer;

    /**
     * @var null|array
     */
    private $sectionsIdSlugMap;

    /**
     * @var null|array
     */
    public $translates;

    /**
     * @var string
     */
    public $appTranslates;

    /**
     * @var bool
     */
    public $isShowAdminNavs;

    public function __construct(AdminInitializer $initializer)
    {
        $this->initializer = $initializer;
        $this->authModerator = AuthEntity::getAuthModerator();
        $this->displayPermissions = $this->getDisplayPermissions();
        $this->sectionsIdSlugMap = $this->initializer->getSectionsIdSlugMap();
        $this->repository = CoreNav::tree()->with('section','info')->get()->groupBy('anchor');
        $this->navBar = $this->setData(CoreNavDashboard::ANCHOR_KEY,
            (new NavBar(Config::getInstance()->getNavigationDrawer())))->getJson();
        $navbarAdmin = $this->setData(CoreNavSettings::ANCHOR_KEY,new NavBar(Config::getInstance()->getNavigationDrawer()));
        $this->navBarAdmin = $navbarAdmin->getJson();
        $this->initializeAppBarData();
        $this->translates = CoreLang::getTranslates();
        $this->appTranslates = json_encode($this->translates,JSON_UNESCAPED_UNICODE);
        $navbarAdminItems = $navbarAdmin->getItems();
        $this->isShowAdminNavs = is_array($navbarAdminItems) && count($navbarAdminItems);
    }

    protected function initializeAppBarData(){
        $appBarData = [
            'languages_data' => (new LanguagesDataDto())->toArray(),
            'moderator' => (new ModeratorDto($this->authModerator))->toArray(),
            'copyright' => $this->getCopyright(),
            'external_link' => Config::getInstance()->getFrontendAppUrl(),
        ];
        $this->appBarData = json_encode($appBarData,JSON_UNESCAPED_UNICODE);
    }

    protected function getCopyright(): string
    {
        return '© ' . Carbon::now()->format('Y') . ' Valmax Digital';
    }

    protected function getDisplayPermissions(){
        return ModeratorRolePermission::where('moderator_role_id',$this->authModerator->moderator_role_id)
            ->where('display_perm',true)->get();
    }

    /**
     * @param $item
     * @throws SectionNotAvailableException
     */
    private function checkAccess($item)
    {
        if(!$this->authModerator->has_full_access && $item->section_id && !$this->displayPermissions->where('core_section_id',$item->section_id)->first()){
            throw new SectionNotAvailableException();
        }
    }

    private function setData($anchor,NavInterface $navBar)
    {
        if(!$data = $this->repository[$anchor] ?? null) return $navBar->getJson();
        foreach ($data as $item){
            try {
                $navBar->appendItem(new NavItemDto($this->recursiveSetItem($item)));
            }
            catch (SectionNotAvailableException $e) {
                continue;
            }
        }

        return $navBar;
    }

    /**
     * @param $item
     * @return NavItemInterface
     * @throws SectionNotAvailableException
     */
    private function recursiveSetItem($item) : NavItemInterface
    {

        if($item->section_id && !isset($this->sectionsIdSlugMap[$item->section_id])) {
            throw new SectionNotAvailableException();
        }

        if(!$item->active) {
            throw new SectionNotAvailableException();
        }

        $this->checkAccess($item);

        $slug = isset($this->sectionsIdSlugMap[$item->section_id]) ? $this->sectionsIdSlugMap[$item->section_id] : null;

        $navItem = (new NavItem())->setId($item->id)
            ->setUrl($slug)
            ->setTitle($item->titleLang)
            ->setIcon($item->icon)
            ->setIsGroupTitle($item->is_group_title);

        try {
            $navItem = $navItem->setChip($this->initializer->getAdminSectionByKey($slug)->getChip());
        }
        catch (\Exception $exception){}

        $children = $item->childrenTree ?? [];

        if(!count($children)) return $navItem;

        foreach ($children as $child) {
            try {
                $navItem->appendChild($this->recursiveSetItem($child));
            }
            catch (SectionNotAvailableException $e) {
                continue;
            }
        }
        if(!$navItem->url && !count($navItem->getChildrenAttribute())) throw new SectionNotAvailableException();

        return $navItem;
    }

}
