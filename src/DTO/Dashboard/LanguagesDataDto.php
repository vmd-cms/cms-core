<?php

namespace VmdCms\CoreCms\DTO\Dashboard;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\CoreCms\CoreModules\Content\Entity\Languages;
use VmdCms\CoreCms\CoreModules\Languages\Models\CoreLanguage;
use VmdCms\CoreCms\Services\CoreRouter;

class LanguagesDataDto implements Arrayable
{
    protected $route;

    public function __construct(){
        $this->route = route(CoreRouter::ROUTE_SET_LOCALE,null,false);
    }

    protected function getLocalesArray(){
        $languagesDTO = [];
        if(is_countable(Languages::getInstance()->getLanguages())){
            foreach (Languages::getInstance()->getLanguages() as $language){
                $languagesDTO[] = [
                    'key' => $language->key,
                    'title' => $language->title,
                    'icon' => $language->imagePath,
                ];
            }
        }
        return $languagesDTO;
    }

    public function toArray()
    {
        return [
            'languages' => $this->getLocalesArray(),
            'default' => Languages::getInstance()->getDefaultDashboardLocale(),
            'current' => Languages::getInstance()->getCurrentLocale(),
            'route' => $this->route
        ];
    }
}
