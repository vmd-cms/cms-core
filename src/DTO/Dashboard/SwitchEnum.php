<?php

namespace VmdCms\CoreCms\DTO\Dashboard;

class SwitchEnum
{
    public $value;

    public $label;

    public function __construct(string $value,string $label)
    {
        $this->value = $value;
        $this->label = $label;
    }

    public function toArray(){
        return [
            'value' => $this->value,
            'label' => $this->label
        ];
    }
}
