<?php

namespace VmdCms\CoreCms\DTO\Dashboard\Navs;

use VmdCms\CoreCms\Contracts\Navigation\NavItemInterface;
use VmdCms\CoreCms\Services\UrlGenerator;

class NavItem implements NavItemInterface
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $icon;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $url;

    /**
     * @var bool
     */
    public $isGroupTitle;

    /**
     * @var array
     */
    public $chip;

    /**
     * @var array[self]
     */
    public $children = [];

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param null|string $icon
     * @return $this
     */
    public function setIcon(?string $icon): self
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @param null|string $url
     * @return $this
     */
    public function setUrl($url): self
    {
        $this->url = UrlGenerator::getInstance()->getDashboardUrl($url);
        return $this;
    }

    /**
     * @param null|ChipItemDto $chip
     * @return $this
     */
    public function setChip(ChipItemDto $chip = null): self
    {
        if($chip instanceof ChipItemDto) $this->chip = $chip->toArray();
        return $this;
    }

    /**
     * @return array|null
     */
    public function getChip(): ?array
    {
        return $this->chip;
    }

    /**
     * @param bool $isGroupTitle
     * @return $this
     */
    public function setIsGroupTitle($isGroupTitle): self
    {
        $this->isGroupTitle = $isGroupTitle;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getIdAttribute(): ?int
    {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function getIconAttribute(): ?string
    {
        return $this->icon;
    }

    /**
     * @inheritDoc
     */
    public function getTitleAttribute(): ?string
    {
        return $this->title;
    }

    /**
     * @inheritDoc
     */
    public function getUrlAttribute(): ?string
    {
        return $this->url;
    }

    /**
     * @inheritDoc
     */
    public function getChildrenAttribute(): array
    {
        return $this->children;
    }

    public function isGroupTitle(): bool
    {
        return $this->isGroupTitle;
    }

    /**
     * @inheritDoc
     */
    public function appendChild(NavItemInterface $navItem): NavItemInterface
    {
        $this->children[] = $navItem;
        return $this;
    }
}
