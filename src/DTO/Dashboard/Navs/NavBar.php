<?php

namespace VmdCms\CoreCms\DTO\Dashboard\Navs;

use VmdCms\CoreCms\Contracts\Navigation\NavInterface;
use VmdCms\CoreCms\Contracts\Navigation\NavItemDtoInterface;

class NavBar implements NavInterface
{

    private $items;

    /**
     * @var array
     */
    private $data;

    public function __construct(array $navBarConfig = [])
    {
        $this->data['logo_path'] = $navBarConfig['logo_path'] ?? null;
        $this->data['color'] = $navBarConfig['color'] ?? null;
        $this->data['color_active'] = $navBarConfig['color_active'] ?? null;
    }

    public function getJson() : string
    {
        $this->data['items'] = $this->items ?? [];
        return json_encode($this->data);
    }

    /**
     * @return null|array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param NavItemDtoInterface $navItem
     * @return $this
     */
    public function appendItem(NavItemDtoInterface $navItem) : NavInterface
    {
        $this->items[] = $navItem;
        return $this;
    }
}
