<?php

namespace VmdCms\CoreCms\DTO\Dashboard\Navs;

use Illuminate\Contracts\Support\Arrayable;

class ChipItemDto implements Arrayable
{
    /**
     * @var int|string
     */
    private $title;

    /**
     * @var string
     */
    private $color;

    /**
     * ChipItemDto constructor.
     * @param string|int $title
     * @param string $color
     */
    public function __construct($title, string $color)
    {
        $this->title = $title;
        $this->color = $color;
    }

    public function toArray()
    {
        return [
            'title' => $this->title,
            'color' => $this->color
        ];
    }
}
