<?php

namespace VmdCms\CoreCms\DTO\Dashboard\Navs;

use VmdCms\CoreCms\Contracts\Navigation\NavItemDtoInterface;
use VmdCms\CoreCms\Contracts\Navigation\NavItemInterface;

final class NavItemDto implements NavItemDtoInterface
{
    /**
     * @var int|null
     */
    public $id;

    /**
     * @var string|null
     */
    public $icon;

    /**
     * @var string|null
     */
    public $url;

    /**
     * @var string|null
     */
    public $title;

    /**
     * @var array
     */
    public $children;

    /**
     * @var array
     */
    public $chip;

    /**
     * @inheritDoc
     */
    public function __construct(NavItemInterface $navItem)
    {
        $this->id = $navItem->getIdAttribute();
        $this->icon = $navItem->getIconAttribute();
        $this->url = $navItem->getUrlAttribute();
        $this->title = $navItem->getTitleAttribute();
        $this->is_group_title = $navItem->isGroupTitle();
        $this->children = $navItem->getChildrenAttribute();
        $this->chip = $navItem->getChip();
    }
}
