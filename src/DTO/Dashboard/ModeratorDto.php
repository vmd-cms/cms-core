<?php

namespace VmdCms\CoreCms\DTO\Dashboard;

use VmdCms\CoreCms\Collections\ModeratorPermissionsCollection;
use VmdCms\CoreCms\Contracts\Collections\ModeratorPermissionsCollectionInterface;
use VmdCms\CoreCms\Contracts\DTO\ModeratorDtoInterface;
use VmdCms\CoreCms\CoreModules\Moderators\Models\Moderator;
use VmdCms\CoreCms\Exceptions\Moderators\AuthModeratorNotFound;
use VmdCms\CoreCms\Exceptions\Moderators\ModeratorPermissionNotFound;
use VmdCms\CoreCms\Services\CoreRouter;

class ModeratorDto implements ModeratorDtoInterface
{
    /**
     * @var Moderator
     */
    private $moderator;

    /**
     * @var ModeratorPermissionsCollectionInterface
     */
    private $moderatorPermissions;

    protected $fullName;

    protected $icon;

    protected $id;

    protected $editRoute;

    protected $logoutRoute;

    public function __construct(Moderator $moderator = null)
    {
        if(!$moderator instanceof Moderator) throw new AuthModeratorNotFound();
        $this->moderator = $moderator;
        $this->setModeratorPermissions();
        $this->fullName = $moderator->full_name;
        $this->icon = $moderator->imagePath;
        $this->id = $moderator->id;
        $this->editRoute = route( CoreRouter::ROUTE_EDIT_GET,['sectionSlug' => 'moderators', 'id' => $moderator->id]);
        $this->logoutRoute = route( CoreRouter::ROUTE_MODERATOR_LOGOUT);
    }

    public function hasFullAccess(): bool
    {
        return $this->moderator->has_full_access ?? false;
    }

    public function getModeratorPermissions(): ModeratorPermissionsCollectionInterface
    {
       return $this->moderatorPermissions;
    }

    /**
     * @param int $coreSectionId
     * @return bool
     * @throws ModeratorPermissionNotFound
     */
    public function hasAccessForCoreSection(int $coreSectionId) : bool
    {
        if($this->hasFullAccess()) return true;
        return $this->moderatorPermissions->getByCoreSectionId($coreSectionId)->isDisplayable();
    }

    /**
     * @throws ModeratorPermissionNotFound
     */
    private function setModeratorPermissions()
    {
        $this->moderatorPermissions = new ModeratorPermissionsCollection();
        if($this->hasFullAccess()) return;

        $permissions = $this->moderator->role->permissions ?? null;
        if(!is_countable($permissions) || !count($permissions)) throw new ModeratorPermissionNotFound();

        foreach ($permissions as $item)
        {
            $this->moderatorPermissions->appendItem(new ModeratorPermissionDto($item));
        }
    }

    public function toArray()
    {
        return [
          'id' => $this->id,
          'icon' => $this->icon,
          'full_name' => $this->fullName,
          'has_full_access' => $this->hasFullAccess(),
          'edit_route' => $this->editRoute,
          'logout_route' => $this->logoutRoute,
        ];
    }
}
