<?php

namespace VmdCms\CoreCms\DTO\Dashboard\Filters;

use VmdCms\CoreCms\Contracts\Dashboard\Filters\FilterComponentItemDTOCollectionInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Filters\FilterComponentItemDTOInterface;

class FilterComponentItemDTO implements FilterComponentItemDTOInterface
{
    /**
     * @var string|int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string|null
     */
    protected $filterSlug;

    /**
     * @var bool
     */
    protected $withChip;

    /**
     * @var string|null
     */
    protected $chipColor;

    /**
     * @var bool
     */
    protected $withCount;

    /**
     * @var int|null
     */
    protected $count;

    /**
     * @var int|null
     */
    protected $parentId;

    /**
     * @var FilterComponentItemDTOCollectionInterface
     */
    protected $children;

    /**
     * ComponentItemDTO constructor.
     * @param int|string $id
     * @param string|null $name
     * @param string|null $filterSlug
     */
    public function __construct($id, string $name = null, string $filterSlug = null)
    {
        $this->filterSlug = $filterSlug;
        $this->id = $id;
        $this->name = $name;
        $this->children = new FilterComponentItemDTOCollection();
    }

    /**
     * @param string|null $chipColor
     * @return FilterComponentItemDTOInterface
     */
    public function setChipColor(?string $chipColor): FilterComponentItemDTOInterface
    {
        $this->withChip = !is_null($chipColor);
        $this->chipColor = $chipColor;
        return $this;
    }

    /**
     * @param int|null $count
     * @return FilterComponentItemDTOInterface
     */
    public function setCount(?int $count): FilterComponentItemDTOInterface
    {
        $this->withCount = !is_null($count);
        $this->count = $count;
        return $this;
    }

    /**
     * @param int|null $parentId
     * @return FilterComponentItemDTOInterface
     */
    public function setParentId(?int $parentId): FilterComponentItemDTOInterface
    {
        $this->parentId = $parentId;
        return $this;
    }

    /**
     * @param FilterComponentItemDTOCollectionInterface $children
     * @return FilterComponentItemDTOInterface
     */
    public function setChildren(FilterComponentItemDTOCollectionInterface $children): FilterComponentItemDTOInterface
    {
        $this->children = $children;
        return $this;
    }


    public function toArray()
    {
        return [
            'name' => $this->name,
            'id' => $this->id,
            'filter_query' => $this->filterSlug . '=' . $this->id,
            'withChip' => boolval($this->withChip),
            'chipColor' => $this->chipColor,
            'withCount' => boolval($this->withCount),
            'count' => $this->count,
            'parent_id' => $this->parentId,
            'children' => $this->children->getItems(),
        ];
    }
}
