<?php

namespace VmdCms\CoreCms\DTO\Dashboard\Filters;

use VmdCms\CoreCms\Collections\CoreCollectionAbstract;
use VmdCms\CoreCms\Contracts\Dashboard\Filters\FilterComponentItemDTOCollectionInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Filters\FilterComponentItemDTOInterface;

class FilterComponentItemDTOCollection extends CoreCollectionAbstract implements FilterComponentItemDTOCollectionInterface
{
    /**
     * @inheritDoc
     */
    public function appendItem(FilterComponentItemDTOInterface $dto): void
    {
        $this->collection->add($dto);
    }
}
