<?php

namespace VmdCms\CoreCms\DTO\Dashboard;

use VmdCms\CoreCms\Contracts\DTO\BreadCrumbDtoInterface;

class BreadCrumbDto implements BreadCrumbDtoInterface
{
    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $title;

    public function __construct(string $url = null, string $title = null)
    {
        $this->url = $url;
        $this->title = $title;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'url' => $this->url,
            'title' => $this->title
        ];
    }
}
