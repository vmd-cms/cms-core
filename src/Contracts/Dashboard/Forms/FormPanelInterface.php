<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Forms;

use Illuminate\Support\Collection;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\HasComponentsInterface;

interface FormPanelInterface extends FormInterface, HasComponentsInterface
{
    /**
     * FormInterface constructor.
     * @param array $components\
     */
    public function __construct(array $components = []);

    /**
     * @return Collection
     */
    public function getMutatedComponents() : Collection;

}
