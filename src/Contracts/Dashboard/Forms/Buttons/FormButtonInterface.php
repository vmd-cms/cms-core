<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Forms\Buttons;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\HasComponentsInterface;

interface FormButtonInterface extends Arrayable
{
    /**
     * @return string
     */
    public function getAction(): string;

    /**
     * @param string $routePath
     * @return FormButtonInterface
     */
    public function setRoutePath(string $routePath): FormButtonInterface;

    /**
     * @param string $groupKey
     * @return FormButtonInterface
     */
    public function setGroupKey(string $groupKey): FormButtonInterface;

    /**
     * @return string
     */
    public function getGroupKey(): string;

    /**
     * @return string
     */
    public function getColor(): string;

    /**
     * @return string
     */
    public function getComponentKey() : string;
}
