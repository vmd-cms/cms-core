<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Forms\Buttons;

interface FormButtonHtmlAttributesInterface
{
    /**
     * @param string $class
     * @param string $element
     * @return FormButtonInterface
     */
    public function addClass(string $class, string $element = 'container'): FormButtonInterface;
}
