<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Forms;

use VmdCms\CoreCms\Contracts\Dashboard\HtmlAttributesInterface;

interface FormHtmlAttributesInterface extends HtmlAttributesInterface
{
    /**
     * @return FormInterface
     */
    public function wide(): FormInterface;

    /**
     * @param string $class
     * @param string $element
     * @return FormInterface
     */
    public function addClass(string $class, string $element = 'form_body'): FormInterface;
}
