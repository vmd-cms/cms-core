<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Forms;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Support\Collection;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Buttons\FormButtonInterface;
use VmdCms\CoreCms\Contracts\Dashboard\HasCmsModelInterface;
use VmdCms\CoreCms\Contracts\Dashboard\HtmlAttributesInterface;
use VmdCms\CoreCms\Dashboard\Forms\AbstractForm;

interface FormInterface extends HasCmsModelInterface, Renderable, Arrayable, FormHtmlAttributesInterface
{
    /**
     * @param string $action
     * @return FormInterface
     */
    public function setAction(string $action): FormInterface;

    /**
     * @param string $method
     * @return FormInterface
     */
    public function setMethod(string $method): FormInterface;

    /**
     * @param string $sectionSlug
     * @return FormInterface
     */
    public function setSectionSlug(string $sectionSlug): FormInterface;

    /**
     * @param FormButtonInterface[] $buttons
     * @return FormInterface
     */
    public function setButtons(array $buttons) : FormInterface;

    /**
     * @return Collection
     */
    public function getComponents() : Collection;

    /**
     * @param string $headTitle
     * @param string|null $field
     * @return $this|FormInterface
     */
    public function setHeadTitle(string $headTitle, string $field = null) : FormInterface;
}
