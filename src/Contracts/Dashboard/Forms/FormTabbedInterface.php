<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Forms;

use VmdCms\CoreCms\Contracts\Collections\FormTabsCollectionInterface;

interface FormTabbedInterface extends FormInterface
{
    /**
     * FormInterface constructor.
     * @param null|FormTabsCollectionInterface $tabsCollection
     */
    public function __construct(FormTabsCollectionInterface $tabsCollection = null);

    /**
     * @param FormTabsCollectionInterface $tabsCollection
     * @return FormTabbedInterface
     */
    public function setTabs(FormTabsCollectionInterface $tabsCollection) : FormTabbedInterface;

}
