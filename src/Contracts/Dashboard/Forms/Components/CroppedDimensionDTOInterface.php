<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Forms\Components;

use Illuminate\Contracts\Support\Arrayable;

interface CroppedDimensionDTOInterface extends Arrayable
{
    /**
     * @param string $key
     * @param int|null $width
     * @param int|null $height
     * @param int $quality
     */
    public function __construct(string $key, int $width = null, int $height = null, int $quality = 90);

    /**
     * @param int $quality
     * @return CroppedDimensionDTOInterface
     */
    public function setQuality(int $quality): CroppedDimensionDTOInterface;

    /**
     * @return string
     */
    public function getKey(): string;

    /**
     * @return int|null
     */
    public function getWidth(): ?int;

    /**
     * @return int|null
     */
    public function getHeight(): ?int;

    /**
     * @return int
     */
    public function getQuality(): int;
}
