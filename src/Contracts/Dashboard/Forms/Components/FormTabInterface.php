<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Forms\Components;

interface FormTabInterface extends HasComponentsInterface
{
    /**
     * FormTabInterface constructor.
     * @param string $key
     * @param array $components = []
     */
    public function __construct(string $key, array $components = []);

    /**
     * @return string
     */
    public function getKey() : string;

    /**
     * @return string
     */
    public function getTitle() : string;

}
