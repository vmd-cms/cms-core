<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Tables;

use Illuminate\Contracts\Support\Arrayable;

interface TableDataInterface extends Arrayable
{
    /**
     * TableData constructor.
     * @param array $headers
     * @param array $items
     * @param array $components
     */
    public function __construct(array $headers,array $items, array $components);

    /**
     * @param string $fieldGroupKey
     * @return TableDataInterface
     */
    public function setFieldGroupKey(string $fieldGroupKey): TableDataInterface;

}
