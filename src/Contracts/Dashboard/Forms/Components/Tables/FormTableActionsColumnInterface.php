<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Tables;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\HasComponentsInterface;

interface FormTableActionsColumnInterface extends FormTableColumnInterface, HasComponentsInterface
{

}
