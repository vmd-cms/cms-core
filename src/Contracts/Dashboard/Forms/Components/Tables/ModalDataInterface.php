<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Tables;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\HasComponentsInterface;
use VmdCms\CoreCms\Contracts\Dashboard\HasCmsModelInterface;

interface ModalDataInterface extends Arrayable, HasCmsModelInterface, HasComponentsInterface
{
    public function __construct();

    /**
     * @param string $serviceMethodPath
     * @return ModalDataInterface
     */
    public function setServiceMethodPath(string $serviceMethodPath): ModalDataInterface;

    /**
     * @param bool $updated
     * @return ModalDataInterface
     */
    public function setUpdated(bool $updated): ModalDataInterface;


}
