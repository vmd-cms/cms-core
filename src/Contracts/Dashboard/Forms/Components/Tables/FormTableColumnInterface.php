<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Tables;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormComponentInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;

interface FormTableColumnInterface extends FormComponentInterface
{
    /**
     * ComponentInterface constructor.
     * @param string $field
     * @param string|null $label
     */
    public function __construct(string $field, ?string $label = null);

    /**
     * @param string $relation
     * @return FormTableColumnInterface
     */
    public function setRelationName(string $relation) : FormTableColumnInterface;

    /**
     * @param CmsModelInterface $relationCmsModel
     * @return FormTableColumnInterface
     */
    public function setRelationCmsModel(CmsModelInterface $relationCmsModel) : FormTableColumnInterface;

    /**
     * @param string $order
     * @return FormTableColumnInterface
     */
    public function setOrderRequestField(string $order) : FormTableColumnInterface;

    /**
     * @param string $fieldPrefix
     * @return FormTableColumnInterface
     */
    public function setFieldPrefix(string $fieldPrefix): FormTableColumnInterface;

    /**
     * @return mixed
     */
    public function getRelationCmsModelValue();
}
