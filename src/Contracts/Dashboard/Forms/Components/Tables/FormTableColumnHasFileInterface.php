<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Tables;

interface FormTableColumnHasFileInterface extends FormTableColumnInterface
{
    /**
     * @param string|null $value
     * @return void
     */
    public function deleteFile(string $value = null);
}
