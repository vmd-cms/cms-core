<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Input;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\CoreCms\Contracts\Collections\DependedComponentsCollectionInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormComponentInterface;
use VmdCms\CoreCms\Contracts\Dashboard\HasCmsModelInterface;

interface DependedComponentInterface extends Arrayable,HasCmsModelInterface
{
    public function __construct(string $field, DependedComponentsCollectionInterface $components);

    /**
     * @param string|null $key
     * @return FormComponentInterface|null
     */
    public function getDependedFormComponentByKey(string $key = null) : ?FormComponentInterface;
}
