<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Input;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormComponentInterface;
use Illuminate\Database\Eloquent\Builder;

interface MultiValueInterface extends FormComponentInterface
{
    /**
     * @param string $className
     * @return MultiValueInterface
     */
    public function setModelForOptions(string $className) : MultiValueInterface;

    /**
     * @param string $field
     * @return MultiValueInterface
     */
    public function setDisplayField(string $field) : MultiValueInterface;

    /**
     * @param Builder $query
     * @return MultiValueInterface
     */
    public function setQuery(Builder $query) : MultiValueInterface;

    /**
     * @param array $where
     * @return MultiValueInterface
     */
    public function setWhere(array $where) : MultiValueInterface;

    /**
     * @param string $field
     * @return MultiValueInterface
     */
    public function setForeignField(string $field) : MultiValueInterface;

    /**
     * @param bool $flag
     * @return MultiValueInterface
     */
    public function setIsIdInt(bool $flag) : MultiValueInterface;

    /**
     * @param array $enums
     * @return MultiValueInterface
     */
    public function setEnumValues(array $enums) : MultiValueInterface;

    /**
     * @return array
     */
    public function getOptions() : array;

    /**
     * @return MultiValueInterface
     */
    public function nullable() : MultiValueInterface;

    /**
     * @param string $value
     * @return MultiValueInterface
     */
    public function nullableValue(string $value) : MultiValueInterface;
}
