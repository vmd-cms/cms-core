<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Input;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\SearchableComponentInterface;

interface AsyncSelectInterface extends SelectInterface, SearchableComponentInterface
{
}
