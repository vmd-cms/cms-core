<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Forms\Components;

use VmdCms\CoreCms\Contracts\Dashboard\ComponentInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Validation\ValidationRulesInterface;
use VmdCms\CoreCms\Exceptions\Dashboard\FormComponentException;

interface FormComponentInterface extends ComponentInterface,ValidationRulesInterface, FormComponentHtmlAttributesInterface
{
    /**
     * @param string $value
     * @return FormComponentInterface
     */
    public function setValue(string $value) : FormComponentInterface;

    /**
     * @param string $error
     * @return FormComponentInterface
     */
    public function setError(string $error): FormComponentInterface;

    /**
     * @param string|int|bool|null $value
     * @return string|int|bool|null
     * @throws FormComponentException
     */
    public function getPreparedValue($value);

    /**
     * @param callable $function
     * @return FormComponentInterface
     */
    public function setBeforeSave(callable $function) : FormComponentInterface;

    /**
     * @return callable|null
     */
    public function getBeforeSaveCallback() : ?callable;

    /**
     * @param callable $function
     * @return FormComponentInterface
     */
    public function setAfterSaveCallback(callable $function): FormComponentInterface;

    /**
     * @return null|callable
     */
    public function getAfterSaveCallback(): ?callable;

    /**
     * @param callable $function
     * @return FormComponentInterface
     */
    public function setStoreCallback(callable $function) : FormComponentInterface;

    /**
     * @param bool $disabled
     * @return FormComponentInterface
     */
    public function setDisabled(bool $disabled) : FormComponentInterface;

    /**
     * @return callable|null
     */
    public function getStoreCallback(): ?callable;

    /**
     * @param $default
     * @return FormComponentInterface
     */
    public function setDefault($default): FormComponentInterface;

    /**
     * @return mixed
     */
    public function getDefault();

}
