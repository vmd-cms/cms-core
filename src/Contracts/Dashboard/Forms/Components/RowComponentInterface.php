<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Forms\Components;

use VmdCms\CoreCms\Collections\FormComponentsCollection;

interface RowComponentInterface extends FormComponentInterface,HasComponentsInterface
{
    public function getColumns(): FormComponentsCollection;

    public function appendColumn(ColumnComponentInterface $component): RowComponentInterface;
}
