<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Forms\Components;

interface SearchableComponentInterface
{
    /**
     * @param callable $callback
     * @return SearchableComponentInterface
     */
    public function setSearchCallback(callable $callback): SearchableComponentInterface;
    /**
     * @return callable|null
     */
    public function getSearchCallback(): ?callable;

    /**
     * @return string
     */
    public function getAsyncMethodPath(): string;
    /**
     * @param string $asyncMethod
     * @return SearchableComponentInterface
     */
    public function setAsyncMethodPath(string $asyncMethod): SearchableComponentInterface;

    /**
     * @param int $limit
     * @return SearchableComponentInterface
     */
    public function setLimit(int $limit): SearchableComponentInterface;

    /**
     * @return int
     */
    public function getLimit(): int;

    /**
     * @param int $offset
     * @return SearchableComponentInterface
     */
    public function setOffset(int $offset): SearchableComponentInterface;

    /**
     * @return int
     */
    public function getOffset(): int;

    /**
     * @param bool $hasMore
     * @return SearchableComponentInterface
     */
    public function setHasMore(bool $hasMore): SearchableComponentInterface;

    /**
     * @return bool
     */
    public function getHasMore(): bool;

    /**
     * @param string|null $search
     * @return SearchableComponentInterface
     */
    public function setSearch(string $search = null): SearchableComponentInterface;

    /**
     * @return string|null
     */
    public function getSearch(): ?string;

    /**
     * @return int
     */
    public function getNextOffset(): int;


}
