<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Forms\Components;

use Illuminate\Support\Collection;
use VmdCms\CoreCms\Contracts\Collections\FormComponentsCollectionInterface;

interface HasComponentsInterface
{
    /**
     * @param array $components
     * @return HasComponentsInterface
     */
    public function setComponents(array $components) : HasComponentsInterface;

    /**
     * @return Collection
     */
    public function getComponents() : Collection;

    /**
     * @return FormComponentsCollectionInterface
     */
    public function getComponentsCollection() : FormComponentsCollectionInterface;
}
