<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Forms\Components;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Input\DependedComponentInterface;

interface HasDependedComponentsInterface
{
    /**
     * @param DependedComponentInterface $dependedComponent
     * @return HasDependedComponentsInterface
     */
    public function setDependedComponent(DependedComponentInterface $dependedComponent) : HasDependedComponentsInterface;

    /**
     * @return null|FormComponentInterface
     */
    public function getDependedFormComponent() : ?FormComponentInterface;

    /**
     * @return null|DependedComponentInterface
     */
    public function getDependedComponent() : ?DependedComponentInterface;
}
