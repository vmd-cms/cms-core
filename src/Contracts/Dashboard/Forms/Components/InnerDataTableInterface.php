<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Forms\Components;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Tables\TableDataInterface;

interface InnerDataTableInterface extends HasComponentsInterface
{
    /**
     * @return TableDataInterface
     */
    public function getTableData(): TableDataInterface;

    /**
     * @return callable
     */
    public function getStoreTableDataCallback(): callable;

    /**
     * @param string $customModelClass
     * @return InnerDataTableInterface
     */
    public function setCustomModelClass(string $customModelClass): InnerDataTableInterface;

    /**
     * @param null|callable $isDeletableCallback
     * @return InnerDataTableInterface
     */
    public function setIsDeletableCallback(callable $isDeletableCallback = null);
}
