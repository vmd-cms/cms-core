<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Forms\Components;

interface FormComponentHtmlAttributesInterface
{
    /**
     * @return FormComponentInterface
     */
    public function wide(): FormComponentInterface;

    /**
     * @return FormComponentInterface
     */
    public function bordered(): FormComponentInterface;

    /**
     * @return FormComponentInterface
     */
    public function borderLeft(): FormComponentInterface;

    /**
     * @return FormComponentInterface
     */
    public function borderRight(): FormComponentInterface;

    /**
     * @return FormComponentInterface
     */
    public function borderTop(): FormComponentInterface;

    /**
     * @return FormComponentInterface
     */
    public function borderBottom(): FormComponentInterface;

    /**
     * @param string $direction
     * @return FormComponentInterface
     */
    public function justify(string $direction): FormComponentInterface;

    /**
     * @param string $class
     * @param string $element
     * @return FormComponentInterface
     */
    public function addClass(string $class, string $element = 'container'): FormComponentInterface;
}
