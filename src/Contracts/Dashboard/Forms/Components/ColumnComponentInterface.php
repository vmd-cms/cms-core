<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Forms\Components;

interface ColumnComponentInterface extends HasComponentsInterface,FormComponentInterface
{
    public function setCols(int $cols): ColumnComponentInterface;
}
