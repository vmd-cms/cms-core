<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Forms\Components;

use VmdCms\CoreCms\Contracts\Collections\CollectionInterface;

interface CroppedDimensionDTOCollectionInterface extends CollectionInterface
{
    /**
     * @param CroppedDimensionDTOInterface $dto
     * @return void
     */
    public function appendItem(CroppedDimensionDTOInterface $dto) : void;
}
