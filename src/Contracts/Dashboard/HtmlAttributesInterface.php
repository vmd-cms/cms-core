<?php

namespace VmdCms\CoreCms\Contracts\Dashboard;

use VmdCms\CoreCms\Contracts\DTO\HtmlAttributesDTOInterface;

interface HtmlAttributesInterface
{
    /**
     * @param HtmlAttributesDTOInterface $dto
     * @return HtmlAttributesInterface
     */
    public function setHtmlAttributes(HtmlAttributesDTOInterface $dto) : HtmlAttributesInterface;

    /**
     * @return HtmlAttributesDTOInterface
     */
    public function getHtmlAttributes(): HtmlAttributesDTOInterface;

}
