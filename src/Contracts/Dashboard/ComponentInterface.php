<?php

namespace VmdCms\CoreCms\Contracts\Dashboard;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Renderable;
use VmdCms\CoreCms\Traits\Dashboard\HasFieldComponent;

interface ComponentInterface extends Arrayable,Renderable,HasCmsModelInterface,HtmlAttributesInterface
{
    /**
     * ComponentInterface constructor.
     * @param string $field
     * @param string|null $label
     */
    public function __construct(string $field, ?string $label = null);

    /**
     * @param string $field
     * @return ComponentInterface
     */
    public function setField(string $field) : ComponentInterface;

    /**
     * @return string
     */
    public function getField() : string;

    /**
     * @param string $label
     * @return ComponentInterface
     */
    public function setLabel(string $label) : ComponentInterface;

    /**
     * @return string
     */
    public function getLabel() : string;

    /**
     * @param string $helpText
     * @return ComponentInterface
     */
    public function setTopHelpText(string $helpText) : ComponentInterface;

    /**
     * @return string|null
     */
    public function getTopHelpText() : ?string;

    /**
     * @param string $helpText
     * @return ComponentInterface
     */
    public function setBottomHelpText(string $helpText) : ComponentInterface;

    /**
     * @return string|null
     */
    public function getBottomHelpText() : ?string;

    /**
     * @param string $placeholder
     * @return ComponentInterface
     */
    public function setPlaceholder(string $placeholder) : ComponentInterface;

    /**
     * @return string
     */
    public function getPlaceholder() : string;

    /**
     * @param int $width
     * @return ComponentInterface
     */
    public function setWidth(int $width) : ComponentInterface;

    /**
     * @return int|null
     */
    public function getWidth() : ?int;

    /**
     * @return ComponentInterface
     */
    public function alignCenter() : ComponentInterface;

    /**
     * @return ComponentInterface
     */
    public function alignEnd() : ComponentInterface;

    /**
     * @return ComponentInterface
     */
    public function alignStart() : ComponentInterface;

    /**
     * @param bool $flag
     * @return ComponentInterface
     */
    public function outlined(bool $flag) : ComponentInterface;

    /**
     * @param int $label
     * @param int $block
     * @return ComponentInterface
     */
    public function setRows(int $label,int $block) : ComponentInterface;

    /**
     * @return string|null
     */
    public function getAlign() : ?string;

}
