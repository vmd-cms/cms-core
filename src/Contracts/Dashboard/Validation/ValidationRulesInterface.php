<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Validation;

interface ValidationRulesInterface
{
    /**
     * @param null|string $message
     * @return ValidationRulesInterface
     */
    public function required($message = null): ValidationRulesInterface;

    /**
     * @param null|string $message
     * @return ValidationRulesInterface
     */
    public function unique($message = null): ValidationRulesInterface;

    /**
     * @param null|string $message
     * @return ValidationRulesInterface
     */
    public function email($message = null): ValidationRulesInterface;

    /**
     * @param int $value
     * @param null|string $message
     * @return ValidationRulesInterface
     */
    public function max($value = 10, $message = null): ValidationRulesInterface;

    /**
     * @param int $value
     * @param null|string $message
     * @return ValidationRulesInterface
     */
    public function min($value = 10, $message = null): ValidationRulesInterface;

    /**
     * @param int $value
     * @param null|string $message
     * @return ValidationRulesInterface
     */
    public function maxLength($value = 10, $message = null): ValidationRulesInterface;

    /**
     * @param int $value
     * @param null|string $message
     * @return ValidationRulesInterface
     */
    public function minLength($value = 10, $message = null): ValidationRulesInterface;

    /**
     * @param null|bool $positive
     * @param null|string $message
     * @return ValidationRulesInterface
     */
    public function integer($positive = true, $message = null): ValidationRulesInterface;

    /**
     * @param null|bool $positive
     * @param null|string $message
     * @return ValidationRulesInterface
     */
    public function float($positive = true, $message = null): ValidationRulesInterface;


}
