<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Validation;

use Illuminate\Contracts\Support\Arrayable;

interface RuleInterface extends Arrayable
{
    /**
     * RuleInterface constructor.
     * @param string $key
     * @param string|null $value
     * @param null $message
     */
    public function __construct(string $key, string $value = null, $message = null);

    /**
     * @return string
     */
    public function getKey() : string;

    /**
     * @return null|string
     */
    public function getValue() : ?string;

    /**
     * @return null|string
     */
    public function getMessage() : ?string;

    /**
     * @param array $data
     * @return RuleInterface
     */
    public function setAdditionalData(array $data) : RuleInterface;
}
