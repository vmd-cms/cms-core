<?php

namespace VmdCms\CoreCms\Contracts\Dashboard;

use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;

interface HasCmsModelInterface
{
    /**
     * @param CmsModelInterface $cmsModel
     * @return HasCmsModelInterface
     */
    public function setCmsModel(CmsModelInterface $cmsModel): HasCmsModelInterface;

}
