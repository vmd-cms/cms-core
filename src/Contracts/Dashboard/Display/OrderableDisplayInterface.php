<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Display;

use VmdCms\CoreCms\Exceptions\Dashboard\OrderableDisplayException;
use VmdCms\CoreCms\Exceptions\Dashboard\TableDisplayException;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;
use VmdCms\CoreCms\Exceptions\Models\NotOrderableException;
use VmdCms\CoreCms\Exceptions\Sections\SectionNotFoundException;

interface OrderableDisplayInterface
{

    /**
     * @return DisplayInterface
     */
    public function orderable() : DisplayInterface;

    /**
     * @return bool
     */
    public function isOrderable() : bool;

    /**
     * @param array $items
     * @return void
     * @throws NotCmsModelException
     * @throws NotOrderableException
     * @throws OrderableDisplayException
     * @throws SectionNotFoundException
     */
    public function updateOrder(array $items) : void;

    /**
     * @return void
     * @throws NotCmsModelException
     * @throws OrderableDisplayException
     * @throws TableDisplayException
     */
    public function prependOrderableTable() : void;
}
