<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Display\Filters;

use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;

interface FilterableDisplayInterface
{
    /**
     * @param FilterDisplayInterface $filterDisplay
     * @return DisplayInterface
     */
    public function setFilter(FilterDisplayInterface $filterDisplay) : DisplayInterface;

    /**
     * @return FilterDisplayInterface|null
     */
    public function getFilter() : ?FilterDisplayInterface;
}
