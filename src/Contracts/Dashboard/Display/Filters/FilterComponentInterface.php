<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Display\Filters;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Renderable;
use VmdCms\CoreCms\Exceptions\Models\NotTreeableException;

interface FilterComponentInterface extends Renderable, Arrayable
{
    /**
     * FilterComponentInterface constructor.
     * @param string $titleField
     */
    public function __construct(string $titleField);

    /**
     * @return string
     */
    public function getComponentKey() : string;

    /**
     * @param string $cmsModelClass
     * @return $this|FilterComponentInterface
     * @throws NotTreeableException
     */
    public function setModelClass(string $cmsModelClass): FilterComponentInterface;

    /**
     * @param callable $callback
     * @return FilterComponentInterface
     */
    public function setRetrieveItemsCallback(callable $callback) : FilterComponentInterface;

    /**
     * @param string $filterSlug
     * @return $this|FilterComponentInterface
     */
    public function setFilterSlug(string $filterSlug): FilterComponentInterface;

    /**
     * @param string $filterField
     * @return $this|FilterComponentInterface
     */
    public function setFilterField(string $filterField): FilterComponentInterface;

    /**
     * @return string
     */
    public function getFilterSlug(): string;

    /**
     * @return string
     */
    public function getFilterField(): string;

    /**
     * @param string $title
     * @return FilterComponentInterface
     */
    public function setHeadTitle(string $title): FilterComponentInterface;

    /**
     * @return string
     */
    public function getHeadTitle(): string;

    /**
     * @param bool $flag
     * @return FilterComponentInterface
     */
    public function setIsExpansion(bool $flag): FilterComponentInterface;

    /**
     * @return bool
     */
    public function isExpansion(): bool;

    /**
     * @param string $title
     * @return FilterComponentInterface
     */
    public function setExpansionTitle(string $title): FilterComponentInterface;

    /**
     * @return string
     */
    public function getExpansionTitle(): string;

    /**
     * @param callable $callback
     * @return FilterComponentInterface
     */
    public function setFilterQueryCallback(callable $callback): FilterComponentInterface;

    /**
     * @return null|callable
     */
    public function getFilterQueryCallback(): ?callable;
}
