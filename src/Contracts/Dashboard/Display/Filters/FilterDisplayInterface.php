<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Display\Filters;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Renderable;
use VmdCms\CoreCms\DTO\NavDTO;

interface FilterDisplayInterface extends Renderable, Arrayable
{
    /**
     * @param FilterComponentInterface $filterComponent
     * @return FilterDisplayInterface
     */
    public function appendFilterComponent(FilterComponentInterface $filterComponent) : FilterDisplayInterface;

    /**
     * @param NavDTO $headerLink
     * @return FilterDisplayInterface
     */
    public function setHeaderLink(NavDTO $headerLink): FilterDisplayInterface;
}
