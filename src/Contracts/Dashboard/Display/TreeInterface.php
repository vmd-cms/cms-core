<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Display;

use VmdCms\CoreCms\Exceptions\Dashboard\TreeDisplayException;
use Illuminate\Http\Request;

interface TreeInterface extends DisplayInterface
{
    /**
     * @param string $field
     * @return TreeInterface
     */
    public function setDisplayField(string $field) : TreeInterface;

    /**
     * @param int $maxLevel
     * @return TreeInterface
     */
    public function setMaxLevel(int $maxLevel) : TreeInterface;

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws TreeDisplayException
     */
    public function updateTree(Request $request);

}
