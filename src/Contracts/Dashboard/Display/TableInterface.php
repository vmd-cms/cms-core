<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Display;

use VmdCms\CoreCms\Contracts\Dashboard\Display\Components\TableColumnInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Filters\FilterableDisplayInterface;
use VmdCms\CoreCms\Dashboard\Display\Table;

interface TableInterface extends DisplayInterface,FilterableDisplayInterface
{
    /**
     * TableInterface constructor.
     * @param array $columns
     */
    public function __construct(array $columns = []);

    /**
     * @param TableColumnInterface[] $columns
     * @return TableInterface
     */
    public function setColumns(array $columns) : TableInterface;

    /**
     * @param bool $showActionColumn
     * @return TableInterface
     */
    public function setShowActionColumn(bool $showActionColumn): TableInterface;

    /**
     * @param bool $flag
     * @return TableInterface
     */
    public function setSearchable(bool $flag) : TableInterface;

    /**
     * @param array $buttons
     * @return TableInterface
     */
    public function setButtons(array $buttons): TableInterface;

    /**
     * @param array $conditions
     * @return TableInterface
     */
    public function setWhereQuery(array $conditions) : TableInterface;

    /**
     * @param array $options
     * @return array
     */
    public function getTableDataAsync(array $options): array;

    /**
     * @param string|null $title
     * @return TableInterface
     */
    public function setCreateBtnTitle(string $title = null): TableInterface;
}
