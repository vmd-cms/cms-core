<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Display\Components;

interface DateColumnInterface extends TableColumnInterface
{
    public function setFormat(string $format) : DateColumnInterface;
}
