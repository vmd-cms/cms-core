<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Display\Components;

use VmdCms\CoreCms\Contracts\Dashboard\Validation\ValidationRulesInterface;

interface EditableColumnInterface extends ValidationRulesInterface
{
    /**
     * @return TableColumnInterface
     */
    public function editable() :TableColumnInterface;

    /**
     * @return bool
     */
    public function isEditable() :bool;
}
