<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Display\Components;

use VmdCms\CoreCms\Contracts\Dashboard\ComponentInterface;

interface TableColumnInterface extends ComponentInterface
{
    public function getValue();

    /**
     * @return string
     */
    public function getComponentKey() : string;

    /**
     * @param bool $flag
     * @return TableColumnInterface
     */
    public function searchable(bool $flag = true): TableColumnInterface;

    /**
     * @return bool|null
     */
    public function isSearchable(): ?bool;

    /**
     * @param callable $searchableCallback
     * @return TableColumnInterface
     */
    public function setSearchableCallback(callable $searchableCallback): TableColumnInterface;

    /**
     * @return callable|null
     */
    public function getSearchableCallback(): ?callable;

    /**
     * @param bool $flag
     * @return TableColumnInterface
     */
    public function sortable(bool $flag = true): TableColumnInterface;

    /**
     * @return bool|null
     */
    public function isSortable(): ?bool;

    /**
     * @param callable $searchableCallback
     * @return TableColumnInterface
     */
    public function setSortableCallback(callable $searchableCallback): TableColumnInterface;

    /**
     * @return callable|null
     */
    public function getSortableCallback(): ?callable;
}
