<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Display\Blocks;

use Illuminate\Contracts\Support\Arrayable;

interface BlockInterface extends Arrayable
{
    /**
     * @param string $routePath
     * @return BlockInterface
     */
    public function setRoutePath(string $routePath): BlockInterface;

    /**
     * @param string $groupKey
     * @return BlockInterface
     */
    public function setGroupKey(string $groupKey): BlockInterface;

    /**
     * @return string
     */
    public function getGroupKey(): string;

    /**
     * @return string
     */
    public function getComponentKey() : string;
}
