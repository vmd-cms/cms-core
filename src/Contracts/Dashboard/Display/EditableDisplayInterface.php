<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Display;

use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;
use VmdCms\CoreCms\Exceptions\Sections\SectionNotFoundException;

interface EditableDisplayInterface
{

    /**
     * @param array $item
     * @param string $value
     * @return void
     * @throws NotCmsModelException
     * @throws SectionNotFoundException
     */
    public function updateItem(array $item, string $value) : void;

}
