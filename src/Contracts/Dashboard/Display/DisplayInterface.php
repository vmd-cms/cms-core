<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Display;

use VmdCms\CoreCms\Contracts\Dashboard\Display\Panels\HasDisplayPanelsInterface;
use VmdCms\CoreCms\Contracts\Dashboard\HasCmsModelInterface;
use VmdCms\CoreCms\Contracts\Sections\AdminSectionInterface;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Renderable;

interface DisplayInterface extends HasCmsModelInterface,HasDisplayPanelsInterface,Renderable,Arrayable
{
    /**
     * @param AdminSectionInterface $section
     * @return DisplayInterface
     */
    public function setSection(AdminSectionInterface $section) : DisplayInterface;

    /**
     * @return AdminSectionInterface
     */
    public function getSection(): AdminSectionInterface;
}
