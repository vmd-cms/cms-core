<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Display\Panels;

use Illuminate\Contracts\Support\Renderable;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Filters\FilterDisplayInterface;

interface FilterPanelInterface extends Renderable
{
    /**
     * FilterPanel constructor.
     * @param FilterDisplayInterface|null $filterDisplay
     */
    public function __construct(?FilterDisplayInterface $filterDisplay);
}
