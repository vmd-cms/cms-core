<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Display\Panels;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Renderable;

interface HeadPanelInterface extends Arrayable,Renderable
{

}
