<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Display\Panels;

use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;

interface HasDisplayPanelsInterface
{
    public function setHeadPanel(HeadPanelInterface $headPanel) : DisplayInterface;

    public function getHeadPanel() : HeadPanelInterface;

    public function setFilterPanel(FilterPanelInterface $headPanel) : DisplayInterface;

    public function getFilterPanel() : FilterPanelInterface;

    public function setSectionPanel(SectionPanelInterface $sectionPanel) : DisplayInterface;

    public function setSectionPanels(array $sectionPanels) : DisplayInterface;

    public function getSectionPanel() : SectionPanelInterface;
}
