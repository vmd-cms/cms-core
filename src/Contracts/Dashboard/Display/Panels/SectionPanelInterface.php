<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Display\Panels;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Renderable;
use VmdCms\CoreCms\Dashboard\Display\Panels\SectionPanel;

interface SectionPanelInterface extends Renderable
{
    /**
     * @param SectionPanel[] $additionalPanels
     * @return SectionPanelInterface
     */
    public function setAdditionalPanels(array $additionalPanels): SectionPanelInterface;

    /**
     * @param int $cols
     * @return SectionPanelInterface
     */
    public function setColsMd(int $cols): SectionPanelInterface;

    /**
     * @param string $viewPath
     * @return SectionPanelInterface
     */
    public function setViewPath(string $viewPath): SectionPanelInterface;

    /**
     * @return int
     */
    public function getColsMd(): int;
}
