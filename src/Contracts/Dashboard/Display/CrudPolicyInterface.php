<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Display;

use VmdCms\CoreCms\Models\CmsModel;

interface CrudPolicyInterface
{
    /**
     * @return bool
     */
    public function isCreatable() : bool;

    /**
     * @param bool $flag
     * @return CrudPolicyInterface
     */
    public function setCreatable(bool $flag) : CrudPolicyInterface;

    /**
     * @param CmsModel|null $model
     * @return bool
     */
    public function isViewable(CmsModel $model = null) : bool;

    /**
     * @param CmsModel|null $model
     * @return bool
     */
    public function isEditable(CmsModel $model = null) : bool;

    /**
     * @param CmsModel|null $model
     * @return bool
     */
    public function isDeletable(CmsModel $model = null) : bool;

}
