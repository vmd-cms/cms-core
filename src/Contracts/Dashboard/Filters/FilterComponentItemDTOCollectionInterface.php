<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Filters;

use VmdCms\CoreCms\Contracts\Collections\CollectionInterface;

interface FilterComponentItemDTOCollectionInterface extends CollectionInterface
{
    /**
     * @param FilterComponentItemDTOInterface $dto
     * @return void
     */
    public function appendItem(FilterComponentItemDTOInterface $dto) : void;
}
