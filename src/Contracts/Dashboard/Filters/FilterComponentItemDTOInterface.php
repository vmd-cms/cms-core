<?php

namespace VmdCms\CoreCms\Contracts\Dashboard\Filters;

use Illuminate\Contracts\Support\Arrayable;

interface FilterComponentItemDTOInterface extends Arrayable
{
    /**
     * ComponentItemDTO constructor.
     * @param int|string $id
     * @param string $name
     * @param string $filterSlug
     */
    public function __construct($id, string $name, string $filterSlug);

    /**
     * @param string|null $chipColor
     * @return FilterComponentItemDTOInterface
     */
    public function setChipColor(?string $chipColor): FilterComponentItemDTOInterface;

    /**
     * @param int|null $count
     * @return FilterComponentItemDTOInterface
     */
    public function setCount(?int $count): FilterComponentItemDTOInterface;

    /**
     * @param int|null $parentId
     * @return FilterComponentItemDTOInterface
     */
    public function setParentId(?int $parentId): FilterComponentItemDTOInterface;

    /**
     * @param FilterComponentItemDTOCollectionInterface $children
     * @return FilterComponentItemDTOInterface
     */
    public function setChildren(FilterComponentItemDTOCollectionInterface $children): FilterComponentItemDTOInterface;
}
