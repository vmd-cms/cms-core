<?php

namespace VmdCms\CoreCms\Contracts\Initializers;

use VmdCms\CoreCms\Contracts\Collections\AdminSectionCollectionInterface;
use VmdCms\CoreCms\Contracts\Repositories\SectionRepositoryInterface;
use VmdCms\CoreCms\Contracts\Sections\AdminSectionInterface;
use VmdCms\CoreCms\DTO\Dashboard\DashboardData;
use VmdCms\CoreCms\Exceptions\Moderators\AuthModeratorNotFound;
use VmdCms\CoreCms\Exceptions\Moderators\ModeratorPermissionNotFound;
use VmdCms\CoreCms\Exceptions\Sections\SectionNotFoundException;
use Illuminate\Contracts\Foundation\Application;

interface AdminInitializerInterface
{
    /**
     * AdminCoreInterface constructor.
     * @param Application $application
     * @param SectionRepositoryInterface $sectionRepository;
     * @throws AuthModeratorNotFound
     * @throws ModeratorPermissionNotFound
     */
    public function __construct(Application $application,SectionRepositoryInterface $sectionRepository);

    /**
     * @return AdminSectionCollectionInterface
     */
    public function getAdminSections() : AdminSectionCollectionInterface;

    /**
     * @param $sectionSlug
     * @return AdminSectionInterface
     * @throws SectionNotFoundException
     */
    public function getAdminSectionByKey($sectionSlug) : AdminSectionInterface;

    /**
     * @return DashboardData
     */
    public function getDashboardData() :DashboardData;

    /**
     * @return array
     */
    public function getSectionsIdSlugMap() : array;
}
