<?php

namespace VmdCms\CoreCms\Contracts\Modules;

use VmdCms\CoreCms\Exceptions\Initializers\ModuleInitializerNotFoundException;

interface ModuleServiceProviderInterface
{
    /**
     * @return ModuleInitializerInterface
     * @throws ModuleInitializerNotFoundException
     */
    public function getModuleInitializer(): ModuleInitializerInterface;

}
