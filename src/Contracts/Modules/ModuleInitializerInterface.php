<?php

namespace VmdCms\CoreCms\Contracts\Modules;

use VmdCms\CoreCms\Exceptions\Services\StubReplacerException;

interface ModuleInitializerInterface
{
    /**
     * @return void
     */
    public function appendRoutes();

    /**
     * @return string
     */
    public static function moduleAlias() : string;

    /**
     * @return string
     */
    public static function moduleSlug() : string;

    /**
     * @return string
     * @throws StubReplacerException
     */
    public function publicMigrationsPath() : string;

    /**
     * @return string
     * @throws StubReplacerException
     */
    public function publicSeedsPath() : string;

    /**
     * @return string
     * @throws StubReplacerException
     */
    public function publicConfigPath() : string;

    /**
     * @return string
     * @throws StubReplacerException
     */
    public function publicRoutesPath() : string;

    /**
     * @return string
     * @throws StubReplacerException
     */
    public function publicViewsPath() : string;

    /**
     * @return string
     * @throws StubReplacerException
     */
    public function publicLangPath() : string;

    /**
     * @throws StubReplacerException
     */
    public function install();

}
