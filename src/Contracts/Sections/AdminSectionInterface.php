<?php

namespace VmdCms\CoreCms\Contracts\Sections;

use VmdCms\CoreCms\DTO\Dashboard\Navs\ChipItemDto;

interface AdminSectionInterface extends AdminSectionResourceInterface
{
    /**
     * @return string
     */
    public function getSectionSlug() : string;

    /**
     * @param string $title
     * @return AdminSectionInterface
     */
    public function setTitle(string $title) : AdminSectionInterface;

    /**
     * @return string|null
     */
    public function getTitle() : ?string;

    /**
     * @param string|null $cmsModelClass
     * @return AdminSectionInterface
     */
    public function setCmsModelClass(?string $cmsModelClass) : AdminSectionInterface;

    /**
     * @return null|string
     */
    public function getCmsModelClass(): ?string;

    /**
     * @return ChipItemDto|null
     */
    public function getChip() : ?ChipItemDto;

}
