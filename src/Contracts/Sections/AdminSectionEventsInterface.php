<?php

namespace VmdCms\CoreCms\Contracts\Sections;

use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;

interface AdminSectionEventsInterface
{
    /**
     * @param CmsModelInterface $model
     */
    public function onCreated(CmsModelInterface $model): void;

    /**
     * @param string|int|null $modelId
     */
    public function onViewed($modelId): void;

    /**
     * @param CmsModelInterface $model
     */
    public function onEdited(CmsModelInterface $model): void;

    /**
     * @param CmsModelInterface $model
     */
    public function onDeleted(CmsModelInterface $model): void;
}
