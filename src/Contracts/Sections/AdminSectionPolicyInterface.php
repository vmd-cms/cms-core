<?php

namespace VmdCms\CoreCms\Contracts\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Display\CrudPolicyInterface;

interface AdminSectionPolicyInterface extends CrudPolicyInterface
{
    /**
     * @param string $action
     * @return bool
     */
    public function isAvailableAction($action) : bool;

    /**
     * @return bool
     */
    public function isDisplayable() : bool;

}
