<?php

namespace VmdCms\CoreCms\Contracts\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use Illuminate\Http\Request;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Models\CmsModel;

interface AdminSectionResourceInterface extends AdminSectionPolicyInterface,AdminSectionEventsInterface
{
    /**
     * @return DisplayInterface
     */
    public function display();

    /**
     * @return string
     */
    public function renderDisplay() : string;

    /**
     * @param Request $request
     * @return mixed
     */
    public function storeDisplay(Request $request);

    /**
     * @return FormInterface
     */
    public function create() : FormInterface;

    /**
     * @return string
     */
    public function renderCreate() : string;

    /**
     * @param Request $request
     * @return mixed
     */
    public function storeCreate(Request $request);

    /**
     * @param int $id
     * @return FormInterface
     */
    public function view(int $id) : FormInterface;

    /**
     * @param int $id
     * @return string
     */
    public function renderView(int $id) : string;

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface;

    /**
     * @param int|null $id
     * @return string
     */
    public function renderEdit(?int $id) : string;

    /**
     * @param Request $request
     * @return mixed
     */
    public function storeEdit(Request $request);

    /**
     * @param int $id
     * @return void|mixed
     * @throws \Exception
     */
    public function delete(int $id);

    /**
     * @param CmsModelInterface $model
     * @return mixed
     */
    public function deleteResponse(CmsModelInterface $model);

    /**
     * @param int $id
     * @return void|mixed
     * @throws \Exception
     */
    public function restore(int $id);

    /**
     * @param CmsModelInterface $model
     * @return mixed
     */
    public function restoreResponse(CmsModelInterface $model);

    /**
     * @param Request $request
     * @return string|null
     */
    public function storeCkeditorUploadFile(Request $request) : ?string;

    /**
     * @param Request $request
     * @return array
     */
    public function getCkeditorUploadedFiles(Request $request): array;
}
