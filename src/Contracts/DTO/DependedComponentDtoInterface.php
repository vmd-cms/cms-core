<?php

namespace VmdCms\CoreCms\Contracts\DTO;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormComponentInterface;

interface DependedComponentDtoInterface extends Arrayable
{
    public function __construct(string $key, FormComponentInterface $component);

    /**
     * @return string
     */
    public function getKey() : string;

    /**
     * @return FormComponentInterface
     */
    public function getFormComponent() : FormComponentInterface;
}
