<?php

namespace VmdCms\CoreCms\Contracts\DTO;

use Illuminate\Contracts\Support\Arrayable;

interface LinkDtoInterface extends Arrayable
{
    /**
     * LinkDtoInterface constructor.
     * @param string $title
     * @param string $link
     */
    public function __construct(string $title = '', string $link = '');
}
