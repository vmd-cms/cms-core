<?php

namespace VmdCms\CoreCms\Contracts\DTO;

use Illuminate\Contracts\Support\Arrayable;

interface SeoableDTOInterface extends Arrayable
{
    /**
     * @param SeoDTOInterface $seoDTO
     * @return SeoableDTOInterface
     */
    public function setSeoDTO(SeoDTOInterface $seoDTO): SeoableDTOInterface;

    /**
     * @return SeoDTOInterface
     */
    public function getSeoDTO(): SeoDTOInterface;

}
