<?php

namespace VmdCms\CoreCms\Contracts\DTO;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\CoreCms\Contracts\Collections\NavsCollectionInterface;

interface NavDtoInterface extends Arrayable
{
    /**
     * NavDtoInterface constructor.
     * @param string $title
     * @param string $link
     */
    public function __construct(string $title = '', string $link = '');

    /**
     * @param NavsCollectionInterface $children
     * @return NavDtoInterface
     */
    public function setChildren(NavsCollectionInterface $children) : NavDtoInterface;

    /**
     * @param string $path
     * @return NavDtoInterface
     */
    public function setIcon(string $path) : NavDtoInterface;
}
