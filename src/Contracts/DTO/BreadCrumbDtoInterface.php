<?php

namespace VmdCms\CoreCms\Contracts\DTO;

use Illuminate\Contracts\Support\Arrayable;

interface BreadCrumbDtoInterface extends Arrayable
{
    /**
     * BreadCrumbDtoInterface constructor.
     * @param string $url
     * @param string $title
     */
    public function __construct(string $url, string $title);
}
