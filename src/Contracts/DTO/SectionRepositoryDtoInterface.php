<?php

namespace VmdCms\CoreCms\Contracts\DTO;

use VmdCms\CoreCms\CoreModules\Modules\Models\CoreModule;
use VmdCms\CoreCms\DTO\SectionRepositoryDto;

interface SectionRepositoryDtoInterface
{
    /**
     * SectionRepositoryDtoInterface constructor.
     * @param int $id
     * @param string $adminSectionClass
     * @param string $title
     */
    public function __construct(int $id, string $adminSectionClass, string $title);

    /**
     * @return int
     */
    public function getId() : int;

    /**
     * @return string
     */
    public function getAdminSectionClass() : string;

    /**
     * @return string
     */
    public function getTitle() : string;

    /**
     * @param null|CoreModule $coreModule
     * @return SectionRepositoryDto
     */
    public function setCoreModule(?CoreModule $coreModule): SectionRepositoryDto;

    /**
     * @return null|CoreModule
     */
    public function getCoreModule(): ?CoreModule;

}
