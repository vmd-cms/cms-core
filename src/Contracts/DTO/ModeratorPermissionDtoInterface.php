<?php

namespace VmdCms\CoreCms\Contracts\DTO;

use VmdCms\CoreCms\CoreModules\Moderators\Models\ModeratorRolePermission;

interface ModeratorPermissionDtoInterface
{
    /**
     * SectionRepositoryDtoInterface constructor.
     * @param ModeratorRolePermission $item
     */
    public function __construct(ModeratorRolePermission $item);

    /**
     * @return int
     */
    public function getModeratorRoleId() : ?int;

    /**
     * @return int
     */
    public function getCoreSectionId() : int;

    /**
     * @return bool
     */
    public function isDisplayable() : bool;

    /**
     * @return bool
     */
    public function isCreatable() : bool;

    /**
     * @return bool
     */
    public function isViewable() : bool;

    /**
     * @return bool
     */
    public function isEditable() : bool;

    /**
     * @return bool
     */
    public function isDeletable() : bool;

}
