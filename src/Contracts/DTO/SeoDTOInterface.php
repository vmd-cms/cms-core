<?php

namespace VmdCms\CoreCms\Contracts\DTO;

use VmdCms\CoreCms\Contracts\Models\SeoableInterface;
use Illuminate\Contracts\Support\Arrayable;

interface SeoDTOInterface extends Arrayable
{
    /**
     * @param SeoableInterface $model
     * @return null|SeoDTOInterface
     */
    public function setData(SeoableInterface $model) : ?SeoDTOInterface;

    /**
     * @return null|string
     */
    public function getMetaTitle(): ?string;

    /**
     * @return null|string
     */
    public function getMetaDescription(): ?string;

    /**
     * @return null|string
     */
    public function getMetaText(): ?string;
}
