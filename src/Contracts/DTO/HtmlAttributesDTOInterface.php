<?php

namespace VmdCms\CoreCms\Contracts\DTO;

use Illuminate\Contracts\Support\Arrayable;

interface HtmlAttributesDTOInterface extends Arrayable
{
    /**
     * @param string $element
     * @param string $class
     * @return $this
     */
    public function setClass(string $element, string $class): HtmlAttributesDTOInterface;

    /**
     * @param string $element
     * @param string $class
     * @return $this
     */
    public function appendClass(string $element, string $class): HtmlAttributesDTOInterface;

}
