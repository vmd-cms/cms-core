<?php

namespace VmdCms\CoreCms\Contracts\DTO;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\CoreCms\Contracts\Collections\ModeratorPermissionsCollectionInterface;
use VmdCms\CoreCms\CoreModules\Moderators\Models\Moderator;
use VmdCms\CoreCms\Exceptions\Moderators\AuthModeratorNotFound;
use VmdCms\CoreCms\Exceptions\Moderators\ModeratorPermissionNotFound;

interface ModeratorDtoInterface extends Arrayable
{
    /**
     * ModeratorDtoInterface constructor.
     * @param Moderator $item
     * @throws AuthModeratorNotFound
     * @throws ModeratorPermissionNotFound
     */
    public function __construct(Moderator $item = null);

    /**
     * @return bool
     */
    public function hasFullAccess() : bool;

    /**
     * @param int $coreSectionId
     * @return bool
     * @throws ModeratorPermissionNotFound
     */
    public function hasAccessForCoreSection(int $coreSectionId) : bool;

    /**
     * @return ModeratorPermissionsCollectionInterface
     */
    public function getModeratorPermissions() : ModeratorPermissionsCollectionInterface;

}
