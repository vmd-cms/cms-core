<?php

namespace VmdCms\CoreCms\Contracts\Collections;

use VmdCms\CoreCms\Exceptions\Api\Models\ApiParamsException;

interface CollectionPaginatedInterface extends CollectionInterface
{
    /**
     * @return int
     */
    public function getTotal(): int;

    /**
     * @return bool
     */
    public function hasMore(): bool;

    /**
     * @param int $total
     * @return CollectionPaginatedInterface
     */
    public function setTotal(int $total): CollectionPaginatedInterface;

    /**
     * @param bool $flag
     * @return CollectionPaginatedInterface
     */
    public function setHasMore(bool $flag): CollectionPaginatedInterface;

    /**
     * @return int
     */
    public function getPage(): int;

    /**
     * @param int $page
     * @return CollectionPaginatedInterface
     * @throws ApiParamsException
     */
    public function setPage(int $page): CollectionPaginatedInterface;

    /**
     * @return int
     */
    public function getTotalPages(): int;

    /**
     * @param int $total
     * @return CollectionPaginatedInterface
     * @throws ApiParamsException
     */
    public function setTotalPages(int $total): CollectionPaginatedInterface;
}
