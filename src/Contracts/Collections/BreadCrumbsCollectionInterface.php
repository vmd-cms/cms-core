<?php

namespace VmdCms\CoreCms\Contracts\Collections;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\CoreCms\Contracts\DTO\BreadCrumbDtoInterface;

interface BreadCrumbsCollectionInterface extends CollectionInterface,Arrayable
{
    /**
     * @param BreadCrumbDtoInterface $item
     */
    public function appendItem(BreadCrumbDtoInterface $item): void;

    /**
     * @param BreadCrumbDtoInterface $item
     */
    public function prependItem(BreadCrumbDtoInterface $item): void;
}
