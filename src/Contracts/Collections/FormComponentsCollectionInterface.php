<?php

namespace VmdCms\CoreCms\Contracts\Collections;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormComponentInterface;
use Illuminate\Contracts\Support\Arrayable;
use VmdCms\CoreCms\Contracts\Dashboard\HasCmsModelInterface;

interface FormComponentsCollectionInterface extends CollectionInterface, Arrayable, HasCmsModelInterface
{
    /**
     * @param FormComponentInterface $component
     * @return void
     */
    public function appendItem(FormComponentInterface $component) : void;

}
