<?php

namespace VmdCms\CoreCms\Contracts\Collections;

use VmdCms\CoreCms\Contracts\Dashboard\Display\Components\TableColumnInterface;
use Illuminate\Contracts\Support\Arrayable;

interface TableColumnsCollectionInterface extends CollectionInterface, Arrayable
{
    /**
     * @param TableColumnInterface $component
     * @return void
     */
    public function appendItem(TableColumnInterface $component) : void;

    /**
     * @param TableColumnInterface $component
     * @return void
     */
    public function prependItem(TableColumnInterface $component) : void;

}
