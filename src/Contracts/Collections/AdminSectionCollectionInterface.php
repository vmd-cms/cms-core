<?php

namespace VmdCms\CoreCms\Contracts\Collections;

use VmdCms\CoreCms\Contracts\Sections\AdminSectionInterface;
use VmdCms\CoreCms\Exceptions\Sections\SectionNotFoundException;

interface AdminSectionCollectionInterface extends CollectionInterface
{
    /**
     * @param AdminSectionInterface $adminSection
     * @return void
     */
    public function appendItem(AdminSectionInterface $adminSection) : void;

    /**
     * @param string $key
     * @return AdminSectionInterface
     * @throws SectionNotFoundException
     */
    public function getByKey($key) : AdminSectionInterface;
}
