<?php

namespace VmdCms\CoreCms\Contracts\Collections;

use VmdCms\CoreCms\Contracts\Dashboard\Validation\RuleInterface;
use Illuminate\Contracts\Support\Arrayable;

interface RulesCollectionInterface extends CollectionInterface,Arrayable
{
    /**
     * @param RuleInterface $rule
     */
    public function append(RuleInterface $rule);

    /**
     * @param string $key
     * @return bool
     */
    public function hasRule(string $key): bool;
}
