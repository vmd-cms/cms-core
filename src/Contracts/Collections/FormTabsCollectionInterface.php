<?php

namespace VmdCms\CoreCms\Contracts\Collections;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormTabInterface;
use VmdCms\CoreCms\Contracts\Dashboard\HasCmsModelInterface;

interface FormTabsCollectionInterface extends CollectionInterface, Arrayable, HasCmsModelInterface
{
    /**
     * @param FormTabInterface $formTab
     * @return void
     */
    public function appendItem(FormTabInterface $formTab) : void;

    /**
     * @param array $formTabs
     * @return void
     */
    public function appendItems(array $formTabs) : void;

}
