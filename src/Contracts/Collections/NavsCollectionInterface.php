<?php

namespace VmdCms\CoreCms\Contracts\Collections;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\CoreCms\Contracts\DTO\NavDtoInterface;

interface NavsCollectionInterface extends CollectionInterface,Arrayable
{
    /**
     * @param NavDtoInterface $nav
     */
    public function append(NavDtoInterface $nav);

}
