<?php

namespace VmdCms\CoreCms\Contracts\Collections;

use VmdCms\CoreCms\Contracts\Dashboard\Display\Components\TableColumnInterface;
use Illuminate\Contracts\Support\Arrayable;
use VmdCms\CoreCms\Contracts\Dashboard\HasCmsModelInterface;
use VmdCms\CoreCms\Contracts\DTO\DependedComponentDtoInterface;

interface DependedComponentsCollectionInterface extends CollectionInterface, Arrayable
{
    /**
     * @param DependedComponentDtoInterface $component
     * @return void
     */
    public function appendItem(DependedComponentDtoInterface $component) : void;

}
