<?php

namespace VmdCms\CoreCms\Contracts\Collections;

use VmdCms\CoreCms\Contracts\Dashboard\Display\Blocks\BlockInterface;
use Illuminate\Contracts\Support\Arrayable;

interface BlocksCollectionInterface extends CollectionInterface, Arrayable
{
    /**
     * @param BlockInterface $block
     * @return void
     */
    public function appendItem(BlockInterface $block) : void;

}
