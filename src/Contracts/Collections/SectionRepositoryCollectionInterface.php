<?php

namespace VmdCms\CoreCms\Contracts\Collections;

use VmdCms\CoreCms\Contracts\DTO\SectionRepositoryDtoInterface;

interface SectionRepositoryCollectionInterface extends CollectionInterface
{
    /**
     * @param SectionRepositoryDtoInterface $sectionRepository
     * @return void
     */
    public function appendItem(SectionRepositoryDtoInterface $sectionRepository) : void;

    /**
     * @param string $key
     * @return SectionRepositoryDtoInterface
     */
    public function getByKey($key) : SectionRepositoryDtoInterface;
}
