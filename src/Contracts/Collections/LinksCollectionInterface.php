<?php

namespace VmdCms\CoreCms\Contracts\Collections;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\CoreCms\Contracts\DTO\LinkDtoInterface;

interface LinksCollectionInterface extends CollectionInterface,Arrayable
{
    /**
     * @param LinkDtoInterface $link
     */
    public function append(LinkDtoInterface $link);

}
