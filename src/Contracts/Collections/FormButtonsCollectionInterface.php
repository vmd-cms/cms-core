<?php

namespace VmdCms\CoreCms\Contracts\Collections;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Buttons\FormButtonInterface;
use Illuminate\Contracts\Support\Arrayable;

interface FormButtonsCollectionInterface extends CollectionInterface, Arrayable
{
    /**
     * @param FormButtonInterface $button
     * @return void
     */
    public function appendItem(FormButtonInterface $button) : void;

}
