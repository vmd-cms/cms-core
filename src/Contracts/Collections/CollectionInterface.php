<?php

namespace VmdCms\CoreCms\Contracts\Collections;

use Illuminate\Support\Collection;

interface CollectionInterface
{
    /**
     * @return Collection
     */
    public function getItems() : Collection;

    /**
     * @return array
     */
    public function getItemsArray() : array;

}
