<?php

namespace VmdCms\CoreCms\Contracts\Collections;

use VmdCms\CoreCms\Contracts\DTO\ModeratorPermissionDtoInterface;
use VmdCms\CoreCms\Exceptions\Moderators\ModeratorPermissionNotFound;

interface ModeratorPermissionsCollectionInterface extends CollectionInterface
{
    /**
     * @param ModeratorPermissionDtoInterface $permDto
     * @return void
     */
    public function appendItem(ModeratorPermissionDtoInterface $permDto) : void;

    /**
     * @param string $key
     * @return ModeratorPermissionDtoInterface
     * @throws ModeratorPermissionNotFound
     */
    public function getByCoreSectionId($key) : ModeratorPermissionDtoInterface;
}
