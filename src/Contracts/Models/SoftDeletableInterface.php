<?php

namespace VmdCms\CoreCms\Contracts\Models;

interface SoftDeletableInterface
{
    /**
     * @return mixed
     */
    public function softDelete();
}
