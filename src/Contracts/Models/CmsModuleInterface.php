<?php

namespace VmdCms\CoreCms\Contracts\Models;

interface CmsModuleInterface extends ActivableInterface
{
    /**
     * @return string
     */
    public function getSlug() : string;
}
