<?php

namespace VmdCms\CoreCms\Contracts\Models;

use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;

interface HasUrlValueInterface
{
    /**
     * @return string|null
     * @throws NotCmsModelException
     */
    public function getUrlValueAttribute() : ?string;
}
