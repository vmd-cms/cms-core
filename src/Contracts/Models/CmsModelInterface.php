<?php

namespace VmdCms\CoreCms\Contracts\Models;

interface CmsModelInterface
{
    /**
     * @return string
     */
    public static function getPrimaryField() : string;

    /**
     * @return string|int|null
     */
    public function getPrimaryValue();
}
