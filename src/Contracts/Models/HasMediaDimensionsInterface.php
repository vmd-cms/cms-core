<?php

namespace VmdCms\CoreCms\Contracts\Models;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\CroppedDimensionDTOCollectionInterface;

interface HasMediaDimensionsInterface
{
    /**
     * @param string $storedPath
     * @param string $field
     */
    public function storeMediaDimensions(string $storedPath, string $field = 'media');

    /**
     * @param string $field
     */
    public function deleteMediaDimensions(string $field = 'media');

    /**
     * @param bool|null $flag
     * @return HasMediaDimensionsInterface
     */
    public function setCopyToWEBP(bool $flag = null): HasMediaDimensionsInterface;

    /**
     * @param CroppedDimensionDTOCollectionInterface $croppedDimensions
     * @return HasMediaDimensionsInterface
     */
    public function setCroppedDimensions(CroppedDimensionDTOCollectionInterface $croppedDimensions): HasMediaDimensionsInterface;

    /**
     * @return string
     */
    public function getMediaDimensionsResourceKey(): string;
}
