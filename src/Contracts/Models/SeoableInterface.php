<?php

namespace VmdCms\CoreCms\Contracts\Models;

interface SeoableInterface
{
    public function seo();
}
