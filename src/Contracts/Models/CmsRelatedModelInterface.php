<?php

namespace VmdCms\CoreCms\Contracts\Models;

interface CmsRelatedModelInterface
{
    /**
     * @return string
     */
    public static function getForeignField() : string;

    /**
     * @return string
     */
    public static function getBaseModelClass() : string ;

}
