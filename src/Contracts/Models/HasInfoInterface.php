<?php

namespace VmdCms\CoreCms\Contracts\Models;

interface HasInfoInterface
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function info();
}
