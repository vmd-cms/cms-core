<?php

namespace VmdCms\CoreCms\Contracts\Models;

interface HasKeyInterface
{
    /**
     * @return null|string
     */
    public static function getModelKey() : ?string;
}
