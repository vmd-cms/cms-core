<?php

namespace VmdCms\CoreCms\Contracts\Models;

interface ActivableInterface
{
    public function scopeActive($query);
}
