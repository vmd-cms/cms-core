<?php

namespace VmdCms\CoreCms\Contracts\Models;

interface HasGroupInterface
{
    /**
     * @return null|string
     */
    public static function getModelGroup() : ?string;
}
