<?php

namespace VmdCms\CoreCms\Contracts\Models;

interface HasSeoTemplateInterface extends SeoableInterface
{
    /**
     * @return SeoTemplateInterface
     */
    public function getSeoTemplateModel(): SeoTemplateInterface;
}
