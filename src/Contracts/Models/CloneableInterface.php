<?php

namespace VmdCms\CoreCms\Contracts\Models;

interface CloneableInterface
{
    /**
     * @return array
     */
    public function cloneableRelations(): array;

    /**
     * @param CmsModelInterface $model
     * @return CmsModelInterface
     */
    public function cloneUniqueFields(CmsModelInterface $model): CmsModelInterface;
}
