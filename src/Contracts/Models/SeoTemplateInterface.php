<?php

namespace VmdCms\CoreCms\Contracts\Models;

interface SeoTemplateInterface
{
    /**
     * @return string
     */
    public static function metaTitleKey(): string;

    /**
     * @return string
     */
    public static function metaDescriptionKey(): string;

    /**
     * @return string|null
     */
    public function getTemplate(): ?string;
}
