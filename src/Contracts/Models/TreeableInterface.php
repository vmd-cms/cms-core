<?php

namespace VmdCms\CoreCms\Contracts\Models;

interface TreeableInterface
{
    public function getParentField() : string;

    public static function getPrimaryField() : string;

    public function scopeTree($query,$onlyActive = false);

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children();

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childrenTree();

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent();
}
