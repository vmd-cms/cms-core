<?php

namespace VmdCms\CoreCms\Contracts\Models;

interface CmsCrossModelInterface
{
    public function getBaseModelClass(): string;

    public function getTargetModelClass(): string;

    public function getBaseModelPrimaryField(): string;

    public function getTargetModelPrimaryField(): string;

    public function getBaseModelForeignField(): string;

    public function getTargetModelForeignField(): string;
}
