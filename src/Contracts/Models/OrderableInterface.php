<?php

namespace VmdCms\CoreCms\Contracts\Models;

interface OrderableInterface
{
    public function getOrderField() : string;

    public function scopeOrder($query);
}
