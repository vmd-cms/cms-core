<?php

namespace VmdCms\CoreCms\Contracts\Models;

interface StaticInstanceInterface
{
    /**
     * @return StaticInstanceInterface
     */
    public static function getInstance() : StaticInstanceInterface;
}
