<?php

namespace VmdCms\CoreCms\Contracts\Models;

use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;

interface HasUrlPathInterface
{
    /**
     * @return string|null
     * @throws NotCmsModelException
     */
    public function getUrlPathAttribute() : ?string;
}
