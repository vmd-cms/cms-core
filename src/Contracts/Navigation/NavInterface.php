<?php

namespace VmdCms\CoreCms\Contracts\Navigation;

use VmdCms\CoreCms\DTO\Dashboard\Navs\NavBarConfig;

interface NavInterface
{
    /**
     * NavInterface constructor.
     * @param array $navBarConfig
     */
    public function __construct(array $navBarConfig = []);

    /**
     * @param NavItemDtoInterface $navItem
     * @return NavInterface
     */
    public function appendItem(NavItemDtoInterface $navItem) : NavInterface;

    /**
     * @return string
     */
    public function getJson() : string;

}
