<?php

namespace VmdCms\CoreCms\Contracts\Navigation;

use VmdCms\CoreCms\DTO\Dashboard\Navs\ChipItemDto;

interface NavItemInterface
{
    /**
     * @param NavItemInterface $navItem
     * @return NavItemInterface
     */
    public function appendChild(NavItemInterface $navItem) : NavItemInterface;

    /**
     * @return int|null
     */
    public function getIdAttribute() : ?int ;

    /**
     * @return string|null
     */
    public function getIconAttribute() : ?string;

    /**
     * @return string|null
     */
    public function getTitleAttribute() : ?string;

    /**
     * @return string|null
     */
    public function getUrlAttribute() : ?string;

    /**
     * @return bool
     */
    public function isGroupTitle() : bool;

    /**
     * @return array
     */
    public function getChildrenAttribute() : array;

    /**
     * @return array|null
     */
    public function getChip() : ?array;

}
