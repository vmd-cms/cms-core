<?php

namespace VmdCms\CoreCms\Contracts\Navigation;

interface NavItemDtoInterface
{
    /**
     * NavItemDtoInterface constructor.
     * @param NavItemInterface $navItem
     */
    public function __construct(NavItemInterface $navItem);

}
