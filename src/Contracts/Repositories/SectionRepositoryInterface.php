<?php

namespace VmdCms\CoreCms\Contracts\Repositories;

use VmdCms\CoreCms\Contracts\Collections\SectionRepositoryCollectionInterface;
use VmdCms\CoreCms\Exceptions\Repositories\SectionRepositoryException;

interface SectionRepositoryInterface
{
    /**
     * @return SectionRepositoryCollectionInterface
     * @throws SectionRepositoryException
     */
    public function getCollection() : SectionRepositoryCollectionInterface;
}
