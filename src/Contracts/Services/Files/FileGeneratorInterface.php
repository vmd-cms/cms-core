<?php

namespace VmdCms\CoreCms\Contracts\Services\Files;

use Illuminate\Filesystem\Filesystem;

interface FileGeneratorInterface
{
    /**
     * @param string $filePath
     * @param string $stubPath
     * @param array $replaceAssocArr
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function createFile(string $filePath,string $stubPath,array $replaceAssocArr = []): void;

    /**
     * @param string $filePath
     */
    public function makeDirectory(string $filePath): void;

    /**
     * @param string $stubPath
     * @param array $replaceAssocArr
     * @return string|string[]
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function getPrepareStub(string $stubPath,array $replaceAssocArr = []) : string;

    /**
     * @param array $baseReplace
     */
    public function setBaseReplace(array $baseReplace): void;
}
