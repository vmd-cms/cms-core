<?php

namespace VmdCms\CoreCms\Contracts\Services;

use Illuminate\Filesystem\Filesystem;

interface PaginationInterface
{
    /**
     * PaginationInterface constructor.
     * @param int $total
     * @param int $page
     * @param int $limit
     */
    public function __construct(int $total, int $page = 1, int $limit = self::LIMIT);

    /**
     * @return int
     */
    public function getNextPage(): int;

    /**
     * @return int
     */
    public function getPrevPage(): int;

    /**
     * @return int
     */
    public function getTotal(): int;

    /**
     * @return int
     */
    public function getPage(): int;

    /**
     * @return int
     */
    public function getLimit(): int;

    /**
     * @return int
     */
    public function getTotalPages(): int;

    /**
     * @return int|null
     */
    public function getResourceId(): ?int;

    /**
     * @param int $resourceId
     * @return PaginationInterface
     */
    public function setResourceId(int $resourceId): PaginationInterface;

}
