<?php

namespace VmdCms\CoreCms\Middleware;

use Closure;
use VmdCms\CoreCms\CoreModules\Content\Entity\Languages;

class ApiLanguagesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            Languages::getInstance()->setLocaleActive($request->header('locale',Languages::getInstance()->getDefaultLocale()));
            return $next($request);
        }
        catch (\Exception $exception)
        {
            return $next($request);
        }
    }
}
