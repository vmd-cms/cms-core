<?php

namespace VmdCms\CoreCms\Middleware;

use Closure;
use VmdCms\CoreCms\Services\Responses\ApiResponse;
use VmdCms\Modules\Users\Entity\Auth\ApiAuthEntity;

class ApiAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            ApiAuthEntity::authUserApiToken($request->header('token'));
            return $next($request);
        }
        catch (\Exception $exception)
        {
            return ApiResponse::error(['error_code' => $exception->getMessage()],403);
        }
    }
}
