<?php

namespace VmdCms\CoreCms\Middleware;

use Closure;
use Illuminate\Http\Request;
use VmdCms\CoreCms\Contracts\Sections\AdminSectionInterface;
use VmdCms\CoreCms\Exceptions\CoreException;
use VmdCms\CoreCms\Exceptions\Initializers\AdminInitializerNotFoundException;
use VmdCms\CoreCms\Exceptions\Sections\SectionNotFoundException;
use VmdCms\CoreCms\Exceptions\Sections\SectionRouteNotAvailableException;
use VmdCms\CoreCms\Initializers\AdminInitializer;
use VmdCms\CoreCms\Repositories\SectionRepository;

class AdminInitializerMiddleware
{
    /**
     * @var AdminInitializer
     */
    private $adminInitializer;

    /**
     * @var AdminSectionInterface
     */
    private $currentAdminSection;

    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $this->adminInitializer =  new AdminInitializer(app(), new SectionRepository());
            $this->currentAdminSection = $this->getCurrentAdminSection($this->adminInitializer);
            $this->adminInitializer->setCurrentAdminSection($this->currentAdminSection);
            $this->isRouteAvailable();
        }
        catch (CoreException $e)
        {
            return abort(404);
        }

        $this->shareDashboardData();

        return $next($request);
    }

    private function shareDashboardData()
    {
        view()->share('dashboardData', $this->adminInitializer->getDashboardData());
    }

    /**
     * @param AdminInitializer $adminInitializer
     * @return AdminSectionInterface
     * @throws AdminInitializerNotFoundException
     * @throws SectionNotFoundException
     */
    private function getCurrentAdminSection(AdminInitializer $adminInitializer)
    {
        if(!$adminInitializer instanceof AdminInitializer) {
            throw new AdminInitializerNotFoundException();
        }
        return $adminInitializer->getAdminSectionByKey(\request()->sectionSlug);
    }

    /**
     * @throws SectionRouteNotAvailableException
     */
    private function isRouteAvailable()
    {
        if(!$this->currentAdminSection->isAvailableAction(\request()->route()->getName())) {
            throw new SectionRouteNotAvailableException();
        }
    }
}
