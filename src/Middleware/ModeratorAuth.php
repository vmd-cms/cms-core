<?php

namespace VmdCms\CoreCms\Middleware;

use Closure;
use VmdCms\CoreCms\Services\CoreRouter;

class ModeratorAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    if(auth()->guard('moderator')->id()){
		    return $next($request);
	    }
	    return redirect(route(CoreRouter::ROUTE_MODERATOR_LOGIN, [],false));
    }
}
