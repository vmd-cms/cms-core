<?php

namespace VmdCms\CoreCms\Middleware;

use Closure;
use VmdCms\CoreCms\CoreModules\Content\Entity\Languages;

class CoreLanguagesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Languages::getInstance()->refreshDashboardAppLocale();
        return $next($request);
    }
}
