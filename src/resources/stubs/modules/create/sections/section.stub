<?php

namespace VmdCms\Modules\{name_module}\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Models\CmsSection;

class {name_single_camel} extends CmsSection
{
    /**
     * @var string
     */
    protected $slug = '{name_plural_snake}';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return "{name_plural_camel}";
    }

    public function display()
    {
        return Display::dataTable([
            Column::text('title','Title'),
            Column::text('active', 'Active'),
            Column::date('created_at', 'Created at')->setFormat('Y-m-d'),
        ])->setSearchable(true);
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return $this->edit(null);
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::input('title','Title')->required(),
            FormComponent::switch('active','Active'),
        ]);
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\{name_module}\Models\{name_single_camel}::class;
    }
}
