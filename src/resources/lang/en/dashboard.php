<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication CoreLanguage Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'append' => 'Add New',
    'error' => 'Error',
    'confirm_action' => 'Are you sure to make this action?',
    'not_set_all_required' => 'Not set all required fields',

];
