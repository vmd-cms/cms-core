import Vue from "vue";
import Router from "vue-router";

// Containers
const DefaultContainer = () => import("../containers/DefaultContainer");

Vue.use(Router);

export default [
    {
        path: "/dashboard",
        name: "Home",
        component: DefaultContainer,
        children: [
            {
                path: "users",
                meta: {label: "Users"},
                component: () => import('../views/users/Users')
            }
        ]
    },
    {
        path: "*",
        name: "404",
        component: () => import('../views/pages/Page404')
    }
]
