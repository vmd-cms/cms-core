import Vue from "vue";
import store from "../store";

Vue.mixin({
    methods: {
        lang(key) {
            return store.state.translates[key] ?? key;
        }
    }
});
