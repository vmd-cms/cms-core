import {isNumber} from "bootstrap-vue/esm/utils/inspect";

let rules = {
    getValidationRules(element){
        let data = [];
        Object.keys(element.rules).forEach((key,val)=> {
            let item = element.rules[key];

            if(key === "required")
            {
                data.push(v => !!v || (item.message ?? lang('required')));
            }
            else if(key === "email")
            {
                data.push(v => /.+@.+\..+/.test(v) || (item.message ?? lang('must_be_valid_email')));
            }
            else if(key === "integer")
            {
                data.push(v => (v != undefined && v != '' ? parseInt(v) == v : true) || (item.message ?? lang('must_be_number')));
                if(item.value) data.push(v => v >= 0 || (item.message ?? lang('must_be_number_positive')));
            }
            else if(key === "float")
            {
                data.push(v => (v != undefined && v != '' ? parseFloat(v) == v : true) || (item.message ?? lang('must_be_float')));
                if(item.value) data.push(v => v >= 0 || (item.message ?? lang('must_be_float_positive')));
            }
            else if(key === "max")
            {
                data.push(v => (v != undefined && v != '' ? parseFloat(v) <= item.value : true) || (item.message ?? lang('max') + ' ' + item.value));
            }
            else if(key === "min")
            {
                data.push(v => (v != undefined && v != '' ? parseFloat(v) >= item.value : true) || (item.message ?? lang('min') + ' ' + item.value));
            }
            else if(key === "maxLength")
            {
                data.push(v => (v || '').length <= item.value || (item.message ??  lang('max') + ' ' + item.value + ' characters'));
            }
            else if(key === "minLength")
            {
                data.push(v => (v || '').length >= item.value || (item.message ?? lang('min') + ' ' + item.value + ' ' + lang('characters')));
            }
        });
        return data;
    },
    async getError(element, value)
    {
        let message = '';
        if(element.rules.unique)
        {
            let valid = await unique_validation(element, value);
            message = !valid ? (element.rules.message ?? lang('must_be_unique')) : '';
        }
        return message;
    }
}

async function unique_validation(element,value) {

    let data = {value:value,field: element.field,cms_model_id: element.cms_model_id,cms_model_class: element.cms_model_class};
    let unique = true;
       await axios.post(element.rules.unique.route, data)
        .then(response => {
            unique = true;
        })
        .catch(error => {
            unique = false;
        });
        return  unique;
}

export default rules
