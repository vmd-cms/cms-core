require('./bootstrap');
require('./styles/styles.css')
require('./styles/icons.css')

window.Vue = require('vue');

import Vue from 'vue';
import vuetify from './plugins/vuetify/vuetify';
import store from './store/index';
import components from './components/index';
import mixins from './mixins/index';

const app = new Vue({
    vuetify,
    store,
    el: '#admin_wrapper',
    components
});


