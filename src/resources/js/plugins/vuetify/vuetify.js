import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import './assets/roboto.css'
import '@mdi/font/css/materialdesignicons.css'
import 'typeface-oswald/index.css'

Vue.use(Vuetify)

const opts = {}

export default new Vuetify(opts)
