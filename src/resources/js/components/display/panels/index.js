import StatisticInfoPanel from './StatisticInfoPanel'
import StatisticCharPanel from './StatisticCharPanel'

export default{
    StatisticInfoPanel,
    StatisticCharPanel
}
