import DataTableDisplay from './DataTableDisplay'
import DraggableNestedTree from './DraggableNestedTree'
import StatisticPanel from './StatisticPanel'
import Panels from './panels/index'

export default{
    DataTableDisplay,
    DraggableNestedTree,
    StatisticPanel,
    ...Panels
}
