import TextColumnLabel from './TextColumnLabel'
import TextColumnSlot from './TextColumnSlot'

import SwitchColumnLabel from './SwitchColumnLabel'
import SwitchColumnSlot from './SwitchColumnSlot'

export default{
    TextColumnLabel,
    TextColumnSlot,
    SwitchColumnLabel,
    SwitchColumnSlot
}
