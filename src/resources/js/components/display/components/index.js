import ColumnTextComponent from './ColumnTextComponent'
import ColumnCustomComponent from './ColumnCustomComponent'
import ColumnPhotoComponent from './ColumnPhotoComponent'
import ColumnChipComponent from './ColumnChipComponent'
import ColumnDateComponent from './ColumnDateComponent'
import ColumnOrderComponent from './ColumnOrderComponent'
import ColumnActionsComponent from './ColumnActionsComponent'
import ColumnColorPickerComponent from './ColumnColorPickerComponent'
import EditableColumnDateTimeComponent from './EditableColumnDateTimeComponent'
import EditableColumnComponent from './EditableColumnComponent'

export default{
    ColumnTextComponent,
    ColumnCustomComponent,
    ColumnPhotoComponent,
    ColumnChipComponent,
    ColumnDateComponent,
    ColumnOrderComponent,
    ColumnActionsComponent,
    ColumnColorPickerComponent,
    EditableColumnDateTimeComponent,
    EditableColumnComponent
}
