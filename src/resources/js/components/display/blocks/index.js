import UploadBlockComponent from './UploadBlockComponent'
import DownloadBlockComponent from './DownloadBlockComponent'

export default{
    UploadBlockComponent,
    DownloadBlockComponent
}
