import AppBar from './bars/app_bar.vue';
import NavigationDrawer from './bars/NavigationDrawer.vue';
import NavigationDrawerAdmin from "./bars/NavigationDrawerAdmin";
import TreeView from './bars/TreeView.vue';
import ListView from './bars/ListView.vue';
import HeadPanel from './bars/HeadPanel.vue';
import FooterPanel from './bars/FooterPanel.vue';

import Containers from './containers/index';
import Forms from "./forms/index";
import Display from "./display/index";
import Filters from "./filters/index";

export default {
        AppBar,
        NavigationDrawer,
        NavigationDrawerAdmin,
        TreeView,
        ListView,
        HeadPanel,
        FooterPanel,
        ...Containers,
        ...Forms,
        ...Display,
        ...Filters
}
