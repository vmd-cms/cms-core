import FilterPanel from './FilterPanel'
import FilterComponents from './components/index'

export default{
    FilterPanel,
    ...FilterComponents
}
