import FilterTreeComponent from './FilterTreeComponent'
import FilterComponent from './FilterComponent'

export default{
    FilterTreeComponent,
    FilterComponent
}
