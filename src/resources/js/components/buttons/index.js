import DefaultButtonComponent from './DefaultButtonComponent'
import CreateButtonComponent from './CreateButtonComponent'
import CopyButtonComponent from './CopyButtonComponent'
import DeleteButtonComponent from './DeleteButtonComponent'
import BackButtonComponent from './BackButtonComponent'
import SaveButtonComponent from './SaveButtonComponent'
import CustomButtonComponent from './CustomButtonComponent'
import SaveCreateButtonComponent from './SaveCreateButtonComponent'

export default{
    DefaultButtonComponent,
    CreateButtonComponent,
    CopyButtonComponent,
    DeleteButtonComponent,
    BackButtonComponent,
    SaveCreateButtonComponent,
    CustomButtonComponent,
    SaveButtonComponent
}
