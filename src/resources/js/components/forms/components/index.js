import InputComponent from './InputComponent.vue'
import PasswordComponent from './PasswordComponent.vue'
import TextAreaComponent from './TextAreaComponent.vue'
import CkeditorComponent from './CkeditorComponent.vue'
import SelectComponent from './SelectComponent.vue'
import AsyncSelectComponent from './AsyncSelectComponent.vue'
import AutocompleteComponent from './AutocompleteComponent.vue'
import DependedSelectComponent from './DependedSelectComponent.vue'
import TreeSelectComponent from './TreeSelectComponent.vue'
import SwitchComponent from './SwitchComponent.vue'
import RadioComponent from './RadioComponent.vue'
import CheckboxComponent from './CheckboxComponent.vue'
import TreeComponent from './TreeComponent.vue'
import ImageComponent from './ImageComponent.vue'
import FileInputComponent from './FileInputComponent.vue'
import DataTableComponent from './DataTableComponent.vue'
import CustomComponent from './CustomComponent.vue'
import ColorPickerComponent from './ColorPickerComponent.vue'
import MaskedComponent from './MaskedComponent.vue'
import DateTimePickerComponent from './DateTimePickerComponent.vue'
import ButtonsComponent from './ButtonsComponent.vue'
import HtmlComponent from './HtmlComponent.vue'
import AlertComponent from './AlertComponent.vue'
import ActionHistoryComponent from './ActionHistoryComponent.vue'
import TableColumns from './table_columns/index'

export default{
    InputComponent,
    PasswordComponent,
    TextAreaComponent,
    CkeditorComponent,
    SelectComponent,
    AsyncSelectComponent,
    AutocompleteComponent,
    DependedSelectComponent,
    TreeSelectComponent,
    SwitchComponent,
    RadioComponent,
    CheckboxComponent,
    TreeComponent,
    ImageComponent,
    FileInputComponent,
    DataTableComponent,
    CustomComponent,
    ColorPickerComponent,
    DateTimePickerComponent,
    MaskedComponent,
    ButtonsComponent,
    ActionHistoryComponent,
    HtmlComponent,
    AlertComponent,
    ...TableColumns
}
