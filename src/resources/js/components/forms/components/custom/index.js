import ProductPriceGroupsPanelComponent from './ProductPriceGroupsPanelComponent.vue'
import ProductPricesPanelComponent from './ProductPricesPanelComponent.vue'

export default{
    ProductPriceGroupsPanelComponent,
    ProductPricesPanelComponent
}
