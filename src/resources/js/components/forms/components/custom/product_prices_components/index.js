import ProductPriceGroupComponent from './ProductPriceGroupComponent.vue'
import ProductPriceParamComponent from './ProductPriceParamComponent.vue'

export default{
    ProductPriceGroupComponent,
    ProductPriceParamComponent
}
