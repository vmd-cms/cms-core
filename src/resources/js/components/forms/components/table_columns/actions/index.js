import DeleteActionComponent from './DeleteActionComponent.vue';
import ExpandActionComponent from './ExpandActionComponent.vue';
import EditActionComponent from './EditActionComponent.vue';

export default{
    DeleteActionComponent,
    EditActionComponent,
    ExpandActionComponent
}
