import ColumnInputComponent from './ColumnInputComponent.vue';
import ColumnAutocompleteComponent from './ColumnAutocompleteComponent.vue';
import ColumnTextComponent from './ColumnTextComponent.vue';
import ColumnSwitchComponent from './ColumnSwitchComponent.vue';
import ColumnActionsComponent from './ColumnActionsComponent.vue';
import ColumnImageComponent from './ColumnImageComponent.vue';
import ColumnFileComponent from './ColumnFileComponent.vue';
import ColumnDateTimeComponent from './ColumnDateTimeComponent.vue';
import ColumnSelectComponent from './ColumnSelectComponent.vue';
import ColumnTextAreaComponent from './ColumnTextAreaComponent.vue';
import ColumnCustomComponent from './ColumnCustomComponent.vue';
import ColumnPhotoComponent from './ColumnPhotoComponent.vue';
import ColumnColorPickerComponent from './ColumnColorPickerComponent.vue';

export default{
    ColumnInputComponent,
    ColumnAutocompleteComponent,
    ColumnTextComponent,
    ColumnSwitchComponent,
    ColumnActionsComponent,
    ColumnImageComponent,
    ColumnFileComponent,
    ColumnDateTimeComponent,
    ColumnSelectComponent,
    ColumnColorPickerComponent,
    ColumnCustomComponent,
    ColumnPhotoComponent,
    ColumnTextAreaComponent
}
