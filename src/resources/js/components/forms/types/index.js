import FormPanelComponent from './FormPanelComponent.vue'
import FormTabbedComponent from './FormTabbedComponent.vue'

export default{
    FormPanelComponent,
    FormTabbedComponent
}
