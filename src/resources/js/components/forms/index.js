import FormWrapperComponent from './FormWrapperComponent.vue'

import FormComponents from './components/index'

export default{
    FormWrapperComponent,
    ...FormComponents
}
