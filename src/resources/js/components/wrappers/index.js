import FormComponentWrapperComponent from './FormComponentWrapperComponent';
import FormRowWrapperComponent from './FormRowWrapperComponent';

export default {
    FormComponentWrapperComponent,
    FormRowWrapperComponent
}
