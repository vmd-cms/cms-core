<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>Laravel</title>
    <script src="{{ asset('/js/vendor/vmd_cms/app.js') }}" defer></script>
    <script src="{{ asset('/js/vendor/vmd_cms/admin.js') }}" defer></script>
    <link href="{{ asset('/css/vendor/vmd_cms/app.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/vendor/vmd_cms/admin.css') }}" rel="stylesheet">
</head>
<body>
<script>
    const
        translates = {!! $dashboardData->appTranslates ?? '{}' !!};
</script>
<div id="admin_wrapper">
    @yield('admin-wrapper')
</div>
</body>
</html>
