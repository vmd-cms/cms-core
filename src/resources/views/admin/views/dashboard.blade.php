@extends('vmd_cms::admin.containers.admin_wrapper')
@section('section_panel')
   <div class="section-panel-wrapper">
       <div>
           {!! $head_panel ?? '' !!}
           <div class="panel-wrapper">
               {!! $filter_panel ?? '' !!}

               {!! $section_panel ?? '' !!}
           </div>
       </div>
       <div>{!! $footer_panel ?? '' !!}</div>
   </div>
@endsection
