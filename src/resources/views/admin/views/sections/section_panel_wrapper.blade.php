@if(isset($panels) && is_countable($panels) && count($panels))
    <div class="composed-panels">
        <div class="section-panel-card">
            @yield('section_panel_card')
        </div>

        <v-row>
            @foreach($panels as $panel)
                <v-col md="{{$panel['cols_md'] ?? 12}}">
                    {!! $panel['content'] ?? '' !!}
                </v-col>
            @endforeach
        </v-row>

    </div>
@else
    <div class="section-panel-card">
        @yield('section_panel_card')
    </div>
@endif
