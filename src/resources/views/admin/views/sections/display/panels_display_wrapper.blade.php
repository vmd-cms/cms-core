<div class="display-wrapper">
<v-container fluid>
    <v-row class="pl-2 pr-3">
        @foreach($panels as $panel)
          <v-col sm="12">
              {!! $panel['content'] ?? '' !!}
          </v-col>
        @endforeach
    </v-row>
</v-container>
</div>
