@extends('vmd_cms::admin.views.sections.section_panel_wrapper')
@section('section_panel_card')
    <draggable-nested-tree :data="{{json_encode($data)}}"></draggable-nested-tree>
@endsection
