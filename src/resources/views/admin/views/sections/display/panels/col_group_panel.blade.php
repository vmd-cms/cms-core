<v-container fluid class="container-wrapper">
    <v-row>
        @foreach($panels as $panel)
            <v-col md="{{$panel['cols_md'] ?? 12}}">
                {!! $panel['content'] ?? '' !!}
            </v-col>
        @endforeach
    </v-row>
</v-container>
