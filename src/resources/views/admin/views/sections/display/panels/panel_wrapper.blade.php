@if(isset($panel))
    <v-container fluid class="container-wrapper">
        {!! $panel !!}
    </v-container>
@endif
