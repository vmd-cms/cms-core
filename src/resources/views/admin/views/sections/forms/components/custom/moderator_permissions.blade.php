<div class="permissions-container">
    <div class="acl-section">
        <table>
            <tr class="t-head">
                <th class="item section">{{\VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang::get('section')}}</th>
                <th class="item"><input type="checkbox" data-scope="display" onclick="aclCheckGroup(this)"><span
                        class="">{{\VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang::get('access')}}</span></th>
                <th class="item"><input type="checkbox" data-scope="create" onclick="aclCheckGroup(this)"><span
                        class="">{{\VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang::get('creating')}}</span></th>
                <th class="item"><input type="checkbox" data-scope="view" onclick="aclCheckGroup(this)"><span class="">{{\VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang::get('viewing')}}</span>
                </th>
                <th class="item"><input type="checkbox" data-scope="edit" onclick="aclCheckGroup(this)"><span class="">{{\VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang::get('editing')}}</span>
                </th>
                <th class="item"><input type="checkbox" data-scope="delete" onclick="aclCheckGroup(this)"><span
                        class="">{{\VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang::get('deleting')}}</span></th>
            </tr>
            @if(isset($sections) && is_countable($sections))
                @foreach($sections as $section)

                    @php
                        $item = isset($permissions) ? $permissions->where('core_section_id',$section->id)->first() : null;
                    @endphp

                    <tr class="t-body-item">

                        <td>
                            <input type="hidden" value="{{$section->id}}" name="section_id[]">
                            <stan>{{$section->title}}</stan>
                        </td>
                        <td>
                            <div class="item"><input type="checkbox" data-acl-display name="display[{{$loop->index}}]"
                                                     @if($item && $item->display_perm) checked @endif></div>
                        </td>
                        <td>
                            <div class="item"><input type="checkbox" data-acl-create name="create[{{$loop->index}}]"
                                                     @if($item && $item->create_perm) checked @endif></div>
                        </td>
                        <td>
                            <div class="item"><input type="checkbox" data-acl-view name="view[{{$loop->index}}]"
                                                     @if($item && $item->view_perm) checked @endif></div>
                        </td>
                        <td>
                            <div class="item"><input type="checkbox" data-acl-edit name="edit[{{$loop->index}}]"
                                                     @if($item && $item->edit_perm) checked @endif></div>
                        </td>
                        <td>
                            <div class="item"><input type="checkbox" data-acl-delete name="delete[{{$loop->index}}]"
                                                     @if($item && $item->delete_perm) checked @endif></div>
                        </td>
                    </tr>
                @endforeach
            @endif
        </table>
    </div>
</div>
<script>
    import StatisticPanel from "../../../../../../../js/components/display/StatisticPanel";
    export default {
        components: {StatisticPanel}
    }
</script>
