@extends('vmd_cms::admin.views.sections.section_panel_wrapper')
@section('section_panel_card')
    <form-tabbed-component :form="{{json_encode($data)}}"></form-tabbed-component>
@endsection
