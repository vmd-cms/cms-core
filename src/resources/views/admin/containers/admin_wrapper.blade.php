@extends('vmd_cms::admin.app')

@section('admin-wrapper')
    <v-app>

        <app-bar :data="{{$dashboardData->appBarData}}"></app-bar>

        <navigation-drawer :nav_bar="{{$dashboardData->navBar}}"></navigation-drawer>

        @if($dashboardData->isShowAdminNavs)
            <navigation-drawer-admin :nav_bar="{{$dashboardData->navBarAdmin}}"></navigation-drawer-admin>
        @endif
        <v-content>
            @yield('section_panel')
        </v-content>
    </v-app>
@endsection

