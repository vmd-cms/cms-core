<?php

namespace VmdCms\CoreCms\Services;

final class Routes
{
    /**
     * @var static
     */
    private static $instance;

    /**
     * @var array
     */
    private $languageRoutes;

    /**
     * @var array
     */
    private $apiRoutes;

    /**
     * @var array
     */
    private $customRoutes;

    private function __construct(){
        $this->languageRoutes = [];
        $this->customRoutes = [];
        $this->apiRoutes = [];
    }

    /**
     * @return static
     */
    public static function getInstance() : self {
        if (!self::$instance) self::$instance = new static();
        return self::$instance;
    }

    /**
     * @param string $key
     * @param callable $func
     */
    public function appendLangRouteGroup(string $key, callable $func)
    {
        $this->languageRoutes[$key] = $func;
    }

    /**
     * @param string $key
     * @param callable $func
     */
    public function appendApiRouteGroup(string $key, callable $func)
    {
        $this->apiRoutes[$key] = $func;
    }

    /**
     * @param string $key
     * @param callable $func
     */
    public function appendCustomRouteGroup(string $key, callable $func)
    {
        $this->customRoutes[$key] = $func;
    }

    public function getLangRouteGroups()
    {
        return $this->languageRoutes;
    }

    public function getApiRouteGroups()
    {
        return $this->apiRoutes;
    }

    public function getCustomRouteGroups()
    {
        return $this->customRoutes;
    }
}
