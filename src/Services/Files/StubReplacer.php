<?php

namespace VmdCms\CoreCms\Services\Files;

use Illuminate\Support\Str;
use VmdCms\CoreCms\Exceptions\Services\StubReplacerException;

class StubReplacer
{
    const NAME_MODULE = '{name_module}';
    const SUB_NAMESPACE = '{sub_namespace}';
    const VENDOR_NAMESPACE = '{vendor_namespace}';
    const NAME_PLURAL_CAMEL = '{name_plural_camel}';
    const NAME_SINGLE_CAMEL = '{name_single_camel}';
    const NAME_PLURAL_KEBAB = '{name_plural_kebab}';
    const NAME_SINGLE_KEBAB = '{name_single_kebab}';
    const NAME_PLURAL_SNAKE = '{name_plural_snake}';
    const NAME_SINGLE_SNAKE = '{name_single_snake}';

    /**
     * @var array
     */
    private $replaceAssoc = [];

    /**
     * StubReplacer constructor.
     * @param string|null $moduleName
     */
    public function __construct(?string $moduleName = null)
    {
        if($moduleName) $this->setNameModule($moduleName);
    }

    /**
     * @param string $nameModule
     */
    public function setNameModule(string $nameModule): void
    {
        $this->replaceAssoc[self::NAME_MODULE] = Str::ucfirst(Str::camel($nameModule));
    }

    /**
     * @param string $subNameSpace
     */
    public function setSubNameSpace(string $subNameSpace): void
    {
        $this->replaceAssoc[self::SUB_NAMESPACE] = $subNameSpace;
    }

    /**
     * @param string $nameClass
     */
    public function setName(string $nameClass): void
    {
        $this->replaceAssoc[self::NAME_PLURAL_CAMEL] = Str::plural(Str::ucfirst(Str::camel($nameClass)));
        $this->replaceAssoc[self::NAME_SINGLE_CAMEL] = Str::ucfirst(Str::camel($nameClass));
        $this->replaceAssoc[self::NAME_PLURAL_KEBAB] = Str::plural(Str::kebab($nameClass));
        $this->replaceAssoc[self::NAME_SINGLE_KEBAB] = Str::kebab($nameClass);
        $this->replaceAssoc[self::NAME_PLURAL_SNAKE] = Str::plural(Str::snake($nameClass));
        $this->replaceAssoc[self::NAME_SINGLE_SNAKE] = Str::snake($nameClass);
    }

    /**
     * @param string $namespace
     */
    public function setVendorNamespace(string $namespace): void
    {
        $this->replaceAssoc[self::VENDOR_NAMESPACE] = $namespace;
    }

    /**
     * @param string $key
     * @return mixed
     * @throws StubReplacerException
     */
    public function getName(string $key) :string
    {
        if($this->replaceAssoc[$key] ?? null) return $this->replaceAssoc[$key];
        throw new StubReplacerException('Item not exist');
    }

    public function getReplaceAssoc()
    {
        return $this->replaceAssoc;
    }
}
