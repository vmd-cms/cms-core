<?php

namespace VmdCms\CoreCms\Services\Files;

use DirectoryIterator;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use VmdCms\CoreCms\Exceptions\Services\StubBuilderException;
use VmdCms\CoreCms\Exceptions\Services\StubReplacerException;

class StubBuilder
{
    /**
     * @var StubReplacer
     */
    private $stubReplacer;

    /**
     * @var FileGenerator
     */
    private $fileGenerator;

    /**
     * @var string
     */
    private $stubsFolder;
    private $stubsModulePublishFolder;
    private $stubsModuleCreateFolder;
    private $packageModuleFolder;
    private $sourceModuleFolder;
    private $publicModuleFolder;

    /**
     * @var bool
     */
    private $publishModels;
    private $publishControllers;
    private $publishSection;
    private $publishSeed;
    private $publishServices;
    private $publishDTO;
    private $publishEntity;

    /**
     * StubBuilder constructor
     */
    public function __construct()
    {
        $this->stubsFolder = __DIR__ . '/../../resources/stubs';
        $this->stubsModulePublishFolder = $this->stubsFolder . '/modules/publish';
        $this->stubsModuleCreateFolder = $this->stubsFolder . '/modules/create';
        $this->publishModels = true;
        $this->publishControllers = true;
        $this->publishSection = true;
        $this->publishSeed = true;
        $this->publishServices = false;
        $this->publishDTO = false;
        $this->publishEntity = false;
    }

    /**
     * @param StubReplacer $stubReplacer
     */
    public function setStubReplacer(StubReplacer $stubReplacer): void
    {
        $this->stubReplacer = $stubReplacer;
    }

    /**
     * @param FileGenerator $fileGenerator
     */
    public function setFileGenerator(FileGenerator $fileGenerator): void
    {
        $this->fileGenerator = $fileGenerator;
    }

    /**
     * @param string $packageModuleFolder
     */
    public function setPackageModuleFolder(string $packageModuleFolder): void
    {
        $this->packageModuleFolder = $packageModuleFolder;
    }

    /**
     * @param string $sourceModuleFolder
     */
    public function setSourceModuleFolder(string $sourceModuleFolder): void
    {
        $this->sourceModuleFolder = $sourceModuleFolder;
    }

    /**
     * @param string $publicModuleFolder
     */
    public function setPublicModuleFolder(string $publicModuleFolder): void
    {
        $this->publicModuleFolder = $publicModuleFolder;
    }


    /**
     * @param bool $publishModels
     */
    public function setPublishModels(bool $publishModels): void
    {
        $this->publishModels = $publishModels;
    }

    /**
     * @param bool $publishControllers
     */
    public function setPublishControllers(bool $publishControllers): void
    {
        $this->publishControllers = $publishControllers;
    }

    /**
     * @param bool $publishSection
     */
    public function setPublishSection(bool $publishSection): void
    {
        $this->publishSection = $publishSection;
    }

    /**
     * @param bool $publishSeed
     */
    public function setPublishSeed(bool $publishSeed): void
    {
        $this->publishSeed = $publishSeed;
    }

    /**
     * @param bool $publishServices
     */
    public function setPublishServices(bool $publishServices): void
    {
        $this->publishServices = $publishServices;
    }

    /**
     * @param bool $publishDTO
     */
    public function setPublishDTO(bool $publishDTO): void
    {
        $this->publishDTO = $publishDTO;
    }

    /**
     * @param bool $publishEntity
     */
    public function setPublishEntity(bool $publishEntity): void
    {
        $this->publishEntity = $publishEntity;
    }

    /**
     * @throws FileNotFoundException
     * @throws StubReplacerException
     */
    public function createModule()
    {
        $this->fileGenerator->createFile(
            $this->packageModuleFolder . "/composer.json",
            $this->stubsModuleCreateFolder . "/composer.stub");
        $modelFileName = $this->stubReplacer->getName(StubReplacer::NAME_SINGLE_CAMEL) . ".php";
        $controllerFileName = $this->stubReplacer->getName(StubReplacer::NAME_SINGLE_CAMEL) . "Controller.php";

        $migrationName = date("Y_m_d") ."_000001_create_" . $this->stubReplacer->getName(StubReplacer::NAME_PLURAL_SNAKE) . "_table.php";

        $this->createModuleFile(
            "/database/migrations/" . $migrationName,
            "/database/migrations/create_table.stub");

        $this->createModuleFile(
            "/ModuleServiceProvider.php",
            "/module.service.provider.stub");

        $this->createModuleFile(
            "/config/vmd_cms.php",
            "/config/vmd_cms.stub");

        $this->createModuleFile(
            "/database/seeds/ModuleSeeder.php",
            "/database/seeds/module.seeder.stub");

        $this->createModuleFile(
            "/Initializers/ModuleInitializer.php",
            "/initializers/module.initializer.stub");

        $this->createModuleFile(
            "/Models/". $modelFileName,
            "/models/model.stub");

        $this->createModuleFile(
            "/Controllers/" . $controllerFileName,
            "/controllers/controller.stub");

        $this->createModuleFile(
            "/Sections/"  .$modelFileName,
            "/sections/section.stub");

        $this->createModuleFile(
            "/routes/web.php",
            "/routes/web.stub");

        $this->createModuleFile(
            "/resources/views/template.blade.php",
            "/resources/views/template.stub");
    }

    /**
     * @throws FileNotFoundException
     * @throws StubBuilderException
     */
    public function publishModule()
    {
        if(!$this->stubReplacer instanceof StubReplacer) throw new StubBuilderException('Empty StubReplacer object');

        if(!$this->fileGenerator instanceof FileGenerator) throw new StubBuilderException('Empty FileGenerator object');

        if($this->publishModels) $this->publishModuleFolder('/Models', '/Models','/models/model.stub');
        if($this->publishControllers) $this->publishModuleFolder('/Controllers','/Controllers','/controllers/controller.stub');
        if($this->publishSection) $this->publishModuleFolder('/Sections','/Sections','/sections/section.stub');
        if($this->publishSeed) $this->publishModuleFolder('/database/seeds','/database/seeds','/database/seeds/module.seeder.stub');
        if($this->publishServices) $this->publishModuleFolder('/Services','/Services','/services/service.stub');
        if($this->publishDTO) $this->publishModuleFolder('/DTO','/DTO','/dto/dto.stub');
        if($this->publishEntity) $this->publishModuleFolder('/Entity','/Entity','/entity/entity.stub');
    }

    /**
     * @param string $sourceFolder
     * @param string $publicFolder
     * @param string $stubFilePath
     * @throws FileNotFoundException
     * @throws StubBuilderException
     */
    private function publishModuleFolder(string $sourceFolder,string $publicFolder,string $stubFilePath)
    {
        $this->publishFolder(
            $this->sourceModuleFolder. $sourceFolder,
            $this->publicModuleFolder. $publicFolder,
            $this->stubsModulePublishFolder . $stubFilePath
        );
    }

    /**
     * @param $sourceFolder
     * @param $publicFolder
     * @param $stubFilePath
     * @param $subNameSpace
     * @throws StubBuilderException
     * @throws FileNotFoundException
     */
    private function publishFolder($sourceFolder,$publicFolder,$stubFilePath,$subNameSpace = '')
    {
        $files = scandir($sourceFolder);

        if(!is_array($files) || !count($files)) throw new StubBuilderException();

        foreach ($files as $file)
        {
            if($file == '.' || $file == '..') continue;

            if(is_dir($sourceFolder . DIRECTORY_SEPARATOR . $file))
            {
                $this->publishFolder(
                    $sourceFolder . DIRECTORY_SEPARATOR . $file,
                    $publicFolder . DIRECTORY_SEPARATOR . $file,
                    $stubFilePath,
                    $subNameSpace . '\\' . $file
                );
            }
            elseif (strpos($file,'.php') !== false)
            {
                $this->stubReplacer->setSubNameSpace($subNameSpace);
                $this->stubReplacer->setName(str_replace('.php','',$file));
                $this->fileGenerator->createFile(
                    $publicFolder . '/' . $file,
                    $stubFilePath,
                    $this->stubReplacer->getReplaceAssoc()
                );
            }
        }
    }

    /**
     * @param string $folder
     * @return array|false
     * @throws StubBuilderException
     */
    private function getPhpFiles(string $folder)
    {
        $files = scandir($folder);
        if(!is_array($files) || !count($files)) throw new StubBuilderException();
        return array_filter($files,function ($file){
            return strpos($file,'.php') !== false;
        });

    }

    /**
     * @param string $filePath
     * @param string $stubFilePath
     * @throws FileNotFoundException
     */
    private function createModuleFile(string $filePath,string $stubFilePath)
    {
        $this->fileGenerator->createFile(
            $this->publicModuleFolder . $filePath,
            $this->stubsModuleCreateFolder . $stubFilePath
        );
    }
}
