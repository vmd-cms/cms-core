<?php

namespace VmdCms\CoreCms\Services\Files;

class StorageAdapter
{
    /**
     * @var StorageAdapter
     */
    private static $instance;

    /**
     * @var StoreFile
     */
    private $storeFile;

    private function __construct()
    {
        $this->storeFile = new StoreFile();
    }

    /**
     * @return StorageAdapter
     */
    public static function getInstance()
    {
        if (!self::$instance) self::$instance = new static();
        return self::$instance;
    }

    /**
     * @param string|null $path
     * @param bool $default
     * @return string|null
     */
    public function getStoragePath(string $path = null, bool $default = true) : ?string
    {
        try {
            return $this->storeFile->getStoragePath($path);
        }
        catch (\Exception $exception)
        {
            return $default ? $this->storeFile->getDefaultImage() : null;
        }
    }

    /**
     * @param string|null $path
     * @param bool $useDefault
     * @return string|null
     */
    public function getAbsolutePath(string $path = null, bool $useDefault = true) : ?string
    {
        $path = $this->getStoragePath($path, $useDefault);
        return $path ? request()->root() . $path : null;
    }

    /**
     * @param string $filePath
     * @return string
     */
    public function getSvgContent(string $filePath) : string
    {
        $filePath = $this->getStoragePath($filePath);
        $filePath = (substr($filePath,0,1) === '/' ? substr($filePath,1) : $filePath);
        return @file_get_contents($filePath);
    }
}
