<?php

namespace VmdCms\CoreCms\Services\Files;

use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\CroppedDimensionDTOCollectionInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\CroppedDimensionDTOInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\DTO\Dashboard\Components\CroppedDimensionDTO;
use VmdCms\CoreCms\DTO\Dashboard\Components\CroppedDimensionDTOCollection;
use VmdCms\CoreCms\Exceptions\Files\FileNotExistException;
use VmdCms\CoreCms\Exceptions\Files\StorageStoreException;
use VmdCms\CoreCms\Services\Logger;

final class StoreFile
{
    private $storageDisc;
    private $defaultImage;
    protected $logger;

    /**
     * @var boolean
     */
    protected $isCopyToWEBP;

    /**
     * @var CroppedDimensionDTOCollectionInterface
     */
    protected $croppedDimensions;

    public function __construct()
    {
        $this->storageDisc = 'public';
        $this->defaultImage = '/files/vendor/vmd_cms/default.jpg';
        $this->logger = new Logger();
        $this->isCopyToWEBP = false;
        $this->croppedDimensions = null;
    }

    /**
     * @param bool|null $isCopyToWEBP
     * @return StoreFile
     */
    public function setIsCopyToWEBP(bool $isCopyToWEBP = null): StoreFile
    {
        $this->isCopyToWEBP = boolval($isCopyToWEBP);
        return $this;
    }

    /**
     * @param CroppedDimensionDTOCollectionInterface|null $croppedDimensions
     * @return StoreFile
     */
    public function setCroppedDimensions(CroppedDimensionDTOCollectionInterface $croppedDimensions = null): StoreFile
    {
        $this->croppedDimensions = $croppedDimensions;
        return $this;
    }

    public function getDefaultImage()
    {
        return $this->defaultImage;
    }

    /**
     * @param string|null $path
     * @return string
     */
    public function getImagePath(string $path = null ) : string
    {
        try {
            return $this->getStoragePath($path);
        }
        catch (\Exception $exception)
        {
            return $this->defaultImage;
        }
    }

    /**
     * @param string|null $filePath
     * @return string
     */
    public function getPublicStoragePath(string $filePath = null) : string
    {
        try {
            $path = $this->getStoragePath($filePath,false);
        }catch (\Exception $exception){
            $path = $this->defaultImage;
        }
        return $path;
    }

    /**
     * @param string|null $filePath
     * @param bool $absolute
     * @return string
     * @throws FileNotExistException
     */
    public function getStoragePath(string $filePath = null, bool $absolute = true) : string
    {
        if(!$filePath) throw new FileNotExistException();
        $exist = strpos('storage/', $filePath);
        $filePath = $exist !== false && $exist === 0 ? $filePath : 'storage/' . $filePath;
        if(!file_exists($filePath)) throw new FileNotExistException();
        return ($absolute ? '/' : '') . $filePath;
    }

    /**
     * @param $filePath
     * @param UploadedFile $file
     * @return string|null
     */
    public function storeFile($filePath,UploadedFile $file) : ?string
    {
        if(substr($filePath,-1) ==='/') $filePath = substr($filePath,0,-1);
        try {

            if($file->getError()){
                throw new StorageStoreException('File has error ' . $file->getErrorMessage());
            }

            $extension = $file->getClientOriginalExtension();

            if(empty($extension) && !empty($file->getMimeType())){
                $extension = last(explode('/',$file->getMimeType()));
            }

            $fileName = md5($file->getClientOriginalName() . time()) . '.' . $extension;

            $storedPath = Storage::disk($this->storageDisc)->putFileAs($filePath,$file,$fileName);

            if(!$storedPath) throw new StorageStoreException();
        }
        catch (\Exception $e){
            $this->logger->error('Exception in ' . __METHOD__);
            $this->logger->error($e->getTrace());
            $this->logger->error($e->getMessage());
            $storedPath = null;
        }
        return $storedPath;
    }

    /**
     * @param $filePath
     * @return array
     */
    public function storeImageDimensions($filePath): array
    {
        if(substr($filePath,-1) ==='/') $filePath = substr($filePath,0,-1);

        $storedPaths = [
            'original' => $filePath
        ];

        try {

            if($this->isCopyToWEBP){
                $storedPaths['original_webp'] = $this->encodeFileToWEBP($storedPaths['original']);
            }

            if($this->croppedDimensions instanceof CroppedDimensionDTOCollection){
                $dimensions = $this->croppedDimensions->getItems();
                foreach ($dimensions as $croppedDimensionDTO){
                    if(!$croppedDimensionDTO instanceof CroppedDimensionDTOInterface){
                        continue;
                    }
                    $storedPaths[$croppedDimensionDTO->getKey()] = $this->storeCroppedDimension($storedPaths['original'],$croppedDimensionDTO);
                    if($this->isCopyToWEBP && !empty($storedPaths[$croppedDimensionDTO->getKey()])){
                        $storedPaths[$croppedDimensionDTO->getKey() . '_webp'] = $this->encodeFileToWEBP($storedPaths[$croppedDimensionDTO->getKey()]);
                    }
                }
            }
        }
        catch (\Exception $e){
            $this->logger->error('Exception in ' . __METHOD__);
            $this->logger->error($e->getTrace());
            $this->logger->error($e->getMessage());
            $storedPaths['original'] = null;
        }
        return $storedPaths;
    }

    /**
     * @param string $filePath
     * @return string|null
     */
    protected function encodeFileToWEBP(string $filePath): ?string
    {
        try {
            if(!class_exists(\Intervention\Image\Facades\Image::class)){
                throw new StorageStoreException();
            }

            $img = \Intervention\Image\Facades\Image::make($this->getStoragePath($filePath,false));
            $fileNameWEBP = pathinfo($filePath,PATHINFO_FILENAME) . '.webp';
            $img = $img->encode('webp')->save(public_path($fileNameWEBP));
            $savedImageUri = $img->dirname.'/'.$img->basename;
            $storedCopyPath = Storage::disk($this->storageDisc)->putFileAs('/' . pathinfo($filePath,PATHINFO_DIRNAME), new File($savedImageUri), $fileNameWEBP);
            $img->destroy();
            unlink($savedImageUri);
        }catch (\Exception $exception){
            $storedCopyPath = null;
        }
        return $storedCopyPath;
    }

    /**
     * @param string $filePath
     * @param CroppedDimensionDTO $dimension
     * @return string|null
     */
    protected function storeCroppedDimension(string $filePath,CroppedDimensionDTO $dimension): ?string
    {
        try {
            if(!class_exists(\Intervention\Image\Facades\Image::class)){
                throw new StorageStoreException();
            }

            $img = \Intervention\Image\Facades\Image::make($this->getStoragePath($filePath,false));
            $img = $img->resize($dimension->getWidth(), $dimension->getHeight(), function ($const) {
                $const->aspectRatio();
            });
            $img->orientate();

            $fileName = $dimension->getKey() . '_' . pathinfo($filePath,PATHINFO_BASENAME);
            $img->save(public_path($fileName),$dimension->getQuality());
            $imageUri = $img->dirname.'/'.$img->basename;
            $storedPath = Storage::disk($this->storageDisc)->putFileAs('/' . pathinfo($filePath,PATHINFO_DIRNAME), new File($imageUri), $fileName);
            $img->destroy();
            unlink($fileName);
        }catch (\Exception $exception){
            $storedPath = null;
        }
        return $storedPath;
    }

    public function getFolderPathByCmsModel(CmsModelInterface $model)
    {
        return $model::table() . '/' . $model->id;
    }

    /**
     * @param $filePath
     * @return bool
     */
    public function deleteFileByPath($filePath)
    {
        return Storage::disk($this->storageDisc)->delete($filePath);
    }

    /**
     * @param $folderPath
     * @return array
     */
    public function getFilesPathesInFolder($folderPath)
    {
        return Storage::disk($this->storageDisc)->files($folderPath);
    }
}
