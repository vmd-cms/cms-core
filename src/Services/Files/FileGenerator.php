<?php

namespace VmdCms\CoreCms\Services\Files;

use Illuminate\Filesystem\Filesystem;
use VmdCms\CoreCms\Contracts\Services\Files\FileGeneratorInterface;

class FileGenerator implements FileGeneratorInterface
{
    /**
     * @var Filesystem
     */
    protected $fileSystem;

    /**
     * @var array;
     */
    private $baseReplace;

    public function __construct()
    {
        $this->fileSystem = new Filesystem();

    }

    /**
     * @param string $filePath
     * @param string $stubPath
     * @param array $replaceAssocArr
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function createFile(string $filePath,string $stubPath,array $replaceAssocArr = []) : void
    {
        $this->makeDirectory($filePath);
        $this->fileSystem->put($filePath,$this->getPrepareStub($stubPath,$replaceAssocArr));
    }

    /**
     * @param string $filePath
     */
    public function makeDirectory(string $filePath) : void
    {
        $this->fileSystem->makeDirectory(dirname($filePath),0777,true, true);
    }

    /**
     * @param string $stubPath
     * @param array $replaceAssocArr
     * @return string|string[]
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function getPrepareStub(string $stubPath,array $replaceAssocArr = []) : string
    {
        if(is_array($this->baseReplace)) $replaceAssocArr = array_merge($this->baseReplace,$replaceAssocArr);

        $stub = $this->fileSystem->get($stubPath);

        return str_replace(
            array_keys($replaceAssocArr),
            array_values($replaceAssocArr),
            $stub
        );
    }

    /**
     * @param array $baseReplace
     */
    public function setBaseReplace(array $baseReplace): void
    {
        $this->baseReplace = $baseReplace;
    }
}
