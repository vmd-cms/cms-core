<?php

namespace VmdCms\CoreCms\Services\Initializers;

use VmdCms\CoreCms\Contracts\Modules\ModuleInitializerInterface;
use VmdCms\CoreCms\Exceptions\Initializers\ModuleInitializerNotFoundException;

class InitializerService
{
    /**
     * @param string $moduleName
     * @return ModuleInitializerInterface
     * @throws ModuleInitializerNotFoundException
     */
    public static function getInitializerByModuleSlug(string $moduleName) : ModuleInitializerInterface
    {
        $namespace = 'VmdCms\\Modules\\' . ucfirst($moduleName) ;
        return self::getInitializerByModuleNamespace($namespace);
    }

    /**
     * @param string $nameSpace
     * @return ModuleInitializerInterface
     * @throws ModuleInitializerNotFoundException
     */
    public static function getInitializerByModuleNamespace(string $nameSpace) : ModuleInitializerInterface
    {
        $className = $nameSpace . '\Initializers\ModuleInitializer';
        if(!class_exists($className)){
            throw new ModuleInitializerNotFoundException("$className not found");
        }
        $initializer = new $className();
        if(!$initializer instanceof ModuleInitializerInterface){
            throw new ModuleInitializerNotFoundException("$className is not instance of ModuleInitializerInterface");
        }
        return $initializer;
    }

}
