<?php

namespace VmdCms\CoreCms\Services;

use ReflectionClass;

abstract class Enums
{
    final public static function getEnums()
    {
        $constantsArr = (new ReflectionClass(static::class))->getConstants();
        $resultArr = [];
        if(!is_countable($constantsArr) || !count($constantsArr)) return $resultArr;
        foreach ($constantsArr as $item) $resultArr[$item] = $item;
        return $resultArr;
    }
}
