<?php

namespace VmdCms\CoreCms\Services\Related;

use Illuminate\Database\Eloquent\Collection;
use VmdCms\CoreCms\Contracts\Models\CmsCrossModelInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Exceptions\Models\ClassNotFoundException;
use VmdCms\CoreCms\Exceptions\Models\NotCmsCrossModelException;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;

final class CrossRelatedService
{
    private $relatedModel;
    private $crossModel;

    /**
     * @var CmsModelInterface
     */
    private $model;

    public function __construct(CmsModelInterface $model)
    {
        $this->model = $model;
    }

    /**
     * @param $relatedModel
     * @return $this
     */
    public function setRelatedModel($relatedModel): self
    {
        $this->relatedModel = $relatedModel;
        return $this;
    }

    /**
     * @param $crossModel
     * @return $this
     */
    public function setCrossModel($crossModel): self
    {
        $this->crossModel = $crossModel;
        return $this;
    }


    /**
     * @param $relationName
     * @param $value
     * @param $crossModelClass
     * @throws NotCmsModelException
     */
    public function saveCrossRelation($relationName, $value, $crossModelClass)
    {
        if(!$this->model instanceof CmsModelInterface) throw new NotCmsModelException();

        $this->model::saved(function () use($relationName,$value, $crossModelClass){
            if(!class_exists($crossModelClass)) throw new ClassNotFoundException();
            $crossModel = new $crossModelClass;
            if(!$crossModel instanceof CmsCrossModelInterface) throw new NotCmsCrossModelException();

            $targetClass = $crossModel->getTargetModelClass();

            $baseForeignField = $crossModel->getBaseModelForeignField();
            $targetForeignField = $crossModel->getTargetModelForeignField();

            $basePrimaryField = $crossModel->getBaseModelPrimaryField();
            $targetPrimaryField = $crossModel->getTargetModelPrimaryField();

            $relation = $this->model->$relationName;
            if($relation instanceof Collection)
            {
                $items = $relation->keyBy($targetPrimaryField);
                foreach ($value as $id)
                {
                    if(isset($items[(int) $id])) {
                        $items->forget((int) $id);
                    }
                    else {
                        $crossModel = new $crossModelClass;
                        $crossModel->$baseForeignField = $this->model->$basePrimaryField;
                        $crossModel->$targetForeignField = (int) $id;
                        $crossModel->save();
                    }
                }
                foreach ($items as $item) {
                    $crossModelClass::where($baseForeignField,$this->model->$basePrimaryField)
                        ->where($targetForeignField,$item->$targetPrimaryField)->delete();
                }
                return;
            }
            elseif($relation instanceof $targetClass)
            {

                if($relation->$targetPrimaryField == $value) return;
                $crossModel = $crossModel::where($baseForeignField,$this->model->$basePrimaryField)
                    ->where($targetForeignField,$relation->$targetPrimaryField)->first();

                if(is_null($value)) {
                    $crossModel->delete();
                    return;
                }
                $crossModel->$targetForeignField = $value;
                $crossModel->save();
                return;
            }
            elseif(!empty($value)){
                $crossModel->$baseForeignField = $this->model->$basePrimaryField;
                $crossModel->$targetForeignField = $value;
                $crossModel->save();
            }
            return;
        });
    }

    public function hasThroughCrossModelWrapper($relationType)
    {
        $crossModelClass = $this->crossModel;
        $crossModel = new $crossModelClass;
        if(!$crossModel instanceof CmsCrossModelInterface) throw new NotCmsCrossModelException();
        return $this->model->$relationType(
            $this->relatedModel,
            $crossModelClass,
            $crossModel->getBaseModelForeignField(),
            $crossModel->getBaseModelPrimaryField(),
            $crossModel->getTargetModelPrimaryField(),
            $crossModel->getTargetModelForeignField()
        );
    }
}
