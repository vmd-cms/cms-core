<?php

namespace VmdCms\CoreCms\Services;

use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Support\Facades\DB;
use VmdCms\CoreCms\Contracts\Models\CloneableInterface;
use VmdCms\CoreCms\Contracts\Models\CmsRelatedModelInterface;
use VmdCms\CoreCms\Exceptions\Services\ReplicateException;
use VmdCms\CoreCms\Models\CmsModel;

class Replicate
{
    /**
     * @param CmsModel $model
     * @return CmsModel
     * @throws ReplicateException
     */
    public function replicateModel(CmsModel $model)
    {
        try {
            DB::beginTransaction();

            if ($model instanceof CloneableInterface) {

                $cloneModel = $model->replicate();

                $model->cloneUniqueFields($cloneModel);

                $cloneModel->push();

                foreach ($model->cloneableRelations() as $relationName) {
                    $modelRelation = $model->$relationName;
                    if (empty($modelRelation)) continue;


                    if ($model->$relationName() instanceof HasManyThrough) {

                        if (is_countable($modelRelation)) {
                            $relatedIds = [];
                            foreach ($modelRelation as $modelRelItem) {
                                $relatedIds[] = $modelRelItem->id;
                            }
                            $cloneModel->$relationName = $relatedIds;
                        } else {
                            $cloneModel->$relationName = $modelRelation->id;
                        }
                        continue;
                    }

                    if (is_countable($modelRelation)) {
                        foreach ($modelRelation as $relItem) {
                            $replicatedRelation = $relItem->replicate();
                            if ($relItem instanceof CmsRelatedModelInterface) {
                                $foreignField = $relItem::getForeignField();

                                $replicatedRelation->$foreignField = $cloneModel->id;
                                $replicatedRelation->save();
                            } else {
                                $cloneModel->$relationName()->save($replicatedRelation);
                            }
                        }
                    } else {

                        $replicatedRelation = $modelRelation->replicate();
                        $model->cloneUniqueFields($replicatedRelation);
                        $cloneModel->$relationName()->save($replicatedRelation);
                    }
                }
                $cloneModel->save();
            }
            DB::commit();
            return $cloneModel;
        }
        catch (\Exception $exception){
            DB::rollBack();
            throw new ReplicateException();
        }
    }
}
