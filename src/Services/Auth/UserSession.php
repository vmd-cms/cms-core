<?php

namespace VmdCms\CoreCms\Services\Auth;

use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;

final class UserSession
{
    const USER_SESSION_ID = 'user_session_id';
    const USER_SESSION_ID_LIFETIME = '43200';

    protected static $userSession;

    public static function getUserSessionId()
    {
        if(!empty(self::$userSession)){
            return self::$userSession;
        }

        try {
            self::$userSession = Cookie::get(static::USER_SESSION_ID);
            if(!empty(self::$userSession) && mb_strlen(self::$userSession) > 64){
                $decrypted = Crypt::decrypt(self::$userSession, false);
                self::$userSession = !empty($decrypted) ? last(explode('|',$decrypted)) : null;
            }
        }catch (\Exception $exception){
            self::$userSession = null;
        }

        if(empty(self::$userSession))
        {
            self::$userSession = md5(date('Y-m-d H:i:s') . '-' . Str::uuid()->toString());
            Cookie::queue(static::USER_SESSION_ID,self::$userSession,static::USER_SESSION_ID_LIFETIME);
        }

        return self::$userSession;
    }
}
