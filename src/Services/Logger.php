<?php

namespace VmdCms\CoreCms\Services;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class Logger
{
    const STATUS_SUCCESS = 'Success';
    const STATUS_ERROR = 'Error';

    protected $logChannel;
    protected $logUuid;
    protected $logHeader;

    public function __construct($channel = 'vmd_cms')
    {
        $this->logUuid = Str::uuid();
        $this->logHeader = true;
        $this->logChannel = $channel;
    }

    /**
     * @param bool $logHeader
     * @return Logger
     */
    public function setLogHeader(bool $logHeader): Logger
    {
        $this->logHeader = $logHeader;
        return $this;
    }

    protected function loggerSeparator(string $type = 'info')
    {
        Log::channel($this->logChannel)->$type("------------------------------------------------------------------");
    }

    public function info($message, string $status = null){
        $this->logger($message,'info');
    }

    public function error($message, string $status = null){
        $this->logger($message,'error');
    }

    public function exception(\Exception $exception){
        $this->logger("Exception in file "  . $exception->getFile()
            . " on line:" . $exception->getLine()
            . " Message:" . $exception->getMessage(),'error');
        $this->logger($exception->getTraceAsString(),'error');
    }

    protected function logger($message, string $type = 'info', string $status = null){
        if(is_array($message)){
            $message = json_encode($message);
        }
        if($this->logHeader){
            $this->logHeader = false;
            $this->loggerSeparator($type);
        }
        Log::channel($this->logChannel)->$type($this->logUuid . (!empty($status) ? " " . $status . " " : " ") . $message);
    }

}
