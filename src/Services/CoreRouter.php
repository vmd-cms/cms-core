<?php

namespace VmdCms\CoreCms\Services;

use Illuminate\Support\Facades\Request;

final class CoreRouter
{
    const ADMIN_SECTION_PREFIX = 'adminSection.';
    const ADMIN_AUTH_PREFIX = 'adminAuth.';

    const ADMIN_SERVICE_PREFIX = 'adminService.';

    const DASHBOARD = 'dashboard';
    const DISPLAY = 'display';
    const DISPLAY_POST = 'display_post';
    const CREATE_GET = 'create_get';
    const CREATE_POST = 'create_post';
    const VIEW = 'view';
    const EDIT_GET = 'edit_get';
    const EDIT_PUT = 'edit_put';
    const DELETE = 'delete';
    const RESTORE = 'restore';
    const DELETE_SOFT = 'delete_soft';
    const XMLHTTP_CK_FILE = 'xmlhttp_ck_file';
    const XMLHTTP_CK_FILES_BROWSE = 'xmlhttp_ck_files_browse';
    const UPDATE_TREE_ORDER = 'update_tree_order';
    const UPDATE_TABLE_ORDER = 'update_table_order';
    const UPDATE_TABLE_ITEM = 'update_table_item';
    const DATATABLE_ASYNC = 'datatable_async';
    const SERVICE_DATA = 'service_data';
    const SERVICE_DATA_GET = 'service_data_get';

    const LOGIN = 'login';
    const LOGIN_POST = 'login_post';
    const LOGOUT = 'logout';

    const CHECK_UNIQUE = 'check_unique';
    const SET_LOCALE = 'set_locale';

    const ROUTE_DASHBOARD = self::ADMIN_SECTION_PREFIX . self::DASHBOARD;
    const ROUTE_DISPLAY = self::ADMIN_SECTION_PREFIX . self::DISPLAY;
    const ROUTE_DISPLAY_POST = self::ADMIN_SECTION_PREFIX . self::DISPLAY_POST;
    const ROUTE_CREATE_GET = self::ADMIN_SECTION_PREFIX .  self::CREATE_GET;
    const ROUTE_CREATE_POST = self::ADMIN_SECTION_PREFIX . self::CREATE_POST;
    const ROUTE_VIEW = self::ADMIN_SECTION_PREFIX . self::VIEW;
    const ROUTE_EDIT_GET = self::ADMIN_SECTION_PREFIX . self::EDIT_GET;
    const ROUTE_EDIT_PUT = self::ADMIN_SECTION_PREFIX . self::EDIT_PUT;
    const ROUTE_DELETE = self::ADMIN_SECTION_PREFIX . self::DELETE;
    const ROUTE_RESTORE = self::ADMIN_SECTION_PREFIX . self::RESTORE;
    const ROUTE_DELETE_SOFT = self::ADMIN_SECTION_PREFIX . self::DELETE_SOFT;
    const ROUTE_XMLHTTP_CK_FILE = self::ADMIN_SECTION_PREFIX . self::XMLHTTP_CK_FILE;
    const ROUTE_XMLHTTP_CK_FILES_BROWSE = self::ADMIN_SECTION_PREFIX . self::XMLHTTP_CK_FILES_BROWSE;
    const ROUTE_UPDATE_TREE_ORDER = self::ADMIN_SECTION_PREFIX . self::UPDATE_TREE_ORDER;
    const ROUTE_UPDATE_TABLE_ORDER = self::ADMIN_SECTION_PREFIX . self::UPDATE_TABLE_ORDER;
    const ROUTE_UPDATE_TABLE_ITEM = self::ADMIN_SECTION_PREFIX . self::UPDATE_TABLE_ITEM;
    const ROUTE_DATATABLE_ASYNC = self::ADMIN_SECTION_PREFIX . self::DATATABLE_ASYNC;
    const ROUTE_SERVICE_DATA = self::ADMIN_SECTION_PREFIX . self::SERVICE_DATA;
    const ROUTE_SERVICE_DATA_GET = self::ADMIN_SECTION_PREFIX . self::SERVICE_DATA_GET;

    const ROUTE_CHECK_UNIQUE = self::ADMIN_SERVICE_PREFIX . self::CHECK_UNIQUE;
    const ROUTE_SET_LOCALE = self::ADMIN_SERVICE_PREFIX . self::SET_LOCALE;

    const ROUTE_MODERATOR_LOGIN = self::ADMIN_AUTH_PREFIX . self::LOGIN;
    const ROUTE_MODERATOR_LOGIN_POST = self::ADMIN_AUTH_PREFIX . self::LOGIN_POST;
    const ROUTE_MODERATOR_LOGOUT = self::ADMIN_AUTH_PREFIX . self::LOGOUT;

    /**
     * @var static
     */
    private static $instance;

    /**
     * @var array
     */
    private $routes;

    /**
     * @var string
     */
    private $urlPrefix;

    /**
     * Router constructor.
     */
    private function __construct()
    {
        $this->urlPrefix = Config::getInstance()->getUrlPrefix();
    }

    /**
     * @return bool
     */
    public function isAdminPanel(): bool
    {
        return Request::segment(1) === self::getUrlPrefix();
    }

    /**
     * @return static
     */
    public static function getInstance() : self {
        return self::$instance ?? new self();
    }

    /**
     * @return array
     */
    public function getRoutes(): array
    {
        return $this->routes;
    }

    /**
     * @return string|null
     */
    public function getUrlPrefix(): ?string
    {
        return $this->urlPrefix;
    }

    /**
     * @param string $sectionSlug
     * @param bool $absolute
     * @return string
     */
    public static function getDisplayRoute(string $sectionSlug,$absolute = false) : string
    {
        return route(self::ROUTE_DISPLAY,['sectionSlug' => $sectionSlug],$absolute);
    }

    /**
     * @param string $sectionSlug
     * @param bool $absolute
     * @return string
     */
    public static function getCreateRoute(string $sectionSlug,$absolute = false) : string
    {
        return route(self::ROUTE_CREATE_GET,['sectionSlug' => $sectionSlug],$absolute);
    }

    /**
     * @param string $sectionSlug
     * @param int $id
     * @param false $absolute
     * @return string
     */
    public static function getEditRoute(string $sectionSlug,int $id, $absolute = false) : string
    {
        return route(self::ROUTE_EDIT_GET,['sectionSlug' => $sectionSlug, 'id' => $id],$absolute);
    }

    /**
     * @param string $sectionSlug
     * @param int $id
     * @param bool $absolute
     * @return string
     */
    public static function getDeleteRoute(string $sectionSlug,int $id,$absolute = false) : string
    {
        return route(self::ROUTE_DELETE,['sectionSlug' => $sectionSlug,'id' => $id],$absolute);
    }

    /**
     * @param string $sectionSlug
     * @param int $id
     * @param bool $absolute
     * @return string
     */
    public static function getRestoreRoute(string $sectionSlug,int $id,$absolute = false) : string
    {
        return route(self::ROUTE_RESTORE,['sectionSlug' => $sectionSlug,'id' => $id],$absolute);
    }

    /**
     * @param string $sectionSlug
     * @param bool $absolute
     * @return string
     */
    public static function getTreeOrderUpdateRoute(string $sectionSlug, $absolute = false) : string
    {
        return route(self::ROUTE_UPDATE_TREE_ORDER,['sectionSlug' => $sectionSlug],$absolute);
    }

    /**
     * @param string $sectionSlug
     * @param bool $absolute
     * @return string
     */
    public static function getTableOrderUpdateRoute(string $sectionSlug, $absolute = false) : string
    {
        return route(self::ROUTE_UPDATE_TABLE_ORDER,['sectionSlug' => $sectionSlug],$absolute);
    }

    /**
     * @param string $sectionSlug
     * @param bool $absolute
     * @return string
     */
    public static function getTableUpdateItemRoute(string $sectionSlug, $absolute = false) : string
    {
        return route(self::ROUTE_UPDATE_TABLE_ITEM,['sectionSlug' => $sectionSlug],$absolute);
    }

    /**
     * @param string|null $sectionSlug
     * @param int|null $id
     * @param false $absolute
     * @return string|null
     */
    public static function getCkEditorUploadFileRoute(string $sectionSlug = null, int $id = null, $absolute = false) : ?string
    {
        return !empty($sectionSlug) && !empty($id) ? route(self::ROUTE_XMLHTTP_CK_FILE,[
            'sectionSlug' => $sectionSlug,
            'id' => $id,
        ],$absolute) : null;
    }

    /**
     * @param string|null $sectionSlug
     * @param int|null $id
     * @param false $absolute
     * @return string|null
     */
    public static function getCkEditorBrowseFilesRoute(string $sectionSlug = null, int $id = null, $absolute = false) : ?string
    {
        return !empty($sectionSlug) && !empty($id) ? route(self::ROUTE_XMLHTTP_CK_FILES_BROWSE,[
            'sectionSlug' => $sectionSlug,
            'id' => $id,
        ],$absolute) : null;
    }
}
