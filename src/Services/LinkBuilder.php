<?php

namespace VmdCms\CoreCms\Services;

use VmdCms\CoreCms\CoreModules\Content\Entity\Languages;

final class LinkBuilder
{
    public static function route(string $routeName,array $params = []){
        return Languages::getInstance()->getLocalePrefix() . route($routeName,$params,false);
    }

    public static function path(string $path = null){

        if(!empty($path) && substr($path,0,1) == '/'){
            $path = substr($path,1);
        }

        $path = Languages::getInstance()->getLocalePrefix() ? Languages::getInstance()->getLocalePrefix() . ( $path ? '/' . $path : '') : '/' . $path;
        return $path === request()->getRequestUri() ? '#' : $path;
    }

    public static function main(){
        if(request()->path() === '/' || request()->path() === Languages::getInstance()->getCurrentLocaleActive()){
            return '#';
        }
        return Languages::getInstance()->getLocalePrefix() ? Languages::getInstance()->getLocalePrefix() : '/';
    }
}
