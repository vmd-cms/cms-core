<?php

namespace VmdCms\CoreCms\Services;

use VmdCms\CoreCms\CoreModules\Content\Sections\Blocks\Block;
use VmdCms\CoreCms\Exceptions\Sections\SectionNotFoundException;

abstract class BlockSectionFactoryAbstract
{
    protected abstract static function getAssocKeySectionClasses(): array;

    /**
     * @param $key
     * @return Block
     * @throws SectionNotFoundException
     */
    final public function getBlockModel($key)
    {
        $arr = static::getAssocKeySectionClasses();
        if(!is_array($arr) || !isset($arr[$key])) throw new SectionNotFoundException();

        $sectionClass = $arr[$key];
        return new $sectionClass;
    }
}
