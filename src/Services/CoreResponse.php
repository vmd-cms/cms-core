<?php

namespace VmdCms\CoreCms\Services;

class CoreResponse
{
    /**
     * @param array $body
     * @param int $status
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public static function success(array $body = [],int $status = 200)
    {
        return response(array_merge(['success' => true], $body),$status);
    }

    /**
     * @param array $body
     * @param int $status
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public static function error(array $body = [],int $status = 400)
    {
        return response(array_merge(['error' => true], $body),$status);
    }
}
