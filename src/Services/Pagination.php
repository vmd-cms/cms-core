<?php

namespace VmdCms\CoreCms\Services;

use VmdCms\CoreCms\Contracts\Services\PaginationInterface;

class Pagination implements PaginationInterface
{
    const LIMIT = 16;

    /**
     * @var int
     */
    protected $total;
    protected $page;
    protected $limit;
    protected $totalPages;
    protected $prevPage;
    protected $nextPage;

    protected $startInterval;
    protected $currentInterval;
    protected $endInterval;

    /**
     * @var int
     */
    protected $resourceId;

    public function __construct(int $total, int $page = 1, int $limit = self::LIMIT)
    {
        $this->total = is_int($total) && $total >= 0 ? $total : 0;
        $this->page = is_int($page) && $page >= 1 ? $page : 1;
        $this->limit = is_int($limit) && $limit >= 1 ? $limit : 1;
        $this->setPaginationData();
        $this->setIntervals();
    }

    protected function setPaginationData()
    {
        $this->totalPages = ceil($this->total / $this->limit);
        $this->prevPage = (($this->page - 1) >= 1) ? ($this->page - 1) : 0;
        $this->nextPage = (($this->page + 1) <= $this->totalPages) ? ($this->page + 1) : 0;
    }

    protected function setIntervals(){

        $this->setCurrentInterval();
        $this->setStartInterval();
        $this->setEndInterval();
    }

    protected function setCurrentInterval(){
        $this->currentInterval = [];
        $startIndex = $this->page > 2 ? $this->page - 2 : 1;

        $endIndex = ($this->page + 2) <= $this->totalPages ? ($this->page + 2) : $this->totalPages;
        for ($i=$startIndex; $i <= $endIndex; $i++){
            $this->currentInterval[] = $i;
        }
    }

    protected function setStartInterval(){
        $this->startInterval = [];
        if($this->page > 3){
            $this->startInterval = [1];
        }
    }

    protected function setEndInterval(){
        $this->endInterval = [];
        if(($this->totalPages - $this->page) > 2){
            $this->endInterval = [$this->totalPages];
        }
    }

    /**
     * @return mixed
     */
    public function getCurrentInterval()
    {
        return $this->currentInterval;
    }

    /**
     * @return mixed
     */
    public function getStartInterval()
    {
        return $this->startInterval;
    }

    /**
     * @return mixed
     */
    public function getEndInterval()
    {
        return $this->endInterval;
    }

    /**
     * @return int
     */
    public function getNextPage(): int
    {
        return $this->nextPage;
    }

    /**
     * @return int
     */
    public function getPrevPage(): int
    {
        return $this->prevPage;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getTotalPages(): int
    {
        return $this->totalPages;
    }

    /**
     * @return int|null
     */
    public function getResourceId(): ?int
    {
        return $this->resourceId;
    }

    /**
     * @param int $resourceId
     * @return PaginationInterface
     */
    public function setResourceId(int $resourceId): PaginationInterface
    {
        $this->resourceId = $resourceId;
        return $this;
    }

}
