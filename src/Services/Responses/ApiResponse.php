<?php

namespace VmdCms\CoreCms\Services\Responses;

use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Response;
use VmdCms\CoreCms\Services\Logger;

class ApiResponse
{
    protected static $logger;

    public static function setLogger(Logger $logger){
        self::$logger = $logger;
    }

    /**
     * @param $data
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public static function error($data,$code = 409)
    {
        $response = [
            'error' => true
        ];
        if(!is_array($data)){
            $data = [
              'error_message' => $data
            ];
        }
        if(self::isEnableLogs()){
            self::$logger->error('Exception execute');
            self::$logger->error(json_encode($data));
            self::$logger->error('Send error Response');
        }
        return Response::json(array_merge($response,$data),$code);
    }

    /**
     * @param $data
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public static function validatorFails($data,$code = 409)
    {
        $response = [
            'error' => true,
            'validator_errors' => $data
        ];
        if(self::isEnableLogs()){
            self::$logger->error('Validator fails');
            self::$logger->error(json_encode($data));
            self::$logger->error('Send Validator Fails Response');
        }
        return Response::json($response,$code);
    }

    /**
     * @param $data
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public static function success($data,$code = 200)
    {
        $response = [
            'error' => false,
        ];
        if(!is_array($data)){
            $data = [
                'message' => $data
            ];
        }

        if(self::isEnableLogs()){
            self::$logger->info('Send success Response');
        }

        return Response::json(array_merge($response,$data),$code);
    }

    protected static function isEnableLogs(){
        return self::$logger instanceof Logger;
    }
}
