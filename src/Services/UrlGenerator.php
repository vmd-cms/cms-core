<?php

namespace VmdCms\CoreCms\Services;

use Illuminate\Support\Str;

final class UrlGenerator
{
    /**
     * @var static
     */
    private static $instance;

    /**
     * @var string
     */
    private $urlPrefix;

    /**
     * UrlGenerator constructor.
     */
    private function __construct()
    {
        $this->urlPrefix = Config::getInstance()->getUrlPrefix();
    }

    /**
     * @return static
     */
    public static function getInstance() : self {
        return self::$instance ?? new self();
    }

    /**
     * @param null|string $url
     * @return string
     */
    public function getDashboardUrl($url)
    {
        return !empty($url) || $url === '' ? $this->getConcatUrl($this->urlPrefix, $url) : null;
    }

    /**
     * @param string $prefix
     * @param null|string $suffix
     * @return string
     */
    public function getConcatUrl($prefix, $suffix)
    {
        $prefix = (substr($prefix,0,1) != '/') ? '/' . $prefix : $prefix;
        $prefix = (substr($prefix,-1) != '/') ?  $prefix : substr($prefix,1);
        if(!$suffix) return $prefix;
        $suffix = (substr($suffix,0,1) != '/') ? $suffix : substr($suffix,1);
        return $suffix ? $prefix . '/' . $suffix : $prefix;
    }

    public function slug($title, $separator = '-', $language = 'en')
    {
        $title = $language ? Str::ascii($title, $language) : $title;

        // Convert all dashes/underscores into separator
        $flip = $separator === '-' ? '_' : '-';

        $title = preg_replace('!['.preg_quote($flip).']+!u', $separator, $title);

        // Replace @ with the word 'at'
        $title = str_replace('@', $separator.'at'.$separator, $title);

        // Remove all characters that are not the separator, letters, numbers, or whitespace.
        $title = preg_replace('![^'.preg_quote($separator).'\pL\pN\s\/\-]+!u', '', Str::lower($title));

        // Replace all separator characters and whitespace by a single separator
        $title = preg_replace('![\s]+!u', $separator, $title);

        return trim($title);
    }
}
