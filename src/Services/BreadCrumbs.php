<?php

namespace VmdCms\CoreCms\Services;

use VmdCms\CoreCms\Collections\BreadCrumbsCollection;
use VmdCms\CoreCms\Contracts\Collections\BreadCrumbsCollectionInterface;
use VmdCms\CoreCms\Contracts\DTO\BreadCrumbDtoInterface;
use VmdCms\CoreCms\CoreModules\Content\Entity\Languages;
use VmdCms\CoreCms\CoreModules\Content\Entity\Translates;
use VmdCms\CoreCms\CoreModules\Translates\Models\Translate;
use VmdCms\CoreCms\DTO\Dashboard\BreadCrumbDto;

class BreadCrumbs
{
    private static $instance;

    /**
     * @var BreadCrumbsCollectionInterface
     */
    private $collection;

    private function __construct(){
        $this->collection = new BreadCrumbsCollection();
    }

    /**
     * @return static
     */
    public static function getInstance() : self {
        if (!self::$instance) self::$instance = new static();
        return self::$instance;
    }

    /**
     * @param BreadCrumbDtoInterface $item
     */
    public function appendItem(BreadCrumbDtoInterface $item): void
    {
        $this->collection->appendItem($item);
    }

    /**
     * @param BreadCrumbDtoInterface $item
     */
    public function prependItem(BreadCrumbDtoInterface $item): void
    {
        $this->collection->prependItem($item);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getItems()
    {
        $this->prependItem(new BreadCrumbDto('/', Translates::getInstance()->getValue('main_page')));
        return $this->collection->getItems();
    }

}
