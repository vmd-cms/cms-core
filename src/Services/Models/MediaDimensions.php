<?php

namespace VmdCms\CoreCms\Services\Models;

use Illuminate\Support\Collection;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Contracts\Models\HasMediaDimensionsInterface;
use VmdCms\CoreCms\DTO\Models\MediaDimensionsDTO;
use VmdCms\CoreCms\Exceptions\Models\NotMediaDimensionModelException;
use VmdCms\CoreCms\Models\Media\MediaDimension;
use VmdCms\CoreCms\Services\Files\StoreFile;

class MediaDimensions
{
    /**
     * @param CmsModelInterface $model
     * @param string $field
     * @return Collection
     * @throws NotMediaDimensionModelException
     */
    public function getMediaDimensions(CmsModelInterface $model, string $field = 'media'): ?Collection
    {
        if(!$model instanceof HasMediaDimensionsInterface){
            throw new NotMediaDimensionModelException();
        }

        try {
            $mediaDimensions = MediaDimension::where('resource',$model->getMediaDimensionsResourceKey())
                ->where('resource_id',$model->id)
                ->where('field',$field)
                ->get();
        }catch (\Exception $exception){
            $mediaDimensions = null;
        }
        return $mediaDimensions;
    }

    /**
     * @param CmsModelInterface $model
     * @param string $field
     * @param bool $useDefault
     * @return MediaDimensionsDTO|null
     */
    public function getMediaDimensionsDTO(CmsModelInterface $model, string $field = 'media', bool $useDefault = false): ?MediaDimensionsDTO
    {
        try {
            $mediaDimensions = $this->getMediaDimensions($model,$field);

        }catch (\Exception $exception){
            $mediaDimensions = null;
        }

        if($useDefault && (!is_countable($mediaDimensions) || !count($mediaDimensions))){
            $original = new MediaDimension();
            $original->key = 'original';
            $original->path = (new StoreFile())->getDefaultImage();
            $mediaDimensions = collect([$original]);
        }

        return  is_countable($mediaDimensions) && count($mediaDimensions) ? new MediaDimensionsDTO($mediaDimensions) : null;
    }
}
