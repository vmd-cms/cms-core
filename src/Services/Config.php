<?php

namespace VmdCms\CoreCms\Services;

use Illuminate\Support\Facades\Config as ConfigFacade;
use VmdCms\CoreCms\Exceptions\Services\ConfigNotFoundException;

final class Config
{
    /**
     * @var static
     */
    private static $instance;

    /**
     * @var string
     */
    private $configFile;

    /**
     * @var string
     */
    private $urlPrefix;

    /**
     * @var bool
     */
    private $isMultiLanguagesUrl;

    /**
     * @var bool
     */
    private $isUsedQueueJobs;

    /**
     * @var bool
     */
    private $isUsedGroupTranslates;

    /**
     * @var array
     */
    private $configs;

    /**
     * @var array
     */
    private $navigationDrawer;

    /**
     * @var array
     */
    private $baseLang;

    /**
     * @var array
     */
    private $dashboardBaseLang;

    /**
     * @var string
     */
    private $frontendAppUrl;

    /**
     * Config constructor.
     */
    private function __construct()
    {
        $this->configFile = 'vmd_cms';
        $this->configs = ConfigFacade::get($this->configFile);
        if(empty($this->configs)) return;
        $this->navigationDrawer = $this->configs['navigation_drawer'] ?? [];
        $this->urlPrefix = $this->configs['url_prefix'] ?? 'dashboard';
        $this->isMultiLanguagesUrl = (bool) ($this->configs['url_multi_languages'] ?? false);
        $this->isUsedQueueJobs = (bool) ($this->configs['use_queue_jobs'] ?? false);
        $this->isUsedGroupTranslates = (bool) ($this->configs['use_group_translates'] ?? false);
        $this->baseLang = $this->configs['base_lang'] ?? [];
        $this->dashboardBaseLang = $this->configs['dashboard_base_lang'] ?? [];
        $this->frontendAppUrl = $this->configs['frontend_app_url'] ?? null;
    }

    /**
     * @return static
     */
    public static function getInstance() : self {
        if (!self::$instance) self::$instance = new static();
        return self::$instance;
    }

    /**
     * @return string|null
     */
    public function getUrlPrefix() : ?string {
        return $this->urlPrefix;
    }

    /**
     * @return string|null
     */
    public function getFrontendAppUrl() : ?string {
        return $this->frontendAppUrl;
    }

    /**
     * @return array
     */
    public function getBaseLang() : array {
        return is_array($this->baseLang) ? $this->baseLang : [];
    }

    /**
     * @return array
     */
    public function getDashboardBaseLang() : array {
        return is_array($this->dashboardBaseLang) ? $this->dashboardBaseLang : [];
    }

    /**
     * @return bool
     */
    public function isMultiLanguagesUrl() : bool {
        return $this->isMultiLanguagesUrl;
    }

    /**
     * @return bool
     */
    public function isUsedQueueJobs() : bool {
        return $this->isUsedQueueJobs;
    }

    /**
     * @return bool
     */
    public function isUsedGroupTranslates() : bool {
        return $this->isUsedGroupTranslates;
    }

    /**
     * @return array
     */
    public function getNavigationDrawer() : array {
        return $this->navigationDrawer;
    }

    /**
     * @param string $keyString
     * @return array|string
     * @throws ConfigNotFoundException
     */
    public function getByKey(string $keyString) {
        $keys = explode('.', $keyString);
        $result = $this->configs;
        foreach ($keys as $key)
        {
            if(!$result = $result[$key] ?? null) throw new ConfigNotFoundException();
        }
        return $result;
    }
}
