<?php

namespace VmdCms\CoreCms\Traits\Database;

use VmdCms\CoreCms\CoreModules\Sections\Models\CoreSection;
use VmdCms\CoreCms\Exceptions\Modules\ModuleSeedException;

trait SectionSeeder
{
    /**
     * @var CoreSection[]
     */
    protected $sections;

    /**
     * @return array
     */
    protected abstract function getSectionsAssoc() : array;

    /**
     * @return void
     * @throws ModuleSeedException
     */
    protected function seedSections()
    {
        try {
            $sections = $this->getSectionsAssoc();
            if(!is_array($sections)) throw new ModuleSeedException();

            foreach ($sections as $class=>$title)
            {
                $section = CoreSection::where("core_section_class",$class)->first();

                if(!$section instanceof CoreSection)
                {
                    $section = CoreSection::create([
                        "module_id" => $this->module->id ?? null,
                        "core_section_class" => $class,
                        "title" => $title,
                        "active" => 1
                    ]);
                }

                if(!$section instanceof CoreSection) throw new ModuleSeedException();

                $this->sections[$class] = $section;
            }
        }
        catch (\Exception $e)
        {
            throw new ModuleSeedException($e);
        }
    }
}
