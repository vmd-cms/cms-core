<?php

namespace VmdCms\CoreCms\Traits\Database;

use VmdCms\CoreCms\CoreModules\Navigations\Models\CoreNav;
use VmdCms\CoreCms\CoreModules\Navigations\Models\CoreNavInfo;
use VmdCms\CoreCms\Exceptions\Modules\ModuleSeedException;

trait NavSeeder
{
    /**
     * @return array
     */
    protected abstract function getNavigationAssoc() : array;

    /**
     * @return string
     */
    protected abstract function getNavClass() : string;

    /**
     * @throws ModuleSeedException
     */
    protected function seedNavigation()
    {
        try {
            $navigations = $this->getNavigationAssoc();
            if(!is_array($navigations)) throw new ModuleSeedException();

            foreach ($navigations as $item)
            {
                $this->recursiveSeedNavigation($item);
            }
        }
        catch (\Exception $e){
            throw new ModuleSeedException($e);
        }
    }

    /**
     * @param $item
     * @param CoreNav|null $parent
     * @throws ModuleSeedException
     */
    private function recursiveSeedNavigation($item, CoreNav $parent = null)
    {
        $navClass = $this->getValidNavigationClass();

        $parentId = $parent->id ?? $item['parent'] ?? null;
        $navigation = $navClass::where("parent_id",$parentId)->where("slug",$item['slug'])->first();
        if(!$navigation)
        {
            $navigation = $navClass::create([
                "parent_id" => $parent->id ?? $item['parent'] ?? null,
                "slug" => $item['slug'],
                "title" => $item['title'],
                "icon" => $item['icon'] ?? null,
                "section_id" => $this->sections[$item['section_class'] ?? null]->id ?? null,
                "is_group_title" => $item['is_group_title'] ?? false,
                "order" => $item['order'] ?? 1,
                "active" => true,
            ]);

            CoreNavInfo::updateOrCreate([
                'lang_key' => 'ru',
                'core_navs_id' => $navigation->id,
            ], [
                'title' => $item['title']
            ]);
        }
        if(!$navigation instanceof $navClass) throw new ModuleSeedException();

        if(isset($item['children']) && is_array($item['children']) && count($item['children']))
        {
            foreach ($item['children'] as $child){
                $this->recursiveSeedNavigation($child, $navigation);
            }
        }
        return;
    }

    /**
     * @return string
     * @throws ModuleSeedException
     */
    private function getValidNavigationClass()
    {
        $navClass = $this->getNavClass();
        if(!class_exists($navClass)) throw new ModuleSeedException();
        $navModel = new $navClass;
        if(!$navModel instanceof CoreNav) throw new ModuleSeedException();
        return $navClass;
    }

}
