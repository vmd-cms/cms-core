<?php

namespace VmdCms\CoreCms\Traits\Models;

trait Orderable
{
    public function getOrderField() : string {
        return $this->orderField ?? 'order';
    }

    protected function getOrderType() : string {
        return $this->orderType ?? 'ASC';
    }

    public function scopeOrder($query)
    {
        return $query->orderBy($this->getOrderField(),$this->getOrderType());
    }
}
