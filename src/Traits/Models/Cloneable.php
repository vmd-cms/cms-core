<?php

namespace VmdCms\CoreCms\Traits\Models;

use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;

trait Cloneable
{
    public function cloneableRelations(): array
    {
        $relations = [];
        if($this instanceof HasInfoInterface){
            $relations[] = 'info';
        }
        return $relations;
    }

    public function cloneUniqueFields(CmsModelInterface $model): CmsModelInterface
    {
        if(isset($model->url)){
            $model->url = $this->getCopyUniqueValue($model,'url');
        }

        if(isset($model->photo)){
            $model->photo = null;
        }

        return $model;
    }

    protected function getCopyUniqueValue(CmsModelInterface $model,$field = 'url'): string
    {
        $copyNumber = 1;
        $modelValue = $model->$field;
        if($modelValue){
            $pos = strpos($modelValue,'-copy-');
            if($pos !== false){
                $modelValue = substr($modelValue,0,$pos + 6);
            }
            else{
                $modelValue .= '-copy-';
            }
        }

        $value = $modelValue ? $modelValue : 'copy-';
        $exist = null;
        do{
            $checkedValue = $value . $copyNumber;
            $exist = get_class($model)::where($field,$checkedValue)->first();
            $copyNumber++;
        }while($exist);
        return $checkedValue;
    }
}
