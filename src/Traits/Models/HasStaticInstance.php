<?php

namespace VmdCms\CoreCms\Traits\Models;

use VmdCms\CoreCms\Contracts\Models\StaticInstanceInterface;

trait HasStaticInstance
{
    protected static $instance;

    /**
     * @return StaticInstanceInterface
     */
    public static function getInstance() : StaticInstanceInterface
    {
        if(!static::$instance) static::$instance = new static();
        return static::$instance;
    }
}
