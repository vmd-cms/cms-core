<?php

namespace VmdCms\CoreCms\Traits\Models;

use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;
use VmdCms\CoreCms\Contracts\Models\TreeableInterface;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;
use VmdCms\CoreCms\Services\Config;

trait HasUrlPath
{
    use HasUrl;

    protected $urlsArray = [];

    /**
     * @return string|null
     * @throws NotCmsModelException
     */
    public function getUrlPathAttribute(): ?string
    {
        if (!$this instanceof CmsModelInterface) throw new NotCmsModelException();

        $this->isMultiLangUrl = Config::getInstance()->isMultiLanguagesUrl();

        $urlItems = $this->getRecursiveUrlPath($this);

        if(count($urlItems)) return  '/' . $this->getUrlPrefix() . implode('/', $urlItems);

        return null;
    }

    protected function getRecursiveUrlPath(CmsModelInterface $model)
    {
        $items = [];

        if($model instanceof TreeableInterface && $model->parent instanceof CmsModelInterface)
        {
            $items = $this->getRecursiveUrlPath($model->parent);
        }

        $url = $this->getUrlValue($model);

        if($url) $items[] = $url;

        return $items;
    }

    /**
     * @return string
     */
    public function getUrlPrefix(): string
    {
        try {
            $prefix = Config::getInstance()->getByKey($this->getConfigModuleKey());
            if (!empty($prefix)) $prefix .= '/';
            return $prefix;
        } catch (\Exception $exception) {
            return '';
        }
    }

    /**
     * @return string
     */
    protected function getConfigModuleKey()
    {
        return '';
    }

    /**
     * @param string $url
     * @return CmsModelInterface|null
     */
    public function getModelByUrl(string $url): ?CmsModelInterface
    {
        $modelClass = static::class;

        $this->isMultiLangUrl = Config::getInstance()->isMultiLanguagesUrl();

        $query = $modelClass::query();

        if($this->isMultiLangUrl)
        {
            $query->whereHas('info',function ($q) use ($url){
                $q->where('url',$url);
            });
        }
        else{
            $query->where('url',$url);
        }

        if($this instanceof ActivableInterface){
            $query->active();
        }

        return $query->first();
    }

    /**
     * @param string $fullUrl
     * @return CmsModelInterface|null
     */
    public function getModelByFullUrl(string $fullUrl): ?CmsModelInterface
    {
        $modelClass = static::class;

        if(!$this instanceof TreeableInterface) return null;

        $this->isMultiLangUrl = Config::getInstance()->isMultiLanguagesUrl();

        $this->urlsArray = explode('/',$fullUrl);

        $query = $modelClass::query();

        if($this->isMultiLangUrl)
        {
            $query->whereHas('info',function ($q){
                $q->where('url',array_pop($this->urlsArray));
            });
        }
        else{
            $query->where('url',array_pop($this->urlsArray));
        }

        $this->recursiveQuery($query);

        return $query->childrenTree(true)->first();
    }

    private function recursiveQuery($query)
    {
        if (count($this->urlsArray))
        {
            $query->whereHas('parent', function ($q) {
                if($this->isMultiLangUrl)
                {
                    $q->whereHas('info', function ($subQ) {
                        $subQ->where('url', array_pop($this->urlsArray));
                    });
                }
                else{
                    $q->where('url',array_pop($this->urlsArray));
                }
                $this->recursiveQuery($q);
            })->with('parentTree');
        }
    }
}
