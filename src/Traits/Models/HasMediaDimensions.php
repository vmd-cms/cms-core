<?php

namespace VmdCms\CoreCms\Traits\Models;

use Illuminate\Support\Facades\DB;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\CroppedDimensionDTOCollectionInterface;
use VmdCms\CoreCms\Contracts\Models\HasMediaDimensionsInterface;
use VmdCms\CoreCms\Models\Media\MediaDimension;
use VmdCms\CoreCms\Services\Files\StoreFile;

trait HasMediaDimensions
{
    /**
     * @var boolean
     */
    protected $isCopyToWEBP;

    /**
     * @var CroppedDimensionDTOCollectionInterface
     */
    protected $croppedDimensions;

    /**
     * @param bool|null $flag
     * @return HasMediaDimensionsInterface
     */
    public function setCopyToWEBP(bool $flag = null): HasMediaDimensionsInterface
    {
        $this->isCopyToWEBP = boolval($flag);
        return $this;
    }

    /**
     * @param CroppedDimensionDTOCollectionInterface $croppedDimensions
     * @return HasMediaDimensionsInterface
     */
    public function setCroppedDimensions(CroppedDimensionDTOCollectionInterface $croppedDimensions): HasMediaDimensionsInterface
    {
        $this->croppedDimensions = $croppedDimensions;
        return $this;
    }

    /**
     * @param string $storedPath
     * @param string $field
     */
    public function storeMediaDimensions(string $storedPath, string $field = 'media')
    {
        try {
            DB::beginTransaction();

            $paths = (new StoreFile())
                ->setIsCopyToWEBP($this->isCopyToWEBP)
                ->setCroppedDimensions($this->croppedDimensions)
                ->storeImageDimensions($storedPath);

            foreach ($paths as $key => $path){
                $mediaDimension = MediaDimension::where('resource',$this->getMediaDimensionsResourceKey())
                    ->where('resource_id',$this->id)
                    ->where('field',$field)
                    ->where('key',$key)
                    ->first();

                if(!$mediaDimension instanceof MediaDimension){
                    $mediaDimension = new MediaDimension();
                    $mediaDimension->resource = $this->getMediaDimensionsResourceKey();
                    $mediaDimension->resource_id = $this->id;
                    $mediaDimension->field = $field;
                    $mediaDimension->key = $key;
                }

                $mediaDimension->path = $path;
                $mediaDimension->save();
            }

            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
        }
    }

    /**
     * @param string $field
     */
    public function deleteMediaDimensions(string $field = 'media')
    {
        try {
            $mediaDimensions = MediaDimension::where('resource',$this->getMediaDimensionsResourceKey())
                ->where('resource_id',$this->id)
                ->where('field',$field)
                ->get();
            if(is_countable($mediaDimensions)){
                $storeFile = new StoreFile();
                foreach ($mediaDimensions as $mediaDimension){
                    $storeFile->deleteFileByPath($mediaDimension->path);
                    $mediaDimension->delete();
                }
            }
        }catch (\Exception $exception){}
    }

    public function getMediaDimensionsResourceKey(): string
    {
        return last(explode('\\',get_class($this)));
    }
}
