<?php

namespace VmdCms\CoreCms\Traits\Models;

use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;
use VmdCms\CoreCms\Services\Config;

trait HasUrl
{

    protected $isMultiLangUrl = false;

    /**
     * @return string|null
     * @throws NotCmsModelException
     */
    public function getUrlValueAttribute(): ?string
    {
        if (!$this instanceof CmsModelInterface) throw new NotCmsModelException();

        $this->isMultiLangUrl = Config::getInstance()->isMultiLanguagesUrl();

        return $this->getUrlValue($this);
    }


    protected function getUrlValue(CmsModelInterface $model)
    {
        if ($this->isMultiLangUrl && $model instanceof HasInfoInterface) {
            return $model->info->url ?? null;
        } else {
            return $model->url ?? null;
        }
    }

}
