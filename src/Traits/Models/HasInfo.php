<?php

namespace VmdCms\CoreCms\Traits\Models;

trait HasInfo
{
    protected function getInfoClass() : string
    {
        return static::class . 'Info';
    }

    public function info()
    {
        return $this->hasOne($this->getInfoClass(), $this->getInfoClass()::getForeignField(), static::getPrimaryField() );
    }

}
