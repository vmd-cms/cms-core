<?php

namespace VmdCms\CoreCms\Traits\Models;

use Illuminate\Database\Eloquent\Builder;
use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;
use VmdCms\CoreCms\CoreModules\Content\Models\Blocks\Block;
use VmdCms\CoreCms\Models\CmsInfoModel;
use VmdCms\CoreCms\Models\CmsModel;

trait JsonData
{
    /**
     * @var array
     */
    public $dataArray = [];

    /**
     * @var array
     */
    public $infoDataArray = [];

    /**
     * @var bool
     */
    protected $isSetSaveInfoCallback = false;

    protected function getModelDataFieldName()
    {
        return 'data';
    }

    protected function getModelDataField()
    {
        $field = $this->getModelDataFieldName();
        return $this->$field ?? null;
    }

    protected function setModelDataField($data)
    {
        $field = $this->getModelDataFieldName();
        $this->$field = $data;
    }

    protected function getDataArray(){
        if(!$data = $this->getModelDataField()) return [];
        if(!$this->dataArray) $this->dataArray = json_decode($data,true);
        return $this->dataArray;
    }

    public function getDataField($field) {
        return $this->getDataArray()[$field] ?? null;
    }

    protected function setDataField($field, $value = null){

        if(!is_array($this->dataArray) || !count($this->dataArray)){
            $this->getDataArray();
        }
        $this->dataArray[$field] = $value;
        $this->setModelDataField(json_encode($this->dataArray,JSON_UNESCAPED_UNICODE));
    }

    public function getDataArrayAttr()
    {
        return $this->dataArray;
    }

    public function getJsonDataFieldsArr() : array
    {
        return [];
    }

    public function getInfoModelDataFieldName()
    {
        return 'data';
    }

    protected function getInfoModelDataField()
    {
        $field = $this->getInfoModelDataFieldName();
        return $this->info->$field ?? null;
    }

    protected function setInfoModelDataField($data)
    {
        if(!$this->isSetSaveInfoCallback){
            $this::saved(static::getSavedCallback());
            $this->isSetSaveInfoCallback = true;
        }
    }

    public function getInfoDataArray(){
        if(!$data = $this->getInfoModelDataField()) return [];
        if(!$this->infoDataArray) $this->infoDataArray = json_decode($data,true);
        return $this->infoDataArray;
    }

    public function getInfoDataField($field) {
        return $this->getInfoDataArray()[$field] ?? null;
    }

    protected function setInfoDataField($field, $value = null){

        if(!is_array($this->infoDataArray) || !count($this->infoDataArray)){
            $this->getInfoDataArray();
        }
        $this->infoDataArray[$field] = $value;
        $this->setInfoModelDataField(json_encode($this->infoDataArray,JSON_UNESCAPED_UNICODE));
    }

    protected static function getSavedCallback(): callable
    {
        return function (CmsModel $model){

            if(!$model instanceof HasInfoInterface || !isset($model->id)){
                return;
            }

            $infoData = $model->infoDataArray ?? null;

            if (!is_countable($infoData) || !count($infoData)){
                return;
            }

            $infoClass = $model->getInfoClass();
            $foreignField = $infoClass::getForeignField();

            $infoModel = $infoClass::where($foreignField,$model->id)->first();


            if(!$infoModel instanceof $infoClass)
            {
                $infoModel = new $infoClass();
                $infoModel->$foreignField = $model->id;
            }
            $infoDataField = $model->getInfoModelDataFieldName();
            $infoModel->$infoDataField = json_encode($infoData,JSON_UNESCAPED_UNICODE);
            $infoModel->save();
        };
    }

    public function getInfoDataArrayAttr()
    {
        return $this->infoDataArray;
    }

    public function getJsonInfoDataFieldsArr() : array
    {
        return [];
    }

    /**
     * Get an attribute from the model.
     *
     * @param  string  $key
     * @return mixed
     */
    public function getAttribute($key)
    {
        if (in_array($key, $this->getJsonDataFieldsArr())) {
            return $this->getDataField($key);
        }
        if (in_array($key, $this->getJsonInfoDataFieldsArr())) {
            return $this->getInfoDataField($key);
        }
        return parent::getAttribute($key);
    }

    /**
     * Set a given attribute on the model.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    public function setAttribute($key, $value)
    {
        if (in_array($key, $this->getJsonDataFieldsArr()))
        {
            return $this->setDataField($key,$value) ?? false;
        }
        if (in_array($key, $this->getJsonInfoDataFieldsArr()))
        {
            return $this->setInfoDataField($key,$value) ?? false;
        }
        return parent::setAttribute($key,$value);
    }
}
