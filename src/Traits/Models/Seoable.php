<?php

namespace VmdCms\CoreCms\Traits\Models;

use VmdCms\CoreCms\CoreModules\Seo\Models\Seo;

trait Seoable
{
    public function seo()
    {
        return $this->morphOne(Seo::class,'resource','resource');
    }
}
