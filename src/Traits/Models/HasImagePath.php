<?php

namespace VmdCms\CoreCms\Traits\Models;

use VmdCms\CoreCms\Services\Files\StorageAdapter;

trait HasImagePath
{
    /**
     * @return string
     */
    public function getImagePathAttribute()
    {
        return StorageAdapter::getInstance()->getStoragePath($this->getImageValue());
    }

    /**
     * @return string|null
     */
    protected function getImageValue(): ?string
    {
        return $this->photo ?? null;
    }

}
