<?php

namespace VmdCms\CoreCms\Traits\Models;

use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;
use VmdCms\CoreCms\Contracts\Models\OrderableInterface;

trait Treeable
{
    protected static $onlyActive;

    public function getParentField() : string {
        return $this->parentKeyField ?? 'parent_id';
    }

    public abstract static function getPrimaryField() : string;

    public function scopeTree($query,$onlyActive = false)
    {
        static::$onlyActive = $onlyActive;
        $query->whereNull($this->getParentField());
        if($this instanceof OrderableInterface) $query->order();
        if($this instanceof HasInfoInterface) $query->with('info');
        if($this instanceof ActivableInterface && static::$onlyActive) $query->where('active',true);
        return $query->with('childrenTree');
    }

    public function scopeChildrenTree($query,$onlyActive = false)
    {
        static::$onlyActive = $onlyActive;
        if($this instanceof OrderableInterface) $query->order();
        if($this instanceof HasInfoInterface) $query->with('info');
        if($this instanceof ActivableInterface && static::$onlyActive) $query->where('active',true);
        return $query->with('childrenTree');
    }

    public function children()
    {
        $query = $this->hasMany(static::class, $this->getParentField(), static::getPrimaryField());
        if($this instanceof OrderableInterface) $query->order();
        if($this instanceof HasInfoInterface) $query->with('info');
        if($this instanceof ActivableInterface && static::$onlyActive) $query->where('active',true);
        return $query;
    }

    public function childrenTree()
    {
        $query = $this->children();
        return $query->with('childrenTree','parent');
    }

    public function parent()
    {
        $query = $this->belongsTo(static::class,$this->getParentField(),'id');
        if($this instanceof HasInfoInterface) $query->with('info');
        return $query;
    }

    public function parentTree()
    {
        $query = $this->belongsTo(static::class,$this->getParentField(),'id');
        if($this instanceof HasInfoInterface) $query->with('info');
        return $query->with('parentTree');
    }
}
