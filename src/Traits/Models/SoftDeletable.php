<?php

namespace VmdCms\CoreCms\Traits\Models;

use VmdCms\CoreCms\Contracts\Models\SoftDeletableInterface;
use VmdCms\CoreCms\CoreModules\Moderators\Entity\AuthEntity;

trait SoftDeletable
{
    /**
     * @return mixed
     */
    public function softDelete()
    {
        if($this instanceof SoftDeletableInterface)
        {
            $this->deleted = true;
            $this->deleted_by = AuthEntity::getAuthModeratorId();
            $this->deleted_at = date('Y-m-d H:i:s');
            $this->save();
        }
    }

    public function scopeNotDeleted($query)
    {
        return $query->where('deleted',false);
    }

    public function scopeOnlyDeleted($query)
    {
        return $query->where('deleted',true);
    }

}
