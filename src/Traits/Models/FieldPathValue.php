<?php

namespace VmdCms\CoreCms\Traits\Models;

use VmdCms\CoreCms\Models\CmsModel;

trait FieldPathValue
{
    /**
     * @param $field
     * @param CmsModel $model
     * @return CmsModel|mixed|null
     */
    protected function getFieldValue($field, CmsModel $model)
    {
        foreach (explode('.',$field) as $field_item){
            $model = $model->$field_item ?? null;
        }
        return $model;
    }
}
