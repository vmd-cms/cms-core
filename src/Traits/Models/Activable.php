<?php

namespace VmdCms\CoreCms\Traits\Models;

trait Activable
{
    protected function getActiveField() : string {
        return $this->activeField ?? 'active';
    }

    public function scopeActive($query)
    {
        return $query->where($this->getActiveField(), 1);
    }

    public function scopeNotActive($query)
    {
        return $query->where($this->getActiveField(), false);
    }
}
