<?php

namespace VmdCms\CoreCms\Traits\Related;

use VmdCms\CoreCms\Services\Related\CrossRelatedService;

trait HasCrossRelatedService
{
    /**
     * @var CrossRelatedService
     */
    private $crossRelatedService;

    public function getCrossRelatedService(): CrossRelatedService
    {
        if(!$this->crossRelatedService instanceof CrossRelatedService)
        {
            $this->crossRelatedService = new CrossRelatedService($this);
        }
        return $this->crossRelatedService;
    }
}
