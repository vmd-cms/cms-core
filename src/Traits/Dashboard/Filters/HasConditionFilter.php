<?php

namespace VmdCms\CoreCms\Traits\Dashboard\Filters;

use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Contracts\Models\SoftDeletableInterface;
use VmdCms\CoreCms\Dashboard\Display\Filters\FilterComponent;
use VmdCms\CoreCms\DTO\Dashboard\Filters\FilterComponentItemDTO;
use VmdCms\CoreCms\DTO\Dashboard\Filters\FilterComponentItemDTOCollection;

trait HasConditionFilter
{
    /**
     * @param CmsModelInterface $model
     * @return \VmdCms\CoreCms\Contracts\Dashboard\Display\Filters\FilterComponentInterface
     */
    protected function getConditionFilterComponent(CmsModelInterface $model){

        return (new FilterComponent('Состояние'))->setHeadTitle('Состояние')
            ->setFilterSlug('condition')
            ->setRetrieveItemsCallback(function () use($model){
                $items = new FilterComponentItemDTOCollection();
                if($model instanceof ActivableInterface && $model instanceof SoftDeletableInterface)
                {
                    $items->appendItem((new FilterComponentItemDTO('active','Вкл','condition'))
                        ->setCount($model::active()->notDeleted()->count())->setChipColor('#2ED47A'));
                    $items->appendItem((new FilterComponentItemDTO('not_active','Выкл','condition'))
                        ->setCount($model::notActive()->notDeleted()->count())->setChipColor('#F7685B'));
                    $items->appendItem((new FilterComponentItemDTO('deleted','Удаленные','condition'))
                        ->setCount($model::onlyDeleted()->count())->setChipColor('#C0C7CD'));
                }
                elseif ($model instanceof ActivableInterface){
                    $items->appendItem((new FilterComponentItemDTO('active','Вкл','condition'))
                        ->setCount($model::active()->count())->setChipColor('#2ED47A'));
                    $items->appendItem((new FilterComponentItemDTO('not_active','Выкл','condition'))
                        ->setCount($model::notActive()->count())->setChipColor('#F7685B'));
                }
                elseif ($model instanceof SoftDeletableInterface){
                    $items->appendItem((new FilterComponentItemDTO('not_deleted','Активные','condition'))
                        ->setCount($model::notDeleted()->count())->setChipColor('#2ED47A'));
                    $items->appendItem((new FilterComponentItemDTO('deleted','Удаленные','condition'))
                        ->setCount($model::onlyDeleted()->count())->setChipColor('#C0C7CD'));
                }
                return $items;
            })
            ->setFilterQueryCallback(function ($query,$value){

                switch ($value){
                    case 'active': $query->where('active',true)->where('deleted',false);
                        break;
                    case 'not_active': $query->where('active',false)->where('deleted',false);
                        break;
                    case 'deleted': $query->where('deleted',true);
                        break;
                    case 'not_deleted': $query->where('deleted',false);
                        break;
                }
            });
    }
}
