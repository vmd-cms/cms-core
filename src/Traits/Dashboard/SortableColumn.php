<?php

namespace VmdCms\CoreCms\Traits\Dashboard;

use VmdCms\CoreCms\Contracts\Dashboard\Display\Components\TableColumnInterface;

trait SortableColumn
{
    /**
     * @var string|null
     */
    protected $sortable;

    /**
     * @var callable|null
     */
    protected $sortableCallback;

    public function sortable(bool $flag = true): TableColumnInterface
    {
        $this->sortable = $flag;
        return $this;
    }

    public function setSortableCallback(callable $sortableCallback): TableColumnInterface
    {
        $this->sortable = true;
        $this->sortableCallback = $sortableCallback;
        return $this;
    }

    public function isSortable(): ?bool
    {
        return boolval($this->sortable);
    }

    public function getSortableCallback(): ?callable
    {
        return $this->sortableCallback;
    }

}
