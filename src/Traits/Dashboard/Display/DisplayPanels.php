<?php

namespace VmdCms\CoreCms\Traits\Dashboard\Display;

use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Filters\FilterableDisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Panels\FilterPanelInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Panels\FooterPanelInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Panels\HeadPanelInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Panels\SectionPanelInterface;
use VmdCms\CoreCms\Dashboard\Display\Breadcrumbs\AdminPanelBreadcrumbs;
use VmdCms\CoreCms\Dashboard\Display\Panels\FilterPanel;
use VmdCms\CoreCms\Dashboard\Display\Panels\FooterPanel;
use VmdCms\CoreCms\Dashboard\Display\Panels\HeadPanel;
use VmdCms\CoreCms\Dashboard\Display\Panels\SectionPanel;

trait DisplayPanels
{
    /**
     * @var HeadPanelInterface
     */
    protected $headPanel;

    /**
     * @var FooterPanelInterface
     */
    protected $footerPanel;

    /**
     * @var FilterPanelInterface
     */
    protected $filterPanel;

    /**
     * @var SectionPanelInterface
     */
    protected $sectionPanel;

    /**
     * @param HeadPanelInterface $headPanel
     * @return $this|DisplayInterface
     */
    public function setHeadPanel(HeadPanelInterface $headPanel) : DisplayInterface
    {
        $this->headPanel = $headPanel;
        return $this;
    }

    /**
     * @return HeadPanelInterface
     */
    public function getHeadPanel() : HeadPanelInterface
    {
        if(!$this->headPanel instanceof HeadPanelInterface && $this instanceof DisplayInterface)
        {
            $this->headPanel = new HeadPanel($this->getSection()->getTitle(),(new AdminPanelBreadcrumbs($this->getSection())));
        }
        return $this->headPanel;
    }

    /**
     * @param FooterPanelInterface $footerPanel
     * @return $this|DisplayInterface
     */
    public function setFooterPanel(FooterPanelInterface $footerPanel) : DisplayInterface
    {
        $this->footerPanel = $footerPanel;
        return $this;
    }

    /**
     * @return FooterPanelInterface
     */
    public function getFooterPanel() : FooterPanelInterface
    {
        if(!$this->footerPanel instanceof FooterPanelInterface && $this instanceof DisplayInterface)
        {
            $this->footerPanel = new FooterPanel();
        }
        return $this->footerPanel;
    }

    /**
     * @param FilterPanelInterface $filterPanel
     * @return $this|DisplayInterface
     */
    public function setFilterPanel(FilterPanelInterface $filterPanel) : DisplayInterface
    {
        $this->filterPanel = $filterPanel;
        return $this;
    }

    /**
     * @return FilterPanelInterface
     */
    public function getFilterPanel() : FilterPanelInterface
    {
        if(!$this->filterPanel instanceof FilterPanelInterface)
        {
            $filterDisplay = $this instanceof FilterableDisplayInterface ? $this->getFilter() : null;
            $this->filterPanel = new FilterPanel($filterDisplay);
        }
        return $this->filterPanel;
    }

    /**
     * @param SectionPanelInterface $sectionPanel
     * @return $this|DisplayInterface
     */
    public function setSectionPanel(SectionPanelInterface $sectionPanel) : DisplayInterface
    {
        $this->sectionPanel = $sectionPanel;
        return $this;
    }

    /**
     * @param array $sectionPanels
     * @return $this|DisplayInterface
     */
    public function setSectionPanels(array $sectionPanels) : DisplayInterface
    {
        $sectionPanel = $this->getSectionPanel();
        $sectionPanel->setAdditionalPanels($sectionPanels);
        return $this;
    }

    /**
     * @return SectionPanelInterface
     */
    public function getSectionPanel() : SectionPanelInterface
    {
        if(!$this->sectionPanel instanceof SectionPanelInterface && $this instanceof DisplayInterface)
        {
            $this->sectionPanel = (new SectionPanel($this->toArray()))->setViewPath($this->getSectionPanelViewPath());
        }
        return $this->sectionPanel;
    }

    protected function getDisplaySectionPanel() : ?SectionPanelInterface
    {
        return null;
    }

    /**
     * @return string
     */
    protected abstract function getSectionPanelViewPath() : string;
}
