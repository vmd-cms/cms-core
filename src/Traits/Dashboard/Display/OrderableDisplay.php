<?php

namespace VmdCms\CoreCms\Traits\Dashboard\Display;

use Illuminate\Support\Facades\DB;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Components\TableColumnInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\TableInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Contracts\Models\OrderableInterface;
use VmdCms\CoreCms\Contracts\Sections\AdminSectionInterface;
use VmdCms\CoreCms\Dashboard\Display\Components\OrderableColumn;
use VmdCms\CoreCms\Dashboard\Display\Components\TextColumn;
use VmdCms\CoreCms\Exceptions\Dashboard\OrderableDisplayException;
use VmdCms\CoreCms\Exceptions\Dashboard\TableDisplayException;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;
use VmdCms\CoreCms\Exceptions\Models\NotOrderableException;
use VmdCms\CoreCms\Exceptions\Sections\SectionNotFoundException;

trait OrderableDisplay
{
    protected $orderable = false;

    /**
     * @return DisplayInterface
     */
    public function orderable() : DisplayInterface
    {
        $this->orderable = true;
        return $this;
    }

    /**
     * @return bool
     */
    public function isOrderable() : bool
    {
        return $this->orderable;
    }

    /**
     * @param array $items
     * @return void
     * @throws NotCmsModelException
     * @throws NotOrderableException
     * @throws OrderableDisplayException
     * @throws SectionNotFoundException
     */
    public function updateOrder(array $items) : void
    {
        if(!$this->orderable) throw new OrderableDisplayException();

        if(!$this->section instanceof AdminSectionInterface) throw new SectionNotFoundException();

        $cmsModelClass = $this->section->getCmsModelClass();

        $cmsModel = new $cmsModelClass;

        if(!$cmsModel instanceof CmsModelInterface) throw new NotCmsModelException();

        if(!$cmsModel instanceof OrderableInterface) throw new NotOrderableException();

        $table = $cmsModel::table();
        $primaryField = $cmsModel::getPrimaryField();
        $orderField = $cmsModel->getOrderField();

        $query = "UPDATE $table SET `$orderField` = CASE";
        $ids = [];
        foreach ($items as $order=>$item)
        {
            if(!isset($item['id'],$item['id']) || !is_int($item['id']) || !is_int($order)) continue;
            $ids[] = $item['id'];
            $query .= " WHEN $primaryField=" . $item['id'] . " THEN " . ++$order;
        }
        $query .= " ELSE 1 END WHERE id IN (" . implode(',',$ids) . ");";
        try {
            DB::statement($query);
        }
        catch (\Exception $exception)
        {
            throw new OrderableDisplayException();
        }
    }

    /**
     * @return void
     * @throws NotCmsModelException
     * @throws OrderableDisplayException
     * @throws TableDisplayException
     */
    public function prependOrderableTable() : void
    {
        if(!$this instanceof TableInterface) throw new TableDisplayException();

        if(!$this->orderable) throw new OrderableDisplayException();

        if(!$this->cmsModel instanceof OrderableInterface) throw new NotCmsModelException();

        $hasPrimaryField = false;
        foreach ($this->columns->getItems() as $item)
        {
            if(!$item instanceof TableColumnInterface) continue;
            if($hasPrimaryField = $item->getField() == 'id') break;
        }
        if(!$hasPrimaryField) {
            $this->columns->prependItem((new TextColumn('id','ID'))->setWidth(75));
        }
        $this->columns->prependItem((new OrderableColumn('_column_order',''))->setWidth(50));
    }
}
