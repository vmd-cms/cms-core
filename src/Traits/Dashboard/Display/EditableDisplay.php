<?php

namespace VmdCms\CoreCms\Traits\Dashboard\Display;

use VmdCms\CoreCms\Contracts\Dashboard\Display\Components\EditableColumnInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Components\TableColumnInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Exceptions\Dashboard\EditableDisplayException;
use VmdCms\CoreCms\Exceptions\Models\ClassNotFoundException;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;

trait EditableDisplay
{

    public function updateItem(array $item, string $value): void
    {
        if(!isset($item['field'])) throw new EditableDisplayException();

        $updatedColumn = null;

        foreach ($this->columns->getItems() as $column)
        {
            if(!$column instanceof TableColumnInterface) continue;

            if($column->getField() === $item['field'])
            {
                $updatedColumn = $column;
                break;
            }
        }

        if(!$updatedColumn instanceof EditableColumnInterface) throw new EditableDisplayException();

        $modelClass = $this->section->getCmsModelClass();
        $model = new $modelClass;
        if(!$model instanceof CmsModelInterface) throw new NotCmsModelException();

        $model = $model::where($model::getPrimaryField(), $item['cms_model_id'])->first();
        if(!$model instanceof CmsModelInterface) throw new ClassNotFoundException();

        $subFields = explode('.',$updatedColumn->getField());
        $field = array_pop($subFields);
        foreach ($subFields as $relation)
        {
            $model = $model->$relation ?? null;
        }

        if(!$model instanceof CmsModelInterface) throw new NotCmsModelException();
        $model->$field = $value;
        $model->save();
    }
}
