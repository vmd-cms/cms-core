<?php

namespace VmdCms\CoreCms\Traits\Dashboard\Display;

use VmdCms\CoreCms\Contracts\Dashboard\Display\Components\EditableColumnInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Components\TableColumnInterface;

trait EditableColumn
{
    protected $editable = false;

    /**
     * @return TableColumnInterface
     */
    public function editable() :TableColumnInterface
    {
        if($this instanceof EditableColumnInterface)
        {
            $this->editable = true;
        }
        return $this;
    }

    /**
     * @return bool
     */
    public function isEditable() :bool
    {
        return $this->editable;
    }
}
