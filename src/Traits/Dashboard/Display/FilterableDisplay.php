<?php

namespace VmdCms\CoreCms\Traits\Dashboard\Display;

use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Filters\FilterDisplayInterface;

trait FilterableDisplay
{
    /**
     * @var FilterDisplayInterface
     */
    protected $filterDisplay;

    /**
     * @param FilterDisplayInterface $filterDisplay
     * @return DisplayInterface
     */
    public function setFilter(FilterDisplayInterface $filterDisplay) : DisplayInterface
    {
        if($this instanceof DisplayInterface) $this->filterDisplay = $filterDisplay;

        return $this;
    }

    /**
     * @return FilterDisplayInterface|null
     */
    public function getFilter() : ?FilterDisplayInterface
    {
        return $this->filterDisplay;
    }
}
