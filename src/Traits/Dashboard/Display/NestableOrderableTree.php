<?php

namespace VmdCms\CoreCms\Traits\Dashboard\Display;

use VmdCms\CoreCms\Contracts\Models\OrderableInterface;
use VmdCms\CoreCms\Contracts\Models\TreeableInterface;
use VmdCms\CoreCms\Contracts\Sections\AdminSectionInterface;
use VmdCms\CoreCms\Exceptions\Dashboard\TreeDisplayException;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Services\CoreRouter;
use VmdCms\CoreCms\Traits\CrudPolicy\CrudRoutes;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

trait NestableOrderableTree
{
    use CrudRoutes;
    /**
     * @var string
     */
    private $cmsModelPrimaryField;
    private $cmsModelParentField;
    private $cmsModelOrderField;

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws TreeDisplayException
     */
    public function updateTree(Request $request)
    {
        if(!$this->section instanceof AdminSectionInterface) throw new TreeDisplayException();

        $cmsModelClass = $this->section->getCmsModelClass();
        $this->cmsModel = new $cmsModelClass;
        $this->setCmsModelFields();

        $models = $cmsModelClass::get();
        if(!$models instanceof Collection) throw new TreeDisplayException();

        $this->recursiveUpdateTree($request->tree, $models->keyBy($this->cmsModelPrimaryField));
    }

    /**
     * @throws TreeDisplayException
     */
    protected function setCmsModelFields()
    {
        if(!$this->cmsModel instanceof TreeableInterface) throw new TreeDisplayException();
        $this->cmsModelPrimaryField = $this->cmsModel->getPrimaryField();
        $this->cmsModelParentField = $this->cmsModel->getParentField();

        if(!$this->cmsModel instanceof OrderableInterface) throw new TreeDisplayException();
        $this->cmsModelOrderField = $this->cmsModel->getOrderField();
    }

    /**
     * @param array $tree
     * @param $models
     * @param null $parentId
     * @throws TreeDisplayException
     */
    protected function recursiveUpdateTree(array $tree,$models, $parentId = null)
    {
        $orderField = $this->cmsModelOrderField;
        $parentField = $this->cmsModelParentField;
        foreach ($tree as $order=>$item)
        {
            $order++;
            if(!$model = $models->get($item['id']) ?? null) {
                continue;
            }
            if($model->$orderField != $order || $model->$parentField != $parentId)
            {
                try {
                    $model->$orderField = $order;
                    $model->$parentField = $parentId;
                    $model->save();
                }
                catch (\Exception $e) {
                    throw new TreeDisplayException();
                }
            }

            if(isset($item['children']) && count($item['children']))
            {
                $this->recursiveUpdateTree($item['children'], $models, $item['id']);
            }
        }
    }

    /**
     * @return array
     * @throws TreeDisplayException
     */
    protected function getTreeData()
    {
        $this->setCmsModelFields();
        $data['route_create'] = $this->getCreateRoute();
        $data['route_update_tree'] = CoreRouter::getTreeOrderUpdateRoute($this->getSection()->getSectionSlug());
        $items = $this->cmsModel->tree()->get();
        foreach ($items as $item)
        {
            $data['items'][] = $this->recursiveGetItem($item);
        }
        return $data;
    }

    /**
     * @param TreeableInterface $item
     * @return array
     */
    protected function recursiveGetItem(TreeableInterface $item)
    {
        $titleField = $this->displayField;
        $primaryField = $this->cmsModelPrimaryField;
        if(!$item instanceof CmsModel) return [];

        $value = $item;
        $subFields = explode('.', $titleField);
        foreach ($subFields as $subField)
        {
            $value = $value->$subField ?? null;
        }
        $data =  [
            'title' => $value,
            'id' => $item->$primaryField,
            'action' => [
                'route_view' => $this->getViewRoute($item),
                'route_edit' => $this->getEditRoute($item),
                'route_delete' => $this->getDeleteRoute($item),
            ]
        ];
        $children = [];
        if(isset($item->childrenTree) && count($item->childrenTree))
        {
            foreach ($item->childrenTree as $child)
            {
                $children[] = $this->recursiveGetItem($child);
            }
        }
        return array_merge($data, ['children' => $children]);
    }
}
