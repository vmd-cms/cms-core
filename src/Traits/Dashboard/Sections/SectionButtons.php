<?php

namespace VmdCms\CoreCms\Traits\Dashboard\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Buttons\FormButtonInterface;
use VmdCms\CoreCms\Contracts\Models\CloneableInterface;
use VmdCms\CoreCms\Contracts\Sections\AdminSectionInterface;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormBackButton;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormButton;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormCancelButton;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormCopyButton;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormCreateButton;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormDeleteButton;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormSaveButton;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormSaveCreateButton;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Services\CoreRouter;

trait SectionButtons
{
    /**
     * @var array
     */
    private $formButtons = [];

    protected function appendFormButton(FormButtonInterface $button)
    {
        $this->formButtons[] = $button;
    }

    protected function getFormButtons(): array
    {
        return $this->formButtons;
    }

    /**
     * @param AdminSectionInterface $section
     * @return array
     */
    protected function getCreateButtons(AdminSectionInterface $section): array
    {
        $this->appendFormButton(new FormSaveButton());
        $this->appendFormButton(new FormSaveCreateButton());
        $this->appendFormButton((new FormCancelButton())->setRoutePath(CoreRouter::getDisplayRoute($section->getSectionSlug())));
        $this->appendFormButton((new FormBackButton())->setRoutePath(CoreRouter::getDisplayRoute($section->getSectionSlug()))
            ->setGroupKey(FormButton::GROUP_BACK));
        return $this->getFormButtons();
    }

    /**
     * @param AdminSectionInterface $section
     * @param CmsModel $model
     * @return array
     */
    protected function getEditButtons(AdminSectionInterface $section, CmsModel $model): array
    {
        if ($section->isCreatable()){
            $this->appendFormButton((new FormCreateButton())
                ->setRoutePath(CoreRouter::getCreateRoute($section->getSectionSlug()))
                ->setGroupKey(FormButton::GROUP_ACTION_FLOAT));
        }

        if($model instanceof CloneableInterface && $section->isCreatable()){
            $this->appendFormButton((new FormCopyButton())
                ->setRoutePath(CoreRouter::getCreateRoute($section->getSectionSlug()))
                ->setGroupKey(FormButton::GROUP_ACTION_FLOAT));
        }

        if ($section->isDeletable($model)){
            $this->appendFormButton((new FormDeleteButton())
                ->setRoutePath(CoreRouter::getDeleteRoute($section->getSectionSlug(),$model->id))
                ->setGroupKey(FormButton::GROUP_ACTION_FLOAT));
        }
        if ($section->isEditable($model)){
            $this->appendFormButton(new FormSaveButton());
        }
        if ($section->isEditable($model) && $section->isCreatable()){
            $this->appendFormButton(new FormSaveCreateButton());
        }
        $this->appendFormButton((new FormCancelButton())->setRoutePath(CoreRouter::getDisplayRoute($section->getSectionSlug())));
        $this->appendFormButton((new FormBackButton())->setRoutePath(CoreRouter::getDisplayRoute($section->getSectionSlug()))
            ->setGroupKey(FormButton::GROUP_BACK));
        return $this->getFormButtons();
    }
}
