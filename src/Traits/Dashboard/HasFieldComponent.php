<?php

namespace VmdCms\CoreCms\Traits\Dashboard;

use VmdCms\CoreCms\Contracts\Dashboard\ComponentInterface;

trait HasFieldComponent
{
    public $field_name;

    /**
     * @var string
     */
    protected $field;
    protected $label;
    protected $topHelpText;
    protected $bottomHelpText;
    protected $placeholder;
    protected $width;
    protected $align;
    protected $paddingTop;
    protected $paddingBottom;
    protected $outlined = true;
    protected $rows;

    /**
     * @var bool
     */
    protected $showLabel = true;
    protected $displayRow = false;

    /**
     * @param string $field
     * @return ComponentInterface
     */
    public function setField(string $field): ComponentInterface
    {
        $this->field_name = $field;
        $this->field = $field;
        return $this;
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @param string $label
     * @return ComponentInterface
     */
    public function setLabel(string $label): ComponentInterface
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $placeholder
     * @return ComponentInterface
     */
    public function setPlaceholder(string $placeholder) : ComponentInterface
    {
        $this->placeholder = $placeholder;
        return $this;
    }

    /**
     * @return string
     */
    public function getPlaceholder() : string
    {
        return $this->placeholder ?? $this->label;
    }

    /**
     * @param string $helpText
     * @return ComponentInterface
     */
    public function setTopHelpText(string $helpText) : ComponentInterface
    {
        $this->topHelpText = $helpText;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTopHelpText() : ?string
    {
        return $this->topHelpText;
    }

    /**
     * @param string $helpText
     * @return ComponentInterface
     */
    public function setBottomHelpText(string $helpText) : ComponentInterface
    {
        $this->bottomHelpText = $helpText;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBottomHelpText() : ?string
    {
        return $this->bottomHelpText;
    }

    /**
     * @param int $width
     * @return $this|ComponentInterface
     */
    public function setWidth(int $width) : ComponentInterface
    {
       $this->width = $width;
       return $this;
    }

    /**
     * @return int
     */
    public function getWidth() : ?int
    {
        return $this->width;
    }

    /**
     * @return ComponentInterface
     */
    public function alignCenter() : ComponentInterface
    {
        $this->align = 'center';
        return $this;
    }

    /**
     * @return ComponentInterface
     */
    public function alignEnd() : ComponentInterface
    {
        $this->align = 'end';
        return $this;
    }

    /**
     * @return ComponentInterface
     */
    public function alignStart() : ComponentInterface
    {
        $this->align = 'start';
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAlign() : ?string
    {
        return $this->align;
    }

    /**
     * @return bool
     */
    public function isShowLabel(): bool
    {
        return $this->showLabel;
    }

    /**
     * @return $this
     */
    public function displayRow()
    {
        $this->displayRow = true;
        return $this;
    }

    /**
     * @param int $label
     * @param int $block
     * @return ComponentInterface
     */
    public function setRows(int $label,int $block) : ComponentInterface
    {
        $this->rows = [
            'label' => $label,
            'block' => $block,
        ];
        return $this;
    }

    /**
     * @return array|null
     */
    public function getRows(): ?array
    {
        return $this->rows;
    }

    /**
     * @param bool $showLabel
     * @return ComponentInterface
     */
    public function setShowLabel(bool $showLabel): ComponentInterface
    {
        $this->showLabel = $showLabel;
        return $this;
    }

    /**
     * @param int $paddingTop
     * @return ComponentInterface
     */
    public function setPaddingTop(int $paddingTop): ComponentInterface
    {
        $this->paddingTop = $paddingTop;
        return $this;
    }

    /**
     * @param int $paddingBottom
     * @return ComponentInterface
     */
    public function setPaddingBottom(int $paddingBottom): ComponentInterface
    {
        $this->paddingBottom = $paddingBottom;
        return $this;
    }

    public function outlined(bool $flag): ComponentInterface{
        $this->outlined = $flag;
        return $this;
    }

    /**
     * @return string
     */
    protected function getCustomStyles() : string
    {
        $styles = '';
        if($this->paddingTop) $styles .= "padding-top:" .$this->paddingTop. "px;";
        if($this->paddingBottom) $styles .= "padding-bottom:" .$this->paddingBottom. "px;";
        return $styles;
    }
}
