<?php

namespace VmdCms\CoreCms\Traits\Dashboard;

use VmdCms\CoreCms\Contracts\Dashboard\Display\Components\TableColumnInterface;

trait SearchableColumn
{
    /**
     * @var string|null
     */
    protected $searchable;

    /**
     * @var callable|null
     */
    protected $searchableCallback;

    public function searchable(bool $flag = true): TableColumnInterface
    {
        $this->searchable = $flag;
        return $this;
    }

    public function setSearchableCallback(callable $searchableCallback): TableColumnInterface
    {
        $this->searchable = true;
        $this->searchableCallback = $searchableCallback;
        return $this;
    }

    public function isSearchable(): ?bool
    {
        return boolval($this->searchable);
    }

    public function getSearchableCallback(): ?callable
    {
        return $this->searchableCallback;
    }

}
