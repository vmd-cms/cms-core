<?php

namespace VmdCms\CoreCms\Traits\Dashboard;

use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;

trait HasCmsModelData
{
    /**
     * @return array
     */
    protected function getCmsModelData() : array
    {
        if(!isset($this->cmsModel) || !$this->cmsModel instanceof CmsModelInterface) return [];

        $primaryField = $this->cmsModel->getPrimaryField();
        return [
            'cms_model_class' => get_class($this->cmsModel),
            'cms_model_id' => $this->cmsModel->$primaryField ?? null,
        ];
    }
}
