<?php

namespace VmdCms\CoreCms\Traits\Dashboard;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormComponentInterface;
use VmdCms\CoreCms\Contracts\Dashboard\HtmlAttributesInterface;
use \VmdCms\CoreCms\Contracts\DTO\HtmlAttributesDTOInterface;
use VmdCms\CoreCms\DTO\Dashboard\Components\HtmlAttributesDTO;

trait HasHtmlAttributes
{
    /**
     * @var HtmlAttributesDTOInterface
     */
    protected $htmlAttributes;

    /**
     *
     */
    protected function initializeComponentHtmlAttributesDTO(): void
    {
        $this->htmlAttributes = new HtmlAttributesDTO();
        $this->setDefaultAttributes($this->htmlAttributes);
    }

    /**
     * @param HtmlAttributesDTOInterface $dto
     */
    protected function setDefaultAttributes(HtmlAttributesDTOInterface $dto): void{}

    /**
     * @param HtmlAttributesDTOInterface $dto
     * @return HtmlAttributesInterface
     */
    public function setHtmlAttributes(HtmlAttributesDTOInterface $dto) : HtmlAttributesInterface
    {
        $this->htmlAttributes = $dto;
        return $this;
    }

    /**
     * @return HtmlAttributesDTOInterface
     */
    public function getHtmlAttributes(): HtmlAttributesDTOInterface
    {
        return $this->htmlAttributes;
    }
}
