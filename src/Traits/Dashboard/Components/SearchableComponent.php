<?php

namespace VmdCms\CoreCms\Traits\Dashboard\Components;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\SearchableComponentInterface;
use VmdCms\CoreCms\Traits\Dashboard\Forms\MultiValuable;

trait SearchableComponent
{
    use MultiValuable;

    /**
     * @var string
     */
    protected $asyncMethodPath;

    /**
     * @var int
     */
    protected $limit;

    /**
     * @var int
     */
    protected $offset;

    /**
     * @var bool
     */
    protected $hasMore;

    /**
     * @var string
     */
    protected $search;

    /**
     * @var callable
     */
    protected $searchCallback;

    /**
     * @param callable $callback
     * @return SearchableComponentInterface
     */
    public function setSearchCallback(callable $callback): SearchableComponentInterface
    {
        $this->searchCallback = $callback;
        return $this;
    }

    /**
     * @return callable|null
     */
    public function getSearchCallback(): ?callable
    {
        return $this->searchCallback;
    }

    /**
     * @return string
     */
    public function getAsyncMethodPath(): string
    {
        return $this->asyncMethodPath;
    }

    /**
     * @param string $asyncMethod
     * @return SearchableComponentInterface
     */
    public function setAsyncMethodPath(string $asyncMethod): SearchableComponentInterface
    {
        $this->asyncMethodPath = $asyncMethod;
        return $this;
    }

    /**
     * @param int $limit
     * @return SearchableComponentInterface
     */
    public function setLimit(int $limit): SearchableComponentInterface
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return is_int($this->limit) ? $this->limit : 20;
    }

    /**
     * @param int $offset
     * @return SearchableComponentInterface
     */
    public function setOffset(int $offset): SearchableComponentInterface
    {
        $this->offset = $offset;
        return $this;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return is_int($this->offset) ? $this->offset : 0;
    }

    /**
     * @param bool $hasMore
     * @return SearchableComponentInterface
     */
    public function setHasMore(bool $hasMore): SearchableComponentInterface
    {
        $this->hasMore = $hasMore;
        return $this;
    }

    /**
     * @return bool
     */
    public function getHasMore(): bool
    {
        return boolval($this->hasMore);
    }

    /**
     * @param string|null $search
     * @return SearchableComponentInterface
     */
    public function setSearch(string $search = null): SearchableComponentInterface
    {
        $this->search = $search;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSearch(): ?string
    {
        return $this->search;
    }

    /**
     * @param $query
     * @return void
     */
    protected function modifyQueryByComponent($query)
    {
        if(is_callable($this->searchCallback)){
            call_user_func($this->searchCallback,$query,$this->search,$this->value);
        }
        if($this->offset){
            $query->offset($this->offset);
        }
        if($this->limit){
            $query->limit($this->limit);
        }
    }

    /**
     * @return int
     */
    public function getNextOffset(): int
    {
        return $this->offset ? intval($this->offset) + intval($this->limit) : intval($this->limit);
    }

    protected function getValueLabel()
    {
        $value = $this->getValue();
        if(!empty($value) && is_callable($this->displayTitleCallback)){
            $item = $this->className::find($value);
            return call_user_func($this->displayTitleCallback,$item);
        }
        return !empty($value) ? $value : null;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $component = parent::toArray();

        $this->hasMore = is_countable($component['options'] ?? null) && count($component['options']) >= $this->limit;

        return array_merge($component,[
            'async_method_path' => $this->getAsyncMethodPath(),
            'limit' => $this->getLimit(),
            'offset' => $this->getNextOffset(),
            'has_more' => $this->getHasMore(),
            'value_label' => $this->getValueLabel()
        ]);
    }
}
