<?php

namespace VmdCms\CoreCms\Traits\Dashboard;

use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\HasCmsModelInterface;
use VmdCms\CoreCms\Contracts\Sections\AdminSectionInterface;

trait HasSection
{
    /**
     * @var AdminSectionInterface
     */
    protected $section;

    public function setSection(AdminSectionInterface $section): DisplayInterface
    {
        $this->section = $section;
        return $this;
    }

    public function getSection(): AdminSectionInterface
    {
        return $this->section;
    }
}
