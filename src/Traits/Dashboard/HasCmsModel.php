<?php

namespace VmdCms\CoreCms\Traits\Dashboard;

use VmdCms\CoreCms\Contracts\Dashboard\HasCmsModelInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;

trait HasCmsModel
{
    /**
     * @var CmsModelInterface
     */
    protected $cmsModel;

    /**
     * @inheritDoc
     */
    public function setCmsModel(CmsModelInterface $cmsModel): HasCmsModelInterface
    {
        $this->cmsModel = $cmsModel;
        return $this;
    }

    /**
     * @return null|CmsModelInterface
     */
    public function getCmsModel(): ?CmsModelInterface
    {
        return $this->cmsModel;
    }
}
