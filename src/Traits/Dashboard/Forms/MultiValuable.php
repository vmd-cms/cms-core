<?php

namespace VmdCms\CoreCms\Traits\Dashboard\Forms;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Route;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\HasDependedComponentsInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Input\MultiValueInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Contracts\Models\OrderableInterface;
use VmdCms\CoreCms\Dashboard\Forms\Components\TreeSelectComponent;
use VmdCms\CoreCms\Exceptions\CoreException;
use VmdCms\CoreCms\Exceptions\Models\ClassNotFoundException;
use VmdCms\CoreCms\Services\CoreRouter;

trait MultiValuable
{
    /**
     * @var string
     */
    protected $displayField = 'title';
    protected $foreignField = 'id';
    protected $className;
    protected $isIdInt = true;

    /**
     * @var callable
     */
    protected $valueCallback;
    protected $modifyQueryCallback;
    protected $displayTitleCallback;

    /**
     * @var bool
     */
    protected $nullable;

    /**
     * @var string
     */
    protected $nullableValue;

    /**
     * @var Builder
     */
    protected $query;

    /**
     * @var array
     */
    protected $where;

    /**
     * @var array
     */
    private $enumValues = [];

    protected $hideDetails;


    protected $multiple;

    /**
     * @param bool $flag
     * @return $this
     */
    public function setMultiple(bool $flag)
    {
        $this->multiple = $flag;
        return $this;
    }

    /**
     * @return bool
     */
    public function getMultiple()
    {
        return boolval($this->multiple);
    }

    public function hideDetails()
    {
        $this->hideDetails = true;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setModelForOptions(string $className): MultiValueInterface
    {
        $this->className = $className;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setDisplayField(string $field): MultiValueInterface
    {
        $this->displayField = $field;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setQuery(Builder $query) : MultiValueInterface
    {
        $this->query = $query;
        return $this;
    }

    /**
     * @param array $where
     * @return $this|MultiValueInterface
     */
    public function setWhere(array $where) : MultiValueInterface
    {
        if(!is_array($this->where)) $this->where = [];
        $this->where[] = $where;
        return $this;
    }

    /**
     * @param callable $callable
     * @return $this|MultiValueInterface
     */
    public function setModifyQueryCallback(callable $callable) : MultiValueInterface
    {
        $this->modifyQueryCallback = $callable;
        return $this;
    }

    /**
     * @param callable $callable
     * @return $this|MultiValueInterface
     */
    public function setDisplayTitleCallback(callable $callable) : MultiValueInterface
    {
        $this->displayTitleCallback = $callable;
        return $this;
    }

    /**
     * @param CmsModelInterface $item
     * @return array
     */
    protected function getCustomOptionDataArr(CmsModelInterface $item) : array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function setForeignField(string $field): MultiValueInterface
    {
        $this->foreignField = $field;
        return $this;
    }

    /**
     * @param bool $flag
     * @return MultiValueInterface
     */
    public function setIsIdInt(bool $flag) : MultiValueInterface
    {
        $this->isIdInt = $flag;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setEnumValues(array $enums): MultiValueInterface
    {
        $this->enumValues = $enums;
        $this->isIdInt = false;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getOptions() : array
    {
        try {
            return $this->prepareModelOptionData();
        }
        catch (CoreException $e) {
        }

        return $this->prepareEnumValues();
    }

    public function prepareEnumValues()
    {
        $options = [];
        foreach ($this->enumValues as $id=>$value)
        {
            $options[] = [
                $this->getOptionIdKey() => $id,
                $this->getOptionTitleKey() => $value
            ];
        }
        return $options;
    }

    /**
     * @return MultiValueInterface
     */
    public function nullable() : MultiValueInterface
    {
        $this->nullable = true;
        return $this;
    }

    /**
     * @param string $value
     * @return MultiValueInterface
     */
    public function nullableValue(string $value) : MultiValueInterface
    {
        $this->nullable();
        $this->nullableValue = $value;
        return $this;
    }

    /**
     * @return string
     */
    protected function getOptionTitleKey(): string{
        return 'title';
    }

    /**
     * @return string
     */
    protected function getOptionIdKey(): string{
        return 'id';
    }



    /**
     * @return array
     * @throws ClassNotFoundException
     */
    protected function prepareModelOptionData()
    {
        $options = [];

        $query = $this->getQuery();
        $items = $query->get() ?? new Collection();

        $field = $this->displayField;
        $foreign = $this->foreignField;

        if(is_array($this->value)){
            foreach ($this->value as $value){
                if(empty($value)) continue;

                if(empty($items->where($foreign,$value)->first())){
                    $currentItem = $query->where($foreign,$value)->first();
                    $items->prepend($currentItem);
                }

            }
        }

        foreach ($items as $item) {

            if(!$item instanceof CmsModelInterface){
                continue;
            }

            if(is_callable($this->displayTitleCallback)){
                $title = call_user_func($this->displayTitleCallback,$item);
            }
            else{
                $path = explode('.',$field);
                $title = $item;
                foreach ($path as $relItem){
                    $title = $title->$relItem ?? null;
                }
            }

            $option = [
                $this->getOptionIdKey() => $item->$foreign,
                $this->getOptionTitleKey() => $title
            ];

            $additionalData = $this->getCustomOptionDataArr($item);

            if(is_array($additionalData)){
                $option = array_merge($option,$additionalData);
            }

            $options[] = $option;

        }
        if($this->nullable){
            array_unshift($options,[
                $this->getOptionIdKey() => null,
                $this->getOptionTitleKey() => $this->nullableValue ?? ''
            ]);
        }

        return $options;
    }

    /**
     * @param $query
     * @return void
     */
    protected function modifyQueryByComponent($query){}

    /**
     * @return Builder
     * @throws ClassNotFoundException
     */
    private function getQuery() : Builder
    {
        if($this->query instanceof Builder) {
            return $this->query;
        }

        if(!class_exists($this->className)) throw new ClassNotFoundException();

        $model = $this->className ? new $this->className : null;
        if(!$model instanceof CmsModelInterface) throw new ClassNotFoundException();

        $query = $model::query();

        if(is_callable($this->modifyQueryCallback)){
            call_user_func($this->modifyQueryCallback,$query,$this->getCmsModel());
        }

        if(is_array($this->where) && count($this->where))
        {
            foreach ($this->where as $where){
                if(!is_array($where)) continue;
                $query->where($where[0] ?? null,$where[1] ?? '=',$where[2] ?? null);
            }
        }

        if($model instanceof OrderableInterface) {
            $query->order();
        }

        $this->modifyQueryByComponent($query);

        return $query;
    }

    protected function getAdditionalData(): array
    {
        return  [
            'options' => $this->getOptions(),
            'is_id_int' => $this->isIdInt,
            'hide_details' => boolval($this->hideDetails),
            'multiple' => $this->getMultiple()
        ];
    }

    protected function getValue()
    {
        $queryParam = request()->query->get($this->field, null);
        if($queryParam !== null && Route::getCurrentRoute()->getName() == CoreRouter::ROUTE_CREATE_GET) {
            $queryParam = $queryParam === 'null' ? null : $queryParam;
            foreach ($this->getOptions() as $option)
            {
                if($option['id'] == $queryParam) return $queryParam;
            }
        }
        if($this->cmsModel instanceof CmsModelInterface && $this->cmsModel->relationLoaded($relationName = $this->field))
        {
            $relation = $this->cmsModel->$relationName;
            if($relation instanceof CmsModelInterface) {
                $primaryField = $relation::getPrimaryField();
                $this->value = $relation->$primaryField;
            }
            if($relation instanceof Collection)
            {
                $this->value = [];
                foreach ($relation as $item)
                {
                    if($item instanceof CmsModelInterface) {
                        $primaryField = $item::getPrimaryField();
                        $this->value[] = $item->$primaryField;
                    }
                }
            }
            if(empty($this->className)) $this->className = get_class($this->cmsModel->$relationName()->getRelated());
        }

        if(is_callable($this->valueCallback)){
            $this->value = call_user_func($this->valueCallback);
        }

        if(empty($this->value)){
            $this->value = $this->getDefault();
        }

        if($this->multiple && !is_array($this->value)){
            $value = [];
            $explodedArr = explode(',',$this->value);
            if($this->isIdInt && count($explodedArr)){
                foreach ($explodedArr as $explodedItem){
                    $value[] = intval($explodedItem);
                 }

            }else{
                $value = $explodedArr;
            }
            $this->value = $value;
        }
        return $this->value;
    }

    /**
     * @param callable $valueCallback
     * @return MultiValuable
     */
    public function setValueCallback($valueCallback)
    {
        $this->valueCallback = $valueCallback;
        return $this;
    }

}
