<?php

namespace VmdCms\CoreCms\Traits\Dashboard\Forms;

use VmdCms\CoreCms\Collections\FormButtonsCollection;
use VmdCms\CoreCms\Contracts\Collections\FormButtonsCollectionInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Buttons\FormButtonInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormButton;

trait HasFormButtons
{
    /**
     * @var FormButtonsCollectionInterface
     */
    protected $buttons;

    /**
     * @var boolean
     */
    protected $hideFloatActionGroup;

    /**
     * @var boolean
     */
    protected $hideFooterActionGroup;

    /**
     * @param array $buttons
     * @return FormInterface
     */
    public function setButtons(array $buttons): FormInterface
    {
        if(!$this->buttons instanceof FormButtonsCollectionInterface)
        {
            $this->buttons = new FormButtonsCollection();
        }

        foreach ($buttons as $key => $button)
        {
            if($button instanceof FormButtonInterface) $this->buttons->appendItem($button);
        }

        return $this;
    }

    /**
     * @return FormInterface
     */
    public function hideFloatActionGroup(): FormInterface
    {
        $this->hideFloatActionGroup = true;
        return $this;
    }

    /**
     * @return FormInterface
     */
    public function hideFooterActionGroup(): FormInterface
    {
        $this->hideFooterActionGroup = true;
        return $this;
    }

    protected function getFormButtons(){

        $groups =  $this->buttons->toArray();
        if($this->hideFloatActionGroup){
            $groups[FormButton::GROUP_ACTION_FLOAT] = [];
        }
        if($this->hideFooterActionGroup){
            $groups[FormButton::GROUP_ACTION_FOOTER] = [];
        }

        return $groups;
    }
}
