<?php

namespace VmdCms\CoreCms\Traits\Dashboard\Forms;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\InnerDataTableInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Tables\FormTableColumnHasFileInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Tables\ModalDataInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Tables\FormTableActionsColumnInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Tables\FormTableColumnInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Tables\TableDataInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Contracts\Models\CmsRelatedModelInterface;
use VmdCms\CoreCms\Contracts\Models\OrderableInterface;
use VmdCms\CoreCms\Dashboard\Forms\Components\Component;
use VmdCms\CoreCms\Dashboard\Forms\Tables\Components\Actions\DeleteAction;
use VmdCms\CoreCms\Dashboard\Forms\Tables\Components\Actions\EditAction;
use VmdCms\CoreCms\Dashboard\Forms\Tables\Components\ColumnActions;
use VmdCms\CoreCms\Dashboard\Forms\Tables\Components\ColumnImage;
use VmdCms\CoreCms\Dashboard\Forms\Tables\Components\ColumnText;
use VmdCms\CoreCms\Dashboard\Forms\Tables\TableData;
use VmdCms\CoreCms\Exceptions\CoreException;
use VmdCms\CoreCms\Exceptions\Models\ClassNotFoundException;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;
use VmdCms\CoreCms\Exceptions\Models\NotCmsRelatedModelException;

trait InnerDataTable
{
    use HasComponents;

    /**
     * @var bool
     */
    private $isCreatable = true;
    private $isEditable = false;
    private $isDeletable = true;
    private $isOrderable = true;

    /**
     * @var ModalDataInterface
     */
    protected $createModal;

    /**
     * @var ModalDataInterface
     */
    protected $editModal;

    /**
     * @var callable
     */
    protected $isDeletableCallback;
    protected $isEditableCallback;

    /**
     * @var array
     */
    private $headers;
    private $items;
    private $tableComponents;
    private $emptyItem;
    private $requestArr;
    private $expandedComponents;

    /**
     * @var int
     */
    private $order;

    private $customModelClass;

    private $info;

    public function setCustomModelClass(string $customModelClass): InnerDataTableInterface
    {
        $this->customModelClass = $customModelClass;
        return $this;
    }

    /**
     * @param null|callable $isEditableCallback
     * @return InnerDataTableInterface
     */
    public function setIsEditableCallback(callable $isEditableCallback = null)
    {
        $this->isEditableCallback = $isEditableCallback;
        return $this;

    }

    /**
     * @param null|callable $isDeletableCallback
     * @return InnerDataTableInterface
     */
    public function setIsDeletableCallback(callable $isDeletableCallback = null)
    {
        $this->isDeletableCallback = $isDeletableCallback;
        return $this;

    }

    /**
     * @param string $info
     * @return InnerDataTableInterface
     */
    public function setInfo(string $info = null): InnerDataTableInterface
    {
        $this->info = $info;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getTableData(): TableDataInterface
    {
        if($this->isOrderable){
            $this->appendHeaderItem('', 'sort',  false, 'center', 50);
        }

        $columns = $this->getColumnsArray();
        $relationItems = $this->getRelationItems();

        foreach ($columns as $column)
        {
            if(!$column instanceof FormTableColumnInterface) continue;

            $this->appendHeaderItem($column->label, $column->field, false, $column->getAlign() ?? 'center', $column->getWidth());
            $this->appendEmptyItemField($column);
            $this->appendItems($column, $relationItems);
            $this->appendTableComponent($column);
            $this->appendExpandedComponents($relationItems);

        }
        $this->appendEditModal($relationItems);
        return (new TableData($this->headers,$this->items,$this->tableComponents))
            ->setIsCreatable($this->isCreatable)
            ->setIsEditable($this->isEditable)
            ->setIsOrderable($this->isOrderable)
            ->setInfo($this->info)
            ->setEmptyItem($this->getEmptyItem());
    }

    /**
     * @param bool $flag
     * @return InnerDataTableInterface
     */
    public function setIsCreatable(bool $flag)
    {
        $this->isCreatable = $flag;
        return $this;
    }

    /**
     * @param bool $flag
     * @return InnerDataTableInterface
     */
    public function setIsEditable(bool $flag)
    {
        $this->isEditable = $flag;
        return $this;
    }

    /**
     * @param bool $flag
     * @return InnerDataTableInterface
     */
    public function setIsDeletable(bool $flag)
    {
        $this->isDeletable = $flag;
        return $this;
    }

    /**
     * @param bool $flag
     * @return InnerDataTableInterface
     */
    public function setIsOrderable(bool $flag)
    {
        $this->isOrderable = $flag;
        return $this;
    }

    /**
     * @param ModalDataInterface $createModal
     * @return InnerDataTableInterface
     */
    public function setCreateModal(ModalDataInterface $createModal)
    {
        $this->createModal = $createModal;
        return $this;
    }

    /**
     * @param ModalDataInterface $editModal
     * @return InnerDataTableInterface
     */
    public function setEditModal(ModalDataInterface $editModal)
    {
        $this->editModal = $editModal;
        return $this;
    }

    protected function getActionsColumn()
    {
        $components = [];

        if($this->isEditable) {
            $components[] = new EditAction('__edit');
        }

        if($this->isDeletable) {
            $components[] = new DeleteAction('__delete');
        }

        if(is_countable($components) && count($components)){
            return (new ColumnActions('__actions', 'Действия'))->setWidth(150)->setCmsModel($this->getCmsModel())->setComponents($components);
        }

        return null;
    }

    private function getColumnsArray()
    {
        $columns = $this->components->getItemsArray();
        if($this->isOrderable){
            $columns = array_merge([(new ColumnText('id'))->setWidth(100)],$columns);
        }
        if($actionColumn = $this->getActionsColumn()) $columns[] = $actionColumn;
        return $columns;
    }

    protected function appendExpandedComponents($relationItems)
    {}

    protected function appendEditModal($relationItems)
    {
        if(!$this->editModal instanceof ModalDataInterface) return;

        if(!$relationItems instanceof Collection) return;

        $emptyModal = clone $this->editModal;

        $this->emptyItem['edit_modal'] = $emptyModal
            ->toArray();

        foreach ($relationItems as $key=>$relationItem)
        {
            if(!$item = $this->items[$key] ?? null)
            {
                $item['id'] = $relationItem->id;
                $item['relation'] = $this->field;
            }
            $clonedModal = clone $this->editModal;
            $this->items[$key]['edit_modal'] = $clonedModal
                ->setUpdated($relationItem->updatedComponent ?? false)
                ->setCmsModel($relationItem)
                ->toArray();
        }
    }

    private function appendHeaderItem(string $text, string $value, bool $sortable = false, string $align = 'center', int $width = null)
    {
        $this->headers[] = [
            'text' => $text,
            'align' => $align,
            'sortable' => $sortable,
            'value' => $value,
            'width' => $width,
        ];
    }

    private function appendTableComponent(FormTableColumnInterface $column)
    {
        $field = $column->field;
        $column->setField($this->field . '.' . $column->field);
        $this->tableComponents[$field] = $column->toArray();
    }

    /**
     * @return CmsModelInterface|null
     */
    protected function getModel() :?CmsModelInterface
    {
        return $this->cmsModel;
    }

    private function hasCustomModel()
    {
        return !empty($this->customModelClass) && class_exists($this->customModelClass);
    }

    private function getCustomModelItems()
    {
        $query = $this->customModelClass::query();
        if((new $this->customModelClass) instanceof OrderableInterface) $query->order();
        return $query->get();
    }

    private function getRelationItems()
    {
        if($this->hasCustomModel())
        {
            return $this->getCustomModelItems();
        }

        if(!$this->field || !$this->getModel() instanceof CmsModelInterface) return null;

        $relations = explode('.', $this->field);
        $result = $this->getModel();

        foreach ($relations as $item)
        {
            $result = $result->$item ?? null;
        }
        return $result;
    }

    private function appendItems(FormTableColumnInterface $column, ?Collection $relationItems)
    {
        $subFields = explode('.',$column->field);

        if(!$relationItems instanceof Collection) return;
        foreach ($relationItems as $key=>$relationItem)
        {
            if(!$item = $this->items[$key] ?? null)
            {
                $item['id'] = $relationItem->id;
                $item['relation'] = $this->field;
            }
            $value = $relationItem;
            foreach ($subFields as $subField)
            {
                $value = $value->$subField ?? null;
            }

            if($column instanceof FormTableActionsColumnInterface)
            {
                $item['is_deletable'] = is_callable($this->isDeletableCallback) ? call_user_func($this->isDeletableCallback,$relationItem) : $this->isDeletable;
                $item['is_editable'] = is_callable($this->isEditableCallback) ? call_user_func($this->isEditableCallback,$relationItem) : $this->isEditable;
            }
            $column->setCmsModel($relationItem);
            $item['fields'][$column->field] = $column->setValue($value ?? '')->toArray();
            $item['index'] = 'item_' . $key;
            $this->items[$key] = $item;
        }
    }

    private function appendEmptyItemField(FormTableColumnInterface $column)
    {
        if(!$this->emptyItem)
        {
            $this->emptyItem = [
                'fields' => [],
                'id' => null,
                'relation' => $this->field
            ];
        }
        $this->emptyItem['fields'][$column->field] = $column->toArray();
        $this->emptyItem['is_deletable'] = $this->isDeletable;
        $this->emptyItem['is_editable'] =  $this->isEditable;
    }

    protected function getEmptyItem()
    {
        return $this->emptyItem;
    }

    protected function getTargetIds($requestFieldId)
    {
        return request()->$requestFieldId;
    }

    /**
     * @param string $requestField
     * @param $order
     * @return mixed|null
     */
    protected function getRequestField($requestField, $order)
    {
        return request()->$requestField[$order] ?? null;
    }

    /**
     * @param string $relationClass
     * @param array $targetIds
     */
    protected function deleteRemovedItems($relationClass, $targetIds)
    {
        if(is_array($targetIds) && count($targetIds)){
            try {
                DB::beginTransaction();

                $query = $relationClass::whereIn('id',$targetIds);
                if($relationClass instanceof CmsRelatedModelInterface)
                {
                    $query->where($relationClass::getForeignField(), $this->cmsModel->id);
                }

                $relModels = $query->get();

                foreach ($this->components->getItems() as $component)
                {
                    if($component instanceof FormTableColumnHasFileInterface){
                        foreach ($relModels as $model){
                            $component->setRelationCmsModel($model);
                            $component->deleteFile($component->getRelationCmsModelValue());
                        }
                    }
                }

                $query->delete();

                DB::commit();
            }catch (\Exception $exception){
                DB::rollBack();
            }
        }
    }

    /**
     * @param string $relationClass
     * @return CmsModelInterface
     */
    protected function getNewRelationModel($relationClass) : CmsModelInterface
    {
        if($this->hasCustomModel())
        {
            return new $this->customModelClass;
        }

        $foreignField = $relationClass::getForeignField();
        $primaryField = $this->getModel()::getPrimaryField();

        $item = new $relationClass;
        $item->$foreignField = $this->getModel()->$primaryField;
        return $item;
    }

    protected function getExistedRelationItems(string $relation)
    {
        return $this->getModel()->$relation->keyBy('id');
    }

    /**
     * @return callable
     */
    public function getStoreTableDataCallback() : callable
    {
        return function (){

            if(!$this->getModel() instanceof CmsModelInterface) return;

            $relation = $this->field;

            if($this->hasCustomModel())
            {
                $relationClass = $this->customModelClass;
                $relationItems = $this->customModelClass::get()->keyBy('id');
            }
            elseif(isset($this->getModel()->$relation)) {
                $relationClass = $this->getModel()->$relation()->getRelated();
                if(!$relationClass instanceof CmsRelatedModelInterface) {
                    throw new NotCmsRelatedModelException();
                }
                $relationItems = $this->getExistedRelationItems($relation);
            }
            else{
                throw new ClassNotFoundException();
            }

            $requestFieldId = $relation . '_id';

            $this->requestArr = request()->toArray();

            $deletedField = 'deleted_' . $this->field_name;

            if($this->isDeletable && array_key_exists($deletedField,$this->requestArr)) {
                $this->deleteRemovedItems($relationClass,$this->getTargetIds($deletedField));
            };

            if(!array_key_exists($requestFieldId,$this->requestArr)) return;

            $targetIds = $this->getTargetIds($requestFieldId);

            $columns = $fields = $requestFieldsNames = [];

            foreach ($this->components->getItemsArray() as $column)
            {
                if(!$column instanceof FormTableColumnInterface) continue;

                if($this->hasCustomModel())
                {
                    $column->setFieldPrefix($relation);
                }
                else{
                    $column->setRelationName($relation);
                    $column->setCmsModel($this->getModel());
                }
                $columns[] = $column;
                $field = $column->field;
                $fields[] = $field;
                $requestFieldsNames[] = $this->field . '_' . $field;
            }

            $newItemIndex = 0;
            foreach ($targetIds as $order => $id)
            {
                if($id != null && intval($id) == 0) continue;

                $isNewItem = !isset($relationItems[$id]);

                $item = $isNewItem ? $this->getNewRelationModel($relationClass) : $relationItems[$id];

                $subRelationComponents = [];

                foreach ($fields as $key => $field)
                {
                    $column = $columns[$key] ?? null;
                    if(!$column instanceof FormTableColumnInterface) continue;
                    if($this->hasCustomModel()){
                        $column->setCmsModel($item);
                    }
                    $column->setRelationCmsModel($item);
                    $column->setOrderRequestField($order);
                    $requestField = $requestFieldsNames[$key] ?? null;

                    $subFields = explode('.',$field);

                    if(count($subFields) > 1){
                        $column->setRelationName($this->getField());
                        $subRelationComponents[$subFields[0]][] = $column;
                        continue;
                    }
                    $item->$field = $column->getPreparedValue($this->getRequestField($requestField,$order));
                }
                $item->order = $order;

                $model = $this->getModel();
                $primaryField = $model::getPrimaryField();
                if($model instanceof Model && empty($model->$primaryField) && $isNewItem){
                    $model::saved(function ($model) use ($item,$isNewItem,$subRelationComponents,$order,$newItemIndex){

                        $foreignField = $item::getForeignField();
                        $primaryField = $model::getPrimaryField();
                        $item->$foreignField = $model->$primaryField;
                        $item->save();
                        $this->storeAdditionalData($item,$isNewItem,$newItemIndex);
                        $this->recursiveStoreRelations($item,$subRelationComponents,1,$order);
                    });
                    $newItemIndex++;
                }
                else{
                    $item->save();
                    $this->storeAdditionalData($item,$isNewItem,($isNewItem ? $newItemIndex++ : 0));
                    $this->recursiveStoreRelations($item,$subRelationComponents,1,$order);
                }
            }
            return true;
        };
    }

    protected function storeAdditionalData($item,$isNewItem,$id=0)
    {
        return true;
    }

    private function recursiveStoreRelations(CmsModelInterface $model, array $components, int $level = 1,int $order = 0)
    {
        foreach ($components as $relation => $relComponents)
        {
            try {
                $relationModel = $model->$relation ?? $model->$relation()->getRelated();
                if(!$relationModel instanceof CmsModelInterface) throw new NotCmsModelException();
                $subRelationComponents = [];
                foreach ($relComponents as $item)
                {
                    $field = $item->getField();
                    $subFields = explode('.',$field);
                    if(count($subFields) > ($level + 1))
                    {
                        if(isset($subFields[$level])) {
                            $subRelationComponents[$subFields[$level]][] = $item;
                        }
                        continue;
                    }
                    if(!isset($subFields[$level])) throw new CoreException();
                    $name = $subFields[$level];
                    $requestField = $item->getRelationName() . '_' . implode('_',$subFields);
                    $item->setRelationCmsModel($model);
                    $item->setOrderRequestField($order);
                    $relationModel->$name = $item->getPreparedValue($this->getRequestField($requestField,$order));
                }

                if($model->$relation) {
                    $relationModel->save();
                }
                else $model->$relation()->save($relationModel);

                $this->recursiveStoreRelations($relationModel,$subRelationComponents,$level+1,$order);
            }
            catch (\Exception $exception){}
        }
    }

}
