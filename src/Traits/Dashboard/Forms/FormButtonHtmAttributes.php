<?php

namespace VmdCms\CoreCms\Traits\Dashboard\Forms;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Buttons\FormButtonInterface;
use VmdCms\CoreCms\Contracts\DTO\HtmlAttributesDTOInterface;
use VmdCms\CoreCms\Traits\Dashboard\HasHtmlAttributes;

trait FormButtonHtmAttributes
{
    use HasHtmlAttributes;

    /**
     * @param HtmlAttributesDTOInterface $dto
     */
    protected function setDefaultAttributes(HtmlAttributesDTOInterface $dto): void
    {
        $dto->setClass('component','form-button');
    }

    /**
     * @param string $class
     * @param string $element
     * @return FormButtonInterface
     */
    public function addClass(string $class, string $element = 'component'): FormButtonInterface
    {
        $this->htmlAttributes->appendClass($element, $class);
        return $this;
    }

}
