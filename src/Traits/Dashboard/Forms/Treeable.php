<?php

namespace VmdCms\CoreCms\Traits\Dashboard\Forms;

use VmdCms\CoreCms\Contracts\Models\TreeableInterface;
use VmdCms\CoreCms\Exceptions\Models\ClassNotFoundException;

trait Treeable
{
    use MultiValuable;

    private $primaryField;

    protected function getNameField() : string{
        return 'name';
    }

    /**
     * @return array
     * @throws ClassNotFoundException
     */
    private function prepareModelOptionData()
    {
        if(!class_exists($this->className)) throw new ClassNotFoundException();
        $model = $this->className ? new $this->className : null;
        if(!$model instanceof TreeableInterface) throw new ClassNotFoundException();

        $this->primaryField = $model::getPrimaryField();
        $options = [];
        $items = $model::tree()->get();
        if(!$items || !count($items)) return $options;

        foreach ($items as $item) {
            $options[] = $this->recursiveGetOptions($item);
        }
        return $options;
    }

    private function recursiveGetOptions(TreeableInterface $model) : array
    {
        $primaryField = $this->primaryField;
        $option = [
            'id' => $model->$primaryField ?? null,
            $this->getNameField() => $model->info->title ?? null,
        ];
        $children = $model->childrenTree ?? null;
        if($children && count($children))
        {
            foreach ($children as $child)
            {
                $option['children'][] = $this->recursiveGetOptions($child);
            }
        }
        return $option;
    }

    /**
     * @param null|bool|int|string $value
     * @return string|bool|int|null
     */
    protected function mutateValue($value = null)
    {
        return $value ? explode(',',$value) : [];
    }
}
