<?php

namespace VmdCms\CoreCms\Traits\Dashboard\Forms;

use Illuminate\Support\Collection;
use VmdCms\CoreCms\Collections\FormComponentsCollection;
use VmdCms\CoreCms\Contracts\Collections\FormComponentsCollectionInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormComponentInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\HasComponentsInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;

trait HasComponents
{
    /**
     * @var FormComponentsCollection
     */
    protected $components;

    /**
     * @return null|CmsModelInterface
     */
    public abstract function getCmsModel(): ?CmsModelInterface;

    /**
     * @param array $components
     * @return HasComponentsInterface
     */
    public function setComponents(array $components) : HasComponentsInterface
    {
        if(empty($this->components)) $this->components = new FormComponentsCollection();

        foreach ($components as $component)
        {
            if($component instanceof FormComponentInterface) {
                $this->components->appendItem($component);
            }
        }
        return $this;
    }

    /**
     * @return Collection
     */
    public function getComponents(): Collection
    {
        return $this->getComponentsCollection()->getItems();
    }

    /**
     * @return FormComponentsCollectionInterface
     */
    public function getComponentsCollection(): FormComponentsCollectionInterface
    {
        if($this->getCmsModel()) {
            foreach ($this->components->getItems() as $component)
            {
                $component->setCmsModel($this->getCmsModel());
            }
        }
        return $this->components;
    }
}
