<?php

namespace VmdCms\CoreCms\Traits\Dashboard\Forms;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormComponentInterface;
use VmdCms\CoreCms\Contracts\DTO\HtmlAttributesDTOInterface;
use VmdCms\CoreCms\Traits\Dashboard\HasHtmlAttributes;

trait FormComponentHtmAttributes
{
    use HasHtmlAttributes;

    /**
     * @param HtmlAttributesDTOInterface $dto
     */
    protected function setDefaultAttributes(HtmlAttributesDTOInterface $dto): void
    {
        $dto->setClass('container','form-component');
    }

    /**
     * @return FormComponentInterface
     */
    public function wide(): FormComponentInterface
    {
        $this->htmlAttributes->appendClass('container','wide');
        return $this;
    }

    /**
     * @return FormComponentInterface
     */
    public function bordered(): FormComponentInterface
    {
        $this->htmlAttributes->appendClass('container', 'bordered');
        return $this;
    }

    /**
     * @return FormComponentInterface
     */
    public function borderLeft(): FormComponentInterface
    {
        $this->htmlAttributes->appendClass('container', 'border-left');
        return $this;
    }

    /**
     * @return FormComponentInterface
     */
    public function borderRight(): FormComponentInterface
    {
        $this->htmlAttributes->appendClass('container', 'border-right');
        return $this;
    }

    /**
     * @return FormComponentInterface
     */
    public function borderTop(): FormComponentInterface
    {
        $this->htmlAttributes->appendClass('container', 'border-top');
        return $this;
    }

    /**
     * @return FormComponentInterface
     */
    public function borderBottom(): FormComponentInterface
    {
        $this->htmlAttributes->appendClass('container', 'border-bottom');
        return $this;
    }

    public function justify(string $direction): FormComponentInterface
    {
        $this->htmlAttributes->appendClass('container', 'd-flex justify-' . $direction);
        return $this;
    }

    /**
     * @param string $class
     * @param string $element
     * @return FormComponentInterface
     */
    public function addClass(string $class, string $element = 'container'): FormComponentInterface
    {
        $this->htmlAttributes->appendClass($element, $class);
        return $this;
    }

}
