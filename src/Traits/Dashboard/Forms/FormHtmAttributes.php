<?php

namespace VmdCms\CoreCms\Traits\Dashboard\Forms;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Contracts\DTO\HtmlAttributesDTOInterface;
use VmdCms\CoreCms\Traits\Dashboard\HasHtmlAttributes;

trait FormHtmAttributes
{
    use HasHtmlAttributes;

    /**
     * @param HtmlAttributesDTOInterface $dto
     */
    protected function setDefaultAttributes(HtmlAttributesDTOInterface $dto): void
    {
        $dto->setClass('form_body','form-content-body');
    }

    /**
     * @return FormInterface
     */
    public function wide(): FormInterface
    {
        $this->htmlAttributes->appendClass('form_body','max-width-100pr');
        return $this;
    }

    /**
     * @param string $class
     * @param string $element
     * @return FormInterface
     */
    public function addClass(string $class, string $element = 'form_body'): FormInterface
    {
        $this->htmlAttributes->appendClass($element, $class);
        return $this;
    }

}
