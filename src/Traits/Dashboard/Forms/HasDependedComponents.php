<?php

namespace VmdCms\CoreCms\Traits\Dashboard\Forms;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormComponentInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\HasDependedComponentsInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Input\DependedComponentInterface;

trait HasDependedComponents
{
    /**
     * @var DependedComponentInterface
     */
    protected $dependedComponent;

    public function setDependedComponent(DependedComponentInterface $dependedComponent) : HasDependedComponentsInterface
    {
        $this->dependedComponent = $dependedComponent;
        return $this;
    }

    /**
     * @return null|DependedComponentInterface
     */
    public function getDependedComponent() : ?DependedComponentInterface
    {
        return $this->dependedComponent;
    }

    /**
     * @return null|FormComponentInterface
     */
    public function getDependedFormComponent() : ?FormComponentInterface
    {
        if(!$this->dependedComponent instanceof DependedComponentInterface) return null;
        $field = str_replace('.','_',$this->getField());
        return $this->dependedComponent->getDependedFormComponentByKey(request()->$field ?? null);
    }
}
