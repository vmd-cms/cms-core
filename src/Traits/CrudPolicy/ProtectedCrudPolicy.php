<?php

namespace VmdCms\CoreCms\Traits\CrudPolicy;

use VmdCms\CoreCms\Models\CmsModel;

trait ProtectedCrudPolicy
{

    public function isCreatable(): bool
    {
        return $this->hasFullAccess();
    }

    public function isEditable(CmsModel $model = null): bool
    {
        return $this->hasFullAccess();
    }

    public function isDeletable(CmsModel $model = null): bool
    {
        return $this->hasFullAccess();
    }

    private function hasFullAccess()
    {
        return auth()->guard('moderator')->user()->has_full_access ?? false;
    }

}
