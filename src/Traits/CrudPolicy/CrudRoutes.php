<?php

namespace VmdCms\CoreCms\Traits\CrudPolicy;

use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Contracts\Sections\AdminSectionInterface;
use VmdCms\CoreCms\Services\CoreRouter;

trait CrudRoutes
{
    public abstract function getSection() : AdminSectionInterface;

    protected function getCreateRoute()
    {
        $create = $this->getSection()->isCreatable() ? $this->getRouteParams(CoreRouter::ROUTE_CREATE_GET) : null;
        if($create && $query = request()->query()) $create .= '?' . http_build_query(request()->query());
        return $create;
    }

    protected function getDataTableAsyncRoute()
    {
        return $this->getSection()->isDisplayable() ? $this->getRouteParams(CoreRouter::ROUTE_DATATABLE_ASYNC) : null;
    }

    protected function getViewRoute(CmsModelInterface $model)
    {
        return $this->getSection()->isViewable($model) ? $this->getRouteParams(CoreRouter::ROUTE_VIEW, $model) : null;
    }

    protected function getEditRoute(CmsModelInterface $model)
    {
        return $this->getSection()->isEditable($model) ? $this->getRouteParams(CoreRouter::ROUTE_EDIT_GET, $model) : null;
    }

    protected function getDeleteRoute(CmsModelInterface $model)
    {
        return $this->getSection()->isDeletable($model) ? $this->getRouteParams(CoreRouter::ROUTE_DELETE, $model) : null;
    }

    private function getRouteParams(string $routeName, CmsModelInterface $model = null)
    {
        $idParam = $model ? ['id' => $model->id] : [];
        return route($routeName, array_merge(['sectionSlug' => $this->getSection()->getSectionSlug()],$idParam),false);
    }

}
