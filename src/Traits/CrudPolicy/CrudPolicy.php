<?php

namespace VmdCms\CoreCms\Traits\CrudPolicy;

use VmdCms\CoreCms\Contracts\Dashboard\Display\CrudPolicyInterface;
use VmdCms\CoreCms\CoreModules\Moderators\Models\Moderator;
use VmdCms\CoreCms\CoreModules\Moderators\Models\ModeratorRolePermission;
use VmdCms\CoreCms\CoreModules\Sections\Models\CoreSection;
use VmdCms\CoreCms\Models\CmsModel;

trait CrudPolicy
{

    private $perms = [];

    private $creatable;

    private function checkPerms($action)
    {
        return $this->sectionCheckPerms(static::class,$action);
    }

    protected function sectionCheckPerms(string $sectionClass, string $action)
    {
        $moderator = auth()->guard('moderator')->user();
        if(!$moderator instanceof Moderator) return false;
        if($moderator->has_full_access ?? false) return true;

        if(!$this->perms)
        {
            $currentSection = CoreSection::where('core_section_class',$sectionClass)->first();
            if(!$currentSection instanceof CoreSection) return false;
            $this->perms = ModeratorRolePermission::where('moderator_role_id',$moderator->moderator_role_id)
                ->where('core_section_id',$currentSection->id)->first();
        }
        return $this->perms->$action ?? false;
    }

    public function isDisplayable(): bool
    {
        return $this->checkPerms('display_perm');
    }

    /**
     * @inheritDoc
     */
    public function isCreatable(): bool
    {
        return is_bool($this->creatable) ? $this->creatable : $this->checkPerms('create_perm');
    }

    /**
     * @param bool $flag
     * @return CrudPolicyInterface
     */
    public function setCreatable(bool $flag): CrudPolicyInterface
    {
        $this->creatable = $flag;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function isViewable(CmsModel $model = null): bool
    {
        return $this->checkPerms('view_perm');
    }

    /**
     * @inheritDoc
     */
    public function isEditable(CmsModel $model = null): bool
    {
        return $this->checkPerms('edit_perm');
    }

    /**
     * @inheritDoc
     */
    public function isDeletable(CmsModel $model = null): bool
    {
        return $this->checkPerms('delete_perm');
    }

}
