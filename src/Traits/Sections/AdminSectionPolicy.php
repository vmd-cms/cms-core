<?php

namespace VmdCms\CoreCms\Traits\Sections;

use VmdCms\CoreCms\Services\CoreRouter;
use VmdCms\CoreCms\Traits\CrudPolicy\CrudPolicy;

trait AdminSectionPolicy
{
    use CrudPolicy;

    /**
     * @inheritDoc
     */
    public function isAvailableAction($action): bool
    {
        switch ($action){
            case CoreRouter::ROUTE_DISPLAY : return $this->isDisplayable();
            case CoreRouter::ROUTE_DATATABLE_ASYNC : return $this->isDisplayable();
                break;
            case CoreRouter::ROUTE_CREATE_GET:
            case CoreRouter::ROUTE_CREATE_POST: return $this->isCreatable();
                break;
            case CoreRouter::ROUTE_VIEW: return $this->isViewable();
                break;
            case CoreRouter::ROUTE_DISPLAY_POST:
            case CoreRouter::ROUTE_UPDATE_TREE_ORDER:
            case CoreRouter::ROUTE_UPDATE_TABLE_ORDER:
            case CoreRouter::ROUTE_UPDATE_TABLE_ITEM:
            case CoreRouter::ROUTE_SERVICE_DATA:
            case CoreRouter::ROUTE_SERVICE_DATA_GET:
            case CoreRouter::ROUTE_EDIT_GET:
            case CoreRouter::ROUTE_EDIT_PUT: return $this->isEditable($this->cmsModelClass::find(request()->id));
                break;
            case CoreRouter::ROUTE_RESTORE:
            case CoreRouter::ROUTE_DELETE:
            case CoreRouter::ROUTE_DELETE_SOFT: return $this->isDeletable($this->cmsModelClass::find(request()->id));
                break;
            case CoreRouter::ROUTE_XMLHTTP_CK_FILE:
            case CoreRouter::ROUTE_XMLHTTP_CK_FILES_BROWSE: return true;
                break;
            default: return false;
        }
    }
}
