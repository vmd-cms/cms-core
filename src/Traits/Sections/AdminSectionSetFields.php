<?php

namespace VmdCms\CoreCms\Traits\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormComponentInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\Modules\Orders\Models\OrderItem;

trait AdminSectionSetFields
{
    private function setComponentValue(FormComponentInterface $component, CmsModelInterface $cmsModel = null)
    {
        $field = $component->getField();
        $subFields = explode('.',$field);
        $value = $cmsModel;
        foreach ($subFields as $item)
        {
            $value = $value->$item ?? '';
        }
        $component->setValue($value);
        if($cmsModel instanceof CmsModelInterface) $component->setCmsModel($cmsModel);
        return $component->toArray();
    }
}
