<?php

namespace VmdCms\CoreCms\Traits\Sections;

use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\CoreModules\Events\Entity\Event;
use VmdCms\CoreCms\CoreModules\Events\Enums\CoreEventEnums;
use VmdCms\CoreCms\CoreModules\Moderators\Entity\AuthEntity;

trait AdminSectionEvents
{
    protected $onActionEvents = [
          'created' => CoreEventEnums::ADMIN_CREATE,
          'viewed' => CoreEventEnums::ADMIN_VIEW,
          'edited' => CoreEventEnums::ADMIN_EDIT,
          'deleted' => CoreEventEnums::ADMIN_DELETE
    ];

    /**
     * @param CmsModelInterface $model
     */
    public function onCreated(CmsModelInterface $model): void
    {
        if($slug = $this->getOnActionEventsSlug('created'))
        {
            $resourceId = is_int($model->getPrimaryValue()) ? $model->getPrimaryValue() : ($model->id ?? null);
            Event::dispatch($slug,[
                Event::PAYLOAD_MODERATOR_ID => AuthEntity::getAuthModeratorId(),
                Event::PAYLOAD_RESOURCE_ID => $resourceId,
                Event::PAYLOAD_RESOURCE_SLUG => $this->getSectionSlug()
            ]);
        }
    }

    /**
     * @param string|int|null $modelId
     */
    public function onViewed($modelId): void
    {
        if($slug = $this->getOnActionEventsSlug('viewed'))
        {
            Event::dispatch($slug,[
                Event::PAYLOAD_MODERATOR_ID => AuthEntity::getAuthModeratorId(),
                Event::PAYLOAD_RESOURCE_ID => $modelId,
                Event::PAYLOAD_RESOURCE_SLUG => $this->getSectionSlug()
            ]);
        }
    }

    /**
     * @param CmsModelInterface $model
     */
    public function onEdited(CmsModelInterface $model): void
    {
        if($slug = $this->getOnActionEventsSlug('edited'))
        {
            $changes = [];
            $modelChanges = $model->getChanges();
            foreach ($modelChanges as $field=>$value){
                if($field === 'updated_at') continue;
                $changes['model'][$field] = [
                    'original' => $model->getOriginal($field),
                    'changed' => $value,
                ];
            }

            foreach ($model->getRelations() as $key=>$relation){
                if(!$relation instanceof CmsModelInterface) continue;
                $relationChanges = $relation->getChanges();
                foreach ($relationChanges as $field=>$value){
                    if($field === 'updated_at') continue;
                    $changes[$key][$field] = [
                        'original' => $relation->getOriginal($field),
                        'changed' => $value,
                    ];
                }
            }

            $resourceId = is_int($model->getPrimaryValue()) ? $model->getPrimaryValue() : ($model->id ?? null);
            Event::dispatch($slug,[
                Event::PAYLOAD_MODERATOR_ID => AuthEntity::getAuthModeratorId(),
                Event::PAYLOAD_RESOURCE_ID => $resourceId,
                Event::PAYLOAD_RESOURCE_SLUG => $this->getSectionSlug(),
                Event::PAYLOAD_CHANGES => $changes
            ]);
        }
    }

    /**
     * @param CmsModelInterface $model
     */
    public function onDeleted(CmsModelInterface $model): void
    {
        if($slug = $this->getOnActionEventsSlug('deleted'))
        {
            Event::dispatch($slug,[
                Event::PAYLOAD_MODERATOR_ID => AuthEntity::getAuthModeratorId(),
                Event::PAYLOAD_RESOURCE_ID => $model->getPrimaryValue(),
                Event::PAYLOAD_RESOURCE_SLUG => $this->getSectionSlug()
            ]);
        }
    }

    /**
     * @param string $slug
     * @return string|null
     */
    protected function getOnActionEventsSlug(string $slug): ?string
    {
        return $this->onActionEvents[$slug] ?? null;
    }
}
