<?php

namespace VmdCms\CoreCms\Traits\Sections;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Panels\SectionPanelInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Contracts\Models\SoftDeletableInterface;
use VmdCms\CoreCms\Contracts\Sections\AdminSectionInterface;
use VmdCms\CoreCms\Dashboard\Display\DataTable;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormButton;
use VmdCms\CoreCms\Exceptions\CoreException;
use VmdCms\CoreCms\Exceptions\Dashboard\DisplayException;
use VmdCms\CoreCms\Exceptions\Files\FileWrongFormatException;
use VmdCms\CoreCms\Exceptions\Models\ModelNotFoundException;
use VmdCms\CoreCms\Exceptions\Sections\SectionCmsModelException;
use VmdCms\CoreCms\Exceptions\Services\ReplicateException;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Services\CoreRouter;
use VmdCms\CoreCms\Services\Files\StoreFile;
use VmdCms\CoreCms\Services\Replicate;
use VmdCms\CoreCms\Services\Responses\ApiResponse;
use VmdCms\CoreCms\Traits\Dashboard\Sections\SectionButtons;

trait AdminSectionResource
{
    use AdminSectionEvents,SectionButtons,AdminSectionStoreFields;

    private $dashboardViewPath = 'vmd_cms::admin.views.dashboard';

    protected abstract function getCurrentSection() : AdminSectionInterface;

    /**
     * @return CmsModelInterface
     * @throws SectionCmsModelException
     */
    protected function getCmsModel() : CmsModelInterface
    {
        if(!$this->cmsModelClass || !($model = new $this->cmsModelClass) instanceof CmsModelInterface)
            throw new SectionCmsModelException();
        return $model;
    }

    /**
     * @return DisplayInterface
     */
    public function display()
    {
        return new DataTable();
    }

    /**
     * @return string
     * @throws \Throwable
     */
    final public function renderDisplay(): string
    {
        $display = $this->display();

        if($display instanceof DisplayInterface)
        {
            return $display->setSection($this->getCurrentSection())
            ->setCmsModel($this->getCmsModel())->render();
        }

        return $display;
    }

    final public function storeDisplay(Request $request)
    {
        $display = $this->display();
        if(!$display instanceof FormInterface) throw new CoreException();
        $model = $this->getCmsModel();
        $components = $display->setCmsModel($model)->getMutatedComponents();
        $this->storeProperties($model,$components);
        return redirect(CoreRouter::getDisplayRoute($this->slug));
    }

    /**
     * @param int $id
     * @return FormInterface
     */
    public function view(int $id) : FormInterface
    {
        return $this->edit($id);
    }

    /**
     * @param int $id
     * @return string
     * @throws \Throwable
     */
    public function renderView(int $id) : string
    {
        $dashboard = $this->view($id);
        if(!$dashboard instanceof DisplayInterface)
        {
            throw new DisplayException();
        }
        $this->onViewed($id);
        return $dashboard->render();
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return new Form();
    }

    /**
     * @var SectionPanelInterface[]
     */
    protected $sectionPanels;

    /**
     * @return string
     * @throws \Throwable
     */
    final public function renderCreate(): string
    {
        $action = route(CoreRouter::ROUTE_CREATE_POST,['sectionSlug' => $this->getSectionSlug()]);
        $dashboard = $this->create()
            ->setAction($action)
            ->setCmsModel($this->getCmsModel())
            ->setMethod('POST')
            ->setSection($this->getCurrentSection())
            ->setButtons($this->getCreateButtons($this))
            ->setSectionSlug($this->getSectionSlug());
        if(!$dashboard instanceof DisplayInterface)
        {
            throw new DisplayException();
        }
        return $dashboard->render();
    }

    final public function storeCreate(Request $request)
    {
        $model = $this->getCmsModel();
        $components = $this->create()->getMutatedComponents();
        $this->storeProperties($model,$components);
        $this->onCreated($model);
        return redirect($this->getRouteByButtonAction($request->submit_action, $model));
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return new Form();
    }

    /**
     * @param int|null $id
     * @return string
     * @throws \Throwable
     */
    final public function renderEdit(?int $id): string
    {
        $model = $this->getCmsModel()::find($id);
        $action = route(CoreRouter::ROUTE_EDIT_PUT,['sectionSlug' => $this->getSectionSlug(), 'id' => $id]);
        $dashboard = $this->edit($id)
            ->setCmsModel($model)
            ->setAction($action)
            ->setMethod('PUT')
            ->setSection($this->getCurrentSection())
            ->setSectionSlug($this->getSectionSlug())
            ->setButtons($this->getEditButtons($this,$model));
        if(!$dashboard instanceof DisplayInterface)
        {
            throw new DisplayException();
        }
        if(is_countable($this->sectionPanels)){
            $dashboard->setSectionPanels($this->sectionPanels);
        }
        return $dashboard->render();
    }

    final public function storeEdit(Request $request)
    {
        $model = $this->getCmsModel()::find($request->id);

        if($request->submit_action == FormButton::ACTION_COPY){

            try {
                $model = (new Replicate())->replicateModel($model);
            }
            catch (ReplicateException $exception){}
            return redirect($this->getRouteByButtonAction(FormButton::ACTION_COPY, $model));

        }

        $components = $this->edit($request->id)->setCmsModel($model)->getMutatedComponents();

        $this->storeProperties($model,$components);
        $this->onEdited($model);
        return redirect($this->getRouteByButtonAction($request->submit_action, $model));
    }

    final public function storeCkeditorUploadFile(Request $request): ?string
    {
        $model = $this->getCmsModel()::find($request->id);

        if(!$model instanceof CmsModelInterface){
            throw new ModelNotFoundException('Model not found with id#' . $request->id);
        }

        $file = request()->file()['upload'] ?? null;

        if(!$file instanceof UploadedFile) {
            throw new FileWrongFormatException('Wrong file format');
        }

        $service = new StoreFile();
        return $service->storeFile($service->getFolderPathByCmsModel($model) . '/ckeditor',$file);
    }

    final public function getCkeditorUploadedFiles(Request $request): array
    {
        $model = $this->getCmsModel()::find($request->id);

        if(!$model instanceof CmsModelInterface){
            throw new ModelNotFoundException('Model not found with id#' . $request->id);
        }

        $service = new StoreFile();
        return $service->getFilesPathesInFolder($service->getFolderPathByCmsModel($model) . '/ckeditor');
    }

    final public function delete(int $id)
    {
        $model = $this->getCmsModel()::find($id);
        if(!$model || !$this->isDeletable($model)) throw new \Exception();
        $this->onDeleted($model);
        $model->delete();
        return $this->deleteResponse($model);

    }

    final public function restore(int $id)
    {
        $model = $this->getCmsModel()::find($id);
        if(!$model || !$this->isDeletable($model) || !$model instanceof SoftDeletableInterface) throw new \Exception();
        $model->deleted = false;
        $model->save();
        return $this->restoreResponse($model);
    }

    /**
     * @param string $action
     * @param CmsModel|null $model
     * @return string
     * @throws \Throwable
     */
    private function getRouteByButtonAction(string $action, CmsModel $model = null)
    {
        switch ($action)
        {
            case FormButton::ACTION_COPY :
            case FormButton::ACTION_SAVE :
                return route(CoreRouter::ROUTE_EDIT_GET,['sectionSlug' => $this->getSectionSlug(),'id' => $model->id ?? null,'submit_action'=>'saved']);
                break;
            case FormButton::ACTION_SAVE_CREATE :
                return route(CoreRouter::ROUTE_CREATE_GET,['sectionSlug' => $this->getSectionSlug()]);
                break;
            default: return route(CoreRouter::ROUTE_DISPLAY,['sectionSlug' => $this->getSectionSlug()],false);
                break;
        }
    }

    /**
     * @param CmsModelInterface $model
     * @return mixed
     */
    public function deleteResponse(CmsModelInterface $model){
        return ApiResponse::success([
            'message' => 'Successfully deleted',
            'location_redirect' => CoreRouter::getDisplayRoute($this->getSectionSlug())
        ]);
    }

    /**
     * @param CmsModelInterface $model
     * @return mixed
     */
    public function restoreResponse(CmsModelInterface $model){
        return ApiResponse::success(['message' => 'Successfully restored', 'location_reload' => true]);
    }
}
