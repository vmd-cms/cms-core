<?php

namespace VmdCms\CoreCms\Traits\Sections;

use Illuminate\Support\Collection;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Exceptions\CoreException;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Services\Logger;

trait AdminSectionStoreFields
{
    /**
     * @var array
     */
    private $requestArr;

    private function storeProperties(CmsModelInterface $model, Collection $components)
    {
        $this->requestArr = request()->toArray();
        $relationComponents = [];
        foreach ($components as $key=>$item)
        {
            $item->setCmsModel($model);

            $afterSaveCallback = $item->getAfterSaveCallback();

            if($callable = $item->getBeforeSaveCallback())
            {
                call_user_func($callable,$model);
            }

            if(is_callable($afterSaveCallback) && $model instanceof CmsModel)
            {
                $model::saved($afterSaveCallback);
            }

            if($callable = $item->getStoreCallback())
            {
                call_user_func($callable,$model);

                if(is_callable($afterSaveCallback) && !$model instanceof CmsModel)
                {
                    call_user_func($afterSaveCallback,$model);
                }

                continue;
            }

            try {
                $field = $item->getField();
                $subFields = explode('.',$field);
                if(count($subFields) > 1)
                {
                    $relationComponents[$subFields[0]][] = $item;
                    continue;
                }
                if(!array_key_exists($field,$this->requestArr)) {
                    continue;
                }
                $model->$field = $item->getPreparedValue(request()->$field);
            }
            catch (\Exception $e){

            }
        }
        try {
            $model->save();
        }
        catch (\Exception $exception){
            $logger = new Logger();
            $logger->error(__METHOD__);
            $logger->error($exception->getMessage());
            return;
        }
        $this->recursiveStoreRelations($model,$relationComponents);
    }

    private function recursiveStoreRelations(CmsModelInterface $model, array $components, int $level = 1)
    {
        foreach ($components as $relation => $relComponents)
        {
            try {
                if(!$model->$relation instanceof CmsModelInterface) {
                    $model->load($relation);
                }
                $relationModel = $model->$relation ?? $model->$relation()->getRelated();
                if(!$relationModel instanceof CmsModelInterface) throw new NotCmsModelException();

                $subRelationComponents = [];
                foreach ($relComponents as $item)
                {
                    $field = $item->getField();
                    $subFields = explode('.',$field);

                    if(count($subFields) > ($level + 1))
                    {
                        if(isset($subFields[$level])) {
                            $subRelationComponents[$subFields[$level]][] = $item;
                        }
                        continue;
                    }
                    if(!isset($subFields[$level])) throw new CoreException();
                    $name = $subFields[$level];

                    $requestField = implode('_',$subFields);
                    if(!array_key_exists($requestField,$this->requestArr)) {
                        continue;
                    }
                    $relationModel->$name = $item->getPreparedValue(request()->$requestField);
                }

                if($model->$relation) {
                    $relationModel->save();
                }
                else {
                    $model->$relation()->save($relationModel);
                }

                $this->recursiveStoreRelations($relationModel,$subRelationComponents,$level+1);
            }
            catch (\Exception $exception){
                $logger = new Logger();
                $logger->error(__METHOD__);
                $logger->error($exception->getMessage());
            }
        }
    }
}
