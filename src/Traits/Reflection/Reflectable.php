<?php


namespace VmdCms\CoreCms\Traits\Reflection;


trait Reflectable
{
    /**
     * @var \ReflectionObject
     */
    protected $reflectionObject;

    /**
     * @return \ReflectionObject
     */
    protected function getReflectionObject()
    {
        if(!$this->reflectionObject instanceof \ReflectionObject){
            $this->reflectionObject = new \ReflectionObject($this);
        }
        return $this->reflectionObject;
    }

    protected function getCurrentNamespace()
    {
        return $this->getReflectionObject()->getNamespaceName();
    }

    /**
     * @return string
     */
    protected function getCurrentDir()
    {
        return dirname( $this->getReflectionObject()->getFileName(),1);
    }
}
