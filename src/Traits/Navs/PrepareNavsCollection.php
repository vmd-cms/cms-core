<?php

namespace VmdCms\CoreCms\Traits\Navs;

use Illuminate\Database\Eloquent\Collection;
use VmdCms\CoreCms\Collections\NavsCollection;
use VmdCms\CoreCms\Contracts\Collections\NavsCollectionInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Contracts\Models\TreeableInterface;
use VmdCms\CoreCms\DTO\NavDTO;

trait PrepareNavsCollection
{
    /**
     * @param CmsModelInterface $model
     * @return string|null
     */
    protected function getNavIconPath(CmsModelInterface $model): ?string
    {
        return null;
    }

    /**
     * @param Collection $items
     * @return NavsCollectionInterface
     */
    protected function prepareCollection(Collection $items): NavsCollectionInterface
    {
        $links = new NavsCollection();
        foreach ($items as $item)
        {
            $navDto = new NavDTO($item->info->title ?? '', $item->urlPath ?? '');
            if($iconPath = $this->getNavIconPath($item)){
                $navDto->setIcon($iconPath);
            }
            if($item instanceof TreeableInterface && isset($item->children) && count($item->children))
            {
                $navDto->setChildren($this->prepareCollection($item->children));
            }
            $links->append($navDto);
        }
        return $links;
    }
}
