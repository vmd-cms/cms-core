<?php

namespace VmdCms\CoreCms\Dashboard\Forms;

use Illuminate\Support\Collection;
use VmdCms\CoreCms\Collections\FormComponentsCollection;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Filters\FilterableDisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\ColumnComponentInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormComponentInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\GroupComponentsInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\HasDependedComponentsInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\RowComponentInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Dashboard\Display\Display;
use VmdCms\CoreCms\Services\CoreRouter;
use VmdCms\CoreCms\Traits\Dashboard\Display\FilterableDisplay;
use VmdCms\CoreCms\Traits\Dashboard\Forms\FormHtmAttributes;
use VmdCms\CoreCms\Traits\Dashboard\Forms\HasComponents;
use VmdCms\CoreCms\Traits\Dashboard\Forms\HasFormButtons;
use VmdCms\CoreCms\Traits\Dashboard\HasCmsModel;

abstract class AbstractForm extends Display implements FormInterface,FilterableDisplayInterface
{
    use HasCmsModel,HasComponents,HasFormButtons,FormHtmAttributes,FilterableDisplay;

    /**
     * @var string
     */
    protected $action;
    protected $method;
    protected $token;
    protected $sectionSlug;
    protected $headTitle;
    protected $headTitleField;

    /**
     * AbstractForm constructor.
     */
    public function __construct()
    {
        $this->token = csrf_token();
        $this->components = new FormComponentsCollection();
        $this->initializeComponentHtmlAttributesDTO();
    }

    /**
     * @inheritDoc
     */
    public function setAction(string $action): FormInterface
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setMethod(string $method): FormInterface
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @param string $sectionSlug
     * @return FormInterface
     */
    public function setSectionSlug(string $sectionSlug): FormInterface
    {
        $this->sectionSlug = $sectionSlug;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function toArray()
    {
        return [
            'action' => $this->action,
            'method' => $this->method,
            'token' => $this->token,
            'buttons' => $this->getFormButtons(),
            'display_route' => CoreRouter::getDisplayRoute($this->sectionSlug),
            'head_panel' => $this->getHeadPanelData(),
            'type' => $this->formType(),
            'htmlAttributes' => $this->htmlAttributes->toArray(),
            'form_component_key'=> $this->getFormComponentKey()
        ];
    }

    protected abstract function getFormComponentKey(): string;

    protected function getHeadPanelData()
    {
        return [
            'display' => !empty($this->getHeadTitle()),
            'title' => $this->getHeadTitle(),
        ];
    }

    protected function formType()
    {
        return 'panel';
    }

    /**
     * @return Collection
     */
    public function getMutatedComponents() : Collection
    {
        $componentsCollection = $this->getComponentsCollection();

        $additionalFormComponents = $this->recursive($componentsCollection->getItems());

        if(count($additionalFormComponents))
        {
            foreach ($additionalFormComponents as $component)
            {
                $componentsCollection->appendItem($component);
            }
        }
        return $componentsCollection->getItems();
    }

    protected function recursive($componentCollectionItems, $level = 1){

        $additionalFormComponents = [];

        foreach ($componentCollectionItems as $item)
        {
            if($item instanceof HasDependedComponentsInterface)
            {
                $additionalFormComponents[] = $item;
                $component = $item->getDependedFormComponent();

                if($component instanceof FormComponentInterface) {
                    $additionalFormComponents[] = $component;
                }
            }
            elseif($item instanceof GroupComponentsInterface)
            {
                $components = $item->getComponentsCollection()->getItems();
                foreach ($components as $component)
                {
                    if($component instanceof FormComponentInterface) {
                        $additionalFormComponents[] = $component;
                    }
                }
            }
            elseif($item instanceof RowComponentInterface)
            {
                $columns = $item->getColumns()->getItems();
                $additionalFormComponents = array_merge(
                    $additionalFormComponents,
                    $this->recursive($columns,$level+1)
                );
            }
            elseif($item instanceof ColumnComponentInterface) {

                $components = $item->getComponentsCollection()->getItems();

                $additionalFormComponents = array_merge(
                    $additionalFormComponents,
                    $this->recursive($components,$level+1)
                );
            }
            elseif($level > 1 && $item instanceof FormComponentInterface){
                $additionalFormComponents[] = $item;
            }
        }
        return $additionalFormComponents;
    }

    /**
     * @param string $headTitle
     * @param string|null $field
     * @return $this|FormInterface
     */
    public function setHeadTitle(string $headTitle, string $field = null) : FormInterface
    {
        $this->headTitle = $headTitle;
        $this->headTitleField = $field;
        return $this;
    }

    /**
     * @return string|null
     */
    protected function getHeadTitle() : ?string
    {
        $title = $this->headTitle ? $this->headTitle : '';
        if(!empty($this->headTitleField))
        {
            $value = $this->cmsModel;
            $subFields = explode('.',$this->headTitleField);
            foreach ($subFields as $field)
            {
                $value = $value->$field ?? '';
            }
            $title .= $value;
        }
        return $title;
    }

    protected function getSectionPanelViewPath(): string
    {
        return 'vmd_cms::admin.views.sections.forms.form_wrapper';
    }

}
