<?php


namespace VmdCms\CoreCms\Dashboard\Forms\Tables;


use Illuminate\Support\Str;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Tables\ModalDataInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Tables\TableDataInterface;

class TableData implements TableDataInterface
{
    /**
     * @var array
     */
    private $headers;
    private $items;
    private $components;
    private $emptyItem;

    /**
     * @var bool
     */
    private $isDeletable;
    private $isCreatable;
    private $isEditable;
    private $isOrderable;

    /**
     * @var string
     */
    private $fieldGroupKey;
    private $info;

    /**
     * TableData constructor.
     * @param array $headers
     * @param null|array $items
     * @param array $components
     */
    public function __construct(array $headers,?array $items, array $components)
    {
        $this->headers = $headers;
        $this->items = $items ?? [];
        $this->components = $components;
        $this->isDeletable = true;
        $this->isCreatable = true;
        $this->isOrderable = true;
        $this->isEditable = false;
        $this->info = null;
    }


    /**
     * @param string|null $info
     * @return $this
     */
    public function setInfo(string $info = null)
    {
        $this->info = $info;
        return $this;

    }

    /**
     * @param bool $isDeletable
     * @return $this
     */
    public function setIsDeletable(bool $isDeletable): self
    {
        $this->isDeletable = $isDeletable;
        return $this;
    }

    /**
     * @param array $emptyItem
     * @return $this
     */
    public function setEmptyItem(array $emptyItem): self
    {
        $this->emptyItem = $emptyItem;
        return $this;
    }

    /**
     * @param bool $isCreatable
     * @return $this
     */
    public function setIsCreatable(bool $isCreatable): self
    {
        $this->isCreatable = $isCreatable;
        return $this;
    }

    /**
     * @param bool $isEditable
     * @return $this
     */
    public function setIsEditable(bool $isEditable): self
    {
        $this->isEditable = $isEditable;
        return $this;
    }

    /**
     * @param bool $isOrderable
     * @return $this
     */
    public function setIsOrderable(bool $isOrderable): self
    {
        $this->isOrderable = $isOrderable;
        return $this;
    }

    /**
     * @param string $fieldGroupKey
     * @return $this
     */
    public function setFieldGroupKey(string $fieldGroupKey): TableDataInterface
    {
        $this->fieldGroupKey = $fieldGroupKey;
        return $this;
    }
    /**
     * @inheritDoc
     */
    public function toArray()
    {
        return [
            'headers' => $this->headers,
            'items' => $this->items,
            'components' => $this->components,
            'is_deletable' => $this->isDeletable,
            'is_creatable' => $this->isCreatable,
            'is_editable' => $this->isEditable,
            'is_orderable' => $this->isOrderable,
            'empty_item' => $this->emptyItem,
            'info' => $this->info,
            'field_group_key' => $this->fieldGroupKey,
            'index' => 'table_data_'. Str::uuid()
        ];
    }
}
