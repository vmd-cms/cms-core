<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Tables\Components;

class ColumnColorPicker extends Column
{
    protected function getComponentKey(): string
    {
        return 'column-color-picker-component';
    }
}
