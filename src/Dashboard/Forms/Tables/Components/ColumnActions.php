<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Tables\Components;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Tables\FormTableActionsColumnInterface;
use VmdCms\CoreCms\Traits\Dashboard\Forms\HasComponents;

class ColumnActions extends Column implements FormTableActionsColumnInterface
{
    use HasComponents;

    protected function getComponentKey(): string
    {
        return 'column-actions-component';
    }

    /**
     * @inheritDoc
     */
    public function toArray()
    {
        return [
            'component' => $this->getComponentKey(),
            'field' => $this->getField(),
            'label' => $this->getLabel(),
            'components' => $this->components->setCmsModel($this->cmsModel)->toArray()
        ];
    }
}
