<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Tables\Components;

class ColumnDateTime extends Column
{
    protected function getComponentKey(): string
    {
        return 'column-date-time-component';
    }

    protected function getAdditionalData(): array
    {
        return [
            'format' => "YYYY-MM-DD HH:mm:ss"
        ];
    }

    protected function mutateValue($value = null)
    {
        return $value === '' ? null : $value;
    }
}
