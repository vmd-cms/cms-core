<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Tables\Components;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Input\SelectInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\SearchableComponentInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Traits\Dashboard\Components\SearchableComponent;

class ColumnAutocomplete extends Column implements SelectInterface, SearchableComponentInterface
{
    use SearchableComponent;

    /**
     * @var callable
     */
    protected $previewCallback;

    /**
     * @var callable
     */
    protected $linkCallback;

    /**
     * @param callable $previewCallback
     * @return ColumnAutocomplete
     */
    public function setPreviewCallback(callable $previewCallback): ColumnAutocomplete
    {
        $this->previewCallback = $previewCallback;
        return $this;
    }

    /**
     * @param callable $callback
     * @return ColumnAutocomplete
     */
    public function setLinkCallback(callable $callback): ColumnAutocomplete
    {
        $this->linkCallback = $callback;
        return $this;
    }

    protected function getComponentKey(): string
    {
        return 'column-autocomplete-component';
    }

    protected function getCustomOptionDataArr(CmsModelInterface $item): array
    {
        return [
            'preview' => is_callable($this->previewCallback) ? call_user_func($this->previewCallback,$item) : null,
            'link' => is_callable($this->linkCallback) ? call_user_func($this->linkCallback,$item) : null,
        ];
    }

}
