<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Tables\Components;

class ColumnNumeric extends Column
{
    protected function getComponentKey(): string
    {
        return 'column-input-component';
    }

    protected function mutateValue($value = null)
    {
        return $value ? intval($value) : 0;
    }
}
