<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Tables\Components\Actions;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormComponentInterface;
use VmdCms\CoreCms\Dashboard\Forms\Components\Component;

class DeleteAction extends Component implements FormComponentInterface
{
    protected function getComponentKey(): string
    {
        return 'delete-action-component';
    }
}
