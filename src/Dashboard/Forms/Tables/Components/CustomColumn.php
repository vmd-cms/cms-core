<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Tables\Components;

class CustomColumn extends Column
{
    protected $contentCallback;

    /**
     * @return string
     */
    public function getComponentKey(): string
    {
        return 'column-custom-component';
    }

    /**
     * @param callable $content
     * @return $this
     */
    public function setContentCallback(callable $content)
    {
        $this->contentCallback = $content;
        return $this;
    }

    /**
     * @return false|mixed|null
     */
    protected function getContent(){
        if(is_callable($this->contentCallback)){
            if(empty($this->cmsModel)) return null;
            return call_user_func($this->contentCallback,$this->cmsModel);
        }
        return null;
    }

    /**
     * @return array
     */
    public function getAdditionalData(): array
    {
        return [
            'content' => $this->getContent()
        ];
    }
}
