<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Tables\Components;

class ColumnText extends Column
{
    protected function getComponentKey(): string
    {
        return 'column-text-component';
    }
}
