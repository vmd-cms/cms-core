<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Tables\Components;

use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\CroppedDimensionDTOCollectionInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\CroppedDimensionDTOInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Tables\FormTableColumnHasFileInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Contracts\Models\HasMediaDimensionsInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\DTO\Dashboard\Components\CroppedDimensionDTOCollection;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;
use VmdCms\CoreCms\Services\Files\StoreFile;

class ColumnImage extends Column implements FormTableColumnHasFileInterface
{

    /**
     * @var StoreFile
     */
    protected $storeFile;

    /**
     * @var string
     */
    protected $helpText;
    protected $uploadText;
    protected $deleteText;

    /**
     * @var boolean
     */
    protected $isCopyToWEBP;

    /**
     * @var CroppedDimensionDTOCollectionInterface
     */
    protected $croppedDimensions;

    public function __construct(string $field, ?string $label = null)
    {
        parent::__construct($field, $label);
        $this->storeFile = new StoreFile();
        $this->setBeforeSave($this->storeFileCallback());
        $this->uploadText = CoreLang::get('upload_image');
        $this->deleteText = CoreLang::get('delete');
        $this->helpText = null;
        $this->isCopyToWEBP = false;
        $this->croppedDimensions = new CroppedDimensionDTOCollection();
    }

    /**
     * @param CroppedDimensionDTOCollectionInterface $croppedDimensions
     * @return ColumnImage
     */
    public function setCroppedDimensions(CroppedDimensionDTOCollectionInterface $croppedDimensions): ColumnImage
    {
        $this->croppedDimensions = $croppedDimensions;
        return $this;
    }

    /**
     * @param CroppedDimensionDTOInterface $croppedDimension
     * @return ColumnImage
     */
    public function appendCroppedDimension(CroppedDimensionDTOInterface $croppedDimension): ColumnImage
    {
        $this->croppedDimensions->appendItem($croppedDimension);
        return $this;
    }

    /**
     * @return ColumnImage
     */
    public function copyToWEBP(): ColumnImage
    {
        $this->isCopyToWEBP = true;
        return $this;
    }

    /**
     * @param string $helpText
     * @return $this
     */
    public function setHelpText(string $helpText): self
    {
        $this->helpText = $helpText;
        return $this;
    }

    /**
     * @param string $uploadText
     * @return $this
     */
    public function setUploadText(string $uploadText): self
    {
        $this->uploadText = $uploadText;
        return $this;
    }

    /**
     * @param string $deleteText
     * @return $this
     */
    public function setDeleteText(string $deleteText): self
    {
        $this->deleteText = $deleteText;
        return $this;
    }

    protected function getFieldFullName()
    {
        return $this->relationName ? $this->relationName . '.' . $this->field : $this->field;
    }

    protected function getFieldPrefix()
    {
        return $this->fieldPrefix . str_replace('.','_',$this->getFieldFullName());
    }

    /**
     * @return mixed
     */
    public function getFieldPath()
    {
        return $this->getFieldPrefix() . '_path';
    }

    /**
     * @return mixed
     */
    public function getFieldFile()
    {
        return $this->getFieldPrefix() . '_file';
    }

    /**
     * @return mixed
     */
    public function getFieldBase64()
    {
        return $this->getFieldPrefix() . '_base64';
    }

    protected function getComponentKey(): string
    {
        return "column-image-component";
    }

    public function deleteFile(string $value = null){

        if(!empty($value)){
            $this->storeFile->deleteFileByPath($value);
            if($this->relationCmsModel instanceof HasMediaDimensionsInterface){

                $this->relationCmsModel->deleteMediaDimensions($this->field);
            }
        }
    }

    protected function storeFileCallback()
    {
       return function (){

           if(!$this->cmsModel instanceof CmsModelInterface) throw new NotCmsModelException();

           if(!$this->relationCmsModel instanceof CmsModelInterface) throw new NotCmsModelException();

           $field = $this->field;
           $oldValue = $this->getRelationCmsModelValue();
           $fieldPath = $this->getFieldPath();
           $fieldFile = $this->getFieldFile();
           $fieldBase64 = $this->getFieldBase64();


           $file = request()->allFiles()[$fieldFile][$this->orderRequestField] ?? null;

           $requestArray = request()->toArray();

           if(empty($file) && $base64 = $requestArray[$fieldBase64][$this->orderRequestField] ?? null){
               try {
                   $fileData = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base64));
                   $tmpFilePath = sys_get_temp_dir() . '/' . Str::uuid()->toString();
                   file_put_contents($tmpFilePath, $fileData);
                   $tmpFile = new File($tmpFilePath);

                   $file = new UploadedFile(
                       $tmpFile->getPathname(),
                       $tmpFile->getFilename(),
                       $tmpFile->getMimeType(),
                       0,
                       true);
               }catch (\Exception $exception){
                   dd($exception->getMessage());
               }

           }

           $requestFieldPathArr = array_key_exists($fieldPath,$requestArray) ? $requestArray[$fieldPath] : null;
           $requestFieldPath = is_array($requestFieldPathArr) ?  $requestFieldPathArr[$this->orderRequestField] ?? null : null;

           if(!$file && $requestFieldPath === $oldValue) {
               return $oldValue;
           }

           $this->deleteFile($oldValue);

           if(!$requestFieldPath)
           {
               return null;
           }

           if(!$file instanceof UploadedFile) {
               return null;
           }
           $folder = $this->storeFile->getFolderPathByCmsModel($this->cmsModel) . '/' . $this->relationName;
           $storedPath = $this->storeFile->storeFile($folder,$file);

           if($this->relationCmsModel instanceof HasMediaDimensionsInterface){

               $this->relationCmsModel::saved(function (HasMediaDimensionsInterface $model) use ($storedPath,$field){
                   $model->setCroppedDimensions($this->croppedDimensions);
                   $model->setCopyToWEBP($this->isCopyToWEBP);
                   $model->storeMediaDimensions($storedPath,$field);
               });
           }

           return $storedPath;
       };
    }

    protected function getAdditionalData(): array
    {
        return [
            'help_text' => $this->helpText,
            'upload_text' => $this->uploadText,
            'delete_text' => $this->deleteText,
            'path' => $this->value,
            'src_preview' => $this->storeFile->getImagePath($this->value),
            'src_default' => $this->storeFile->getDefaultImage(),
            'field_path' => $this->getFieldPath(),
            'field_file' => $this->getFieldFile(),
            'field_base64' => $this->getFieldBase64(),
            'file' => null,
        ];
    }

}
