<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Tables\Components;

class ColumnTextArea extends Column
{
    protected function getComponentKey(): string
    {
        return 'column-text-area-component';
    }
}
