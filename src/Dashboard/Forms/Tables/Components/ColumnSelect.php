<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Tables\Components;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Input\SelectInterface;
use VmdCms\CoreCms\Traits\Dashboard\Forms\MultiValuable;

class ColumnSelect extends Column implements SelectInterface
{
    use MultiValuable;

    protected function getComponentKey(): string
    {
        return 'column-select-component';
    }
}
