<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Tables\Components;

use VmdCms\CoreCms\Contracts\Models\HasMediaDimensionsInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;

class ColumnFile extends ColumnImage
{
    /**
     * @var string
     */
    protected $downloadText;
    protected $previewFileIcon;
    protected $uploadFileIcon;

    /**
     * @var boolean
     */
    protected $isDownloadable;
    protected $isDeletable;

    public function __construct(string $field, ?string $label = null)
    {
        parent::__construct($field, $label);
        $this->uploadText = CoreLang::get('upload_file');
        $this->downloadText = CoreLang::get('download');
        $this->deleteText = CoreLang::get('delete');
        $this->helpText = null;
        $this->uploadFileIcon = '/files/vendor/vmd_cms/svg/icons/upload_file_icon.svg';
        $this->previewFileIcon = '/files/vendor/vmd_cms/svg/icons/preview_file_icon.svg';

    }

    /**
     * @param string|null $previewFileIcon
     * @return ColumnFile
     */
    public function setPreviewFileIcon(string $previewFileIcon = null): ColumnFile
    {
        $this->previewFileIcon = $previewFileIcon;
        return $this;
    }

    /**
     * @param string|null $uploadFileIcon
     * @return ColumnFile
     */
    public function setUploadFileIcon(string $uploadFileIcon = null): ColumnFile
    {
        $this->uploadFileIcon = $uploadFileIcon;
        return $this;
    }

    /**
     * @param bool $isDownloadable
     * @return ColumnFile
     */
    public function setIsDownloadable(bool $isDownloadable): ColumnFile
    {
        $this->isDownloadable = $isDownloadable;
        return $this;
    }

    /**
     * @param bool $isDeletable
     * @return ColumnFile
     */
    public function setIsDeletable(bool $isDeletable): ColumnFile
    {
        $this->isDeletable = $isDeletable;
        return $this;
    }

    protected function getComponentKey(): string
    {
        return "column-file-component";
    }

    protected function getAdditionalData(): array
    {
        return array_merge(parent::getAdditionalData(),[
            'download_text' => $this->downloadText,
            'is_downloadable' => is_bool($this->isDownloadable) ? $this->isDownloadable : !empty($this->value),
            'is_deletable' => is_bool($this->isDeletable) ? $this->isDeletable : !empty($this->value),
            'has_preview_icon' => !empty($this->previewFileIcon),
            'has_upload_icon' => !empty($this->uploadFileIcon),
            'upload_file_icon' => $this->uploadFileIcon,
            'preview_file_icon' => $this->previewFileIcon,
            'download_path' => $this->value ? '/storage/' . $this->value : null,
        ]);
    }

}
