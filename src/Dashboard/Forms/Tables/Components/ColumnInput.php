<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Tables\Components;

class ColumnInput extends Column
{
    protected function getComponentKey(): string
    {
        return 'column-input-component';
    }
}
