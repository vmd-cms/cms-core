<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Tables\Components;

class ColumnSwitch extends Column
{
    protected function getComponentKey(): string
    {
        return "column-switch-component";
    }

    /**
     * @return bool|int|string
     */
    public function getValue()
    {
        return $this->cmsModel ? boolval($this->value) : boolval($this->default);
    }

    /**
     * @param null|bool|int|string $value
     * @return string|bool|int|null
     */
    protected function mutateValue($value = null)
    {
        return $value === 'true';
    }
}
