<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Tables\Components;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Tables\FormTableColumnInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Dashboard\Forms\Components\Component;

abstract class Column extends Component implements FormTableColumnInterface
{
    protected $isRefreshable = true;

    protected $relationName;
    protected $orderRequestField;

    /**
     * @var CmsModelInterface
     */
    protected $relationCmsModel;

    protected $fieldPrefix;

    /**
     * @param string $relation
     * @return FormTableColumnInterface
     */
    public function setRelationName(string $relation) : FormTableColumnInterface
    {
        $this->relationName = $relation;
        return $this;
    }

    /**
     * @return string
     */
    public function getRelationName()
    {
        return $this->relationName;

    }

    /**
     * @param CmsModelInterface $relationCmsModel
     * @return FormTableColumnInterface
     */
    public function setRelationCmsModel(CmsModelInterface $relationCmsModel) : FormTableColumnInterface
    {
        $this->relationCmsModel = $relationCmsModel;
        return $this;
    }

    /**
     * @param string $order
     * @return FormTableColumnInterface
     */
    public function setOrderRequestField(string $order) : FormTableColumnInterface
    {
        $this->orderRequestField = $order;
        return $this;
    }

    /**
     * @param string $fieldPrefix
     * @return $this
     */
    public function setFieldPrefix(string $fieldPrefix): FormTableColumnInterface
    {
        $this->fieldPrefix = $fieldPrefix . '_';
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRelationCmsModelValue()
    {
        $field = $this->field;
        $subFields = explode('.',$field);
        $oldValue = $this->relationCmsModel;
        foreach ($subFields as $item)
        {
            $oldValue = $oldValue->$item ?? null;
        }
        return $oldValue;
    }

}
