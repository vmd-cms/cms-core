<?php

namespace VmdCms\CoreCms\Dashboard\Display\Components;

use VmdCms\CoreCms\Services\Files\StorageAdapter;

class PhotoColumn extends Column
{
    protected $pathCallback;

    /**
     * @return string
     */
    public function getComponentKey() : string
    {
        return 'column-photo-component';
    }

    public function setPathCallback(callable $pathCallback)
    {
        $this->pathCallback = $pathCallback;
        return $this;
    }

    protected function getFormattedValue($value = null)
    {
        return StorageAdapter::getInstance()->getStoragePath($value,false);
    }

    public function getValue()
    {
        if(is_callable($this->pathCallback)){
            return call_user_func($this->pathCallback,$this->cmsModel);
        }
        return parent::getValue();
    }
}
