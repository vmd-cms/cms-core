<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Tables\Modals;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Tables\ModalDataInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Traits\Dashboard\Forms\HasComponents;
use VmdCms\CoreCms\Traits\Dashboard\HasCmsModel;

class ModalData implements ModalDataInterface
{
    use HasCmsModel,HasComponents;

    protected $serviceMethodPath;

    protected $updated;

    public function __construct()
    {
        $this->serviceMethodPath = null;
        $this->updated = false;
    }

    /**
     * @param string $serviceMethodPath
     * @return ModalDataInterface
     */
    public function setServiceMethodPath(string $serviceMethodPath): ModalDataInterface
    {
        $this->serviceMethodPath = $serviceMethodPath;
        return $this;
    }

    /**
     * @param bool $updated
     * @return ModalDataInterface
     */
    public function setUpdated(bool $updated): ModalDataInterface
    {
        $this->updated = $updated;
        return $this;
    }

    public function toArray()
    {
        $this->setRefreshableComponents();

        if($this->getCmsModel() instanceof CmsModelInterface){
            $this->components->setCmsModel($this->getCmsModel());
        }

        return [
            'service_method_path' => $this->serviceMethodPath,
            'components' => $this->components->toArray(),
            'updated' => $this->updated,
            'modal_dialog' => false
        ];
    }

    protected function setRefreshableComponents(){
        foreach ($this->components->getItems() as $key=>$component){
            $component->setIsRefreshable(true);
        }
    }

}
