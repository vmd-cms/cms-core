<?php

namespace VmdCms\CoreCms\Dashboard\Forms;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormPanelInterface;

class FormPanel extends AbstractForm implements FormPanelInterface
{
    /**
     * Form constructor.
     * @param array $components
     */
    public function __construct(array $components = [])
    {
        parent::__construct();
        $this->setComponents($components);
    }

    /**
     * @inheritDoc
     */
    public function toArray()
    {
        return array_merge(
            parent::toArray(),
            [
                'components' => $this->components->setCmsModel($this->cmsModel)->toArray(),
            ]
        );
    }

    protected function getFormComponentKey(): string{
        return 'form-panel-component';
    }
}
