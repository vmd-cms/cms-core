<?php

namespace VmdCms\CoreCms\Dashboard\Forms;

use VmdCms\CoreCms\Contracts\Collections\FormTabsCollectionInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormTabbedInterface;

class FormTabbed extends AbstractForm implements FormTabbedInterface
{
    /**
     * @var FormTabsCollectionInterface
     */
    private $tabsCollection;

    /**
     * FormInterface constructor.
     * @param null|FormTabsCollectionInterface $tabsCollection
     */
    public function __construct(FormTabsCollectionInterface $tabsCollection = null)
    {
        parent::__construct();
        if($tabsCollection) $this->setTabs($tabsCollection);
    }

    /**
     * @inheritDoc
     */
    public function setTabs(FormTabsCollectionInterface $tabsCollection): FormTabbedInterface
    {
        $this->tabsCollection = $tabsCollection;
        foreach ($this->tabsCollection->getItems() as $tab)
        {
            foreach ($tab->getComponents() as $component)
            {
                $this->components->appendItem($component);
            }
        }
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function toArray()
    {
        return array_merge(
            parent::toArray(),
            [
                'components' => $this->components->setCmsModel($this->cmsModel)->toArray(),
                'tabs' => $this->tabsCollection->setCmsModel($this->cmsModel)->toArray(),
            ]
        );
    }

    protected function formType()
    {
        return 'tabbed';
    }

    protected function getFormComponentKey(): string{
        return 'form-tabbed-component';
    }
}
