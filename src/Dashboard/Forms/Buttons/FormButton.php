<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Buttons;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Buttons\FormButtonHtmlAttributesInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Buttons\FormButtonInterface;
use VmdCms\CoreCms\Traits\Dashboard\Forms\FormButtonHtmAttributes;

abstract class FormButton implements FormButtonInterface, FormButtonHtmlAttributesInterface
{
    use FormButtonHtmAttributes;

    const ACTION_CREATE = 'create';
    const ACTION_SAVE = 'save';
    const ACTION_COPY = 'copy';
    const ACTION_SAVE_CREATE = 'save_create';
    const ACTION_DELETE = 'delete';
    const ACTION_RESTORE = 'restore';
    const ACTION_CANCEl = 'cancel';
    const ACTION_BACK = 'back';
    const ACTION_SERVICE_PATH = 'service_path';

    const GROUP_ACTION_HEADER = 'action_header';
    const GROUP_ACTION_FOOTER = 'action_footer';
    const GROUP_ACTION_FLOAT = 'action_float';
    const GROUP_BACK = 'back_btn';

    /**
     * @var string
     */
    protected $action;
    protected $title;
    protected $routePath;
    protected $groupKey;

    protected $iconClass;
    protected $color;

    public function __construct(string $title)
    {
        $this->title = $title;
        $this->action = $this->getAction();
        $this->color = $this->getColor();
        $this->groupKey = static::GROUP_ACTION_FOOTER;
        $this->initializeComponentHtmlAttributesDTO();
    }

    /**
     * @param string $routePath
     * @return FormButtonInterface
     */
    public function setRoutePath(string $routePath): FormButtonInterface
    {
        $this->routePath = $routePath;
        return $this;
    }

    public function getColor(): string
    {
        return '#109CF1';
    }

    public function setIconClass(?string $iconClass): FormButtonInterface
    {
        $this->iconClass = $iconClass;
        return $this;
    }

    public function setColor(?string $color): FormButtonInterface
    {
        $this->color = $color;
        return $this;
    }

    public function setAction(?string $action): FormButtonInterface
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return string
     */
    public function getComponentKey() : string
    {
        return 'default-button-component';
    }

    /**
     * @param string $groupKey
     * @return FormButtonInterface
     */
    public function setGroupKey(string $groupKey): FormButtonInterface
    {
        $this->groupKey = $groupKey;
        return $this;
    }

    /**
     * @return string
     */
    public function getGroupKey(): string
    {
        return $this->groupKey;
    }

    /**
     * @inheritDoc
     */
    public function toArray()
    {
        return [
            'action' => $this->action,
            'title' => $this->title,
            'icon_class' => $this->iconClass,
            'route_path' => $this->routePath,
            'color' => $this->color,
            'component' => $this->getComponentKey(),
            'htmlAttributes' => $this->htmlAttributes->toArray()
        ];
    }
}
