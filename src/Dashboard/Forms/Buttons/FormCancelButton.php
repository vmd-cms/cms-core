<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Buttons;

use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;

class FormCancelButton extends FormButton
{
    public function __construct(string $title = null)
    {
        parent::__construct($title ?? CoreLang::get('cancel'));
    }

    public function getAction(): string
    {
        return self::ACTION_CANCEl;
    }

    public function getColor(): string
    {
        return '#C2CFE0';
    }
}
