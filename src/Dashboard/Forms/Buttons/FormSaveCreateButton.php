<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Buttons;

use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;

class FormSaveCreateButton extends FormButton
{
    public function __construct(string $title = null)
    {
        parent::__construct($title ?? CoreLang::get('save_and_create'));
    }

    public function getAction(): string
    {
        return self::ACTION_SAVE_CREATE;
    }

    public function getComponentKey(): string
    {
        return 'save-create-button-component';
    }

}
