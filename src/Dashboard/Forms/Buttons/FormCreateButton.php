<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Buttons;

use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;

class FormCreateButton extends FormButton
{
    public function __construct(string $title = null)
    {
        parent::__construct($title ?? CoreLang::get('append'));
    }

    public function getAction(): string
    {
        return self::ACTION_CREATE;
    }

    public function getColor(): string
    {
        return '#109CF1';
    }

    public function getComponentKey(): string
    {
        return 'create-button-component';
    }
}
