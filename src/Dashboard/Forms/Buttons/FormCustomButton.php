<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Buttons;

use Illuminate\Support\Facades\Lang;

class FormCustomButton extends FormButton
{
    /**
     * @var string
     */
    protected $confirmMessage;

    public function __construct(string $title = '')
    {
        parent::__construct($title);
        $this->color = '#90A0B7';
        $this->htmlAttributes->setClass('component','custom-block');
        $this->confirmMessage = Lang::get('vmd_cms::dashboard.confirm_action');
    }

    public function getAction(): string
    {
        return self::ACTION_SERVICE_PATH;
    }

    /**
     * @param string|null $confirmMessage
     * @return FormCustomButton
     */
    public function setConfirmMessage(string $confirmMessage = null): FormCustomButton
    {
        $this->confirmMessage = $confirmMessage;
        return $this;
    }

    /**
     * @return string
     */
    public function getComponentKey() : string
    {
        return 'custom-button-component';
    }

    public function toArray()
    {
        return array_merge(parent::toArray(),[
           'confirm_message' => $this->confirmMessage
        ]);
    }
}
