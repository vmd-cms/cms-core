<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Buttons;

use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;

class FormSaveButton extends FormButton
{
    public function __construct(string $title = null)
    {
        parent::__construct($title ?? CoreLang::get('save'));
    }

    public function getAction(): string
    {
        return self::ACTION_SAVE;
    }
}
