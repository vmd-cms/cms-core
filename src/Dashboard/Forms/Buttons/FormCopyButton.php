<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Buttons;

use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;

class FormCopyButton extends FormButton
{
    public function __construct(string $title = null)
    {
        parent::__construct($title ?? CoreLang::get('make_copy'));
    }

    public function getAction(): string
    {
        return self::ACTION_COPY;
    }

    public function getComponentKey(): string
    {
        return 'copy-button-component';
    }

    public function getColor(): string
    {
        return '#334D6E';
    }
}
