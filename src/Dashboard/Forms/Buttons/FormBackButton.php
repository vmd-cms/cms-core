<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Buttons;

use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;

class FormBackButton extends FormButton
{
    public function __construct(string $title = null)
    {
        parent::__construct($title ?? CoreLang::get('back'));
    }

    public function getAction(): string
    {
        return self::ACTION_BACK;
    }

    public function getColor(): string
    {
        return '#109CF1';
    }

    public function getComponentKey(): string
    {
        return 'back-button-component';
    }
}
