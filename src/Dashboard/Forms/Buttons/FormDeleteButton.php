<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Buttons;

use Illuminate\Support\Facades\Lang;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;

class FormDeleteButton extends FormButton
{
    /**
     * @var string
     */
    protected $confirmMessage;

    public function __construct(string $title = null)
    {
        parent::__construct($title ?? CoreLang::get('delete'));
        $this->iconClass = 'icon-delete';
        $this->htmlAttributes->setClass('component','delete-block');
        $this->confirmMessage = Lang::get('vmd_cms::dashboard.confirm_action');
    }

    public function getAction(): string
    {
        return self::ACTION_DELETE;
    }

    public function getColor(): string
    {
        return '#90A0B7';
    }

    /**
     * @return string
     */
    public function getComponentKey() : string
    {
        return 'delete-button-component';
    }

    /**
     * @param string|null $confirmMessage
     * @return FormDeleteButton
     */
    public function setConfirmMessage(string $confirmMessage = null): FormDeleteButton
    {
        $this->confirmMessage = $confirmMessage;
        return $this;
    }

    public function toArray()
    {
        return array_merge(parent::toArray(),[
            'confirm_message' => $this->confirmMessage
        ]);
    }
}
