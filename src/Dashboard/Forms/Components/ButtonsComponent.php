<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Buttons\FormButtonInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormComponentInterface;

class ButtonsComponent extends Component
{
    protected $buttons;

    protected function getComponentKey(): string
    {
        return "buttons-component";
    }

    /**
     * @param FormButtonInterface|FormButtonInterface[] $buttons
     * @return ButtonsComponent
     */
    public function appendButtons($buttons):ButtonsComponent
    {
        if(!is_array($buttons)){
            $buttons = [$buttons];
        }
        foreach ($buttons as $button){
            if($button instanceof FormButtonInterface)
            {
                $this->buttons[] = $button->toArray();
            }
        }
        return $this;
    }

    public function justify(string $direction): FormComponentInterface
    {
        $this->htmlAttributes->appendClass('component', 'd-flex justify-' . $direction);
        return $this;
    }

    protected function getAdditionalData(): array
    {
        return [
            'buttons' => $this->buttons
        ];
    }

    public function isShowLabel(): bool
    {
        return false;
    }
}
