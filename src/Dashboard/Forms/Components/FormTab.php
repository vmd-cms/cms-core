<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

use Illuminate\Support\Str;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormTabInterface;
use VmdCms\CoreCms\Traits\Dashboard\Forms\HasComponents;
use VmdCms\CoreCms\Traits\Dashboard\HasCmsModel;
use VmdCms\CoreCms\Traits\Dashboard\HasCmsModelData;

class FormTab implements FormTabInterface
{
    use HasCmsModel, HasComponents;

    /**
     * @var string
     */
    private $title;
    private $key;

    public function __construct(string $title, array $components = [])
    {
        $this->title = $title;
        $this->key = Str::slug($title);
        $this->setComponents($components);
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

}
