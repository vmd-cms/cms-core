<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

use VmdCms\CoreCms\Collections\RulesCollection;
use VmdCms\CoreCms\Contracts\Collections\RulesCollectionInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormComponentInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\HasDependedComponentsInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Dashboard\Validation\ValidationRules;
use VmdCms\CoreCms\Traits\Dashboard\Forms\FormComponentHtmAttributes;
use VmdCms\CoreCms\Traits\Dashboard\HasCmsModel;
use VmdCms\CoreCms\Traits\Dashboard\HasCmsModelData;
use VmdCms\CoreCms\Traits\Dashboard\HasFieldComponent;

abstract class Component implements FormComponentInterface
{
    use ValidationRules,HasFieldComponent,HasCmsModel,HasCmsModelData,FormComponentHtmAttributes;

    /**
     * @var string
     */
    private $componentKey;
    private $error;

    /**
     * @var string|bool|int
     */
    protected $value;

    /**
     * @var string|bool|int
     */
    protected $default;

    /**
     * @var RulesCollectionInterface
     */
    protected $rules;

    /**
     * @var callable
     */
    protected $beforeSave;

    /**
     * @var callable
     */
    protected $afterSave;

    /**
     * @var callable
     */
    protected $storeCallback;

    /**
     * @var array
     */
    private $array;

    private $disabled;

    /**
     * @var bool
     */
    protected $hideDetails;

    protected $isRefreshable = false;

    /**
     * Component constructor.
     * @param string $field
     * @param null|string $label
     */
    public function __construct(string $field, ?string $label = null)
    {
        $this->setField($field);
        $this->setLabel(is_null($label) ? CoreLang::get($field) : $label);
        $this->error = '';
        $this->componentKey = $this->getComponentKey();
        $this->disabled = false;
        $this->rules = new RulesCollection();
        $this->initializeComponentHtmlAttributesDTO();
        $this->hideDetails = false;
    }

    /**
     * @return RulesCollectionInterface
     */
    protected function getRulesCollection() : RulesCollectionInterface
    {
        return $this->rules;
    }

    /**
     * @return string
     */
    protected abstract function getComponentKey() : string;

    /**
     * @param string $value
     * @return FormComponentInterface
     */
    public function setValue($value): FormComponentInterface
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return bool|int|string
     */
    protected function getValue()
    {
        return !empty($this->value) ? $this->value : $this->default;
    }

    /**
     * @param $default
     * @return FormComponentInterface
     */
    public function setDefault($default): FormComponentInterface
    {
        $this->default = $default;
        return $this;
    }

    /**
     * @return bool|int|mixed|string
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * @param string $error
     * @return FormComponentInterface
     */
    public function setError(string $error): FormComponentInterface
    {
        $this->error = $error;
        return $this;
    }

    /**
     * @return array
     */
    protected function getAdditionalData() : array
    {
        return [];
    }

    /**
     * @param bool $isRefreshable
     * @return Component
     */
    public function setIsRefreshable(bool $isRefreshable): FormComponentInterface
    {
        $this->isRefreshable = $isRefreshable;
        return $this;
    }

    /**
     * @param null|bool|int|string $value
     * @return string|bool|int|null
     */
    protected function validateValue($value = null)
    {
        return $value;
    }

    /**
     * @param null|bool|int|string $value
     * @return string|bool|int|null
     */
    protected function mutateValue($value = null)
    {
        return $value;
    }

    /**
     * @inheritDoc
     */
    public function getPreparedValue($value)
    {
        $value = $this->validateValue($value);

        $value = $this->mutateValue($value);

        if($this->beforeSave && is_callable($this->beforeSave))
        {
            $value = call_user_func($this->beforeSave,$value);
        }
        return $value;
    }

    /**
     * @inheritDoc
     */
    public function setBeforeSave(callable $function): FormComponentInterface
    {
        $this->beforeSave = $function;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getBeforeSaveCallback(): ?callable
    {
        return $this->beforeSave;
    }

    /**
     * @inheritDoc
     */
    public function setStoreCallback(callable $function): FormComponentInterface
    {
        $this->storeCallback = $function;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getStoreCallback(): ?callable
    {
        return $this->storeCallback;
    }

    /**
     * @inheritDoc
     */
    public function setAfterSaveCallback(callable $function): FormComponentInterface
    {
        $this->afterSave = $function;
        return $this;
    }

    /**
     * @return null|callable
     */
    public function getAfterSaveCallback(): ?callable
    {
        return $this->afterSave;
    }

    /**
     * @param bool $disabled
     * @return FormComponentInterface
     */
    public function setDisabled(bool $disabled) : FormComponentInterface
    {
        $this->disabled = $disabled;
        return $this;
    }

    /**
     * @param bool $hideDetails
     * @return FormComponentInterface
     */
    public function setHideDetails(bool $hideDetails) : FormComponentInterface
    {
        $this->hideDetails = $hideDetails;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function toArray()
    {
        if(empty($this->array) || $this->isRefreshable)
        {
            $this->array = array_merge(
                [
                    'component' => $this->getComponentKey(),
                    'field' => $this->getField(),
                    'label' => $this->getLabel() . ($this->isRequired ? '*' : ''),
                    'topHelpText' => $this->getTopHelpText(),
                    'bottomHelpText' => $this->getBottomHelpText(),
                    'placeholder' => $this->getPlaceholder(),
                    'isShowLabel' => $this->isShowLabel(),
                    'rows' => $this->getRows(),
                    'outlined' => $this->outlined,
                    'displayRow' => $this->displayRow,
                    'customStyles' => $this->getCustomStyles(),
                    'value' => $this->getValue(),
                    'rules' => $this->rules ? $this->rules->toArray() : [],
                    'disabled' => $this->disabled,
                    'hide_details' => $this->hideDetails,
                    'error' => $this->error,
                    'htmlAttributes' => $this->htmlAttributes->toArray()
                ],
                $this->getCmsModelData(),
                $this->getAdditionalData()
            );
            if($this instanceof HasDependedComponentsInterface && $dependComponent = $this->getDependedComponent())
            {
                $this->array['depended_components'] = $dependComponent->setCmsModel($this->cmsModel)->toArray();
            }
        }
        return $this->array;
    }

    /**
     * @return string|void
     */
    public function render()
    {
        return '<' . $this->getComponentKey() . ' :value="' . $this->value . '" :element="' . json_encode($this->toArray()) . '"></' . $this->getComponentKey() .'>';
    }
}
