<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

use VmdCms\CoreCms\CoreModules\Content\Entity\Languages;
use VmdCms\CoreCms\Services\CoreRouter;

class CkeditorComponent extends Component
{
    protected function getComponentKey(): string
    {
        return "ckeditor-component";
    }

    protected function getEditorLocale(): string
    {
        $locale = Languages::getInstance()->getCurrentLocale();
        return $locale == 'ua' ? 'ru' : $locale;
    }

    /**
     * @return array
     */
    protected function getAdditionalData() : array
    {
        return [
            'editor_locale' => $this->getEditorLocale(),
            'upload_path' => CoreRouter::getCkEditorUploadFileRoute(request()->sectionSlug, $this->cmsModel->id ?? null),
            'browse_path' => CoreRouter::getCkEditorBrowseFilesRoute(request()->sectionSlug, $this->cmsModel->id ?? null)
        ];
    }
}
