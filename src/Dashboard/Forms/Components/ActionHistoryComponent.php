<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

use VmdCms\CoreCms\DTO\Dashboard\Components\ActionHistoryDTO;

class ActionHistoryComponent extends Component
{
    /**
     * @var ActionHistoryDTO[]
     */
    protected $itemsDto;

    /**
     * @var array
     */
    protected $items;

    /**
     * @var string
     */
    protected $appendTitle;

    /**
     * @param string $appendTitle
     * @return ActionHistoryComponent
     */
    public function setAppendTitle(string $appendTitle): ActionHistoryComponent
    {
        $this->appendTitle = $appendTitle;
        return $this;
    }

    protected function getComponentKey(): string
    {
        return "action-history-component";
    }

    /**
     * @param ActionHistoryDTO[] $items
     * @return $this
     */
    public function appendItems(array $items): ActionHistoryComponent
    {
        foreach ($items as $item){
            if($item instanceof ActionHistoryDTO)
            {
                $this->itemsDto[] = $item;
                $this->items[] = $item->toArray();
            }
        }
        return $this;
    }

    public function toArray()
    {
        return array_merge(parent::toArray(),[
            'items' => $this->items,
            'append_title' => $this->appendTitle
        ]);
    }

    protected function getValue()
    {
        return null;
    }
}
