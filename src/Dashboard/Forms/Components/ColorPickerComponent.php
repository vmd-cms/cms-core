<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

class ColorPickerComponent extends Component
{
    protected function getComponentKey(): string
    {
        return "color-picker-component";
    }
}
