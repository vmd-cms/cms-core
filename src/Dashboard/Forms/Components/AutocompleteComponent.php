<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Input\SelectInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\SearchableComponentInterface;
use VmdCms\CoreCms\Traits\Dashboard\Components\SearchableComponent;

class AutocompleteComponent extends Component implements SelectInterface, SearchableComponentInterface
{
    use SearchableComponent;

    protected function getComponentKey(): string
    {
        return "autocomplete-component";
    }

}
