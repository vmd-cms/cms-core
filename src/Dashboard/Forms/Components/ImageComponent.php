<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use VmdCms\CoreCms\Collections\CoreCollectionAbstract;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\CroppedDimensionDTOCollectionInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\CroppedDimensionDTOInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Contracts\Models\HasMediaDimensionsInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\DTO\Dashboard\Components\CroppedDimensionDTOCollection;
use VmdCms\CoreCms\Exceptions\CoreException;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;
use VmdCms\CoreCms\Services\Files\StoreFile;

class ImageComponent extends Component
{
    /**
     * @var StoreFile
     */
    protected $storeFile;

    /**
     * @var string
     */
    protected $helpText;
    protected $uploadText;
    protected $deleteText;

    /**
     * @var boolean
     */
    protected $isCopyToWEBP;

    /**
     * @var CroppedDimensionDTOCollectionInterface
     */
    protected $croppedDimensions;


    /**
     * @var boolean
     */
    protected $isStoreToRelationFolder;

    /**
     * @var callable
     */
    protected $storePathCallback;

    public function __construct(string $field, ?string $label = null)
    {
        parent::__construct($field, $label);
        $this->storeFile = new StoreFile();
        $this->storeCallback = $this->storeFileCallback();
        $this->uploadText = CoreLang::get('upload_image');
        $this->deleteText = CoreLang::get('delete');
        $this->helpText = null;
        $this->isCopyToWEBP = false;
        $this->isStoreToRelationFolder = false;
        $this->croppedDimensions = new CroppedDimensionDTOCollection();
    }

    /**
     * @param callable $pathCallback
     * @return ImageComponent
     */
    public function setStorePathCallback(callable $pathCallback): ImageComponent
    {
        $this->storePathCallback = $pathCallback;
        return $this;
    }

    /**
     * @param bool $isStoreToRelationFolder
     * @return $this
     */
    public function setIsStoreToRelationFolder(bool $isStoreToRelationFolder): ImageComponent
    {
        $this->isStoreToRelationFolder = $isStoreToRelationFolder;
        return $this;
    }

    /**
     * @param CroppedDimensionDTOCollectionInterface $croppedDimensions
     * @return ImageComponent
     */
    public function setCroppedDimensions(CroppedDimensionDTOCollectionInterface $croppedDimensions): ImageComponent
    {
        $this->croppedDimensions = $croppedDimensions;
        return $this;
    }

    /**
     * @param CroppedDimensionDTOInterface $croppedDimension
     * @return ImageComponent
     */
    public function appendCroppedDimension(CroppedDimensionDTOInterface $croppedDimension): ImageComponent
    {
        $this->croppedDimensions->appendItem($croppedDimension);
        return $this;
    }

    /**
     * @return ImageComponent
     */
    public function copyToWEBP(): ImageComponent
    {
        $this->isCopyToWEBP = true;
        return $this;
    }

    /**
     * @param string $helpText
     * @return $this
     */
    public function setHelpText(string $helpText): self
    {
        $this->helpText = $helpText;
        return $this;
    }

    /**
     * @param string $uploadText
     * @return $this
     */
    public function setUploadText(string $uploadText): self
    {
        $this->uploadText = $uploadText;
        return $this;
    }

    /**
     * @param string $deleteText
     * @return $this
     */
    public function setDeleteText(string $deleteText): self
    {
        $this->deleteText = $deleteText;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFieldPath()
    {
        return str_replace('.','_',$this->field) . '_path';
    }

    /**
     * @return mixed
     */
    public function getFieldFile()
    {
        return str_replace('.','_',$this->field) . '_file';
    }

    protected function getComponentKey(): string
    {
        return "image-component";
    }

    protected function storeFileCallback()
    {
        return function (){

            if(!$this->cmsModel instanceof CmsModelInterface) throw new NotCmsModelException();

            $field = $this->field;
            $subFields = explode('.',$field);
            $oldValue = $this->cmsModel;
            foreach ($subFields as $item)
            {
                $oldValue = $oldValue->$item ?? null;
            }

            $fieldPath = $this->getFieldPath();
            $fieldFile = $this->getFieldFile();
            $file = request()->allFiles()[$fieldFile] ?? null;

            if(!array_key_exists($fieldPath,request()->toArray()))
            {
                return $oldValue;
            }

            if(!$file && request()->$fieldPath === $oldValue) {
                return $oldValue;
            }

            if(!empty($oldValue)){
                $this->storeFile->deleteFileByPath($oldValue);
            }

            if($this->cmsModel instanceof HasMediaDimensionsInterface){
                $this->cmsModel->deleteMediaDimensions($field);
            }

            $baseModel = $this->cmsModel;
            $targetModel = $baseModel;
            $fieldsArr = $subFields;
            $field = array_pop($fieldsArr);

            if(count($fieldsArr))
            {
                foreach ($fieldsArr as $rel){
                    $targetModel = $targetModel->$rel ?? null;
                }
            }

            if(!$file instanceof UploadedFile) {
                $targetModel->$field = null;
                return null;
            }

            if(empty($this->cmsModel->id)){

                $this->cmsModel::created(function (CmsModelInterface $model) use($file,$subFields){
                    $targetModel = $model;
                    $field = array_pop($subFields);
                    if(count($subFields))
                    {
                        foreach ($subFields as $rel){
                            $targetModel = $targetModel->$rel ?? null;
                        }
                    }
                    if($targetModel instanceof CmsModelInterface)
                    {
                        $folderPath = $this->getTargetFolderPath($model,$targetModel);
                        $storedPath = $this->storeFile->storeFile($folderPath,$file);
                        $targetModel->$field = $storedPath;
                        $targetModel->saveQuietly();
                        // DB::table($targetModel::table())->where('id',$targetModel->id)->update([$field => $storedPath]);
                        if($model instanceof HasMediaDimensionsInterface){
                            $model->setCroppedDimensions($this->croppedDimensions);
                            $model->setCopyToWEBP($this->isCopyToWEBP);
                            $model->storeMediaDimensions($storedPath,$field);
                        }
                    }
                });
                return;
            }

            if($targetModel instanceof CmsModelInterface)
            {
                $targetModel::saving(function (CmsModelInterface $model) use ($field,$file,$baseModel)
                {
                    $folderPath = $this->getTargetFolderPath($baseModel,$model);
                    $storedPath = $this->storeFile->storeFile($folderPath,$file);
                    $model->$field = $storedPath;
                    if($model instanceof HasMediaDimensionsInterface){
                        $model->setCroppedDimensions($this->croppedDimensions);
                        $model->setCopyToWEBP($this->isCopyToWEBP);
                        $model->storeMediaDimensions($storedPath,$field);
                    }
                });
            }

        };
    }

    /**
     * @param CmsModelInterface|null $baseModel
     * @param CmsModelInterface|null $targetModel
     * @return string
     */
    protected function getTargetFolderPath(CmsModelInterface $baseModel = null, CmsModelInterface $targetModel = null): string
    {
        if(is_callable($this->storePathCallback)){
            $path = call_user_func($this->storePathCallback,$baseModel,$targetModel);
        }else{
            $path = $this->storeFile->getFolderPathByCmsModel($this->isStoreToRelationFolder ? $targetModel : $baseModel);
        }
        return $path;
    }

    protected function getAdditionalData(): array
    {
        return [
            'help_text' => $this->helpText,
            'upload_text' => $this->uploadText,
            'delete_text' => $this->deleteText,
            'path' => $this->value,
            'src_preview' => $this->storeFile->getImagePath($this->value),
            'src_default' => $this->storeFile->getDefaultImage(),
            'field_path' => $this->getFieldPath(),
            'field_file' => $this->getFieldFile(),
        ];
    }

}
