<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Input\CheckboxInterface;
use VmdCms\CoreCms\Traits\Dashboard\Forms\MultiValuable;

class CheckBoxComponent extends Component implements CheckboxInterface
{
    use MultiValuable;

    public function __construct(string $field, ?string $label = null)
    {
        parent::__construct($field, $label);
        $this->beforeSave = $this->getDefaultBeforeSaveCallback();
    }

    protected function getComponentKey(): string
    {
        return "checkbox-component";
    }

    /**
     * @return \Closure
     */
    protected function getDefaultBeforeSaveCallback(){
        return  function ($value){
            return is_array($value) ? array_filter($value, function($var){return !is_null($var);} ) : $value;
        };
    }

}
