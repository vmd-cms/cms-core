<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

use Illuminate\Database\Eloquent\RelationNotFoundException;
use Illuminate\Support\Str;

class UrlComponent extends Component
{
    protected $dependedField;

    protected function getComponentKey(): string
    {
        return "input-component";
    }

    /**
     * @param null|bool|int|string $value
     * @return string|bool|int|null
     */
    protected function mutateValue($value = null)
    {
        if (empty($value) && !$this->dependedField) {
            return null;
        }

        if (empty($value)) {
            $value = request()->get(str_replace('.', '_', $this->dependedField));
        }

        $exploded = explode('/',$value);
        if(is_countable($exploded)){
            foreach ($exploded as $key=>$item){
                $exploded[$key] = Str::slug($item);
            }
            $value = implode('/',$exploded);
        }

        if ($this->rules->hasRule('unique')) {
            $fieldArr = explode('.', $this->field);
            $field = array_pop($fieldArr);
            $modelClass = get_class($this->cmsModel);
            $model = $this->cmsModel;
            if (is_countable($fieldArr) && count($fieldArr)) {
                foreach ($fieldArr as $rel) {
                    $modelClass = get_class($model->$rel()->getRelated());
                    $model = $model->$rel ?? null;
                }
            }
            $isExist = $modelClass::where($field, $value)->where('id', '!=', $model->id ?? null)->first();
            if ($isExist) {
                $value .= $model->id ?? date('Y-m-d-H:i:s');
            }
        }

        return $value;
    }

    /**
     * @param string $field
     * @return UrlComponent
     */
    public function setDependedField(string $field): UrlComponent
    {
        $this->dependedField = $field;
        return $this;
    }
}
