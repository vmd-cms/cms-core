<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\CustomFormComponentInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormComponentInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;

class CustomComponent extends Component
{
    /**
     * @var CustomFormComponentInterface
     */
    protected $customComponent;

    public function isShowLabel(): bool
    {
        return false;
    }

    /**
     * @param CustomFormComponentInterface $customComponent
     * @return FormComponentInterface
     */
    public function setCustomComponent(CustomFormComponentInterface $customComponent): FormComponentInterface
    {
        $this->customComponent = $customComponent;
        return $this;
    }

    protected function getComponentKey(): string
    {
        return "custom-component";
    }

    public function toArray()
    {
        return array_merge(parent::toArray(),[
            'custom_component' => $this->customComponent->setCmsModel($this->cmsModel)->toArray()
        ]);
    }

    public function getStoreCallback(): ?callable
    {
        if(!$this->cmsModel instanceof CmsModelInterface) return null;
        return $this->customComponent->setCmsModel($this->cmsModel)->getStoreCallback();
    }

    protected function getValue()
    {
        return null;
    }
}
