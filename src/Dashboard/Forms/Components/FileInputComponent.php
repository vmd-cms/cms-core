<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;
use VmdCms\CoreCms\Services\Files\StoreFile;

class FileInputComponent extends Component
{
    /**
     * @var StoreFile
     */
    protected $storeFile;

    /**
     * @var string
     */
    protected $helpText;
    protected $uploadText;
    protected $downloadText;
    protected $deleteText;
    protected $previewFileIcon;
    protected $uploadFileIcon;

    /**
     * @var boolean
     */
    protected $isStoreToRelationFolder;
    protected $isDownloadable;
    protected $isDeletable;

    /**
     * @var callable
     */
    protected $storePathCallback;

    public function __construct(string $field, ?string $label = null)
    {
        parent::__construct($field, $label);
        $this->storeFile = new StoreFile();
        $this->storeCallback = $this->storeFileCallback();
        $this->uploadText = CoreLang::get('upload_file');
        $this->downloadText = CoreLang::get('download');
        $this->deleteText = CoreLang::get('delete');
        $this->isStoreToRelationFolder = false;
        $this->uploadFileIcon = '/files/vendor/vmd_cms/svg/icons/upload_file_icon.svg';
        $this->previewFileIcon = '/files/vendor/vmd_cms/svg/icons/preview_file_icon.svg';
    }

    /**
     * @param string|null $previewFileIcon
     * @return FileInputComponent
     */
    public function setPreviewFileIcon(string $previewFileIcon = null): FileInputComponent
    {
        $this->previewFileIcon = $previewFileIcon;
        return $this;
    }

    /**
     * @param string|null $uploadFileIcon
     * @return FileInputComponent
     */
    public function setUploadFileIcon(string $uploadFileIcon = null): FileInputComponent
    {
        $this->uploadFileIcon = $uploadFileIcon;
        return $this;
    }

    /**
     * @param bool $isDownloadable
     * @return FileInputComponent
     */
    public function setIsDownloadable(bool $isDownloadable): FileInputComponent
    {
        $this->isDownloadable = $isDownloadable;
        return $this;
    }

    /**
     * @param bool $isDeletable
     * @return FileInputComponent
     */
    public function setIsDeletable(bool $isDeletable): FileInputComponent
    {
        $this->isDeletable = $isDeletable;
        return $this;
    }

    /**
     * @param callable $pathCallback
     * @return ImageComponent
     */
    public function setStorePathCallback(callable $pathCallback): ImageComponent
    {
        $this->storePathCallback = $pathCallback;
        return $this;
    }

    /**
     * @param bool $isStoreToRelationFolder
     * @return $this
     */
    public function setIsStoreToRelationFolder(bool $isStoreToRelationFolder): ImageComponent
    {
        $this->isStoreToRelationFolder = $isStoreToRelationFolder;
        return $this;
    }

    /**
     * @param string $helpText
     * @return $this
     */
    public function setHelpText(string $helpText): self
    {
        $this->helpText = $helpText;
        return $this;
    }

    /**
     * @param string $uploadText
     * @return $this
     */
    public function setUploadText(string $uploadText): self
    {
        $this->uploadText = $uploadText;
        return $this;
    }

    /**
     * @param string $deleteText
     * @return $this
     */
    public function setDeleteText(string $deleteText): self
    {
        $this->deleteText = $deleteText;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFieldPath()
    {
        return str_replace('.','_',$this->field) . '_path';
    }

    /**
     * @return mixed
     */
    public function getFieldFile()
    {
        return str_replace('.','_',$this->field) . '_file';
    }

    protected function getComponentKey(): string
    {
        return "file-input-component";
    }

    protected function storeFileCallback()
    {
        return function (){

            if(!$this->cmsModel instanceof CmsModelInterface) throw new NotCmsModelException();

            $field = $this->field;
            $subFields = explode('.',$field);
            $oldValue = $this->cmsModel;
            foreach ($subFields as $item)
            {
                $oldValue = $oldValue->$item ?? null;
            }

            $fieldPath = $this->getFieldPath();
            $fieldFile = $this->getFieldFile();
            $file = request()->allFiles()[$fieldFile] ?? null;

            if(!array_key_exists($fieldPath,request()->toArray()))
            {
                return $oldValue;
            }

            if(!$file && request()->$fieldPath === $oldValue) {

                return $oldValue;
            }

            $this->storeFile->deleteFileByPath($oldValue);

            $baseModel = $this->cmsModel;
            $targetModel = $baseModel;
            $fieldsArr = $subFields;
            $field = array_pop($fieldsArr);

            if(count($fieldsArr))
            {
                foreach ($fieldsArr as $rel){
                    $targetModel = $targetModel->$rel ?? null;
                }
            }

            if(!$file instanceof UploadedFile) {
                $targetModel->$field = null;
                return null;
            }

            if(empty($this->cmsModel->id)){
                $this->cmsModel::created(function (CmsModelInterface $model) use($file,$subFields){
                    $targetModel = $model;
                    $field = array_pop($subFields);
                    if(count($subFields))
                    {
                        foreach ($subFields as $rel){
                            $targetModel = $targetModel->$rel ?? null;
                        }
                    }
                    if($targetModel instanceof CmsModelInterface)
                    {
                        $folderPath = $this->getTargetFolderPath($model,$targetModel);
                        $storedPath = $this->storeFile->storeFile($folderPath,$file);

                        $targetModel->$field = $storedPath;
                        $targetModel->saveQuietly();
                       // DB::table($targetModel::table())->where('id',$targetModel->id)->update([$field => $storedPath]);
                    }
                });
                return;
            }

            if($targetModel instanceof CmsModelInterface)
            {
                $targetModel::saving(function (CmsModelInterface $model) use ($field,$file,$baseModel)
                {
                    $folderPath = $this->getTargetFolderPath($baseModel,$model);
                    $storedPath = $this->storeFile->storeFile($folderPath,$file);
                    $model->$field = $storedPath;
                });
            }

        };
    }

    /**
     * @param CmsModelInterface|null $baseModel
     * @param CmsModelInterface|null $targetModel
     * @return string
     */
    protected function getTargetFolderPath(CmsModelInterface $baseModel = null, CmsModelInterface $targetModel = null): string
    {
        if(is_callable($this->storePathCallback)){
            $path = call_user_func($this->storePathCallback,$baseModel,$targetModel);
        }else{
            $path = $this->storeFile->getFolderPathByCmsModel($this->isStoreToRelationFolder ? $targetModel : $baseModel);
        }
        return $path;
    }

    protected function getAdditionalData(): array
    {
        return [
            'help_text' => $this->helpText,
            'upload_text' => $this->uploadText,
            'download_text' => $this->downloadText,
            'delete_text' => $this->deleteText,
            'is_downloadable' => is_bool($this->isDownloadable) ? $this->isDownloadable : !empty($this->value),
            'is_deletable' => is_bool($this->isDeletable) ? $this->isDeletable : !empty($this->value),
            'path' => $this->value,
            'download_path' => $this->value ? '/storage/' . $this->value : null,
            'field_path' => $this->getFieldPath(),
            'field_file' => $this->getFieldFile(),
            'has_preview_icon' => !empty($this->previewFileIcon),
            'has_upload_icon' => !empty($this->uploadFileIcon),
            'preview_file_icon' => $this->previewFileIcon,
            'upload_file_icon' => $this->uploadFileIcon,
        ];
    }

}
