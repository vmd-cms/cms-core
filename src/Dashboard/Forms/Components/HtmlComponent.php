<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

class HtmlComponent extends Component
{
    protected $html;

    public function __construct(string $html, ?string $label = null)
    {
        parent::__construct($html, null);
    }

    /**
     * @return bool
     */
    public function isShowLabel(): bool
    {
        return false;
    }

    protected function getComponentKey(): string
    {
        return "html-component";
    }

    /**
     * @param string $html
     * @return $this
     */
    public function setHtml(string $html): HtmlComponent
    {
        $this->html = $html;
        return $this;
    }

    protected function getAdditionalData(): array
    {
        return [
            'html' => !empty($this->html) ? $this->html : $this->field
        ];
    }

    protected function getValue()
    {
        return null;
    }
}
