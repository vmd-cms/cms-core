<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

use VmdCms\CoreCms\Services\YouTubeHash;

class YoutubeCodeComponent extends Component
{
    protected function getComponentKey(): string
    {
        return "input-component";
    }

    protected function mutateValue($value = null)
    {
        return YouTubeHash::getYoutubeHash($value);
    }
}
