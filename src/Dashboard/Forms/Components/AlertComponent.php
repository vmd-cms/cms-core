<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

class AlertComponent extends Component
{
    /**
     * @var string
     */
    protected $text;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $border;

    /**
     * @var bool
     */
    protected $dense;

    /**
     * @var bool
     */
    protected $outlined;

    public function __construct(string $text, ?string $label = null)
    {
        parent::__construct(' ', null);
        $this->text = $text;
    }

    /**
     * @return bool
     */
    public function isShowLabel(): bool
    {
        return false;
    }

    protected function getComponentKey(): string
    {
        return "alert-component";
    }

    /**
     * @param string $text
     * @return $this
     */
    public function setText(string $text): AlertComponent
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @param string $type
     * @return AlertComponent
     */
    public function setType(string $type): AlertComponent
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param string $border
     * @return AlertComponent
     */
    public function setBorder(string $border): AlertComponent
    {
        $this->border = $border;
        return $this;
    }

    /**
     * @param bool $dense
     * @return AlertComponent
     */
    public function setDense(bool $dense): AlertComponent
    {
        $this->dense = $dense;
        return $this;
    }

    /**
     * @param bool $outlined
     * @return AlertComponent
     */
    public function setOutlined(bool $outlined): AlertComponent
    {
        $this->outlined = $outlined;
        return $this;
    }

    protected function getAdditionalData(): array
    {
        return [
            'text' => $this->text,
            'type' => $this->type,
            'border' => $this->border,
            'dense' => $this->dense,
            'outlined' => $this->outlined,
        ];
    }

    protected function getValue()
    {
        return null;
    }
}
