<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

class MaskedComponent extends Component
{
    protected $mask;

    protected function getComponentKey(): string
    {
        return "masked-component";
    }

    public function setMask(string $mask): MaskedComponent{
        $this->mask = $mask;
        return $this;
    }

    protected function getMask(): string{
        return $this->mask;
    }

    /**
     * @return array
     */
    protected function getAdditionalData() : array
    {
        return ['mask' => $this->getMask()];
    }

}
