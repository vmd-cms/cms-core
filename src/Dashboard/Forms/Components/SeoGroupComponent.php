<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

use VmdCms\CoreCms\Contracts\Models\SeoableInterface;
use VmdCms\CoreCms\Facades\FormComponent;

class SeoGroupComponent extends GroupComponent
{
    protected function getFormComponentsArray(): array
    {
        return [
            FormComponent::input('seo.info.meta_title', 'Meta Title'),
            FormComponent::text('seo.info.meta_description', 'Meta description'),
            FormComponent::ckeditor('seo.info.meta_text', 'Seo text'),
            FormComponent::image('seo.meta_photo', 'Meta Photo')
        ];
    }

    /**
     * @return bool
     */
    protected function checkImplements() : bool
    {
        return $this->cmsModel instanceof SeoableInterface;
    }
}
