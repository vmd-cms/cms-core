<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\HasDependedComponentsInterface;
use VmdCms\CoreCms\Traits\Dashboard\Forms\HasDependedComponents;

class DependedSelectComponent extends SelectComponent implements HasDependedComponentsInterface
{
    use HasDependedComponents;

    protected function getComponentKey(): string
    {
        return "depended-select-component";
    }
}
