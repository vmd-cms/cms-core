<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\InnerDataTableInterface;
use VmdCms\CoreCms\Services\Files\StoreFile;
use VmdCms\CoreCms\Traits\Dashboard\Forms\InnerDataTable;

class DataTableComponent extends Component implements InnerDataTableInterface
{
    use InnerDataTable;

    /**
     * @var bool
     */
    protected $hasMultipleImageUploader;

    /**
     * @var StoreFile
     */
    protected $storeFile;

    protected $columns;

    public function __construct(string $field, ?string $label = null)
    {
        parent::__construct($field, $label);
        $this->storeFile = new StoreFile();
        $this->setStoreCallback($this->getStoreTableDataCallback());
        $this->hasMultipleImageUploader = false;
    }

    /**
     * @param bool $isOrderable
     * @return $this
     */
    public function setIsOrderable(bool $isOrderable): self
    {
        $this->isOrderable = $isOrderable;
        return $this;
    }

    /**
     * @param string $info
     * @return $this
     */
    public function setInfo(string $info): self
    {
        $this->info = $info;
        return $this;
    }

    /**
     * @param bool $hasMultipleImageUploader
     * @return DataTableComponent
     */
    public function setHasMultipleImageUploader(bool $hasMultipleImageUploader): DataTableComponent
    {
        $this->hasMultipleImageUploader = $hasMultipleImageUploader;
        return $this;
    }

    protected function getComponentKey(): string
    {
        return "data-table-component";
    }

    protected function getAdditionalData(): array
    {
        return [
            'tableData' => $this->getTableData()->toArray(),
            'has_multiple_image_uploader' => boolval($this->hasMultipleImageUploader),
        ];
    }

    /**
     * @return bool|int|string
     */
    protected function getValue()
    {
        return null;
    }
}
