<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

use Illuminate\Support\Facades\Hash;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormComponentInterface;
use VmdCms\CoreCms\Exceptions\Dashboard\FormComponentException;
use VmdCms\CoreCms\Exceptions\Dashboard\SkipComponentException;

class PasswordComponent extends Component
{
    protected function getComponentKey(): string
    {
        return "password-component";
    }

    /**
     * @param null|string $value
     * @return string|null
     * @throws FormComponentException
     */
    protected function validateValue($value = null)
    {
        if(!$value) throw new SkipComponentException();
        return $value;
    }

    /**
     * @param null|string $value
     * @return string|null
     */
    protected function mutateValue($value = null)
    {
        return !empty($value) ? Hash::make($value) : null;
    }

    /**
     * @param string $value
     * @return FormComponentInterface
     */
    public function setValue($value): FormComponentInterface
    {
        $this->value = $value;
        return $this;
    }
}
