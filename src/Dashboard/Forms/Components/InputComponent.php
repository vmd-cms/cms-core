<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

class InputComponent extends Component
{
    protected function getComponentKey(): string
    {
        return "input-component";
    }
}
