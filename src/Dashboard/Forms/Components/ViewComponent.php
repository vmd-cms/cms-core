<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

class ViewComponent extends Component
{
    protected $viewPath;
    protected $data = [];

    protected function getComponentKey(): string
    {
        return "html-component";
    }

    public function isShowLabel(): bool
    {
        return false;
    }

    /**
     * @param string $viewPath
     * @return $this
     */
    public function setViewPath(string $viewPath): ViewComponent
    {
        $this->viewPath = $viewPath;
        return $this;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function setData(array $data): ViewComponent
    {
        $this->data = $data;
        return $this;
    }

    protected function getRenderedView()
    {
        $data = array_merge(['model'=>$this->cmsModel],$this->data);
        $viewPath = !empty($this->viewPath) ? $this->viewPath : $this->field;
        return view($viewPath,$data)->render();
    }

    protected function getAdditionalData(): array
    {
        return [
            'html' => $this->getRenderedView()
        ];
    }

    protected function getValue()
    {
        return null;
    }
}
