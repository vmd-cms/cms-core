<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

use VmdCms\CoreCms\Collections\FormComponentsCollection;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\ColumnComponentInterface;
use VmdCms\CoreCms\Traits\Dashboard\Forms\HasComponents;

class ColumnComponent extends Component implements ColumnComponentInterface
{
    use HasComponents;

    protected $cols;

    public function __construct(string $cols = '6', ?string $label = null)
    {
        parent::__construct('_raw_col_' . $cols, '');
        $this->cols = intval($cols);
    }

    /**
     * @return bool
     */
    protected function checkImplements() : bool
    {
        return true;
    }

    protected function getAdditionalData(): array
    {
        if(!$this->checkImplements()) return parent::getAdditionalData();
        return array_merge(
            parent::getAdditionalData(),
            [
                'components' => $this->components->setCmsModel($this->cmsModel)->toArray(),
                'cols' => $this->cols,
            ]
        );
    }

    public function appendComponents(array $components): ColumnComponentInterface
    {
        $this->setComponents($components);
        return $this;
    }

    protected function getComponentKey(): string
    {
        return '';
    }

    public function setCols(int $cols): ColumnComponentInterface
    {
        $this->cols = $cols;
        return $this;
    }
}
