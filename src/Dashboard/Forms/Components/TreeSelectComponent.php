<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Input\SelectInterface;
use VmdCms\CoreCms\Traits\Dashboard\Forms\Treeable;

class TreeSelectComponent extends Component implements SelectInterface
{
    use Treeable;

    protected function getNameField(): string
    {
        return 'label';
    }

    protected function getComponentKey(): string
    {
        return "tree-select-component";
    }

    /**
     * @param null|bool|int|string $value
     * @return string|bool|int|null
     */
    protected function mutateValue($value = null)
    {
        if($this->getMultiple() && is_string($value)){
            $value = explode(',',$value);
        }
        return $value;
    }
}
