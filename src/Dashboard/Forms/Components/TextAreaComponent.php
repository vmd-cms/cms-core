<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

class TextAreaComponent extends Component
{
    protected function getComponentKey(): string
    {
        return "text-area-component";
    }
}
