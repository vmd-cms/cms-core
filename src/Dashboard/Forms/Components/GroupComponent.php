<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\GroupComponentsInterface;
use VmdCms\CoreCms\Traits\Dashboard\Forms\HasComponents;

abstract class GroupComponent extends Component implements GroupComponentsInterface
{
    use HasComponents;

    public function __construct(string $field = null, ?string $label = null)
    {
        parent::__construct('_group_' . $field, '');
        $this->setComponents($this->getFormComponentsArray());
    }

    protected abstract function getFormComponentsArray() : array;

    /**
     * @return bool
     */
    protected function checkImplements() : bool
    {
        return true;
    }

    protected function getAdditionalData(): array
    {
        if(!$this->checkImplements()) return parent::getAdditionalData();
        return array_merge(
            parent::getAdditionalData(),
            [
                'group_components' => $this->components->setCmsModel($this->cmsModel)->toArray()
            ]
        );
    }


    protected function getComponentKey(): string
    {
        return '';
    }
}
