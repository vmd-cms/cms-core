<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

class RadioComponent extends Component
{
    protected function getComponentKey(): string
    {
        return "radio-component";
    }

    protected function getAdditionalData(): array
    {
        return [
            'title_on' => 'Вкл.',
            'title_off' => 'Выкл.',
        ];
    }

    /**
     * @return bool|int|string
     */
    public function getValue()
    {
        return boolval($this->value);
    }

    /**
     * @param bool|int|string|null $value
     * @return bool
     */
    public function getPreparedValue($value)
    {
        return is_bool($value) ? $value : $value !== 'false';
    }

    /**
     * @param null|bool|int|string $value
     * @return string|bool|int|null
     */
    protected function mutateValue($value = null)
    {
        return boolval($value);
    }
}
