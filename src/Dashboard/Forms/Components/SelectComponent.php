<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Input\SelectInterface;
use VmdCms\CoreCms\Traits\Dashboard\Forms\MultiValuable;

class SelectComponent extends Component implements SelectInterface
{
    use MultiValuable;

    protected function getComponentKey(): string
    {
        return "select-component";
    }


}
