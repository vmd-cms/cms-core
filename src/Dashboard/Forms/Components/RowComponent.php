<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

use VmdCms\CoreCms\Collections\FormComponentsCollection;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\ColumnComponentInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\RowComponentInterface;
use VmdCms\CoreCms\Traits\Dashboard\Forms\HasComponents;

class RowComponent extends Component implements RowComponentInterface
{
    use HasComponents;

    public function __construct(string $field = null, ?string $label = null)
    {
        parent::__construct('_raw_' . $field, '');
        $this->htmlAttributes->setClass('container','py-0');
    }

    /**
     * @return FormComponentsCollection
     */
    public function getColumns(): FormComponentsCollection
    {
        return $this->components;
    }

    /**
     * @param ColumnComponentInterface $component
     * @return RowComponentInterface
     */
    public function appendColumn(ColumnComponentInterface $component): RowComponentInterface
    {
        $this->setComponents([$component]);
        return $this;
    }

    /**
     * @return bool
     */
    protected function checkImplements() : bool
    {
        return true;
    }

    protected function getAdditionalData(): array
    {
        if(!$this->checkImplements()) return parent::getAdditionalData();
        return array_merge(
            parent::getAdditionalData(),
            [
                'is_row' => true,
                'columns' => $this->components->setCmsModel($this->cmsModel)->toArray()
            ]
        );
    }


    protected function getComponentKey(): string
    {
        return '';
    }
}
