<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

class SwitchComponent extends Component
{
    protected function getComponentKey(): string
    {
        return "switch-component";
    }

    /**
     * @return bool|int|string
     */
    public function getValue()
    {
        return $this->cmsModel ? boolval($this->value) : boolval($this->default);
    }

    /**
     * @param bool|int|string|null $value
     * @return bool
     */
    public function getPreparedValue($value)
    {
        return is_bool($value) ? $value : $value !== 'false';
    }

    /**
     * @param null|bool|int|string $value
     * @return string|bool|int|null
     */
    protected function mutateValue($value = null)
    {
        return boolval($value);
    }
}
