<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

class PhoneComponent extends MaskedComponent
{
    protected function getMask(): string
    {
        return empty($this->mask) ? '+##(###)###-##-##' : $this->mask;
    }
}
