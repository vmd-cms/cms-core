<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

class DatePicker extends Component
{
    protected function getComponentKey(): string
    {
        return 'date-time-picker-component';
    }

    protected function getAdditionalData(): array
    {
        return [
            'format' => "YYYY-MM-DD"
        ];
    }

    protected function mutateValue($value = null)
    {
        return $value === '' ? null : $value;
    }
}
