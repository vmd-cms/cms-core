<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Input\AsyncSelectInterface;
use VmdCms\CoreCms\Traits\Dashboard\Components\SearchableComponent;

class AsyncSelectComponent extends Component implements AsyncSelectInterface
{
    use SearchableComponent;

    /**
     * @return string
     */
    protected function getComponentKey(): string
    {
        return "async-select-component";
    }
}
