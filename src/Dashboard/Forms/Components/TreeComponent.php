<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Input\TreeInterface;
use VmdCms\CoreCms\Traits\Dashboard\Forms\Treeable;

class TreeComponent extends Component implements TreeInterface
{
    use Treeable;

    protected $selectionType = 'leaf';

    /**
     * @return TreeComponent
     */
    public function setSelectionTypeIndependent(): TreeComponent
    {
        $this->selectionType = 'independent';
        return $this;
    }

    protected function getComponentKey(): string
    {
        return "tree-component";
    }

    protected function getAdditionalData(): array
    {
        return  [
            'options' => $this->getOptions(),
            'is_id_int' => $this->isIdInt,
            'hide_details' => boolval($this->hideDetails),
            'multiple' => $this->getMultiple(),
            'selection_type' => $this->selectionType
        ];
    }

}
