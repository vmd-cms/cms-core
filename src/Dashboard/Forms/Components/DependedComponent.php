<?php

namespace VmdCms\CoreCms\Dashboard\Forms\Components;

use VmdCms\CoreCms\Contracts\Collections\DependedComponentsCollectionInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormComponentInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Input\DependedComponentInterface;
use VmdCms\CoreCms\Contracts\DTO\DependedComponentDtoInterface;
use VmdCms\CoreCms\Traits\Dashboard\HasCmsModel;
use VmdCms\CoreCms\Traits\Sections\AdminSectionSetFields;

class DependedComponent implements DependedComponentInterface
{
    use HasCmsModel,AdminSectionSetFields;

    /**
     * @var string
     */
    protected $field;

    /**
     * @var DependedComponentsCollectionInterface
     */
    protected $components;

    public function __construct(string $field, DependedComponentsCollectionInterface $components)
    {
        $this->field = $field;
        foreach ($components->getItems() as $component)
        {
            if(!$component instanceof DependedComponentDtoInterface) continue;
            $component->getFormComponent()->setField($this->field);
        }
        $this->components = $components;
    }

    /**
     * @param string|null $key
     * @return FormComponentInterface|null
     */
    public function getDependedFormComponentByKey(string $key = null) : ?FormComponentInterface
    {
        foreach ($this->components->getItems() as $dto)
        {
            if($dto instanceof DependedComponentDtoInterface && $dto->getKey() === $key)
            {
                return $dto->getFormComponent();
            }
        }
        return null;
    }

    private function prepareComponents()
    {
        foreach ($this->components->getItems() as $dto)
        {
            if($dto instanceof DependedComponentDtoInterface)
            {
                $dto->getFormComponent()->setField($this->field);
                $this->setComponentValue($dto->getFormComponent(),$this->cmsModel);
            }
        }
    }

    public function toArray()
    {
        $this->prepareComponents();
        return [
            'field' => $this->field,
            'components' => $this->components->toArray()
        ];
    }
}
