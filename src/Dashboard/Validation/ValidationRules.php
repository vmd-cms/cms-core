<?php

namespace VmdCms\CoreCms\Dashboard\Validation;

use VmdCms\CoreCms\Contracts\Collections\RulesCollectionInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Validation\ValidationRulesInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Services\CoreRouter;

trait ValidationRules
{
    /**
     * @var bool
     */
    protected $isRequired;

    /**
     * @return RulesCollectionInterface
     */
    protected abstract function getRulesCollection() : RulesCollectionInterface;

    /**
     * @param null|string $message
     * @return ValidationRulesInterface
     */
    public function required($message = null): ValidationRulesInterface
    {
        $this->isRequired = true;
        $this->getRulesCollection()->append(new Rule('required',null, $message ?? $this->label . ' ' . CoreLang::get('is_required')));
        return $this;
    }

    /**
     * @param null|string $message
     * @return ValidationRulesInterface
     */
    public function unique($message = null): ValidationRulesInterface
    {
        $this->getRulesCollection()->append(
            (new Rule('unique', $message ?? $this->label . ' ' . CoreLang::get('must_be_unique')))
                ->setAdditionalData(['route' => route(CoreRouter::ROUTE_CHECK_UNIQUE,null,false)])
        );
        return $this;
    }

    /**
     * @param null|string $message
     * @return ValidationRulesInterface
     */
    public function email($message = null): ValidationRulesInterface
    {
        $this->getRulesCollection()->append(new Rule('email',null, $message ?? $this->label . ' ' . CoreLang::get('must_be_valid_email')));
        return $this;
    }

    /**
     * @param int $value
     * @param null|string $message
     * @return ValidationRulesInterface
     */
    public function max($value = 10, $message = null): ValidationRulesInterface
    {
        $this->getRulesCollection()->append(new Rule('max',$value, $message ?? 'Max ' . $value));
        return $this;
    }

    /**
     * @param int $value
     * @param null|string $message
     * @return ValidationRulesInterface
     */
    public function min($value = 10, $message = null): ValidationRulesInterface
    {
        $this->getRulesCollection()->append(new Rule('min',$value, $message ?? 'Min ' . $value));
        return $this;
    }

    /**
     * @param int $value
     * @param null|string $message
     * @return ValidationRulesInterface
     */
    public function maxLength($value = 10, $message = null): ValidationRulesInterface
    {
        $this->getRulesCollection()->append(new Rule('maxLength',$value, $message ?? 'Max ' . $value . ' ' . CoreLang::get('characters')));
        return $this;
    }

    /**
     * @param int $value
     * @param null|string $message
     * @return ValidationRulesInterface
     */
    public function minLength($value = 10, $message = null): ValidationRulesInterface
    {
        $this->getRulesCollection()->append(new Rule('minLength',$value, $message ?? 'Min ' . $value . ' ' . CoreLang::get('characters')));
        return $this;
    }

    /**
     * @param null|bool $positive
     * @param null|string $message
     * @return ValidationRulesInterface
     */
    public function integer($positive = true, $message = null): ValidationRulesInterface
    {
        $this->getRulesCollection()->append(new Rule('integer',$positive, $message ?? $this->label . ' ' . CoreLang::get('must_be_number')));
        return $this;
    }

    /**
     * @param null|bool $positive
     * @param null|string $message
     * @return ValidationRulesInterface
     */
    public function float($positive = true, $message = null): ValidationRulesInterface
    {
        $this->getRulesCollection()->append(new Rule('float',$positive, $message ?? $this->label . ' ' . CoreLang::get('must_be_float_positive')));
        return $this;
    }
}
