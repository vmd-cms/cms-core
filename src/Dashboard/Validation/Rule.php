<?php

namespace VmdCms\CoreCms\Dashboard\Validation;

use VmdCms\CoreCms\Contracts\Dashboard\Validation\RuleInterface;

class Rule implements RuleInterface
{
    private $key;

    private $value;

    private $message;

    /**
     * @var array
     */
    private $additionalData;

    /**
     * @inheritDoc
     */
    public function __construct(string $key, string $value = null, $message = null)
    {
        $this->key = $key;
        $this->value = $value;
        $this->message = $message;
        $this->additionalData = [];
    }

    /**
     * @inheritDoc
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @inheritDoc
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @inheritDoc
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param array $data
     * @return RuleInterface
     */
    public function setAdditionalData(array $data) : RuleInterface
    {
        $this->additionalData = $data;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function toArray()
    {
        return [$this->key => array_merge([
            'value' => $this->value,
            'message' => $this->message
        ],$this->additionalData)
        ];
    }
}
