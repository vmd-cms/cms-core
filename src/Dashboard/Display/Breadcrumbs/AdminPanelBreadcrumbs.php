<?php

namespace VmdCms\CoreCms\Dashboard\Display\Breadcrumbs;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Facades\Route;
use VmdCms\CoreCms\Collections\BreadCrumbsCollection;
use VmdCms\CoreCms\Contracts\Sections\AdminSectionInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\CoreModules\Navigations\Models\CoreNav;
use VmdCms\CoreCms\CoreModules\Sections\Models\CoreSection;
use VmdCms\CoreCms\DTO\Dashboard\BreadCrumbDto;
use VmdCms\CoreCms\Services\CoreRouter;

class AdminPanelBreadcrumbs implements Arrayable
{
    /**
     * @var AdminSectionInterface
     */
    protected $section;

    /**
     * @var BreadCrumbsCollection
     */
    protected $breadCrumbsCollection;

    /**
     * AdminPanelBreadcrumbs constructor.
     * @param AdminSectionInterface $section
     */
    public function __construct(AdminSectionInterface $section)
    {
        $this->section = $section;
    }

    /**
     * @param BreadCrumbsCollection $collection
     * @return $this
     */
    public function setBreadCrumbsCollection(BreadCrumbsCollection $collection): AdminPanelBreadcrumbs
    {
        $this->breadCrumbsCollection = $collection;
        return $this;
    }

    protected function prepareBreadCrumbs(){
        $this->breadCrumbsCollection = new BreadCrumbsCollection();

        $coreSection = CoreSection::where('core_section_class',get_class($this->section))->first();
        if(!$coreSection instanceof CoreSection) return;

        $coreNav = CoreNav::where('section_id',$coreSection->id)->with('parent')->first();
        if(!$coreNav instanceof CoreNav) return;

        $this->setRecursiveParentGroups($coreNav);

        switch (Route::getCurrentRoute()->getName()){
            case CoreRouter::ROUTE_EDIT_GET: $title = CoreLang::get('editing');
            break;
            case CoreRouter::ROUTE_CREATE_GET: $title = CoreLang::get('new_entry');
            break;
            default: $title = null;
        }

        if($title){
            $this->breadCrumbsCollection->appendItem(new BreadCrumbDto('#',$title));
        }
    }

    protected function setRecursiveParentGroups(CoreNav $coreNav = null){
        if(!$coreNav instanceof CoreNav){
            return;
        }
        $this->breadCrumbsCollection->prependItem(new BreadCrumbDto($this->getNavLink($coreNav),$coreNav->titleLang));
        $this->setRecursiveParentGroups($coreNav->parent);
    }

    /**
     * @param CoreNav $coreNav
     * @return string|null
     */
    protected function getNavLink(CoreNav $coreNav): ?string
    {
        $link = null;
        if($coreNav->section && class_exists($coreNav->section->core_section_class)) {
            $sectionClass = $coreNav->section->core_section_class;
            $section = new $sectionClass();
            $link = $section instanceof AdminSectionInterface ? \route(CoreRouter::ROUTE_DISPLAY,['sectionSlug' => $section->getSectionSlug()]) : null;
        }
        return $link;
    }

    public function toArray()
    {
        if(!$this->breadCrumbsCollection instanceof BreadCrumbsCollection){
            $this->prepareBreadCrumbs();
        }
        return $this->breadCrumbsCollection->toArray();
    }
}
