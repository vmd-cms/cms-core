<?php

namespace VmdCms\CoreCms\Dashboard\Display\Blocks;

class DownloadBlock extends AbstractBlock
{
    /**
     * @var string
     */
    protected $confirmMessage;

    /**
     * @var string
     */
    protected $filename;

    /**
     * @param string $confirmMessage
     * @return DownloadBlock
     */
    public function setConfirmMessage(string $confirmMessage): DownloadBlock
    {
        $this->confirmMessage = $confirmMessage;
        return $this;
    }

    /**
     * @param string $filename
     * @return DownloadBlock
     */
    public function setFilename(string $filename): DownloadBlock
    {
        $this->filename = $filename;
        return $this;
    }

    protected function getAdditionalArr(): array
    {
        return [
            'filename' => $this->filename,
            'confirm_message' => $this->confirmMessage
        ];
    }

    /**
     * @return string
     */
    public function getComponentKey() : string
    {
        return 'download-block-component';
    }
}
