<?php

namespace VmdCms\CoreCms\Dashboard\Display\Blocks;

use VmdCms\CoreCms\Contracts\Dashboard\Display\Blocks\BlockInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Buttons\FormButtonHtmlAttributesInterface;
use VmdCms\CoreCms\Traits\Dashboard\Forms\FormButtonHtmAttributes;

abstract class AbstractBlock implements BlockInterface, FormButtonHtmlAttributesInterface
{
    use FormButtonHtmAttributes;

    const GROUP_ACTION_HEADER = 'action_header';
    const GROUP_ACTION_FOOTER = 'action_footer';

    /**
     * @var string
     */
    protected $title;
    protected $toolTip;
    protected $routePath;
    protected $groupKey;

    public function __construct(string $title = null)
    {
        $this->title = $title;
        $this->groupKey = static::GROUP_ACTION_FOOTER;
        $this->initializeComponentHtmlAttributesDTO();
    }

    /**
     * @param string $routePath
     * @return BlockInterface
     */
    public function setRoutePath(string $routePath): BlockInterface
    {
        $this->routePath = $routePath;
        return $this;
    }

    /**
     * @param string $groupKey
     * @return BlockInterface
     */
    public function setGroupKey(string $groupKey): BlockInterface
    {
        $this->groupKey = $groupKey;
        return $this;
    }

    /**
     * @param string $toolTip
     * @return AbstractBlock
     */
    public function setToolTip(string $toolTip)
    {
        $this->toolTip = $toolTip;
        return $this;
    }

    /**
     * @return string
     */
    public function getGroupKey(): string
    {
        return $this->groupKey;
    }

    protected function getAdditionalArr(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function toArray()
    {
        return array_merge([
            'title' => $this->title,
            'tooltip' => $this->toolTip,
            'route_path' => $this->routePath,
            'component' => $this->getComponentKey(),
            'htmlAttributes' => $this->htmlAttributes->toArray()
        ],$this->getAdditionalArr());
    }
}
