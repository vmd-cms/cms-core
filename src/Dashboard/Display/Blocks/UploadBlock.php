<?php

namespace VmdCms\CoreCms\Dashboard\Display\Blocks;

class UploadBlock extends AbstractBlock
{
    /**
     * @var string
     */
    protected $confirmMessage;
    /**
     * @param string $confirmMessage
     * @return UploadBlock
     */
    public function setConfirmMessage(string $confirmMessage): UploadBlock
    {
        $this->confirmMessage = $confirmMessage;
        return $this;
    }

    protected function getAdditionalArr(): array
    {
        return ['confirm_message' => $this->confirmMessage];
    }

    /**
     * @return string
     */
    public function getComponentKey() : string
    {
        return 'upload-block-component';
    }
}
