<?php

namespace VmdCms\CoreCms\Dashboard\Display;

use Illuminate\Database\Eloquent\Builder;
use VmdCms\CoreCms\Collections\BlocksCollection;
use VmdCms\CoreCms\Collections\CoreCollectionAbstract;
use VmdCms\CoreCms\Collections\FormButtonsCollection;
use VmdCms\CoreCms\Collections\TableColumnsCollection;
use VmdCms\CoreCms\Contracts\Collections\BlocksCollectionInterface;
use VmdCms\CoreCms\Contracts\Collections\FormButtonsCollectionInterface;
use VmdCms\CoreCms\Contracts\Collections\TableColumnsCollectionInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Blocks\BlockInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Components\TableColumnInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Filters\FilterableDisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Filters\FilterComponentInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Filters\FilterDisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\OrderableDisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\TableInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Buttons\FormButtonInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Contracts\Models\CmsInfoModelInterface;
use VmdCms\CoreCms\Contracts\Models\OrderableInterface;
use VmdCms\CoreCms\CoreModules\Content\Entity\Languages;
use VmdCms\CoreCms\Dashboard\Display\Components\ActionsColumn;
use VmdCms\CoreCms\Dashboard\Display\Components\Column;
use VmdCms\CoreCms\Dashboard\Display\Filters\FilterTreeComponent;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormButton;
use VmdCms\CoreCms\Exceptions\Dashboard\OrderableDisplayException;
use VmdCms\CoreCms\Services\CoreRouter;
use VmdCms\CoreCms\Traits\CrudPolicy\CrudRoutes;
use VmdCms\CoreCms\Traits\Dashboard\Display\FilterableDisplay;
use VmdCms\CoreCms\Traits\Dashboard\Display\OrderableDisplay;
use VmdCms\CoreCms\Traits\Models\FieldPathValue;

abstract class Table extends Display implements TableInterface
{
    use CrudRoutes,FieldPathValue,OrderableDisplay,FilterableDisplay;

    /**
     * @var TableColumnsCollectionInterface
     */
    protected $columns;

    /**
     * @var int
     */
    protected $itemsPerPage;

    /**
     * @var int
     */
    protected $rowsPerPageItems;

    /**
     * @var bool
     */
    protected $isSearchable;

    /**
     * @var array
     */
    protected $whereQuery;

    /**
     * @var bool
     */
    protected $whereNull;

    /**
     * @var callable
     */
    protected $orderDefault;

    /**
     * @var bool
     */
    protected $hideFooter;

    /**
     * @var string
     */
    protected $headerHTML;

    /**
     * @var string
     */
    protected $createBtnTitle;

    /**
     * @var FormButtonsCollectionInterface
     */
    protected $buttons;

    /**
     * @var BlocksCollectionInterface
     */
    protected $blocks;

    /**
     * @var bool
     */
    protected $showActionColumn;

    /**
     * @inheritDoc
     */
    public function __construct(array $columns = [])
    {
        $this->columns = new TableColumnsCollection();
        $this->setColumns($columns);
        $this->itemsPerPage = 20;
        $this->rowsPerPageItems = [20,50,100];
        $this->isSearchable = false;
        $this->whereQuery = [];
        $this->orderDefault = null;
        $this->hideFooter = false;
        $this->headerHTML = null;
        $this->showActionColumn = true;
        $this->createBtnTitle = trans('vmd_cms::dashboard.append');
    }

    /**
     * @param array $buttons
     * @return TableInterface
     */
    public function setButtons(array $buttons): TableInterface
    {
        if(!$this->buttons instanceof FormButtonsCollectionInterface)
        {
            $this->buttons = new FormButtonsCollection();
        }

        foreach ($buttons as $key => $button)
        {
            if($button instanceof FormButtonInterface) $this->buttons->appendItem($button);
        }
        return $this;
    }

    protected function getButtonsArray(){
        return $this->buttons instanceof FormButtonsCollectionInterface ? $this->buttons->toArray() : [];
    }

    /**
     * @param array $blocks
     * @return TableInterface
     */
    public function setBlocks(array $blocks): TableInterface
    {
        if(!$this->blocks instanceof BlocksCollectionInterface)
        {
            $this->blocks = new BlocksCollection();
        }

        foreach ($blocks as $key => $block)
        {
            if($block instanceof BlockInterface) $this->blocks->appendItem($block);
        }
        return $this;
    }

    protected function getBlocksArray(){
        return $this->blocks instanceof BlocksCollectionInterface ? $this->blocks->toArray() : [];
    }

    /**
     * @param string|null $title
     * @return TableInterface
     */
    public function setCreateBtnTitle(string $title = null): TableInterface
    {
        $this->createBtnTitle = $title;
        return $this;
    }

    /**
     * @param int $itemsPerPage
     * @return Table
     */
    public function setItemsPerPage(int $itemsPerPage): TableInterface
    {
        $this->itemsPerPage = $itemsPerPage;
        return $this;
    }

    /**
     * @param bool $hideFooter
     * @return TableInterface
     */
    public function setHideFooter(bool $hideFooter): TableInterface
    {
        $this->hideFooter = $hideFooter;
        return $this;
    }

    /**
     * @param bool $showActionColumn
     * @return TableInterface
     */
    public function setShowActionColumn(bool $showActionColumn): TableInterface
    {
        $this->showActionColumn = $showActionColumn;
        return $this;
    }

    /**
     * @param string $headerHTML
     * @return Table
     */
    public function setHeaderHTML(string $headerHTML): TableInterface
    {
        $this->headerHTML = $headerHTML;
        return $this;
    }

    public function setRowsPerPageItems(array $rowsPerPage){
        $this->rowsPerPageItems = $rowsPerPage;
        return $this;
    }

    /**
     * @param array|null $conditions
     * @return TableInterface
     */
    public function setWhereQuery(array $conditions = null) : TableInterface
    {
        if(is_array($conditions) && isset($conditions[0]) && is_array($conditions[0])){
            $this->whereQuery = array_merge($this->whereQuery,$conditions);
        }
        elseif(is_array($conditions)){
            $this->whereQuery[] = $conditions;
        }
        return $this;
    }

    /**
     * @return TableInterface
     */
    public function whereNull(string $field): TableInterface
    {
        $this->whereNull = $field;
        return $this;
    }

    /**
     * @param callable $callback
     * @return TableInterface
     */
    public function orderDefault(callable $callback): TableInterface
    {
        $this->orderDefault = $callback;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setColumns(array $columns): TableInterface
    {
        foreach ($columns as $column)
        {
            if($column instanceof TableColumnInterface) $this->columns->appendItem($column);
        }
        return $this;
    }

    /**
     * @throws \VmdCms\CoreCms\Exceptions\Dashboard\TableDisplayException
     * @throws \VmdCms\CoreCms\Exceptions\Models\NotCmsModelException
     */
    protected function prependColumns()
    {
        if($this instanceof OrderableDisplayInterface) {
            try {
                $this->prependOrderableTable();
            }
            catch (OrderableDisplayException $exception){}
        }

        if($this->showActionColumn){
            $this->columns->appendItem(((new ActionsColumn('_column_actions','Действие'))
                ->setWidth(150))->setSection($this->section));
        }

    }

    /**
     * @param array $options
     * @return mixed
     */
    protected function getItems(array $options = [])
    {
        $query = $this->getItemsQuery($options);

        $query = $this->appendSortQuery($query,$options);

        if(isset($options['itemsPerPage'])){
            $query->limit($options['itemsPerPage']);
        }

        if(isset($options['itemsPerPage']) && isset($options['page']) && $options['page'] > 1){
            $query->skip($options['itemsPerPage'] * ($options['page'] - 1));
        }

        return $query->get();
    }

    /**
     * @param array $options
     * @return Builder
     */
    protected function getItemsQuery(array $options = []){

        $query = $this->cmsModel::query();

        if($this instanceof FilterableDisplayInterface &&
            $this->filterDisplay instanceof FilterDisplayInterface)
        {
            $filterComponents = $this->filterDisplay->getComponentsArray();
            foreach ($filterComponents as $componentObj)
            {
                if(!$componentObj instanceof FilterComponentInterface){
                    continue;
                }
                $component = $componentObj->toArray();
                $filterSlug = $component['filter_slug'] ?? null;
                if(!$filterSlug) continue;
                $filterValue = request()->query->get($filterSlug, null);
                $filterValue = $filterValue === 'null' ? null : $filterValue;

                if(is_callable($componentObj->getFilterQueryCallback()))
                {
                    call_user_func($componentObj->getFilterQueryCallback(),$query,$filterValue);
                    continue;
                }

                $filterField = $component['filter_field'] ?? null;
                if(!$filterField) continue;


                if(empty($filterValue) && !$component instanceof FilterTreeComponent){
                    continue;
                }

                $query->where($filterField, $filterValue);
                if($this->whereNull == $filterField){
                    $this->whereNull = null;
                }
            }
        }

        $searchValue = $options['search'] ?? null;

        if(!empty($searchValue) && mb_strlen($searchValue) >= 1){

            $query->where(function ($searchQuery) use ($searchValue,$query){

                foreach ($this->columns->getItems() as $column)
                {
                    if(!$column instanceof TableColumnInterface || !$column->isSearchable()){
                        continue;
                    }

                    $searchableCallback = $column->getSearchableCallback();

                    if(is_callable($searchableCallback)){
                        call_user_func($searchableCallback,$searchQuery,$searchValue);
                        continue;
                    }

                    $fieldPath = explode('.',$column->getField());
                    if(count($fieldPath) == 1){
                        $searchQuery->orWhere($column->getField(),'like','%' . $searchValue . '%');
                    }
                    else{

                        $field = array_pop($fieldPath);
                        $relation = array_shift($fieldPath);
                        $searchQuery->orWhereHas($relation, function ($whereHasQuery)use ($field,$searchValue){
                            $whereHasQuery->where($field,'like','%' . $searchValue . '%');
                        });
                    }
                }
            });
        }



        if(is_array($this->whereQuery))
        {
            foreach ($this->whereQuery as $conditions)
            {
                if(!is_array($conditions) || !isset($conditions[0])) continue;
                $query->where(
                    $conditions[0],
                    array_key_exists(2,$conditions) ? $conditions[1] : '=',
                    array_key_exists(2,$conditions) ? $conditions[2] : $conditions[1]
                );

            }
        }

        if($this->whereNull){
            $query->whereNull($this->whereNull);
        }

        return $query;
    }

    protected function appendSortQuery($query,array $options = [])
    {
        if(isset($options['sortBy']) && count($options['sortBy'])){

            foreach ($options['sortBy'] as $key=>$sortBy){

                $column = $this->columns->getItem($sortBy);

                if(!$column instanceof TableColumnInterface || !$column->isSortable()){
                    continue;
                }

                $sortType = isset($options['sortDesc'][$key]) && boolval($options['sortDesc'][$key]) ? 'desc' : 'asc';

                $sortableCallback = $column->getSortableCallback();

                if(is_callable($sortableCallback)){
                    call_user_func($sortableCallback,$query,$sortType);
                    continue;
                }

                $fieldPath = explode('.',$sortBy);
                if(count($fieldPath) == 1){
                    $query->orderBy($sortBy,$sortType);
                }
                else{
                    $field = array_pop($fieldPath);
                    $relation = array_shift($fieldPath);
                    $related = $this->cmsModel->$relation()->getRelated();
                    $foreignField = $related::getForeignField();
                    $primaryField = $this->cmsModel::table() . '.' . $this->cmsModel::getPrimaryField();
                    if($related instanceof CmsInfoModelInterface){
                        $query->join($related::table(), function ($join) use ($related,$primaryField,$foreignField){
                            $join->on($primaryField,'=',$related::table() . '.' . $foreignField);
                            $join->where($related::table() . '.lang_key',Languages::getInstance()->getCurrentLocale());
                        });
                        $query->orderBy($related::table() . '.' . $field, $sortType);
                    }
                    else{
                        $query->join($related::table(),$related::table() .'.' . $foreignField , '=' , $this->cmsModel::table() . '.' . $this->cmsModel::getPrimaryField());
                        $query->orderBy($related::table() . '.' . $field, $sortType);
                    }
                }
            }
        }
        elseif (is_callable($this->orderDefault)){
            call_user_func($this->orderDefault,$query);
        }
        elseif ($this->cmsModel instanceof OrderableInterface){
            $query->order();
        }
        return $query;
    }

    /**
     * @param array|null $options
     * @return mixed
     */
    protected function getTotalItems(array $options = null)
    {
        $query = $this->getItemsQuery($options);

        return $query->count();
    }

    /**
     * @return array[]
     * @throws \VmdCms\CoreCms\Exceptions\Dashboard\TableDisplayException
     * @throws \VmdCms\CoreCms\Exceptions\Models\NotCmsModelException
     */
    protected function getTableData() : array
    {
        $this->prependColumns();

        $headers = $items = $items_objs = [];

        foreach ($this->columns->getItems() as $column){
            $headers[] = [
                'value' => $column->getField(),
                'text' => $column->getLabel(),
                'component' => $column->getComponentKey(),
                'width' => $column->getWidth(),
                'align' => $column->getAlign(),
                'sortable' => $column->isSortable(),
            ];
        }

//        foreach ($this->getItems() as $model)
//        {
//            $item = $item_obj = [];
//            foreach ($this->columns->getItems() as $column)
//            {
//                $column->setCmsModel($model);
//                $item[$column->getField()] = $column->getValue();
//                $item_obj[$column->getField()] = $column->toArray();
//            }
//            $items[] = $item;
//            $items_objs[] = $item_obj;
//        }

        return [
            'table' => [
                'headers' => $headers,
//                'items' => $items,
//                'items_objs' => $items_objs,
//                'total' => $this->getTotalItems($options)
            ]
        ];
    }

    /**
     * @param array $options
     * @return array[]
     * @throws \VmdCms\CoreCms\Exceptions\Dashboard\TableDisplayException
     * @throws \VmdCms\CoreCms\Exceptions\Models\NotCmsModelException
     */
    public function getTableDataAsync(array $options): array{

        $this->prependColumns();

        $items_objs = $items = [];

        foreach ($this->getItems($options) as $model)
        {
            $item = $item_obj = [];
            foreach ($this->columns->getItems() as $column)
            {
                $column->setCmsModel($model);
                $item[$column->getField()] = $column->getValue();
                $item_obj[$column->getField()] = $column->toArray();
            }

            $items[] = $item;
            $items_objs[] = $item_obj;
        }

        return [
            'table' => [
                'items' => $items,
                'items_objs' => $items_objs,
                'total' => $this->getTotalItems($options)
            ]
        ];
    }

    /**
     * @return array
     */
    protected function getAdditionalData() : array
    {
        return [];
    }

    protected function getServiceData()
    {
        $serviceData['routes'] = [
            'create' => $this->getCreateRoute(),
            'create_btn_title' => $this->createBtnTitle,
            'dataTableAsync' => $this->getDataTableAsyncRoute()
        ];
        $serviceData['buttons'] = $this->getButtonsArray();
        $serviceData['blocks'] = $this->getBlocksArray();
        $serviceData['info'] = [
            'title' => $this->getSection()->getTitle(),
            'rowsPerPageItems' => $this->rowsPerPageItems,
            'itemsPerPage' => $this->itemsPerPage,
            'is_searchable' => $this->isSearchable,
            'hideFooter' => $this->hideFooter,
            'customHeader' => $this->headerHTML,
        ];
        $serviceData['where_query'] = $this->whereQuery;

        if($this instanceof OrderableDisplayInterface && $slugSection = $this->getSection()->getSectionSlug())
        {
            $serviceData['routes']['updateTableOrder'] = CoreRouter::getTableOrderUpdateRoute($slugSection);
            $serviceData['routes']['updateTableItem'] = CoreRouter::getTableUpdateItemRoute($slugSection);
            $serviceData['info']['orderable'] = $this->orderable;
        }
        return $serviceData;
    }

    /**
     * @inheritDoc
     */
    public function setSearchable(bool $flag): TableInterface
    {
        $this->isSearchable = $flag;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function toArray()
    {
        return array_merge(
            $this->getTableData(),
            $this->getAdditionalData(),
            $this->getServiceData()
        );
    }
}
