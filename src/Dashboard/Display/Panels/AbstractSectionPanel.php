<?php

namespace VmdCms\CoreCms\Dashboard\Display\Panels;

use VmdCms\CoreCms\Contracts\Dashboard\Display\Panels\SectionPanelInterface;

abstract class AbstractSectionPanel implements SectionPanelInterface
{
    /**
     * @var string;
     */
    protected $viewPath;

    /**
     * @var array
     */
    protected $data;

    protected $additionalPanels;

    /**
     * @var int
     */
    protected $colsMd;

    /**
     * SectionPanel constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->viewPath = 'vmd_cms::admin.views.sections.display.panels.col_group_panel';
        $this->data = $data;
        $this->colsMd = 6;
    }

    /**
     * @param string $viewPath
     * @return SectionPanelInterface
     */
    public function setViewPath(string $viewPath): SectionPanelInterface
    {
        $this->viewPath = $viewPath;
        return $this;
    }

    /**
     * @param SectionPanelInterface[] $additionalPanels
     * @return SectionPanelInterface
     */
    public function setAdditionalPanels(array $additionalPanels): SectionPanelInterface
    {
        $this->additionalPanels = $additionalPanels;
        return $this;
    }

    /**
     * @param int $cols
     * @return SectionPanelInterface
     */
    public function setColsMd(int $cols): SectionPanelInterface
    {
        $this->colsMd = $cols;
        return $this;
    }

    public function getColsMd(): int
    {
        return $this->colsMd;
    }

    protected function getPreparedData(): array
    {
        $data = [
            'data' => $this->data,
            'panels' => []
        ];

        if(is_countable($this->additionalPanels)){
            foreach ($this->additionalPanels as $additionalPanel)
            {
                if(!$additionalPanel instanceof SectionPanelInterface) continue;

                $data['panels'][] = [
                    'cols_md' => $additionalPanel->getColsMd(),
                    'content' => $additionalPanel->render(),
                ];
            }
        }
        return $data;
    }

    public function render()
    {
        return view($this->viewPath,$this->getPreparedData())->render();
    }
}
