<?php

namespace VmdCms\CoreCms\Dashboard\Display\Panels;

use VmdCms\CoreCms\Contracts\Dashboard\Display\Filters\FilterDisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Panels\FilterPanelInterface;

class FilterPanel implements FilterPanelInterface
{
    protected $filterDisplay;

    /**
     * FilterPanel constructor.
     * @param FilterDisplayInterface|null $filterDisplay
     */
    public function __construct(?FilterDisplayInterface $filterDisplay = null)
    {
        $this->filterDisplay = $filterDisplay;
    }

    public function render()
    {
        return $this->filterDisplay instanceof FilterDisplayInterface ? $this->filterDisplay->render() : '';
    }
}
