<?php

namespace VmdCms\CoreCms\Dashboard\Display\Panels;

use VmdCms\CoreCms\Contracts\Dashboard\Display\Panels\FooterPanelInterface;

class FooterPanel implements FooterPanelInterface
{

    public function render()
    {
        return view('vmd_cms::admin.views.sections.panels.footer_panel',['data' => []])->render();
    }
}
