<?php

namespace VmdCms\CoreCms\Dashboard\Display\Panels;

use VmdCms\CoreCms\Collections\BreadCrumbsCollection;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Panels\HeadPanelInterface;
use VmdCms\CoreCms\Dashboard\Display\Breadcrumbs\AdminPanelBreadcrumbs;

class HeadPanel implements HeadPanelInterface
{
    /**
     * @var string
     */
    protected $title;

    /**
     * @var AdminPanelBreadcrumbs
     */
    protected $breadCrumbs;

    /**
     * HeadPanel constructor.
     * @param string $title
     * @param AdminPanelBreadcrumbs|null $breadCrumbs
     */
    public function __construct(string $title,AdminPanelBreadcrumbs $breadCrumbs = null)
    {
        $this->title = $title;
        $this->breadCrumbs = $breadCrumbs;
    }

    /**
     * @param AdminPanelBreadcrumbs $breadCrumbs
     */
    public function setBreadCrumbs(AdminPanelBreadcrumbs $breadCrumbs)
    {
        $this->breadCrumbs = $breadCrumbs;
    }

    public function toArray()
    {
        return [
          'title' => $this->title,
          'bread_crumbs' => $this->breadCrumbs instanceof AdminPanelBreadcrumbs ? $this->breadCrumbs->toArray() : []
        ];
    }

    public function render()
    {
        return view('vmd_cms::admin.views.sections.panels.head_panel',['data' => $this->toArray()])->render();
    }
}
