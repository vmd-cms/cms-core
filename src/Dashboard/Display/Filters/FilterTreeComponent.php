<?php

namespace VmdCms\CoreCms\Dashboard\Display\Filters;

use Illuminate\Support\Facades\Route;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Filters\FilterComponentInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Filters\FilterComponentItemDTOCollectionInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Filters\FilterComponentItemDTOInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Contracts\Models\TreeableInterface;
use VmdCms\CoreCms\DTO\Dashboard\Filters\FilterComponentItemDTO;
use VmdCms\CoreCms\DTO\Dashboard\Filters\FilterComponentItemDTOCollection;
use VmdCms\CoreCms\Exceptions\Models\NotTreeableException;
use VmdCms\CoreCms\Services\CoreRouter;

class FilterTreeComponent extends AbstractFilterComponent
{
    /**
     * @var int
     */
    protected $maxLevel;

    /**
     * FilterTreeComponent constructor.
     * @param string $titleField
     */
    public function __construct(string $titleField)
    {
        parent::__construct($titleField);
        $this->maxLevel = 1;
    }

    public function render()
    {
       return view('vmd_cms::admin.views.sections.filters.filter_tree_component',['data' => $this->toArray()])->render();
    }


    public function getComponentKey(): string
    {
        return 'filter-tree-component';
    }

    /**
     * @param int $level
     * @return $this|FilterComponentInterface
     */
    public function setMaxLevel(int $level) : FilterComponentInterface
    {
        $this->maxLevel = $level;
        return $this;
    }

    protected function retrieveItems() : FilterComponentItemDTOCollectionInterface
    {
        if(is_array($this->items)) return $this->items;

        if(is_callable($this->retrieveItems)) return $this->items = call_user_func($this->retrieveItems);
        $models = $this->cmsModel::tree()->get();
        $items = new FilterComponentItemDTOCollection();
        foreach ($models as $item)
        {
            $dto = $this->recursiveGetItem($item);
            if($dto instanceof FilterComponentItemDTOInterface) {
                $items->appendItem($dto);
            }

        }
        return $this->items = $items;
    }

    /**
     * @param CmsModelInterface $item
     * @return null|FilterComponentItemDTOInterface
     */
    protected function recursiveGetItem(CmsModelInterface $item): ?FilterComponentItemDTOInterface
    {
        if(!$item instanceof CmsModelInterface) return null;

        $primaryField = $this->cmsModel::getPrimaryField();

        $dto = (new FilterComponentItemDTO($item->$primaryField,$this->getTitle($item),$this->getFilterSlug()))
        ->setParentId($item->parent_id);
        $children = new FilterComponentItemDTOCollection();
        if(isset($item->childrenTree) && count($item->childrenTree))
        {
            foreach ($item->childrenTree as $child)
            {
                $childDto = $this->recursiveGetItem($child);
                if($childDto instanceof FilterComponentItemDTOInterface){
                    $children->appendItem($this->recursiveGetItem($child));
                }
            }
        }
        return $dto->setChildren($children);
    }

    /**
     * @return array|null
     */
    protected function getCurrentNode() :array
    {
        $queryParam = request()->query->get($this->getFilterSlug(), null);
        if($queryParam !== null) {
            $queryParam = $queryParam === 'null' ? null : $queryParam;
        }
        return [
            'item' => [$queryParam],
            'parents' => $this->recursiveGetParents($this->items, $queryParam)
        ];
    }

    /*
     *
     */
    private function recursiveGetParents($items,$target)
    {
        if(empty($target)) return null;

        foreach ($items as $item)
        {
            if($item['id'] == $target) {
                return [];
            }

            if(isset($item['children']) && count($item['children']))
            {

                $result = $this->recursiveGetParents($item['children'],  $target);
                if(is_array($result))
                {
                    return array_merge($result,[$item['id']]);
                }
            }
        }
        return null;
    }

}
