<?php

namespace VmdCms\CoreCms\Dashboard\Display\Filters;

use Illuminate\Support\Facades\Route;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Filters\FilterComponentInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Filters\FilterComponentItemDTOCollectionInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Contracts\Models\TreeableInterface;
use VmdCms\CoreCms\DTO\Dashboard\Filters\FilterComponentItemDTO;
use VmdCms\CoreCms\DTO\Dashboard\Filters\FilterComponentItemDTOCollection;
use VmdCms\CoreCms\Exceptions\Models\ClassNotFoundException;
use VmdCms\CoreCms\Exceptions\Models\InstanceException;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;
use VmdCms\CoreCms\Exceptions\Models\NotTreeableException;
use VmdCms\CoreCms\Services\CoreRouter;

abstract class AbstractFilterComponent implements FilterComponentInterface
{
    /**
     * @var callable
     */
    protected $retrieveItems;

    /**
     * @var callable
     */
    protected $chipColorCallback;

    /**
     * @var callable
     */
    protected $countCallback;

    /**
     * @var callable
     */
    protected $filterQueryCallback;

    /**
     * @var bool
     */
    protected $withChip;

    /**
     * @var bool
     */
    protected $withCount;

    /**
     * @var CmsModelInterface
     */
    protected $cmsModel;

    /**
     * @var string
     */
    protected $titleField;

    /**
     * @var string
     */
    protected $filterSlug;

    /**
     * @var string
     */
    protected $filterField;

    /**
     * @var string
     */
    protected $headTitle;

    /**
     * @var string
     */
    protected $expansion;

    /**
     * @var FilterComponentItemDTOCollectionInterface
     */
    protected $items;

    /**
     * @var bool
     */
    protected $isExpansion;

    /**
     * FilterTreeComponent constructor.
     * @param string $titleField
     */
    public function __construct(string $titleField)
    {
        $this->titleField = $titleField;
        $this->expansion = "Все";
    }

    /**
     * @param string $filterSlug
     * @return $this|FilterComponentInterface
     */
    public function setFilterSlug(string $filterSlug): FilterComponentInterface
    {
        $this->filterSlug = $filterSlug;
        return $this;
    }

    /**
     * @param string $filterField
     * @return $this|FilterComponentInterface
     */
    public function setFilterField(string $filterField): FilterComponentInterface
    {
        $this->filterField = $filterField;
        return $this;
    }

    public function getFilterSlug(): string
    {
        return $this->filterSlug ?? 'filter_id';
    }

    public function getFilterField(): string
    {
        return $this->filterField ?? 'filter_id';
    }

    /**
     * @param string $title
     * @return FilterComponentInterface
     */
    public function setHeadTitle(string $title): FilterComponentInterface
    {
        $this->headTitle = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getHeadTitle(): string
    {
        return $this->headTitle;
    }

    /**
     * @param string $title
     * @return FilterComponentInterface
     */
    public function setExpansionTitle(string $title): FilterComponentInterface
    {
        $this->expansion = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getExpansionTitle(): string
    {
        return $this->expansion;
    }

    /**
     * @param string $cmsModelClass
     * @return FilterComponentInterface
     * @throws NotCmsModelException
     */
    public function setModelClass(string $cmsModelClass): FilterComponentInterface
    {
        $cmsModel = new $cmsModelClass;
        if (!$cmsModel instanceof CmsModelInterface) throw new NotCmsModelException();
        $this->cmsModel = $cmsModel;
        return $this;
    }

    /**
     * @param callable $callback
     * @return FilterComponentInterface
     */
    public function setRetrieveItemsCallback(callable $callback): FilterComponentInterface
    {
        $this->retrieveItems = $callback;
        return $this;
    }

    /**
     * @param callable $callback
     * @return FilterComponentInterface
     */
    public function setCountCallback(callable $callback): FilterComponentInterface
    {
        $this->withCount = true;
        $this->countCallback = $callback;
        return $this;
    }

    /**
     * @param callable $callback
     * @return FilterComponentInterface
     */
    public function setChipColorCallback(callable $callback): FilterComponentInterface
    {
        $this->withChip = true;
        $this->chipColorCallback = $callback;
        return $this;
    }

    /**
     * @param callable $callback
     * @return FilterComponentInterface
     */
    public function setFilterQueryCallback(callable $callback): FilterComponentInterface
    {
        $this->filterQueryCallback = $callback;
        return $this;
    }

    /**
     * @return null|callable
     */
    public function getFilterQueryCallback(): ?callable
    {
        return $this->filterQueryCallback;
    }

    /**
     * @param bool $flag
     * @return FilterComponentInterface
     */
    public function setIsExpansion(bool $flag): FilterComponentInterface
    {
        $this->isExpansion = $flag;
        return $this;
    }

    /**
     * @return bool
     */
    public function isExpansion(): bool
    {
        return boolval($this->isExpansion);
    }


    public function toArray()
    {
        return [
            'items' => $this->retrieveItems()->getItems(),
            'filter_slug' => $this->getFilterSlug(),
            'filter_field' => $this->getFilterField(),
            'component' => $this->getComponentKey(),
            'head_title' => $this->getHeadTitle(),
            'is_expansion' => $this->isExpansion(),
            'expansion_title' => $this->getExpansionTitle(),
            'current' => $this->getCurrentNode()
        ];
    }

    protected function retrieveItems(): FilterComponentItemDTOCollectionInterface
    {
        if (is_array($this->items)) return $this->items;

        if (is_callable($this->retrieveItems)) {
            $items = call_user_func($this->retrieveItems);
            if(!$items instanceof FilterComponentItemDTOCollectionInterface){
                throw new InstanceException('Wrong component collection interface');
            }
            return $this->items = $items;
        }
        $models = $this->cmsModel::get();
        $items = new FilterComponentItemDTOCollection();
        $primaryField = $this->cmsModel::getPrimaryField();
        foreach ($models as $item) {
            $items->appendItem((new FilterComponentItemDTO(
                $item->$primaryField,
                $this->getTitle($item),
                $this->getFilterSlug()))
                ->setChipColor($this->getItemChipColor($item))
                ->setCount($this->getItemCount($item))
            );
        }
        return $this->items = $items;
    }

    protected function getTitle(CmsModelInterface $item)
    {
        $fields = explode('.', $this->titleField);
        $value = $item;
        foreach ($fields as $subField) {
            $value = $value->$subField ?? null;

        }
        return $value;
    }

    protected function getItemChipColor(CmsModelInterface $model)
    {
        if (is_callable($this->chipColorCallback)) {
            return call_user_func($this->chipColorCallback, $model);
        }
        return null;
    }

    protected function getItemCount(CmsModelInterface $model)
    {
        if (is_callable($this->countCallback)) {
            return call_user_func($this->countCallback, $model);
        }
        return null;
    }

    /**
     * @return array|null
     */
    protected function getCurrentNode(): array
    {
        $queryParam = request()->query->get($this->getFilterSlug(), null);
        if ($queryParam !== null) {
            $queryParam = $queryParam === 'null' ? null : $queryParam;
        }
        return [
            'item' => [$queryParam]
        ];
    }

}
