<?php

namespace VmdCms\CoreCms\Dashboard\Display\Filters;

class FilterComponent extends AbstractFilterComponent
{
    public function render()
    {
       return view('vmd_cms::admin.views.sections.filters.filter_component',['data' => $this->toArray()])->render();
    }

    public function getComponentKey(): string
    {
        return 'filter-component';
    }
}
