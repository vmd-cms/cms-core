<?php

namespace VmdCms\CoreCms\Dashboard\Display\Filters;

use VmdCms\CoreCms\Contracts\Dashboard\Display\Filters\FilterComponentInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Filters\FilterDisplayInterface;
use VmdCms\CoreCms\DTO\NavDTO;

class FilterDisplay implements FilterDisplayInterface
{

    /**
     * @var FilterComponentInterface[]
     */
    protected $filterComponents;

    /**
     * @var NavDTO
     */
    protected $headerLink;

    public function render()
    {
        return view('vmd_cms::admin.views.sections.filters.filter_panel',['data' => $this->toArray()])->render();
    }

    /**
     * @param NavDTO $headerLink
     * @return FilterDisplayInterface
     */
    public function setHeaderLink(NavDTO $headerLink): FilterDisplayInterface
    {
        $this->headerLink = $headerLink;
        return $this;
    }

    public function appendFilterComponent(FilterComponentInterface $filterComponent): FilterDisplayInterface
    {
        $this->filterComponents[] = $filterComponent;
        return $this;
    }

    public function getComponentsArray(): array
    {
        return $this->filterComponents;
    }

    /**
     * @return array
     */
    protected function getComponents()
    {
        $components = [];
        if(!is_array($this->filterComponents)) return $components;

        foreach ($this->filterComponents as $component)
        {
            $components[] = $component->toArray();
        }
        return $components;
    }

    public function toArray()
    {
        return [
            'components' => $this->getComponents(),
            'header_link' => $this->headerLink instanceof NavDTO ? $this->headerLink->toArray() : null
        ];
    }

}
