<?php

namespace VmdCms\CoreCms\Dashboard\Display\Components;

class TextColumn extends Column
{
    /**
     * @return string
     */
    public function getComponentKey() : string
    {
        return 'column-text-component';
    }
}
