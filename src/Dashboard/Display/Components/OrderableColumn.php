<?php

namespace VmdCms\CoreCms\Dashboard\Display\Components;

class OrderableColumn extends Column
{
    public function getAdditionalData(): array
    {
        $primaryField = $this->cmsModel::getPrimaryField();
        return [
            'template' => 'orderable',
            'id' => $this->cmsModel->$primaryField
        ];
    }

    /**
     * @return string
     */
    public function getComponentKey() : string
    {
        return 'column-order-component';
    }
}
