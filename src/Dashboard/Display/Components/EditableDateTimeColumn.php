<?php

namespace VmdCms\CoreCms\Dashboard\Display\Components;

use Illuminate\Support\Carbon;

class EditableDateTimeColumn extends EditableColumn
{
    private $format;

    public function getComponentKey(): string
    {
        return 'editable-column-date-time-component';
    }

    public function getSlotComponentKey(): string
    {
        return '';
    }

    public function getLabelComponentKey(): string
    {
        return '';
    }

    public function setFormat(string $format): EditableDateTimeColumn
    {
        $this->format = $format;
        return $this;
    }

    protected function getFormattedValue($value)
    {
        return Carbon::parse($value)->format($this->format ?? "Y-m-d H:i:s");
    }

    public function toArray(): array
    {
        return array_merge(parent::toArray(),[
            'format' => $this->format ?? "YYYY-MM-DD HH:mm:ss"
        ]);
    }
}
