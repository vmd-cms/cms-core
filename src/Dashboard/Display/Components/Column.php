<?php

namespace VmdCms\CoreCms\Dashboard\Display\Components;

use VmdCms\CoreCms\Contracts\Dashboard\Display\Components\TableColumnInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Traits\Dashboard\Display\EditableColumn;
use VmdCms\CoreCms\Traits\Dashboard\HasCmsModel;
use VmdCms\CoreCms\Traits\Dashboard\HasFieldComponent;
use VmdCms\CoreCms\Traits\Dashboard\HasHtmlAttributes;
use VmdCms\CoreCms\Traits\Dashboard\SearchableColumn;
use VmdCms\CoreCms\Traits\Dashboard\SortableColumn;
use VmdCms\CoreCms\Traits\Models\FieldPathValue;

abstract class Column implements TableColumnInterface
{
    use HasFieldComponent,FieldPathValue,HasCmsModel,EditableColumn,HasHtmlAttributes, SearchableColumn, SortableColumn;

    /**
     * @var callable
     */
    protected $valueCallback;

    /**
     * @inheritDoc
     */
    public function __construct(string $field, string $label = null)
    {
        $this->setField($field);
        $this->setLabel($label ?? CoreLang::get($field));
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        return '';
    }

    public function setValueCallback(callable $callback){
        $this->valueCallback = $callback;
        return $this;
    }

    public function getValue()
    {
        if(is_callable($this->valueCallback))
        {
            return call_user_func($this->valueCallback,$this->cmsModel);
        }
        return $this->getFormattedValue($this->getFieldValue($this->getField(),$this->cmsModel));
    }

    protected function getFormattedValue($value)
    {
        return $value;
    }

    /**
     * @return array
     */
    public function getAdditionalData() : array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function toArray()
    {
        return array_merge([
            'field' => $this->getField(),
            'label' => $this->getLabel(),
            'component' => $this->getComponentKey(),
            'value' => $this->getValue(),
            'editable' => $this->editable,
            'sortable' => $this->isSortable()
        ],$this->getAdditionalData());
    }
}
