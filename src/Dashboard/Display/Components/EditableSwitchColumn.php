<?php

namespace VmdCms\CoreCms\Dashboard\Display\Components;

use VmdCms\CoreCms\CoreModules\Content\Entity\Languages;
use VmdCms\CoreCms\CoreModules\Content\Entity\Translates;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\DTO\Dashboard\SwitchEnum;

class EditableSwitchColumn extends EditableColumn
{
    /**
     * @var SwitchEnum[]
     */
    protected $switchEnums;

    /**
     * @return string
     */
    public function getSlotComponentKey() : string
    {
        return 'switch-column-slot';
    }

    /**
     * @return string
     */
    public function getLabelComponentKey() : string
    {
        return 'switch-column-label';
    }

    public function setSwitchEnums(array $switchEnums) : EditableSwitchColumn
    {
        $this->switchEnums = $switchEnums;
        return $this;
    }

    protected function getSwitchEnums() : array
    {
        $enums = [];
        if(is_array($this->switchEnums) && count($this->switchEnums))
        {
            foreach ($this->switchEnums as $enum)
            {
                if(!$enum instanceof SwitchEnum) continue;
                $enums[] = $enum->toArray();
            }
        }
        else {
            $enums = [
              [
                  'value' => 0,
                  'label' => CoreLang::get('turn_off')
              ], [
                  'value' => 1,
                  'label' => CoreLang::get('turn_on')
              ]
            ];
        }
        return $enums;
    }

    public function getAdditionalData(): array
    {
        return [
            'options' => $this->getSwitchEnums()
        ];
    }
}
