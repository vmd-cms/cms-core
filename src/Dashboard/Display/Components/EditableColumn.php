<?php

namespace VmdCms\CoreCms\Dashboard\Display\Components;

use VmdCms\CoreCms\Collections\RulesCollection;
use VmdCms\CoreCms\Contracts\Collections\RulesCollectionInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Components\EditableColumnInterface;
use VmdCms\CoreCms\Dashboard\Validation\ValidationRules;

abstract class EditableColumn extends Column implements EditableColumnInterface
{
    use ValidationRules;

    /**
     * @var RulesCollectionInterface
     */
    protected $rules;

    /**
     * @var string
     */
    private $error;

    /**
     * @return string
     */
    public function getComponentKey() : string
    {
        return 'editable-column-component';
    }

    /**
     * @return string
     */
    public abstract function getSlotComponentKey() : string;

    /**
     * @return string
     */
    public abstract function getLabelComponentKey() : string;

    public function toArray(): array
    {
        $primaryField = $this->cmsModel::getPrimaryField();
        return array_merge(parent::toArray(),[
            'label_component' => $this->getLabelComponentKey(),
            'slot_component' => $this->getSlotComponentKey(),
            'rules' => $this->rules ? $this->rules->toArray() : [],
            'error' => $this->error,
            'cms_model_class' => get_class($this->cmsModel),
            'cms_model_id' => $this->cmsModel->$primaryField
        ]);
    }

    /**
     * @return RulesCollectionInterface
     */
    protected function getRulesCollection() : RulesCollectionInterface
    {
        if(!$this->rules instanceof RulesCollectionInterface)
        {
            $this->rules = new RulesCollection();
        }
        return $this->rules;
    }
}
