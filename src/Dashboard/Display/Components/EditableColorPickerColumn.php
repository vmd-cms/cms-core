<?php

namespace VmdCms\CoreCms\Dashboard\Display\Components;

class EditableColorPickerColumn extends EditableColumn
{
    public function getComponentKey(): string
    {
        return 'column-color-picker-component';
    }

    public function toArray(): array
    {
        $primaryField = $this->cmsModel::getPrimaryField();
        return array_merge(parent::toArray(),[
            'cms_model_class' => get_class($this->cmsModel),
            'cms_model_id' => $this->cmsModel->$primaryField
        ]);
    }

    public function getSlotComponentKey(): string
    {
        return '';
    }

    public function getLabelComponentKey(): string
    {
        return '';
    }
}
