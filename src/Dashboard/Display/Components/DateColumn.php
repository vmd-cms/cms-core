<?php

namespace VmdCms\CoreCms\Dashboard\Display\Components;

use VmdCms\CoreCms\Contracts\Dashboard\Display\Components\DateColumnInterface;
use Illuminate\Support\Carbon;

class DateColumn extends Column implements DateColumnInterface
{
    private $format;

    public function setFormat(string $format): DateColumnInterface
    {
        $this->format = $format;
        return $this;
    }

    protected function getFormattedValue($value)
    {
        return Carbon::parse($value)->format($this->format ?? 'Y-m-d H:i:s');
    }


    /**
     * @return string
     */
    public function getComponentKey() : string
    {
        return 'column-date-component';
    }
}
