<?php

namespace VmdCms\CoreCms\Dashboard\Display\Components;

class EditableTextColumn extends EditableColumn
{
    /**
     * @return string
     */
    public function getSlotComponentKey() : string
    {
        return 'text-column-slot';
    }

    /**
     * @return string
     */
    public function getLabelComponentKey() : string
    {
        return 'text-column-label';
    }
}
