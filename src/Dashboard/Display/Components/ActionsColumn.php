<?php

namespace VmdCms\CoreCms\Dashboard\Display\Components;

use VmdCms\CoreCms\Contracts\Dashboard\Display\Components\TableColumnInterface;
use VmdCms\CoreCms\Contracts\Sections\AdminSectionInterface;
use VmdCms\CoreCms\Traits\CrudPolicy\CrudRoutes;

class ActionsColumn extends Column
{
    use CrudRoutes;

    /**
     * @var AdminSectionInterface
     */
    protected $section;

    public function toArray(): array
    {
        return [
            'label' => $this->getLabel(),
            'component' => $this->getComponentKey(),
            'route_view' => $this->getViewRoute($this->cmsModel),
            'route_edit' => $this->getEditRoute($this->cmsModel),
            'route_delete' => $this->getDeleteRoute($this->cmsModel),
        ];
    }

     /**
     * @return string
     */
    public function getComponentKey() : string
    {
        return 'column-actions-component';
    }

    /**
     * @return AdminSectionInterface
     */
    public function getSection(): AdminSectionInterface
    {
        return $this->section;
    }

    /**
     * @param AdminSectionInterface $section
     * @return $this|TableColumnInterface
     */
    public function setSection(AdminSectionInterface $section) : TableColumnInterface
    {
        $this->section = $section;
        return $this;
    }


}
