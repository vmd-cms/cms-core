<?php

namespace VmdCms\CoreCms\Dashboard\Display\Components;

class ChipTextColumn extends Column
{
    protected $colorCallback;

    public function getAdditionalData(): array
    {
        return [
            'color' => $this->getColor()
        ];
    }

    private function getColor()
    {
        if(is_callable($this->colorCallback))
        {
            return call_user_func($this->colorCallback,$this->cmsModel);
        }
        return '129,142,155,1';
    }

    /**
     * @return string
     */
    public function getComponentKey() : string
    {
        return 'column-chip-component';
    }

    /**
     * @param callable $callback
     * @return $this
     */
    public function setColorCallback(callable $callback){
        $this->colorCallback = $callback;
        return $this;
    }
}
