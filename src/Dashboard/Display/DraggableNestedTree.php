<?php

namespace VmdCms\CoreCms\Dashboard\Display;

use VmdCms\CoreCms\Contracts\Dashboard\Display\TreeInterface;
use VmdCms\CoreCms\Contracts\Models\OrderableInterface;
use VmdCms\CoreCms\Contracts\Models\TreeableInterface;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Services\CoreRouter;
use VmdCms\CoreCms\Traits\Dashboard\Display\NestableOrderableTree;
use VmdCms\CoreCms\Traits\Dashboard\HasCmsModel;
use VmdCms\CoreCms\Traits\Dashboard\HasSection;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Whoops\Util\TemplateHelper;

class DraggableNestedTree extends Display implements TreeInterface
{
    use NestableOrderableTree;

    /**
     * @var string
     */
    protected $displayField;

    /**
     * @var int
     */
    protected $maxLevel;

    public function __construct()
    {
        $this->displayField = 'title';
        $this->maxLevel = 2;
    }

    /**
     * @inheritDoc
     */
    public function setDisplayField(string $field): TreeInterface
    {
        $this->displayField = $field;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setMaxLevel(int $maxLevel): TreeInterface
    {
        $this->maxLevel = $maxLevel;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function toArray()
    {
        return array_merge($this->getTreeData(),[
            "max_level" => $this->maxLevel,
        ]);
    }

    protected function getSectionPanelViewPath(): string
    {
        return 'vmd_cms::admin.views.sections.display.draggable_nested_tree';
    }
}
