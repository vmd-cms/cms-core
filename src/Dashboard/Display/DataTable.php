<?php

namespace VmdCms\CoreCms\Dashboard\Display;

use VmdCms\CoreCms\Contracts\Dashboard\Display\DataTableInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\EditableDisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\OrderableDisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Panels\HeadPanelInterface;
use VmdCms\CoreCms\Dashboard\Display\Panels\HeadPanel;
use VmdCms\CoreCms\Traits\Dashboard\Display\EditableDisplay;

class DataTable extends Table implements DataTableInterface, OrderableDisplayInterface, EditableDisplayInterface
{
    use EditableDisplay;

    protected function getSectionPanelViewPath(): string
    {
        return 'vmd_cms::admin.views.sections.display.datatable';
    }
}
