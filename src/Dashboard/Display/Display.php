<?php

namespace VmdCms\CoreCms\Dashboard\Display;

use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Traits\Dashboard\Display\DisplayPanels;
use VmdCms\CoreCms\Traits\Dashboard\HasCmsModel;
use VmdCms\CoreCms\Traits\Dashboard\HasSection;

abstract class Display implements DisplayInterface
{
    use HasSection,HasCmsModel,DisplayPanels;

    protected $dashboardViewPath = 'vmd_cms::admin.views.dashboard';

    public function render()
    {
        $data['head_panel'] = $this->getHeadPanel()->render();
        $data['filter_panel'] = $this->getFilterPanel()->render();
        $data['section_panel'] = $this->getSectionPanel()->render();
        $data['footer_panel'] = $this->getFooterPanel()->render();
        return view($this->dashboardViewPath,$data)->render();
    }

}
