<?php

namespace VmdCms\CoreCms\Facades;

use VmdCms\CoreCms\Initializers\AliasesRegister;
use Illuminate\Support\Facades\Facade;
use VmdCms\CoreCms\Dashboard\Forms\Components;

/**
 * @method static Components\InputComponent input($name, $label = null)
 * @method static Components\PhoneComponent phone($name, $label = null)
 * @method static Components\UrlComponent url($name, $label = null)
 * @method static Components\PasswordComponent password($name, $label = null)
 * @method static Components\TextAreaComponent text($name, $label = null)
 * @method static Components\CkeditorComponent ckeditor($name, $label = null)
 * @method static Components\SelectComponent select($name, $label = null)
 * @method static Components\AutocompleteComponent autocomplete($name, $label = null)
 * @method static Components\AsyncSelectComponent asyncSelect($name, $label = null)
 * @method static Components\DependedSelectComponent dependedSelect($name, $label = null)
 * @method static Components\TreeSelectComponent treeSelect($name, $label = null)
 * @method static Components\SwitchComponent switch($name, $label = null)
 * @method static Components\RadioComponent radio($name, $label = null)
 * @method static Components\CheckBoxComponent checkbox($name, $label = null)
 * @method static Components\TreeComponent tree($name, $label = null)
 * @method static Components\ImageComponent image($name, $label = null)
 * @method static Components\FileInputComponent file($name, $label = null)
 * @method static Components\DataTableComponent datatable($name, $label = null)
 * @method static Components\YoutubeCodeComponent youtubeCode($name, $label = null)
 * @method static Components\ColorPickerComponent colorPicker($name, $label = null)
 * @method static Components\RowComponent row($name=null, $label = null)
 * @method static Components\SeoGroupComponent seo($name=null, $label = null)
 * @method static Components\CustomComponent custom($name=null, $label = null)
 * @method static Components\ViewComponent view($name=null, $label = null)
 * @method static Components\HtmlComponent html($name=null, $label = null)
 * @method static Components\AlertComponent alert($name=null, $label = null)
 * @method static Components\DatePicker date($name=null, $label = null)
 * @method static Components\DateTimePicker dateTime($name=null, $label = null)
 * @method static Components\ButtonsComponent buttons($name=null, $label = null)
 * @method static Components\ActionHistoryComponent actionHistory($name=null, $label = null)
 */
class FormComponent extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return AliasesRegister::FORM_COMPONENTS;
    }
}
