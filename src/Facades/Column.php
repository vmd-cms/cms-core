<?php

namespace VmdCms\CoreCms\Facades;

use VmdCms\CoreCms\Initializers\AliasesRegister;
use Illuminate\Support\Facades\Facade;
use VmdCms\CoreCms\Dashboard\Display\Components;

/**
 * @method static Components\TextColumn text($name, $label = null)
 * @method static Components\CustomColumn custom($name, $label = null)
 * @method static Components\PhotoColumn photo($name, $label = null)
 * @method static Components\ChipTextColumn chipText($name, $label = null)
 * @method static Components\DateColumn date($name, $label = null)
 */
class Column extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return AliasesRegister::COLUMN_COMPONENTS;
    }
}
