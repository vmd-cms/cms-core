<?php

namespace VmdCms\CoreCms\Facades;

use VmdCms\CoreCms\Initializers\AliasesRegister;
use Illuminate\Support\Facades\Facade;
use VmdCms\CoreCms\Dashboard\Forms\Tables\Components;

/**
 * @method static Components\ColumnInput input($name, $label = null)
 * @method static Components\ColumnNumeric numeric($name, $label = null)
 * @method static Components\ColumnText text($name, $label = null)
 * @method static Components\ColumnTextArea textArea($name, $label = null)
 * @method static Components\ColumnImage image($name, $label = null)
 * @method static Components\ColumnSelect select($name, $label = null)
 * @method static Components\ColumnAutocomplete autocomplete($name, $label = null)
 * @method static Components\ColumnSwitch switch($name, $label = null)
 * @method static Components\ColumnDateTime dateTime($name, $label = null)
 * @method static Components\CustomColumn custom($name, $label = null)
 * @method static Components\ColumnFile file($name, $label = null)
 * @method static Components\ColumnColorPicker colorPicker($name, $label = null)
 */
class FormTableColumn extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return AliasesRegister::FORM_TABLE_COLUMN_COMPONENTS;
    }
}
