<?php

namespace VmdCms\CoreCms\Facades;

use VmdCms\CoreCms\Initializers\AliasesRegister;
use Illuminate\Support\Facades\Facade;
use VmdCms\CoreCms\Dashboard;

/**
 * @method static Dashboard\Display\DataTable dataTable(array $columns)
 * @method static Dashboard\Display\DraggableNestedTree tree()
 */
class Display extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return AliasesRegister::DISPLAY;
    }
}
