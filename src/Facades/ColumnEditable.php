<?php

namespace VmdCms\CoreCms\Facades;

use VmdCms\CoreCms\Initializers\AliasesRegister;
use Illuminate\Support\Facades\Facade;
use VmdCms\CoreCms\Dashboard\Display\Components;

/**
 * @method static Components\EditableTextColumn text($name, $label = null)
 * @method static Components\EditableSwitchColumn switch($name, $label = null)
 * @method static Components\EditableColorPickerColumn colorPicker($name, $label = null)
 * @method static Components\EditableDateTimeColumn dateTime($name, $label = null)
 */
class ColumnEditable extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return AliasesRegister::COLUMN_EDITABLE_COMPONENTS;
    }
}
