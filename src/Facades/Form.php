<?php

namespace VmdCms\CoreCms\Facades;

use VmdCms\CoreCms\Contracts\Collections\FormTabsCollectionInterface;
use VmdCms\CoreCms\Initializers\AliasesRegister;
use Illuminate\Support\Facades\Facade;
use VmdCms\CoreCms\Dashboard\Forms;

/**
 * @method static Forms\FormPanel panel(array $component = [])
 * @method static Forms\FormTabbed tabbed(FormTabsCollectionInterface $tabsCollection = null)
 */
class Form extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return AliasesRegister::FORM;
    }
}
