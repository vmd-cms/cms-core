<?php

namespace VmdCms\CoreCms\Exceptions\Initializers;

use VmdCms\CoreCms\Exceptions\CoreException;

class AdminInitializerNotFoundException extends CoreException
{

}
