<?php

namespace VmdCms\CoreCms\Exceptions\Initializers;

use VmdCms\CoreCms\Exceptions\CoreException;

class ModuleInitializerNotFoundException extends CoreException
{

}
