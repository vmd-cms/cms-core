<?php

namespace VmdCms\CoreCms\Exceptions;

use Throwable;

class CoreException extends \Exception
{
    /**
     * CoreException constructor.
     * @param string|\Exception|null $messageOrException
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($messageOrException = 'Core Error', $code = 409, Throwable $previous = null)
    {
        if($messageOrException instanceof \Exception)
        {
            $message = "Exception in File:".$messageOrException->getFile() . " Line:" . $messageOrException->getLine() . " Message: " . $messageOrException->getMessage();
        }
        else $message = $messageOrException;

        parent::__construct($message, $code, $previous);
    }
}
