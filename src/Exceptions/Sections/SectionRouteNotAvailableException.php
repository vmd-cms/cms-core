<?php

namespace VmdCms\CoreCms\Exceptions\Sections;

use VmdCms\CoreCms\Exceptions\CoreException;

class SectionRouteNotAvailableException extends CoreException
{

}
