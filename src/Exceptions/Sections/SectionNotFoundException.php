<?php

namespace VmdCms\CoreCms\Exceptions\Sections;

use VmdCms\CoreCms\Exceptions\CoreException;

class SectionNotFoundException extends CoreException
{

}
