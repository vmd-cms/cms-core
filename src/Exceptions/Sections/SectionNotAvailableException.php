<?php

namespace VmdCms\CoreCms\Exceptions\Sections;

use VmdCms\CoreCms\Exceptions\CoreException;

class SectionNotAvailableException extends CoreException
{

}
