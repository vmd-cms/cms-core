<?php

namespace VmdCms\CoreCms\Exceptions\Dashboard;

use VmdCms\CoreCms\Exceptions\CoreException;

class EditableDisplayException extends CoreException
{

}
