<?php

namespace VmdCms\CoreCms\Exceptions\Models;

use Throwable;
use VmdCms\CoreCms\Exceptions\CoreException;

class ModelNotFoundException extends CoreException
{
    public function __construct($messageOrException = 'Model Not Found', $code = 404, Throwable $previous = null)
    {
        if($messageOrException instanceof \Exception)
        {
            $message = "Exception in File:".$messageOrException->getFile() . " Line:" . $messageOrException->getLine() . " Message: " . $messageOrException->getMessage();
        }
        else $message = $messageOrException;

        parent::__construct($message, $code, $previous);
    }
}
