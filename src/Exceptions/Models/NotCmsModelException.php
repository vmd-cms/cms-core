<?php

namespace VmdCms\CoreCms\Exceptions\Models;

use VmdCms\CoreCms\Exceptions\CoreException;

class NotCmsModelException extends CoreException
{

}
