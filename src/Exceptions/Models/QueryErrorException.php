<?php

namespace VmdCms\CoreCms\Exceptions\Models;

use VmdCms\CoreCms\Exceptions\CoreException;

class QueryErrorException extends CoreException
{

}
