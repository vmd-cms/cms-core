<?php

namespace VmdCms\CoreCms\Exceptions\Moderators;

use VmdCms\CoreCms\Exceptions\CoreException;

class AuthModeratorNotFound extends CoreException
{

}
