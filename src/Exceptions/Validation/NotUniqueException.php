<?php

namespace VmdCms\CoreCms\Exceptions\Validation;

use VmdCms\CoreCms\Exceptions\CoreException;

class NotUniqueException extends CoreException
{

}
