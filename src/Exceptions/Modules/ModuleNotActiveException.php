<?php

namespace VmdCms\CoreCms\Exceptions\Modules;

use VmdCms\CoreCms\Exceptions\CoreException;

class ModuleNotActiveException extends CoreException
{

}
