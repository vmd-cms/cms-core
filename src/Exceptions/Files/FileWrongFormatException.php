<?php

namespace VmdCms\CoreCms\Exceptions\Files;

use VmdCms\CoreCms\Exceptions\CoreException;

class FileWrongFormatException extends CoreException
{

}
