<?php

namespace VmdCms\CoreCms\Console\Commands;

use Illuminate\Console\Command;
use VmdCms\CoreCms\Contracts\Services\Files\FileGeneratorInterface;
use VmdCms\CoreCms\Exceptions\Services\StubReplacerException;
use VmdCms\CoreCms\Services\Files\FileGenerator;
use VmdCms\CoreCms\Services\Files\StubBuilder;
use VmdCms\CoreCms\Services\Files\StubReplacer;

class CreateModuleCommand extends Command
{
    /**
     * @var FileGeneratorInterface
     */
    protected $fileGenerator;

    /**
     * @var StubReplacer
     */
    protected $stubsReplacer;

    /**
     * @var StubBuilder
     */
    protected $stubsBuilder;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vmd-cms:create-module {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->fileGenerator = new FileGenerator();
        $this->stubsReplacer = new StubReplacer();
        $this->stubsBuilder = new StubBuilder();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->setData();
            $this->stubsBuilder->createModule();
            $this->info("Created");
        }
        catch (\Exception $exception)
        {
            $this->info("Error:" . $exception->getMessage());
        }
    }

    /**
     * @throws StubReplacerException
     */
    private function setData()
    {
        $this->stubsReplacer->setNameModule($this->argument('name'));
        $this->stubsReplacer->setName($this->argument('name'));
        $this->fileGenerator->setBaseReplace($this->stubsReplacer->getReplaceAssoc());
        $packagePath = base_path("packages/vmd-cms/modules/" . $this->stubsReplacer->getName(StubReplacer::NAME_PLURAL_KEBAB));
        $srcPath = $packagePath . "/src";
        $this->stubsBuilder->setStubReplacer($this->stubsReplacer);
        $this->stubsBuilder->setFileGenerator($this->fileGenerator);
        $this->stubsBuilder->setPublicModuleFolder($srcPath);
        $this->stubsBuilder->setPackageModuleFolder($packagePath);
    }

}
