<?php

namespace VmdCms\CoreCms\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use VmdCms\CoreCms\CoreCmsServiceProvider;
use VmdCms\CoreCms\CoreModules\Events\Entity\CoreEventsSetup;
use VmdCms\CoreCms\Exceptions\Initializers\ModuleInitializerNotFoundException;
use VmdCms\CoreCms\Exceptions\Services\StubReplacerException;
use VmdCms\CoreCms\Services\Config;
use VmdCms\CoreCms\Services\Initializers\InitializerService;

class InstallCoreCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vmd-cms:install-core';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install vmd-cms core';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $baseLang = Config::getInstance()->getBaseLang();
            $locale = $baseLang['key'] ?? 'ru';
            app()->setLocale($locale);

            Artisan::call('vendor:publish',[
                "--provider" => CoreCmsServiceProvider::class
            ]);

            $this->info("Publish completed");

            Artisan::call('migrate',[
                "--path" => "vendor/vmd-cms/core-cms/src/database/migrations"
            ]);
            $this->info("Migration completed");

            Artisan::call('db:seed',[
                "--class" => "VmdCms\CoreCms\database\seeds\CoreSeeder"
            ]);
            $this->info("core seed completed");

            Artisan::call('db:seed',[
                "--class" => "VmdCms\CoreCms\database\seeds\PublicSeeder"
            ]);
            $this->info("public seed completed");

            $this->installModules();

            CoreEventsSetup::getInstance()->seedCoreEvents();
        }
        catch (\Exception $exception)
        {
            $this->info("Error:" . $exception->getMessage());
            return;
        }
        $this->info("Install completed");
    }

    /**
     * @throws ModuleInitializerNotFoundException
     * @throws StubReplacerException
     */
    private function installModules()
    {
        InitializerService::getInitializerByModuleNamespace('VmdCms\CoreCms\CoreModules\Translates')->install();
        InitializerService::getInitializerByModuleNamespace('VmdCms\CoreCms\CoreModules\Settings')->install();
        InitializerService::getInitializerByModuleNamespace('VmdCms\CoreCms\CoreModules\Content')->install();
    }
}
