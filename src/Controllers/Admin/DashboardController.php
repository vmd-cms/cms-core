<?php

namespace VmdCms\CoreCms\Controllers\Admin;

class DashboardController extends AdminCoreController
{
    public function getDashboard(){
        return view('vmd_cms::admin.views.dashboard')->render();
    }
}


