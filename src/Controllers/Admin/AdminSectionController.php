<?php

namespace VmdCms\CoreCms\Controllers\Admin;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use VmdCms\CoreCms\Contracts\Dashboard\Display\EditableDisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\OrderableDisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\TreeInterface;
use VmdCms\CoreCms\Contracts\Sections\AdminSectionInterface;
use VmdCms\CoreCms\Exceptions\Dashboard\TableDisplayException;
use VmdCms\CoreCms\Exceptions\Dashboard\TreeDisplayException;
use VmdCms\CoreCms\Initializers\AdminInitializer;
use VmdCms\CoreCms\Services\CoreResponse;
use VmdCms\CoreCms\Services\Responses\ApiResponse;

class AdminSectionController extends AdminCoreController
{
    /**
     * @var AdminSectionInterface
     */
    protected $currentAdminSection;

    public function __construct(Application $application)
    {
        parent::__construct($application);
    }

    /**
     * @return AdminSectionInterface
     */
    protected function getCurrentAdminSection()
    {
        $adminInitializer = $this->app->adminInitializer ?? null;
        if(!$adminInitializer instanceof AdminInitializer) abort(404);

        return $adminInitializer->getCurrentAdminSection();
    }


    public function display()
    {
        if(request()->method() === 'GET'){
            return $this->getCurrentAdminSection()->renderDisplay();
        }
        return $this->getCurrentAdminSection()->storeDisplay(\request());
    }

    public function create(Request $request){
        if(request()->method() === 'GET'){
            return $this->getCurrentAdminSection()->renderCreate();
        }
        return $this->getCurrentAdminSection()->storeCreate($request);
    }

    public function view($id){

        return view('vmd_cms::admin.views.dashboard',['section' => $this->getCurrentAdminSection()->getTitle(). ' View'])->render();
    }

    public function edit(Request $request)
    {
        if(request()->method() === 'GET'){
            return $this->getCurrentAdminSection()->renderEdit($request->id);
        }
        return $this->getCurrentAdminSection()->storeEdit($request);
    }

    public function delete(Request $request)
    {
        if(request()->method() === 'POST'){
            return $this->getCurrentAdminSection()->delete($request->id);
        }
    }

    public function restore(Request $request)
    {
        if(request()->method() === 'POST'){
            return $this->getCurrentAdminSection()->restore($request->id);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function updateTreeOrder(Request $request)
    {
        try {
            $display = $this->getCurrentAdminSection()->display();
            if(!$display instanceof TreeInterface) throw new TreeDisplayException();
            $display->setSection($this->getCurrentAdminSection())->updateTree($request);
            return CoreResponse::success();
        }
        catch (\Exception $e)
        {
            return CoreResponse::error([$e->getMessage()]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function updateTableOrder(Request $request)
    {
        try {
            $display = $this->getCurrentAdminSection()->display();
            if(!$display instanceof OrderableDisplayInterface || !$display->isOrderable()) {
                throw new TableDisplayException();
            }
            $display->setSection($this->getCurrentAdminSection());
            $display->updateOrder($request->items);
            return CoreResponse::success();
        }
        catch (\Exception $e)
        {
            return CoreResponse::error([$e->getMessage()]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function updateTableItem(Request $request)
    {
        try {
            $display = $this->getCurrentAdminSection()->display();
            if(!$display instanceof EditableDisplayInterface) {
                throw new TableDisplayException();
            }
            $display->setSection($this->getCurrentAdminSection());
            $display->updateItem($request->item, $request->value);
            return CoreResponse::success();
        }
        catch (\Exception $e)
        {
            return CoreResponse::error([$e->getMessage()]);
        }
    }

    public function ckeditorUploadFile(Request $request)
    {
        try {
            $path = $this->getCurrentAdminSection()->storeCkeditorUploadFile($request);
        }catch (\Exception $exception){
            return CoreResponse::error([$exception->getMessage()]);
        }
        $func = $request->get('CKEditorFuncNum',0);
        echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($func, '/storage/$path', 'Uploaded successfully');</script>";
    }

    public function ckeditorBrowseFiles(Request $request)
    {
        try {
            $filePaths = $this->getCurrentAdminSection()->getCkeditorUploadedFiles($request);
            $response = [];
            if(is_countable($filePaths)){
                foreach ($filePaths as $path){
                    $response[] = [
                      'fileNmae' => 'test',
                      'url' => '/storage/' . $path
                    ];
                }
            }

        }catch (\Exception $exception){
            return CoreResponse::error([$exception->getMessage()]);
        }
        return response($response,200);
    }

    public function datatableAsync(Request $request)
    {
        $currentSection = $this->getCurrentAdminSection();
        $cmsModelClass = $currentSection->getCmsModelClass();
        $display = $currentSection->display()->setSection($currentSection)->setCmsModel(new $cmsModelClass());
        $tableData = $display->setWhereQuery($request->get('where_query',[]))->getTableDataAsync($request->options);
        return ApiResponse::success($tableData);
    }

    public function postServiceSectionMethod(Request $request){

        $currentSection = $this->getCurrentAdminSection();
        $sectionMethod = $request->sectionMethod;
        if(method_exists($currentSection,$sectionMethod)){
            return $currentSection->$sectionMethod();
        }
        abort(404);
    }

    public function getServiceSectionMethod(Request $request){

        $currentSection = $this->getCurrentAdminSection();
        $sectionMethod = $request->sectionMethod;
        if(method_exists($currentSection,$sectionMethod)){
            return $currentSection->$sectionMethod();
        }
        abort(404);
    }
}
