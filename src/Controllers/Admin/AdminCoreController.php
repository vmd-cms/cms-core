<?php

namespace VmdCms\CoreCms\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use VmdCms\CoreCms\Middleware\AdminInitializerMiddleware;

abstract class AdminCoreController extends Controller
{
    /**
     * @var Application
     */
    protected $app;

    public function __construct(Application $application)
    {
        $this->app = $application;
        $this->middleware(AdminInitializerMiddleware::class);
    }

}
