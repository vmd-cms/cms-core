<?php

namespace VmdCms\CoreCms\Controllers\Admin;

use Illuminate\Http\Request;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Controllers\CoreController;
use VmdCms\CoreCms\CoreModules\Content\Entity\Languages;
use VmdCms\CoreCms\Exceptions\CoreException;
use VmdCms\CoreCms\Exceptions\Models\ClassNotFoundException;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;
use VmdCms\CoreCms\Exceptions\Validation\NotUniqueException;
use VmdCms\CoreCms\Services\CoreResponse;

class AdminServiceController extends CoreController
{
    public function checkUniqueField(Request $request)
    {
        $request->validate([
            'cms_model_class' => 'required|max:100',
            'cms_model_id' => 'nullable|max:64',
            'value' => 'nullable',
            'field' => 'required|max:64'
        ]);

        try {
            if(!class_exists($request->cms_model_class)) throw new ClassNotFoundException();

            $coreModel = new $request->cms_model_class;

            if(!$coreModel instanceof CmsModelInterface)  throw new NotCmsModelException();

            $subFields = explode('.',$request->field);
            $field = array_pop($subFields);
            $value = $request->value;

            $query = $coreModel::query();
            if(count($subFields))
            {
                $rel = array_shift($subFields);
                $query->whereHas($rel,function ($q)use ($field,$value){
                      $q->where($field,$value);
                });
            }
            else{
                $query->where($field,$value);
            }
            if($request->cms_model_id) $query->where($coreModel->getPrimaryField(),'!=',$request->cms_model_id);

            $model = $query->first();

            if($model instanceof CmsModelInterface)  throw new NotUniqueException('Not unique');
        }
        catch (CoreException $e)
        {
            return CoreResponse::error([$e->getMessage()]);
        }
        return CoreResponse::success();
    }

    public function setLocale(Request $request){
        $request->validate([
           'locale' => 'required|max:3'
        ]);
        Languages::getInstance()->setLocale($request->get('locale'));
        return CoreResponse::success(['error' => false, 'reload' => true]);
    }
}
