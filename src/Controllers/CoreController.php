<?php

namespace VmdCms\CoreCms\Controllers;

use App\Http\Controllers\Controller;
use VmdCms\CoreCms\Services\Logger;

class CoreController extends Controller
{
    protected $logChannel;

    /**
     * @var Logger
     */
    protected $logger;

    public function __construct()
    {
        $this->logger = new Logger($this->logChannel ?? 'vmd_cms');
    }
}
