<?php

namespace VmdCms\CoreCms\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use VmdCms\CoreCms\Controllers\CoreController;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\CoreModules\Events\Entity\Event;
use VmdCms\CoreCms\CoreModules\Events\Enums\CoreEventEnums;
use VmdCms\CoreCms\CoreModules\Moderators\Entity\AuthEntity;
use VmdCms\CoreCms\Services\Auth\AuthenticatesUsers;
use VmdCms\CoreCms\Services\CoreRouter;

class ModeratorAuthController extends CoreController
{
    use AuthenticatesUsers;

    protected $redirectTo = '/dashboard';

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('moderator');
    }

    protected function signInEvent(){
        Event::dispatch(CoreEventEnums::ADMIN_SIGN_IN,['moderator' => AuthEntity::getAuthModerator()]);
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        if($this->guard()->check()) {
            return redirect(route(CoreRouter::ROUTE_DISPLAY,['sectionSlug' => null]));
        }

        $data = new \stdClass();
        $data->appTranslates = json_encode(CoreLang::getTranslates(),JSON_UNESCAPED_UNICODE);
        view()->share('dashboardData', $data);

        return view('vmd_cms::admin.auth.login');
    }

    /**
     * The user has logged out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
        return redirect('/');
    }


}
