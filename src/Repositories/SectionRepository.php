<?php

namespace VmdCms\CoreCms\Repositories;

use VmdCms\CoreCms\Collections\SectionRepositoryCollection;
use VmdCms\CoreCms\Contracts\Collections\SectionRepositoryCollectionInterface;
use VmdCms\CoreCms\Contracts\Repositories\SectionRepositoryInterface;
use VmdCms\CoreCms\CoreModules\Sections\Models\CoreSection;
use VmdCms\CoreCms\DTO\SectionRepositoryDto;
use VmdCms\CoreCms\Exceptions\Repositories\SectionRepositoryException;


class SectionRepository implements SectionRepositoryInterface
{
    /**
     * @return SectionRepositoryCollectionInterface
     * @throws SectionRepositoryException
     */
    public function getCollection(): SectionRepositoryCollectionInterface
    {
        if(!$coreSections = CoreSection::active()->with('coreModule')->order()->get())
        {
            throw new SectionRepositoryException();
        }

        $collection = new SectionRepositoryCollection();

        foreach ($coreSections as $item)
        {
            $collection->appendItem(
                (new SectionRepositoryDto($item->id, $item->core_section_class, $item->title))
                ->setCoreModule($item->coreModule ?? null)
            );
        }

        return $collection;
    }
}
