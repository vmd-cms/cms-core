<?php

namespace VmdCms\CoreCms\Repositories;

use VmdCms\CoreCms\CoreModules\Modules\Sections\CoreModule;
use VmdCms\CoreCms\CoreModules\Navigations\Sections\CoreNavDashboard;
use VmdCms\CoreCms\CoreModules\Sections\Sections\CoreSection;

class CoreSectionRepository
{
    /**
     * @return array
     */
    public function getAdminCoreSectionsModels() : array
    {
        return [
            CoreModule::getInstance(),
            CoreNavDashboard::getInstance(),
            CoreSection::getInstance()
        ];
    }
}
