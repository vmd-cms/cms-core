<?php

namespace VmdCms\CoreCms\Initializers;

use VmdCms\CoreCms\Contracts\Factories\ColumnEditableFactoryInterface;
use VmdCms\CoreCms\Contracts\Factories\ColumnFactoryInterface;
use VmdCms\CoreCms\Contracts\Factories\DisplayFactoryInterface;
use VmdCms\CoreCms\Contracts\Factories\FormComponentsFactoryInterface;
use VmdCms\CoreCms\Contracts\Factories\FormFactoryInterface;
use VmdCms\CoreCms\Contracts\Factories\FormTableColumnComponentsFactoryInterface;
use VmdCms\CoreCms\Factories\ColumnEditableFactory;
use VmdCms\CoreCms\Factories\ColumnFactory;
use VmdCms\CoreCms\Factories\DisplayFactory;
use VmdCms\CoreCms\Factories\FormComponentsFactory;
use VmdCms\CoreCms\Factories\FormFactory;
use Illuminate\Contracts\Foundation\Application;
use VmdCms\CoreCms\Factories\FormTableColumnComponentsFactory;

class AliasesRegister
{
    const VMD_CMS = 'vmd_cms';
    const FORM = self::VMD_CMS . '.form';
    const FORM_COMPONENTS = self::FORM . '.components';
    const FORM_TABLE_COLUMN_COMPONENTS = self::FORM . '.tables.components';

    const DISPLAY = self::VMD_CMS . '.display';
    const COLUMN_COMPONENTS = self::DISPLAY . '.table.components';
    const COLUMN_EDITABLE_COMPONENTS = self::DISPLAY . '.table.components.editable';

    /**
     * @var Application
     */
    private $app;

    /**
     * AliasesRegister constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }


    public function register()
    {
        $this->registerFormComponents();
        $this->registerFormTableColumnsComponents();
        $this->registerForm();
        $this->registerDisplay();
        $this->registerColumn();
        $this->registerColumnEditable();
    }

    protected function registerFormComponents()
    {
        $this->app->instance(
            self::FORM_COMPONENTS,
            $this->app->make(FormComponentsFactory::class)
        );
        $this->app->alias(
            self::FORM_COMPONENTS,
            FormComponentsFactoryInterface::class
        );
    }

    protected function registerFormTableColumnsComponents()
    {
        $this->app->instance(
            self::FORM_TABLE_COLUMN_COMPONENTS,
            $this->app->make(FormTableColumnComponentsFactory::class)
        );
        $this->app->alias(
            self::FORM_TABLE_COLUMN_COMPONENTS,
            FormTableColumnComponentsFactoryInterface::class
        );
    }

    protected function registerForm()
    {
        $this->app->instance(
            self::FORM,
            $this->app->make(FormFactory::class)
        );
        $this->app->alias(
            self::FORM,
            FormFactoryInterface::class
        );
    }

    protected function registerDisplay()
    {
        $this->app->instance(
            self::DISPLAY,
            $this->app->make(DisplayFactory::class)
        );
        $this->app->alias(
            self::DISPLAY,
            DisplayFactoryInterface::class
        );
    }

    protected function registerColumn()
    {
        $this->app->instance(
            self::COLUMN_COMPONENTS,
            $this->app->make(ColumnFactory::class)
        );
        $this->app->alias(
            self::COLUMN_COMPONENTS,
            ColumnFactoryInterface::class
        );
    }

    protected function registerColumnEditable()
    {
        $this->app->instance(
            self::COLUMN_EDITABLE_COMPONENTS,
            $this->app->make(ColumnEditableFactory::class)
        );
        $this->app->alias(
            self::COLUMN_EDITABLE_COMPONENTS,
            ColumnEditableFactoryInterface::class
        );
    }
}
