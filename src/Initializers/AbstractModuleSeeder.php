<?php

namespace VmdCms\CoreCms\Initializers;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use VmdCms\CoreCms\Contracts\Modules\ModuleInitializerInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Entity\CoreTranslatesEntity;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\CoreModules\Modules\Models\CoreModule;
use VmdCms\CoreCms\CoreModules\Navigations\Models\CoreNavDashboard;
use VmdCms\CoreCms\CoreModules\Sections\Models\CoreSection;
use VmdCms\CoreCms\Exceptions\Initializers\ModuleInitializerNotFoundException;
use VmdCms\CoreCms\Exceptions\Modules\ModuleSeedException;
use VmdCms\CoreCms\Services\Config;
use VmdCms\CoreCms\Traits\Database\NavSeeder;
use VmdCms\CoreCms\Traits\Database\SectionSeeder;
use VmdCms\CoreCms\Traits\Reflection\Reflectable;

abstract class AbstractModuleSeeder extends Seeder
{
    use Reflectable,SectionSeeder,NavSeeder;

    /**
     * @var ModuleInitializerInterface
     */
    protected $moduleInitializer;

    /**
     * @var CoreModule
     */
    protected $module;

    /**
     * @return string
     */
    protected function getNavClass() : string
    {
        return CoreNavDashboard::class;
    }

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        try {
            DB::transaction(function (){

                $this->seedModule();

                $this->seedCoreTranslates();

                CoreLang::setTranslates(true);

                $this->seedSections();

                $this->seedNavigation();

                $this->seedModelData();

            });
        }
        catch (\Exception $e)
        {
            echo $e->getMessage();
        }

    }

    protected function seedCoreTranslates()
    {
        try {
            $baseLang = Config::getInstance()->getBaseLang();
            $key = $baseLang['key'] ?? 'ru';
            (new CoreTranslatesEntity())->seedCoreTranslates($key,$this->moduleInitializer::moduleSlug());
        }
        catch (\Exception $e) {}
    }

    protected function seedModelData()
    {
        return;
    }

    protected function getModuleInitializer() : ModuleInitializerInterface
    {
        if(!$this->moduleInitializer)
        {
            $className = str_replace('database\seeds','Initializers\ModuleInitializer',$this->getCurrentNamespace());
            if(!class_exists($className)) throw new ModuleInitializerNotFoundException();

            $moduleInitializer = new $className;
            if(!$moduleInitializer instanceof ModuleInitializerInterface) throw new ModuleInitializerNotFoundException();

            $this->moduleInitializer = $moduleInitializer;
        }
        return $this->moduleInitializer;
    }

    /**
     * @return void
     * @throws ModuleSeedException
     */
    protected function seedModule()
    {
        try {

            $this->module = CoreModule::where("slug",$this->getModuleInitializer()::moduleSlug())->first();
            if(!$this->module instanceof CoreModule){
                $this->module = CoreModule::create([
                    "slug" => $this->getModuleInitializer()::moduleSlug(),
                    "title" => $this->getModuleInitializer()::moduleAlias(),
                    "module_initializer_class" => get_class($this->getModuleInitializer()),
                    "active" => true
                ]);
            }

            if(!$this->module instanceof CoreModule) throw new \Exception();
        }
        catch (\Exception $e)
        {
            throw new ModuleSeedException($e);
        }
    }

}
