<?php

namespace VmdCms\CoreCms\Initializers;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Artisan;
use VmdCms\CoreCms\Contracts\Modules\ModuleInitializerInterface;
use VmdCms\CoreCms\Contracts\Services\Files\FileGeneratorInterface;
use VmdCms\CoreCms\CoreModules\Events\Entity\CoreEventsSetup;
use VmdCms\CoreCms\Exceptions\Services\StubBuilderException;
use VmdCms\CoreCms\Exceptions\Services\StubReplacerException;
use VmdCms\CoreCms\Services\Files\FileGenerator;
use VmdCms\CoreCms\Services\Files\StubBuilder;
use VmdCms\CoreCms\Services\Files\StubReplacer;
use VmdCms\CoreCms\Services\Routes;
use VmdCms\CoreCms\Traits\Reflection\Reflectable;

abstract class AbstractModuleInitializer implements ModuleInitializerInterface
{
    use Reflectable;

    /**
     * @var FileGeneratorInterface
     */
    protected $fileGenerator;

    /**
     * @var StubReplacer
     */
    protected $stubReplacer;

    /**
     * @var StubBuilder
     */
    protected $stubBuilder;

    public function __construct()
    {
        $this->fileGenerator = new FileGenerator();
        $this->stubReplacer = new StubReplacer(static::moduleSlug());
        $this->stubReplacer->setName(static::moduleSlug());
        $this->stubReplacer->setVendorNamespace($this->getModuleNameSpace());
        $this->stubBuilder = new StubBuilder();
    }

    /**
     * @return void
     */
    public function appendRoutes()
    {
        $path = app_path($this->publicRoutesPath().'/lang_routes.php');
        if(file_exists($path))
        {
            try {
                $langRoutes = @include_once $path;

                if(is_callable($langRoutes)){
                    Routes::getInstance()->appendLangRouteGroup(static::moduleSlug(), $langRoutes);
                }
            }
            catch (\Exception $exception){}
        }

        $path = app_path($this->publicRoutesPath().'/api_routes.php');
        if(file_exists($path))
        {
            try {
                $apiRoutes = @include_once $path;
                if(is_callable($apiRoutes)){
                    Routes::getInstance()->appendApiRouteGroup(static::moduleSlug(), $apiRoutes);
                }

            }
            catch (\Exception $exception){}
        }

        $path = app_path($this->publicRoutesPath().'/custom_routes.php');
        if(file_exists($path))
        {
            try {
                $customRoutes = @include_once $path;
                if(is_callable($customRoutes)){
                    Routes::getInstance()->appendCustomRouteGroup(static::moduleSlug(), $customRoutes);
                }
            }
            catch (\Exception $exception){}
        }
    }

    /**
     * @return string
     * @throws StubReplacerException
     */
    protected function moduleFolderPath() : string
    {
        return "app/" . $this->folderPath();
    }

    protected function folderPath() : string
    {
        return "Modules/".$this->stubReplacer->getName(StubReplacer::NAME_MODULE);
    }

    /**
     * @return string
     * @throws StubReplacerException
     */
    public function publicMigrationsPath() : string
    {
        return $this->moduleFolderPath()."/database/migrations";
    }

    /**
     * @return string
     * @throws StubReplacerException
     */
    public function publicSeedsPath() : string
    {
        return $this->moduleFolderPath()."/database/seeds";
    }

    /**
     * @return string
     */
    public function publicRoutesPath() : string
    {
        return $this->folderPath() . "/routes";
    }

    /**
     * @return string
     * @throws StubReplacerException
     */
    public function publicConfigPath(): string
    {
        return $this->moduleFolderPath()."/config";
    }

    /**
     * @return string
     */
    public function publicViewsPath(): string
    {
        return $this->folderPath() . "/resources/views";
    }

    /**
     * @return string
     */
    public function publicLangPath(): string
    {
        return $this->folderPath() . "/resources/lang";
    }

    /**
     * @throws FileNotFoundException
     * @throws StubBuilderException
     * @throws StubReplacerException
     */
    public function install()
    {
        $this->publish();
        $this->publishModuleClasses();
        $this->migrate();
        $this->seed();
        $this->seedCoreEvents();

    }

    protected function publish()
    {
        Artisan::call('vendor:publish',[
            "--provider" => $this->getServiceProviderClass()
        ]);
    }

    /**
     * @throws StubBuilderException
     * @throws StubReplacerException
     * @throws FileNotFoundException
     */
    protected function publishModuleClasses()
    {
        $this->stubBuilder->setFileGenerator($this->fileGenerator);
        $this->stubBuilder->setStubReplacer($this->stubReplacer);
        $this->stubBuilder->setSourceModuleFolder($this->getCurrentDir()."/..");
        $this->stubBuilder->setPublicModuleFolder(base_path($this->moduleFolderPath()));
        $this->stubBuilder->publishModule();
    }

    /**
     * @throws StubReplacerException
     */
    protected function migrate()
    {
        Artisan::call('migrate',[
            "--path" => $this->publicMigrationsPath()
        ]);
    }

    protected function seed()
    {
        Artisan::call('db:seed',[
            "--class" => $this->getModuleNameSpace() . "\database\seeds\ModuleSeeder"
        ]);
    }

    public function seedCoreEvents(){}

    protected function getServiceProviderClass()
    {
        return $this->getModuleNameSpace() . '\ModuleServiceProvider';
    }

    protected function getModuleNameSpace()
    {
        return str_replace('\Initializers','',$this->getCurrentNamespace());
    }
}
