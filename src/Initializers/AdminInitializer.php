<?php

namespace VmdCms\CoreCms\Initializers;

use Illuminate\Contracts\Foundation\Application;
use VmdCms\CoreCms\Collections\AdminSectionCollection;
use VmdCms\CoreCms\Contracts\Collections\AdminSectionCollectionInterface;
use VmdCms\CoreCms\Contracts\DTO\ModeratorDtoInterface;
use VmdCms\CoreCms\Contracts\DTO\SectionRepositoryDtoInterface;
use VmdCms\CoreCms\Contracts\Initializers\AdminInitializerInterface;
use VmdCms\CoreCms\Contracts\Repositories\SectionRepositoryInterface;
use VmdCms\CoreCms\Contracts\Sections\AdminSectionInterface;
use VmdCms\CoreCms\CoreModules\Modules\Models\CoreModule;
use VmdCms\CoreCms\DTO\Dashboard\DashboardData;
use VmdCms\CoreCms\DTO\Dashboard\ModeratorDto;
use VmdCms\CoreCms\Exceptions\CoreException;
use VmdCms\CoreCms\Exceptions\Modules\ModuleNotActiveException;
use VmdCms\CoreCms\Exceptions\Repositories\SectionRepositoryException;
use VmdCms\CoreCms\Exceptions\Sections\SectionCmsModelException;
use VmdCms\CoreCms\Exceptions\Sections\SectionNotFoundException;
use VmdCms\CoreCms\Repositories\CoreSectionRepository;

class AdminInitializer implements AdminInitializerInterface
{
    /**
     * @var AdminSectionCollectionInterface
     */
    private $adminSections;

    /**
     * @var array
     */
    private $sectionsIdSlugMap;

    /**
     * @var DashboardData
     */
    private $dashboardData;

    /**
     * @var AdminSectionInterface
     */
    private $currentAdminSection;

    /**
     * @var ModeratorDtoInterface
     */
    private $moderatorDTO;

    /**
     * @inheritDoc
     */
    public function __construct(Application $application, SectionRepositoryInterface $sectionRepository)
    {
        $this->moderatorDTO = new ModeratorDto(auth()->guard('moderator')->user());
        $this->adminSections = new AdminSectionCollection();

        try {
           // $this->registerCoreSections();
            $this->registerSections($sectionRepository);
            $application->instance('adminInitializer',$this);

            $this->dashboardData = new DashboardData($this);
        }
        catch (CoreException $e){

        }
    }

    /**
     * @return AdminSectionCollectionInterface
     */
    public function getAdminSections() : AdminSectionCollectionInterface
    {
        return $this->adminSections;
    }

    /**
     * @param $sectionSlug
     * @return AdminSectionInterface
     * @throws SectionNotFoundException
     */
    public function getAdminSectionByKey($sectionSlug) : AdminSectionInterface
    {
        return $this->adminSections->getByKey($sectionSlug);
    }

    /**
     * @return DashboardData
     */
    public function getDashboardData() :DashboardData
    {
        return $this->dashboardData;
    }

    /**
     * @return AdminSectionInterface
     */
    public function getCurrentAdminSection(): AdminSectionInterface
    {
        return $this->currentAdminSection;
    }

    /**
     * @param AdminSectionInterface $currentAdminSection
     */
    public function setCurrentAdminSection(AdminSectionInterface $currentAdminSection): void
    {
        $this->currentAdminSection = $currentAdminSection;
    }

    /**
     * @return array
     */
    public function getSectionsIdSlugMap() : array
    {
        return $this->sectionsIdSlugMap;
    }

    /**
     * @return void
     */
    private function registerCoreSections() : void
    {
        $coreSections = (new CoreSectionRepository())->getAdminCoreSectionsModels();
        foreach ($coreSections as $section) $this->adminSections->appendItem($section);
    }

    /**
     * @param SectionRepositoryInterface $sectionRepository
     * @throws SectionRepositoryException
     */
    private function registerSections(SectionRepositoryInterface $sectionRepository) : void
    {

        $sectionDtoCollection = $sectionRepository->getCollection()->getItems();

        foreach ($sectionDtoCollection as $item)
        {
            try {

                if(!$item instanceof SectionRepositoryDtoInterface) throw new SectionRepositoryException();

                $this->moderatorDTO->hasAccessForCoreSection($item->getId());

                $coreModule = $item->getCoreModule();
                if($coreModule instanceof CoreModule && !$coreModule->active) throw new ModuleNotActiveException();

                $adminSectionClass = $item->getAdminSectionClass();

                $section = class_exists($adminSectionClass) ? new $adminSectionClass : null;
                if(!$section instanceof AdminSectionInterface) throw new SectionNotFoundException();

                $section->setTitle($item->getTitle());
                if(!$section->getCmsModelClass()) throw new SectionCmsModelException();

                $this->adminSections->appendItem($section);
                $this->sectionsIdSlugMap[$item->getId()] = $section->getSectionSlug();

            }
            catch (CoreException $e) {
                continue;
            }
        }
    }

}
