<?php

namespace VmdCms\CoreCms\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use VmdCms\CoreCms\Contracts\Models\CmsInfoModelInterface;
use VmdCms\CoreCms\CoreModules\Content\Entity\Languages;

abstract class CmsInfoModel extends CmsModel implements CmsInfoModelInterface
{
    const LANG_KEY = 'lang_key';

    /**
     * CmsInfoModel constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('current_language', function (Builder $builder) {
            $builder->where(static::LANG_KEY, Languages::getInstance()->getCurrentLocale());
        });

        static::saving(function (CmsInfoModelInterface $model){
            $key = static::LANG_KEY;
            $model->$key = Languages::getInstance()->getCurrentLocale();
        });

        static::saved(function (CmsInfoModelInterface $model){

            $languages = Languages::getInstance()->getLanguages();
            if(is_countable($languages)){
                $modelClass = get_class($model);
                $foreignField = $model::getForeignField();
                $currentLocale = Languages::getInstance()->getCurrentLocale();
                foreach ($languages as $language){
                    if($model->lang_key == $language->key) continue;

                    Languages::getInstance()->setLocale($language->key);
                    $localeModel =  $modelClass::where($foreignField,$model->$foreignField)->first();

                    if($localeModel instanceof $modelClass){
                        foreach ($model->getAttributes() as $attribute=>$value){

                            if(in_array($attribute,['id','created_at','updated_at'])){
                                continue;
                            }

                            if(!isset($localeModel->$attribute) || is_null($localeModel->$attribute)){
                                $localeModel->$attribute = $model->$attribute;
                            }
                        }

                    }else{
                        $localeModel = $model->replicate();
                    }

                    $localeModel->lang_key = $language->key;
                    $localeModel->saveQuietly();
                }
                Languages::getInstance()->setLocale($currentLocale);
            }
        });
    }

    /**
     * @return string
     */
    public static function getForeignField() : string
    {
        return static::getBaseModelClass()::table() . '_id';
    }

    /**
     * @return string
     */
    public static function getBaseModelClass(): string
    {
        return substr(static::class,0 , -4);
    }

}
