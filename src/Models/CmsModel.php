<?php

namespace VmdCms\CoreCms\Models;

use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use Illuminate\Database\Eloquent\Model;
use VmdCms\CoreCms\Contracts\Models\SoftDeletableInterface;
use function foo\func;

abstract class CmsModel extends Model implements CmsModelInterface
{
    public abstract static function table(): string;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->table = static::table();

    }

    protected static function boot()
    {
        parent::boot();

        static::replicating(function (CmsModel $model){
            static::replicateData($model);
        });

        static::deleting(function (CmsModel $model){
            if($model instanceof SoftDeletableInterface)
            {
                $model->softDelete();
                return false;
            }
        });
    }

    protected static function replicateData(CmsModel $model){}

    /**
     * @return string
     */
    public static function getPrimaryField(): string
    {
        return 'id';
    }

    public function getPrimaryValue()
    {
        $field = static::getPrimaryField();
        return $this->$field ?? null;
    }
}
