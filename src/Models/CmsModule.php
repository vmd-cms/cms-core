<?php

namespace VmdCms\CoreCms\Models;

use VmdCms\CoreCms\Contracts\Models\CmsModuleInterface;
use VmdCms\CoreCms\Traits\Models\Activable;

abstract class CmsModule extends CmsModel implements CmsModuleInterface
{
    use Activable;

    /**
     * @inheritDoc
     */
    public function getSlug(): string
    {
        return static::table() ?? mb_strtolower(last(explode('/',static::class)));
    }
}
