<?php

namespace VmdCms\CoreCms\Models\Media;

use VmdCms\CoreCms\Models\CmsModel;

class MediaDimension extends CmsModel
{
    protected $fillable = ['resource_id','resource','field','key','path'];

    public static function table(): string
    {
        return 'media_dimensions';
    }
}
