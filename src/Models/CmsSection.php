<?php

namespace VmdCms\CoreCms\Models;

use VmdCms\CoreCms\Contracts\Sections\AdminSectionInterface;
use VmdCms\CoreCms\DTO\Dashboard\Navs\ChipItemDto;
use VmdCms\CoreCms\Services\UrlGenerator;
use VmdCms\CoreCms\Traits\Sections\AdminSectionPolicy;
use VmdCms\CoreCms\Traits\Sections\AdminSectionResource;

abstract class CmsSection implements AdminSectionInterface
{
    use AdminSectionPolicy,AdminSectionResource;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $slug;

    /**
     * @var string
     */
    protected $cmsModelClass;

    public function __construct()
    {
        $this->cmsModelClass = $this->getCmsModelClass() ?? null;
    }

    /**
     * @return string
     */
    public function getSectionSlug() : string
    {
        if(empty($this->slug) && $this->slug !== '') {
            $this->slug = UrlGenerator::getInstance()->slug($this->title);
        }
        return $this->slug;
    }

    /**
     * @inheritDoc
     */
    public function setTitle(string $title): AdminSectionInterface
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getTitle() : ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $cmsModelClass
     * @return AdminSectionInterface
     */
    public function setCmsModelClass(?string $cmsModelClass): AdminSectionInterface
    {
        $cmsModel = class_exists($cmsModelClass) ? new $cmsModelClass : null;
        if($cmsModel instanceof CmsModel){
            $this->cmsModelClass = $cmsModel;
        }
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getCmsModelClass(): ?string
    {
        return $this->cmsModelClass;
    }

    protected function getCurrentSection(): AdminSectionInterface
    {
        return $this;
    }

    /**
     * @return ChipItemDto|null
     */
    public function getChip(): ?ChipItemDto
    {
        return null;
    }

}
