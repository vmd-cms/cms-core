<?php

namespace VmdCms\CoreCms\Models;

use VmdCms\CoreCms\Contracts\Models\CmsCrossModelInterface;

abstract class CmsCrossModel extends CmsModel implements CmsCrossModelInterface
{
    public function getBaseModelForeignField(): string
    {
        return $this->getBaseModelClass()::table() . '_' . $this->getBaseModelClass()::getPrimaryField();
    }

    public function getTargetModelForeignField(): string
    {
        return $this->getTargetModelClass()::table() . '_' . $this->getTargetModelClass()::getPrimaryField();
    }

    public function getBaseModelPrimaryField(): string
    {
        return $this->getBaseModelClass()::getPrimaryField();
    }

    public function getTargetModelPrimaryField(): string
    {
        return $this->getTargetModelClass()::getPrimaryField();
    }
}
