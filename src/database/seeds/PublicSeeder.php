<?php
namespace VmdCms\CoreCms\database\seeds;

use App\Modules\Content\Sections\Menu\MenuFooter;
use App\Modules\Content\Sections\Menu\MenuHeader;
use App\Modules\Content\Sections\Pages\Custom\MainPage;
use App\Modules\Content\Sections\Pages\Blogs\BlogsPage;
use App\Modules\Content\Sections\Pages\Blogs\Blog;
use App\Modules\Content\Sections\Pages\Custom\InfoPage;
use App\Modules\Content\Sections\Blocks\Groups\GeneralGroupBlock;
use App\Modules\Translates\Sections\Translate;
use App\Modules\Translates\Sections\Groups\GeneralGroupTranslate;
use App\Modules\Translates\Sections\Groups\FormGroupTranslate;
use App\Modules\Settings\Sections\Settings;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use VmdCms\CoreCms\CoreModules\Content\Sections\Dashboard;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\CoreModules\Events\Sections\CoreEventDependency;
use VmdCms\CoreCms\CoreModules\Events\Sections\Templates\CoreEventMailTemplate;
use VmdCms\CoreCms\CoreModules\Events\Sections\Templates\CoreEventSmsTemplate;
use VmdCms\CoreCms\CoreModules\Navigations\Models\CoreNavDashboard;
use VmdCms\CoreCms\CoreModules\Seo\Sections\Seo;
use VmdCms\CoreCms\Traits\Database\NavSeeder;
use VmdCms\CoreCms\Traits\Database\SectionSeeder;

class PublicSeeder extends Seeder
{
    use SectionSeeder,NavSeeder;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::transaction(function (){
                $this->seedSections();
                $this->seedNavigation();
            });
        }
        catch (\Exception $e)
        {
            echo $e->getMessage();
        }
    }

    /**
     * @inheritDoc
     */
    protected function getNavigationAssoc(): array
    {
        return [
            [
                "slug" => "dashboard",
                "title" => CoreLang::get('main_page'),
                "icon" => "icon icon-home",
                "order" => 1,
                "section_class" => Dashboard::class
            ],
            [
                "slug" => "content_group",
                "title" => CoreLang::get('content'),
                "is_group_title" => true,
                "order" => 3,
                "children" => [
                    [
                        "slug" => "menu_groups",
                        "title" => CoreLang::get('menu'),
                        "icon" => "icon icon-menu-burger",
                        "children" => [
                            [
                                "slug" => "menu_header",
                                "title" => CoreLang::get('menu_header'),
                                "section_class" => MenuHeader::class,
                                "order" => 1,
                            ],
                            [
                                "slug" => "menu_footer",
                                "title" => CoreLang::get('menu_footer'),
                                "section_class" => MenuFooter::class,
                                "order" => 2,
                            ]
                        ]
                    ],
                    [
                        "slug" => "pages_groups",
                        "title" => CoreLang::get('info_pages'),
                        "icon" => "icon icon-copy",
                        "children" => [
                            [
                                "slug" => "main_page",
                                "title" => CoreLang::get('main_page'),
                                "section_class" => MainPage::class,
                                "order" => 1,
                            ],
                            [
                                "slug" => "blogs_page",
                                "title" => CoreLang::get('blogs_page'),
                                "section_class" => BlogsPage::class,
                                "order" => 2,
                            ],
                            [
                                "slug" => "blog",
                                "title" => CoreLang::get('blog_page'),
                                "section_class" => Blog::class,
                                "order" => 3,
                            ],
                            [
                                "slug" => "info_pages",
                                "title" => CoreLang::get('info_pages'),
                                "section_class" => InfoPage::class,
                                "order" => 4,
                            ]
                        ]
                    ],
                    [
                        "slug" => "block_groups",
                        "title" => CoreLang::get('info_blocks'),
                        "icon" => "icon icon-copy",
                        "children" => [
                            [
                                "slug" => "general_blocks",
                                "title" => CoreLang::get('general'),
                                "section_class" => GeneralGroupBlock::class,
                                "order" => 2,
                            ]
                        ]
                    ]
                ]
            ],
            [
                "slug" => "configs",
                "title" => CoreLang::get('configurations'),
                "is_group_title" => true,
                "order" => 5,
                "children" => [
                    [
                        "slug" => "settings",
                        "title" => CoreLang::get('settings'),
                        "section_class" => Settings::class,
                        "order" => 1,
                        "icon" => "icon icon-options",
                    ],
                    [
                        "slug" => "translate_groups",
                        "title" => CoreLang::get('translates'),
                        "order" => 2,
                        "icon" => "icon icon-card",
                        "children" => [
                            [
                                "slug" => "all_translates",
                                "title" => CoreLang::get('all'),
                                "section_class" => Translate::class,
                                "order" => 1,
                            ],
                            [
                                "slug" => "general_translates",
                                "title" => CoreLang::get('general'),
                                "section_class" => GeneralGroupTranslate::class,
                                "order" => 2,
                            ],
                            [
                                "slug" => "form_translates",
                                "title" => CoreLang::get('forms'),
                                "section_class" => FormGroupTranslate::class,
                                "order" => 3,
                            ],
                        ]
                    ],
                    [
                        "slug" => "seo",
                        "title" => "SEO",
                        "section_class" => Seo::class,
                        "order" => 3,
                        "icon" => "icon icon-search",
                    ],
                    [
                        "slug" => "core_events",
                        "title" => CoreLang::get('alerts'),
                        "order" => 4,
                        "icon" => "icon icon-card",
                        "children" => [
                            [
                                "slug" => "core_events_mail_templates",
                                "title" => CoreLang::get('mail_templates'),
                                "section_class" => CoreEventMailTemplate::class,
                                "order" => 1,
                            ],
                            [
                                "slug" => "core_events_sms_templates",
                                "title" => CoreLang::get('sms_templates'),
                                "section_class" => CoreEventSmsTemplate::class,
                                "order" => 2,
                            ],
                            [
                                "slug" => "core_events_dependency",
                                "title" => CoreLang::get('events'),
                                "section_class" => CoreEventDependency::class,
                                "order" => 3,
                            ],
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * @return string
     */
    protected function getNavClass(): string
    {
        return CoreNavDashboard::class;
    }

    /**
     * @inheritDoc
     */
    protected function getSectionsAssoc(): array
    {
        return [
            Dashboard::class => CoreLang::get('main_page'),
            Settings::class => CoreLang::get('settings'),
            Translate::class => CoreLang::get('translates'),
            GeneralGroupTranslate::class => CoreLang::get('translates_general'),
            FormGroupTranslate::class => CoreLang::get('translates_forms'),
            Seo::class => "Seo",
            CoreEventMailTemplate::class => CoreLang::get('mail_templates'),
            CoreEventSmsTemplate::class => CoreLang::get('sms_templates'),
            CoreEventDependency::class => CoreLang::get('events'),
            InfoPage::class => CoreLang::get('info_pages'),
            MainPage::class => CoreLang::get('main_page'),
            BlogsPage::class => CoreLang::get('blogs_page'),
            Blog::class => CoreLang::get('blog_page'),
            GeneralGroupBlock::class => CoreLang::get('info_blocks_general'),
            MenuHeader::class => CoreLang::get('menu_header'),
            MenuFooter::class => CoreLang::get('menu_footer')
        ];
    }
}
