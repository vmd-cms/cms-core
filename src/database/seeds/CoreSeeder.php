<?php
namespace VmdCms\CoreCms\database\seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Entity\CoreTranslatesEntity;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\CoreModules\Languages\Models\CoreLanguage;
use VmdCms\CoreCms\CoreModules\Moderators\Models\Moderator;
use VmdCms\CoreCms\CoreModules\Moderators\Models\ModeratorRole;
use VmdCms\CoreCms\CoreModules\Navigations\Models\CoreNavSettings;
use VmdCms\CoreCms\Services\Config;
use VmdCms\CoreCms\Traits\Database\NavSeeder;
use VmdCms\CoreCms\Traits\Database\SectionSeeder;

class CoreSeeder extends Seeder
{
    use SectionSeeder,NavSeeder;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::transaction(function (){

                $this->seedLanguages();
                $this->seedCoreTranslates();

                $this->seedSections();
                $this->seedNavigation();
                $adminRoleId = $this->seedModeratorRole();
                $this->seedModerator('developer@valmax.com',null,true);
                $this->seedModerator('admin@valmax.com', $adminRoleId);
            });
        }
        catch (\Exception $e)
        {
            echo $e->getMessage();
        }
    }

    private function seedModeratorRole($slug = "admin")
    {
        try {
            $role = ModeratorRole::where("slug", $slug)->first();
            if(!$role instanceof ModeratorRole){
                $role = ModeratorRole::create([
                    "slug" => $slug,
                    "title" => Str::ucfirst($slug),
                    "description" => $slug,
                    "active" => true
                ]);
            }
            return $role->id;
        }
        catch (\Exception $e)
        {
            return null;
        }
    }

    private function seedModerator($email,$roleId = null,$hasFullAccess = false)
    {
        try {
            $moderator = Moderator::where("email", $email)->first();
            if(!$moderator instanceof Moderator){
                $moderator = Moderator::create([
                    "first_name" => $email,
                    "email" => $email,
                    "password" => Hash::make($email),
                    "moderator_role_id" => $roleId,
                    "active" => true,
                    "has_full_access" => $hasFullAccess
                ]);
            }
            return $moderator->id;
        }
        catch (\Exception $e)
        {
            return null;
        }
    }

    private function seedLanguages()
    {
        try {

        }catch (\Exception $exception){}

        try {
            $baseLang = Config::getInstance()->getBaseLang();
            $key = $baseLang['key'] ?? 'ru';
            $symbol = $baseLang['symbol'] ?? 'Рус';
            $title = $baseLang['title'] ?? 'Русский';

            $model = CoreLanguage::where("key", $key)->first();
            if(!$model instanceof CoreLanguage){
                $model = CoreLanguage::create([
                    "key" => $key,
                    "symbol" => $symbol,
                    "title" => $title,
                    "icon" => '',
                    "default" => true,
                    "active" => true,
                ]);
            }
            return $model->id;
        }
        catch (\Exception $e)
        {
            return null;
        }
    }

    protected function seedCoreTranslates()
    {
        try {
            $baseLang = Config::getInstance()->getBaseLang();
            $key = $baseLang['key'] ?? 'ru';
            (new CoreTranslatesEntity())->seedCoreTranslates($key);
        }
        catch (\Exception $e) {}
    }

    /**
     * @inheritDoc
     */
    protected function getNavigationAssoc(): array
    {
        return [
            [
                "slug" => "core_settings",
                "title" => CoreLang::get('settings'),
                "children" => [
                    [
                        "slug" => "core_modules",
                        "title" => CoreLang::get('modules'),
                        "section_class" => \VmdCms\CoreCms\CoreModules\Modules\Sections\CoreModule::class
                    ],
                    [
                        "slug" => "core_sections",
                        "title" => CoreLang::get('sections'),
                        "section_class" => \VmdCms\CoreCms\CoreModules\Sections\Sections\CoreSection::class
                    ],
                    [
                        "slug" => "core_languages",
                        "title" => CoreLang::get('languages'),
                        "section_class" => \VmdCms\CoreCms\CoreModules\Languages\Sections\CoreLanguage::class
                    ],
                    [
                        "slug" => "core_translates",
                        "title" => CoreLang::get('translates'),
                        "section_class" => \VmdCms\CoreCms\CoreModules\CoreTranslates\Sections\CoreTranslate::class
                    ]
                ]
            ],
            [
                "slug" => "core_moderators",
                "title" => CoreLang::get('moderators'),
                "children" => [
                    [
                        "slug" => "core_moderator",
                        "title" => CoreLang::get('moderators'),
                        "section_class" => \VmdCms\CoreCms\CoreModules\Moderators\Sections\Moderator::class
                    ],
                    [
                        "slug" => "core_moderator_roles",
                        "title" => CoreLang::get('moderator_roles'),
                        "section_class" => \VmdCms\CoreCms\CoreModules\Moderators\Sections\ModeratorRole::class
                    ]
                ]
            ],
            [
                "slug" => "core_navigation",
                "title" => CoreLang::get('navigation'),
                "children" => [
                    [
                        "slug" => "core_nav_dashboard",
                        "title" => CoreLang::get('navigation_dashboard'),
                        "section_class" => \VmdCms\CoreCms\CoreModules\Navigations\Sections\CoreNavDashboard::class
                    ],
                    [
                        "slug" => "core_nav_settings",
                        "title" => CoreLang::get('navigation_settings'),
                        "section_class" => \VmdCms\CoreCms\CoreModules\Navigations\Sections\CoreNavSettings::class
                    ]
                ]
            ]
        ];
    }

    /**
     * @return string
     */
    protected function getNavClass(): string
    {
        return CoreNavSettings::class;
    }

    /**
     * @inheritDoc
     */
    protected function getSectionsAssoc(): array
    {
        return [
            \VmdCms\CoreCms\CoreModules\Modules\Sections\CoreModule::class => CoreLang::get('modules'),
            \VmdCms\CoreCms\CoreModules\Sections\Sections\CoreSection::class => CoreLang::get('sections'),
            \VmdCms\CoreCms\CoreModules\Languages\Sections\CoreLanguage::class => CoreLang::get('languages'),
            \VmdCms\CoreCms\CoreModules\CoreTranslates\Sections\CoreTranslate::class => CoreLang::get('translates'),
            \VmdCms\CoreCms\CoreModules\Moderators\Sections\Moderator::class => CoreLang::get('moderators'),
            \VmdCms\CoreCms\CoreModules\Moderators\Sections\ModeratorRole::class => CoreLang::get('moderator_roles'),
            \VmdCms\CoreCms\CoreModules\Navigations\Sections\CoreNavDashboard::class => CoreLang::get('navigation_dashboard'),
            \VmdCms\CoreCms\CoreModules\Navigations\Sections\CoreNavSettings::class => CoreLang::get('navigation_settings'),
        ];
    }
}
