<?php

use VmdCms\CoreCms\CoreModules\Moderators\Models\Moderator as model;
use VmdCms\CoreCms\CoreModules\Moderators\Models\ModeratorRole as modelForeign;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModeratorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table){
            $table->increments(model::getPrimaryField());
            $table->string('email',128)->unique();
            $table->string('phone')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('father_name')->nullable();
            $table->string('position')->nullable();
            $table->string('photo')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->boolean('has_full_access')->default(false);
            $table->integer('moderator_role_id')->unsigned()->nullable();
            $table->boolean('active')->default(false);
            $table->rememberToken();
            $table->timestamps();
        });
        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('moderator_role_id', model::table() . '_moderator_role_id_fk')->references('id')->on(modelForeign::table())->onUpdate('CASCADE')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
