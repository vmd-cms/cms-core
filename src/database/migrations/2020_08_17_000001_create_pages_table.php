<?php

use VmdCms\CoreCms\CoreModules\Content\Models\Pages\Page as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('key',128)->nullable();
            $table->string('url',128)->nullable()->unique();
            $table->string('title',255)->nullable();
            $table->string('photo_preview',512)->nullable();
            $table->string('photo',512)->nullable();
            $table->string('media',512)->nullable();
            $table->longText('data')->nullable();
            $table->boolean('flag')->default(false);
            $table->boolean('active')->default(true);
            $table->integer('order')->unsigned()->default(1);
            $table->timestamp('public_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
