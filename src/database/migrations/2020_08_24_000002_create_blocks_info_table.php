<?php

use VmdCms\CoreCms\CoreModules\Languages\Models\CoreLanguage;
use VmdCms\CoreCms\CoreModules\Content\Models\Blocks\BlockInfo as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlocksInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table){
            $table->increments('id');
            $table->string(model::LANG_KEY,5)->nullable();
            $table->integer(model::getForeignField())->unsigned();
            $table->longText('body')->nullable();
            $table->text('data')->nullable();
            $table->string('media',1024)->nullable();
            $table->timestamps();
            $table->unique([model::LANG_KEY, model::getForeignField()]);
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign(model::getForeignField(), model::table() . '_' . model::getForeignField().'_fk')
                ->references(model::getBaseModelClass()::getPrimaryField())->on(model::getBaseModelClass()::table())
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign(model::LANG_KEY, model::table() . '_' . model::LANG_KEY.'_fk')
                ->references(CoreLanguage::getPrimaryField())->on(CoreLanguage::table())
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
