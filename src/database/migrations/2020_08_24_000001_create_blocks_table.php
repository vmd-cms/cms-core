<?php

use VmdCms\CoreCms\CoreModules\Content\Models\Blocks\Block as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table){
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('group',64)->nullable();
            $table->string('key',64)->nullable();
            $table->integer('resource_id')->unsigned()->nullable();
            $table->string('resource',64)->nullable();
            $table->string('description',512)->nullable();
            $table->string('media',512)->nullable();
            $table->longText('data')->nullable();
            $table->boolean('active')->default(true);
            $table->integer('order')->unsigned()->default(1);
            $table->timestamps();
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('parent_id', model::table() . '_parent_id_fk')
                ->references('id')->on(model::table())
                ->onUpdate('CASCADE')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
