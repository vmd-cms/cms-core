<?php

use VmdCms\CoreCms\CoreModules\Languages\Models\CoreLanguage as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoreLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table){
            $table->increments('id');
            $table->string(model::getPrimaryField(),5)->unique();
            $table->string('title',255)->nullable();
            $table->string('symbol',64)->unique();
            $table->string('icon',255)->nullable();
            $table->integer('order')->default(1);
            $table->boolean('default')->default(false);
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
