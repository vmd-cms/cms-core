<?php

use VmdCms\CoreCms\CoreModules\Translates\Models\Translate as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTranslatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table){
            $table->increments('id');
            $table->string('group',64)->nullable();
            $table->string('key',64)->unique();
            $table->string('description',512)->nullable();
            $table->boolean('active')->default(true);
            $table->integer('order')->unsigned()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
