<?php

use VmdCms\CoreCms\CoreModules\Settings\Models\Settings as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table){
            $table->increments('id');
            $table->string('group',64)->nullable();
            $table->string('key',64)->nullable();
            $table->string('slug',64)->nullable();
            $table->string('title',255)->nullable();
            $table->string('value',255)->nullable();
            $table->string('media',255)->nullable();
            $table->longText('data')->nullable();
            $table->integer('order')->default(1);
            $table->boolean('default')->default(false);
            $table->boolean('active')->default(true);
            $table->timestamps();

            $table->unique(['group','key','slug']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
