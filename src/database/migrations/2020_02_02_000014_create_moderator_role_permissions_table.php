<?php

use VmdCms\CoreCms\CoreModules\Moderators\Models\Moderator;
use VmdCms\CoreCms\CoreModules\Moderators\Models\ModeratorRole;
use VmdCms\CoreCms\CoreModules\Moderators\Models\ModeratorRolePermission as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use VmdCms\CoreCms\CoreModules\Sections\Models\CoreSection;

class CreateModeratorRolePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table){
            $table->increments(model::getPrimaryField());
            $table->integer('moderator_role_id')->unsigned();
            $table->integer('core_section_id')->unsigned();
            $table->boolean('display_perm')->default(false);
            $table->boolean('create_perm')->default(false);
            $table->boolean('view_perm')->default(false);
            $table->boolean('edit_perm')->default(false);
            $table->boolean('delete_perm')->default(false);
            $table->integer('moderator_id')->unsigned()->nullable();
            $table->integer('order')->unsigned()->default(1);
            $table->timestamps();

            $table->foreign('moderator_role_id')->references('id')->on(ModeratorRole::table())->onDelete('cascade');
            $table->foreign('core_section_id')->references('id')->on(CoreSection::table())->onDelete('cascade');
            $table->foreign('moderator_id')->references('id')->on(Moderator::table())->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
