<?php

use VmdCms\CoreCms\CoreModules\Events\Models\CoreEventDependency as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoreEventDependenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments('id');
            $table->integer('core_event_id')->unsigned();
            $table->integer('core_event_template_id')->unsigned();
            $table->string('group',32)->nullable();
            $table->string('type',32);
            $table->text('params')->nullable();
            $table->string('condition',32)->nullable();
            $table->string('condition_value',128)->nullable();
            $table->boolean('active')->nullable();
            $table->timestamps();
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('core_event_id', model::table() . '_core_event_id_fk')
                ->references('id')->on(\VmdCms\CoreCms\CoreModules\Events\Models\CoreEvent::table())
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('core_event_template_id', model::table() . '_core_event_template_id_fk')
                ->references('id')->on(\VmdCms\CoreCms\CoreModules\Events\Models\Templates\CoreEventTemplate::table())
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
