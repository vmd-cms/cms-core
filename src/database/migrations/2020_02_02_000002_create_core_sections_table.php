<?php

use VmdCms\CoreCms\CoreModules\Sections\Models\CoreSection as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoreSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table){
            $table->increments(model::getPrimaryField());
            $table->integer('module_id')->nullable();
            $table->string('core_section_class',128)->unique();
            $table->string('title',255)->nullable();
            $table->integer('order')->default(1);
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
