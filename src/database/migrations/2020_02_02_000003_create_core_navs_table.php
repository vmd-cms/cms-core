<?php

use VmdCms\CoreCms\CoreModules\Navigations\Models\CoreNav as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoreNavsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table){
            $table->increments(model::getPrimaryField());
            $table->integer('parent_id')->nullable();
            $table->string('slug',128)->nullable();
            $table->integer('section_id')->nullable();
            $table->string('title',255)->nullable();
            $table->string('icon',255)->nullable();
            $table->string('anchor',255)->nullable();
            $table->integer('order')->default(1);
            $table->boolean('is_group_title')->default(false);
            $table->boolean('active')->default(true);
            $table->timestamps();
            $table->unique(['parent_id', 'slug']);
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
