<?php

use VmdCms\CoreCms\CoreModules\Notifications\Models\NotificationLog as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments('id');
            $table->integer('core_event_id')->unsigned()->nullable();
            $table->integer('core_event_dependency_id')->unsigned()->nullable();
            $table->integer('core_event_template_id')->unsigned()->nullable();
            $table->string('type')->nullable();
            $table->string('group')->nullable();
            $table->string('recipients')->nullable();
            $table->string('subject')->nullable();
            $table->text('body')->nullable();
            $table->string('status')->nullable();
            $table->text('error')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
