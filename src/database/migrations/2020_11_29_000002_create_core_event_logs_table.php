<?php

use VmdCms\CoreCms\CoreModules\Events\Models\CoreEventLog as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoreEventLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments('id');
            $table->integer('core_event_id')->unsigned()->nullable()->index('event_id');
            $table->string('core_event_slug',64)->nullable()->index('event_slug');
            $table->integer('resource_id')->unsigned()->nullable()->index('resource_id');
            $table->string('resource_slug',32)->nullable()->index('resource_slug');
            $table->integer('moderator_id')->unsigned()->nullable()->index('moderator_id');
            $table->integer('user_id')->unsigned()->nullable()->index('user_id');
            $table->string('user_session_id',128)->nullable();
            $table->text('payload')->nullable();
            $table->text('error')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
