<?php

use VmdCms\CoreCms\CoreModules\Seo\Models\Seo as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments(model::getPrimaryField());
            $table->string('resource',128)->nullable();
            $table->integer('resource_id')->nullable()->unsigned();
            $table->string('url',128)->nullable();
            $table->string('meta_photo',255)->nullable();
            $table->longText('data')->nullable();
            $table->boolean('active')->default(true);
            $table->timestamps();

            $table->unique(['resource', 'resource_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
