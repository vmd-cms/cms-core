<?php

namespace VmdCms\CoreCms\Factories;

use VmdCms\CoreCms\Contracts\Factories\FormComponentsFactoryInterface;
use VmdCms\CoreCms\Dashboard\Forms\Tables\Components;
use Illuminate\Contracts\Foundation\Application;

class FormTableColumnComponentsFactory extends FactoryAbstract implements FormComponentsFactoryInterface
{
    /**
     * FormComponentsFactory constructor.
     * @param Application $application
     */
    public function __construct(Application $application)
    {
        parent::__construct($application);

        $this->register([
            'input' => Components\ColumnInput::class,
            'numeric' => Components\ColumnNumeric::class,
            'text' => Components\ColumnText::class,
            'textArea' => Components\ColumnTextArea::class,
            'image' => Components\ColumnImage::class,
            'select' => Components\ColumnSelect::class,
            'autocomplete' => Components\ColumnAutocomplete::class,
            'switch' => Components\ColumnSwitch::class,
            'dateTime' => Components\ColumnDateTime::class,
            'file' => Components\ColumnFile::class,
            'custom' => Components\CustomColumn::class,
            'colorPicker' => Components\ColumnColorPicker::class,
        ]);
    }

}
