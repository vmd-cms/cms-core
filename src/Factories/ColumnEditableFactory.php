<?php

namespace VmdCms\CoreCms\Factories;

use VmdCms\CoreCms\Contracts\Factories\FormFactoryInterface;
use VmdCms\CoreCms\Dashboard\Display\Components;
use Illuminate\Contracts\Foundation\Application;

class ColumnEditableFactory extends FactoryAbstract implements FormFactoryInterface
{
    /**
     * ColumnFactory constructor.
     * @param Application $application
     */
    public function __construct(Application $application)
    {
        parent::__construct($application);

        $this->register([
            'text' => Components\EditableTextColumn::class,
            'switch' => Components\EditableSwitchColumn::class,
            'colorPicker' => Components\EditableColorPickerColumn::class,
            'dateTime' => Components\EditableDateTimeColumn::class,
        ]);
    }

}
