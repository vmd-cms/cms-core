<?php

namespace VmdCms\CoreCms\Factories;

use VmdCms\CoreCms\Contracts\Factories\FormFactoryInterface;
use VmdCms\CoreCms\Dashboard\Forms;
use Illuminate\Contracts\Foundation\Application;

class FormFactory extends FactoryAbstract implements FormFactoryInterface
{
    /**
     * FormComponentsFactory constructor.
     * @param Application $application
     */
    public function __construct(Application $application)
    {
        parent::__construct($application);

        $this->register([
            'panel' => Forms\FormPanel::class,
            'tabbed' => Forms\FormTabbed::class,
        ]);
    }

}
