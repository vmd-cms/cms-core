<?php

namespace VmdCms\CoreCms\Factories;

use VmdCms\CoreCms\Contracts\Factories\DisplayFactoryInterface;
use VmdCms\CoreCms\Dashboard\Display;
use Illuminate\Contracts\Foundation\Application;

class DisplayFactory extends FactoryAbstract implements DisplayFactoryInterface
{
    /**
     * FormComponentsFactory constructor.
     * @param Application $application
     */
    public function __construct(Application $application)
    {
        parent::__construct($application);

        $this->register([
            'dataTable' => Display\DataTable::class,
            'tree' => Display\DraggableNestedTree::class,
        ]);
    }

}
