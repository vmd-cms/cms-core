<?php

namespace VmdCms\CoreCms\Factories;

use VmdCms\CoreCms\Exceptions\Factories\AliasMethodCallException;
use Illuminate\Contracts\Foundation\Application;
use ReflectionClass;

abstract class FactoryAbstract
{
    /**
     * @var Application
     */
    protected $app;

    protected $aliases = [];

    /**
     * FactoryAbstract constructor.
     * @param Application $application
     */
    public function __construct(Application $application)
    {
        $this->app = $application;
    }

    /**
     * @param array $classes
     * @return $this
     */
    protected function register(array $classes)
    {
        foreach ($classes as $alias => $class) {
            $this->bind($alias, $class);
        }

        return $this;
    }

    /**
     * @param $alias
     * @param $class
     * @return $this
     */
    public function bind($alias, $class)
    {
        $this->aliases[$alias] = $class;

        return $this;
    }

    /**
     * @param $name
     * @param $arguments
     * @return object
     * @throws AliasMethodCallException
     * @throws \ReflectionException
     */
    public function __call($name, $arguments)
    {
        if (! $this->hasAlias($name)) {
            throw new AliasMethodCallException($name);
        }

        return $this->makeClass($name, $arguments);
    }

    /**
     * @param $alias
     * @return bool
     */
    public function hasAlias($alias)
    {
        return array_key_exists($alias, $this->aliases);
    }

    /**
     * @param $alias
     * @param array $arguments
     * @return object
     * @throws \ReflectionException
     */
    public function makeClass($alias, array $arguments)
    {
        $reflection = new ReflectionClass($this->getAlias($alias));

        return $reflection->newInstanceArgs($arguments);
    }

    /**
     * @param $alias
     * @return mixed
     */
    public function getAlias($alias)
    {
        return $this->aliases[$alias];
    }
}
