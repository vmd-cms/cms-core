<?php

namespace VmdCms\CoreCms\Factories;

use VmdCms\CoreCms\Contracts\Factories\FormFactoryInterface;
use VmdCms\CoreCms\Dashboard\Display\Components;
use Illuminate\Contracts\Foundation\Application;

class ColumnFactory extends FactoryAbstract implements FormFactoryInterface
{
    /**
     * ColumnFactory constructor.
     * @param Application $application
     */
    public function __construct(Application $application)
    {
        parent::__construct($application);

        $this->register([
            'text' => Components\TextColumn::class,
            'custom' => Components\CustomColumn::class,
            'photo' => Components\PhotoColumn::class,
            'chipText' => Components\ChipTextColumn::class,
            'date' => Components\DateColumn::class
        ]);
    }

}
