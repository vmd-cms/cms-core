<?php

namespace VmdCms\CoreCms\Factories;

use VmdCms\CoreCms\Contracts\Factories\FormComponentsFactoryInterface;
use VmdCms\CoreCms\Dashboard\Forms\Components;
use Illuminate\Contracts\Foundation\Application;

class FormComponentsFactory extends FactoryAbstract implements FormComponentsFactoryInterface
{
    /**
     * FormComponentsFactory constructor.
     * @param Application $application
     */
    public function __construct(Application $application)
    {
        parent::__construct($application);

        $this->register([
            'input' => Components\InputComponent::class,
            'phone' => Components\PhoneComponent::class,
            'url' => Components\UrlComponent::class,
            'password' => Components\PasswordComponent::class,
            'text' => Components\TextAreaComponent::class,
            'select' => Components\SelectComponent::class,
            'asyncSelect' => Components\AsyncSelectComponent::class,
            'autocomplete' => Components\AutocompleteComponent::class,
            'dependedSelect' => Components\DependedSelectComponent::class,
            'treeSelect' => Components\TreeSelectComponent::class,
            'switch' => Components\SwitchComponent::class,
            'radio' => Components\RadioComponent::class,
            'ckeditor' => Components\CkeditorComponent::class,
            'checkbox' => Components\CheckBoxComponent::class,
            'tree' => Components\TreeComponent::class,
            'image' => Components\ImageComponent::class,
            'file' => Components\FileInputComponent::class,
            'datatable' => Components\DataTableComponent::class,
            'youtubeCode' => Components\YoutubeCodeComponent::class,
            'colorPicker' => Components\ColorPickerComponent::class,
            'row' => Components\RowComponent::class,
            'seo' => Components\SeoGroupComponent::class,
            'custom' => Components\CustomComponent::class,
            'view' => Components\ViewComponent::class,
            'html' => Components\HtmlComponent::class,
            'alert' => Components\AlertComponent::class,
            'date' => Components\DatePicker::class,
            'dateTime' => Components\DateTimePicker::class,
            'buttons' => Components\ButtonsComponent::class,
            'actionHistory' => Components\ActionHistoryComponent::class,
        ]);
    }

}
