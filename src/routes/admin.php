<?php

use VmdCms\CoreCms\Services\CoreRouter;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;


Route::group([
    'prefix' => CoreRouter::getInstance()->getUrlPrefix(),
    'middleware' => [
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \VmdCms\CoreCms\Middleware\CoreLanguagesMiddleware::class
    ]
],function (Router $router){
    Route::group([
        'as' => CoreRouter::ADMIN_AUTH_PREFIX,
        'namespace' => "VmdCms\\CoreCms\\Controllers\\Auth",
    ],function (Router $router){
        $router->get('login', [
            'as'   => CoreRouter::LOGIN,
            'uses' => 'ModeratorAuthController@showLoginForm',
        ]);
        $router->post('login', [
            'as'   => CoreRouter::LOGIN_POST,
            'uses' => 'ModeratorAuthController@login',
        ])->middleware(['throttle:30,1']);
        $router->any('logout', [
            'as'   => CoreRouter::LOGOUT,
            'uses' => 'ModeratorAuthController@logout',
        ]);
    }
    );
    Route::group([
        'namespace' => "VmdCms\\CoreCms\\Controllers\\Admin",
        'as' => CoreRouter::ADMIN_SECTION_PREFIX,
        'middleware' => [
            \VmdCms\CoreCms\Middleware\ModeratorAuth::class,
        ]
    ], function (Router $router){

     //   $router->get('', ['as' => CoreRouter::DASHBOARD, 'uses' => 'DashboardController@getDashboard']);

        $router->get('{sectionSlug?}', [
            'as'   => CoreRouter::DISPLAY,
            'uses' => 'AdminSectionController@display',
        ]);

        $router->post('{sectionSlug}', [
            'as'   => CoreRouter::DISPLAY_POST,
            'uses' => 'AdminSectionController@display',
        ]);

        $router->get('{sectionSlug}/create', [
            'as'   => CoreRouter::CREATE_GET,
            'uses' => 'AdminSectionController@create',
        ]);

        $router->post('{sectionSlug}/create', [
            'as'   => CoreRouter::CREATE_POST,
            'uses' => 'AdminSectionController@create',
        ]);

        $router->get('{sectionSlug}/view/{id}', [
            'as'   => CoreRouter::VIEW,
            'uses' => 'AdminSectionController@view',
        ]);

        $router->get('{sectionSlug}/edit/{id}', [
            'as'   => CoreRouter::EDIT_GET,
            'uses' => 'AdminSectionController@edit',
        ]);

        $router->put('{sectionSlug}/edit/{id}', [
            'as'   => CoreRouter::EDIT_PUT,
            'uses' => 'AdminSectionController@edit',
        ]);

        $router->post('{sectionSlug}/delete/{id}', [
            'as'   => CoreRouter::DELETE,
            'uses' => 'AdminSectionController@delete',
        ]);

        $router->post('{sectionSlug}/restore/{id}', [
            'as'   => CoreRouter::RESTORE,
            'uses' => 'AdminSectionController@restore',
        ]);

        $router->put('{sectionSlug}/delete/{id}', [
            'as'   => CoreRouter::DELETE_SOFT,
            'uses' => 'AdminSectionController@delete',
        ]);

        $router->post('{sectionSlug}/tree/order', [
            'as' => CoreRouter::UPDATE_TREE_ORDER,
            'uses' => 'AdminSectionController@updateTreeOrder'
        ]);

        $router->post('{sectionSlug}/table/order', [
            'as' => CoreRouter::UPDATE_TABLE_ORDER,
            'uses' => 'AdminSectionController@updateTableOrder'
        ]);

        $router->post('{sectionSlug}/table/update-item', [
            'as' => CoreRouter::UPDATE_TABLE_ITEM,
            'uses' => 'AdminSectionController@updateTableItem'
        ]);

        $router->post('{sectionSlug}/{id}/ckeditor/file', [
            'as' => CoreRouter::XMLHTTP_CK_FILE,
            'uses' => 'AdminSectionController@ckeditorUploadFile'
        ]);

        $router->get('{sectionSlug}/{id}/ckeditor/files', [
            'as' => CoreRouter::XMLHTTP_CK_FILES_BROWSE,
            'uses' => 'AdminSectionController@ckeditorBrowseFiles'
        ]);

        $router->post('{sectionSlug}/datatables/async', [
            'as'   => CoreRouter::DATATABLE_ASYNC,
            'uses' => 'AdminSectionController@datatableAsync',
        ]);

        $router->post('{sectionSlug}/service-data/{sectionMethod}/{id?}', [
            'as'   => CoreRouter::SERVICE_DATA,
            'uses' => 'AdminSectionController@postServiceSectionMethod',
        ]);

        $router->get('{sectionSlug}/service-data/{sectionMethod}/{id?}', [
            'as'   => CoreRouter::SERVICE_DATA_GET,
            'uses' => 'AdminSectionController@getServiceSectionMethod',
        ]);

    });

    Route::group([
        'namespace' => "VmdCms\\CoreCms\\Controllers\\Admin",
        'as' => CoreRouter::ADMIN_SERVICE_PREFIX,
    ], function (Router $router){

        $router->post('/core-cms/service/check-unique', [
            'as' => CoreRouter::CHECK_UNIQUE,
            'uses' => 'AdminServiceController@checkUniqueField'
        ]);

        $router->post('core-cms/set-locale', [
            'as' => CoreRouter::SET_LOCALE,
            'uses' => 'AdminServiceController@setLocale'
        ]);

    });
});
