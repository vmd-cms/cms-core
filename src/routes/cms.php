<?php

use Illuminate\Support\Facades\Route;
use VmdCms\CoreCms\CoreModules\Content\Entity\Languages;
use VmdCms\CoreCms\CoreModules\Content\Middleware\LanguagesMiddleware;
use VmdCms\CoreCms\CoreModules\Content\Middleware\DataShareMiddleware;
use VmdCms\CoreCms\CoreModules\Languages\Models\CoreLanguage;

$languages = Languages::getInstance()->getLanguagesActive();
$defaultLanguageKey = Languages::getInstance()->getDefaultLocale();
app()->setLocale($defaultLanguageKey);
$languagesRoutesArray = \VmdCms\CoreCms\Services\Routes::getInstance()->getLangRouteGroups();
$customRoutesArray = \VmdCms\CoreCms\Services\Routes::getInstance()->getCustomRouteGroups();

$langRoutes = function () use ($languagesRoutesArray) {
    Route::middleware([
        'web',
        DataShareMiddleware::class,
    ])->group(function () use ($languagesRoutesArray) {
        foreach ($languagesRoutesArray as $group)
        {
            call_user_func($group);
        }
    });
};

$customRoutes = function () use($customRoutesArray) {
    Route::middleware([
        DataShareMiddleware::class
    ])->group(function () use ($customRoutesArray) {
        foreach ($customRoutesArray as $group)
        {
            call_user_func($group);
        }
    });
};
if(is_countable($languages) && count($languages))
{
    foreach ($languages as $language) {
        if(!$language instanceof CoreLanguage) continue;
        if($language->key == $defaultLanguageKey) continue;
        Route::prefix($language->key)->middleware([
            LanguagesMiddleware::class
        ])->group(function () use ($langRoutes) {
            $langRoutes();
        });
    }
}

$langRoutes();
$customRoutes();
