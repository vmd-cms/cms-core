<?php

use Illuminate\Support\Facades\Route;
use VmdCms\CoreCms\CoreModules\Content\Entity\Languages;
use VmdCms\CoreCms\CoreModules\Content\Middleware\LanguagesMiddleware;
use VmdCms\CoreCms\CoreModules\Languages\Models\CoreLanguage;

$languages = Languages::getInstance()->getLanguagesActive();
$defaultLanguageKey = Languages::getInstance()->getDefaultLocale();
app()->setLocale($defaultLanguageKey);
$apiRoutesArray = \VmdCms\CoreCms\Services\Routes::getInstance()->getApiRouteGroups();

$apiRoutes = function () use ($apiRoutesArray) {
    Route::middleware([
        'api',
        \VmdCms\CoreCms\Middleware\ApiAuthMiddleware::class,
        \Illuminate\Session\Middleware\StartSession::class
    ])->prefix('api/v1')
        ->group(function () use ($apiRoutesArray) {
        foreach ($apiRoutesArray as $group)
        {
            call_user_func($group);
        }
    });
};

if(is_countable($languages) && count($languages))
{
    foreach ($languages as $language) {
        if(!$language instanceof CoreLanguage) continue;
        if($language->key == $defaultLanguageKey) continue;
        Route::prefix($language->key)->middleware([
            LanguagesMiddleware::class
        ])->group(function () use ($apiRoutes) {
            $apiRoutes();
        });
    }
}

$apiRoutes();
