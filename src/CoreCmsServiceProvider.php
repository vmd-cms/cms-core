<?php

namespace VmdCms\CoreCms;

use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;
use VmdCms\CoreCms\Console\Commands\CreateModuleCommand;
use VmdCms\CoreCms\Console\Commands\InstallCoreCommand;
use VmdCms\CoreCms\Console\Commands\InstallModuleCommand;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\CoreModules\Events\Services\Events\CoreEvent;
use VmdCms\CoreCms\CoreModules\Events\Services\Listeners\CoreEventListener;
use VmdCms\CoreCms\Initializers\AliasesRegister;
use VmdCms\CoreCms\Services\CoreRouter;
use VmdCms\CoreCms\Services\LinkBuilder;

class CoreCmsServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(
            \VmdCms\CoreCms\CoreModules\Content\ModuleServiceProvider::class
        );
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('Link',LinkBuilder::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/config/vmd_cms.php' => config_path('vmd_cms.php'),
            __DIR__ . '/config/auth.php' => config_path('auth.php')
        ],'config');

        $this->publishes([
            __DIR__ . '/routes/cms.php' => base_path('routes/cms.php'),
            __DIR__ . '/routes/api_cms.php' => base_path('routes/api_cms.php'),
        ],'routes');

        if(CoreRouter::getInstance()->isAdminPanel())
        {
            $this->registerCoreAdmin();
        }

        if ($this->app->runningInConsole()) {
            $this->commands([
                InstallCoreCommand::class,
                InstallModuleCommand::class,
                CreateModuleCommand::class,
            ]);
        }

        $this->loadViewsFrom(__DIR__ . '/resources/views', 'vmd_cms');
        $this->loadTranslationsFrom(__DIR__ . '/resources/lang', 'vmd_cms');

        $this->publishes([
            __DIR__ . '/../public/fonts/vendor' => public_path('fonts/vendor'),
            __DIR__ . '/../public/css' => public_path('css/vendor/vmd_cms'),
            __DIR__ . '/../public/js' => public_path('js/vendor/vmd_cms'),
            __DIR__ . '/../public/files' => public_path('files/vendor/vmd_cms'),
            __DIR__ . '/resources/views' => resource_path('views/vendor/vmd_cms'),
            __DIR__ . '/resources/lang' => resource_path('lang/vendor/vmd_cms'),
        ],'public');

        if(file_exists(base_path('routes/cms.php')))
        {
            $this->loadRoutesFrom(base_path('routes/cms.php'));
        }

        if(file_exists(base_path('routes/api_cms.php')))
        {
            $this->loadRoutesFrom(base_path('routes/api_cms.php'));
        }

        Event::listen(CoreEvent::class,CoreEventListener::class);
    }

    protected function registerCoreAdmin()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes/admin.php');
        (new AliasesRegister($this->app))->register();
    }
}

