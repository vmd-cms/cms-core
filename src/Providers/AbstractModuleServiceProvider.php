<?php

namespace VmdCms\CoreCms\Providers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\ServiceProvider;
use VmdCms\CoreCms\Contracts\Modules\ModuleInitializerInterface;
use VmdCms\CoreCms\Contracts\Modules\ModuleServiceProviderInterface;
use VmdCms\CoreCms\Exceptions\Initializers\ModuleInitializerNotFoundException;
use VmdCms\CoreCms\Services\CoreRouter;
use VmdCms\CoreCms\Services\Initializers\InitializerService;
use VmdCms\CoreCms\Traits\Reflection\Reflectable;

abstract class AbstractModuleServiceProvider extends ServiceProvider implements ModuleServiceProviderInterface
{
    use Reflectable;

    /**
     * @var ModuleInitializerInterface
     */
    protected $moduleInitializer;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        try {
            $this->moduleInitializer = $this->getModuleInitializer();
            $this->moduleInitializer->appendRoutes();
            $this->mergeConfigFrom(
                base_path($this->moduleInitializer->publicConfigPath()).'/vmd_cms.php', 'vmd_cms'
            );
        }
        catch (\Exception $e)
        {
            Log::error($e->getMessage());
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            $this->moduleDirPath().'/database/migrations' => base_path($this->moduleInitializer->publicMigrationsPath()),
            $this->moduleDirPath().'/config' => base_path($this->moduleInitializer->publicConfigPath()),
            $this->moduleDirPath().'/routes' => base_path('app/' . $this->moduleInitializer->publicRoutesPath()),
            $this->moduleDirPath(). '/resources/views' => base_path('app/' .$this->moduleInitializer->publicViewsPath()),
            $this->moduleDirPath(). '/resources/lang' => base_path('app/' .$this->moduleInitializer->publicLangPath()),
        ],'public');

        if(Request::segment(1) === CoreRouter::getInstance()->getUrlPrefix()) $this->registerCoreAdminRoutes();

        $this->loadViewsFrom(app_path($this->moduleInitializer->publicViewsPath()), 'content');
        $this->loadTranslationsFrom(app_path($this->moduleInitializer->publicLangPath()), $this->moduleInitializer::moduleSlug());
        $this->loadMigrationsFrom(app_path($this->moduleInitializer->publicMigrationsPath()));
    }

    protected function registerCoreAdminRoutes()
    {
        if (file_exists($this->moduleDirPath().'/routes/admin.php')) $this->loadRoutesByPath($this->moduleDirPath().'/routes/admin.php');
        if (file_exists(app_path($this->moduleInitializer->publicRoutesPath().'/admin.php'))) $this->loadRoutesByPath(app_path($this->moduleInitializer->publicRoutesPath().'/admin.php'));
    }

    /**
     * @param string $path
     */
    protected function loadRoutesByPath(string $path)
    {
        try {
            $this->loadRoutesFrom($path);
        }
        catch (\Exception $e){}
    }

    /**
     * @return ModuleInitializerInterface
     * @throws ModuleInitializerNotFoundException
     */
    public function getModuleInitializer(): ModuleInitializerInterface
    {
        return InitializerService::getInitializerByModuleNamespace($this->getCurrentNamespace());
    }

    /**
     * @return string
     */
    protected function moduleDirPath(): string
    {
        return $this->getCurrentDir();
    }

}
