<?php

namespace VmdCms\CoreCms\Collections;

use VmdCms\CoreCms\Contracts\Collections\FormTabsCollectionInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormTabInterface;
use VmdCms\CoreCms\Traits\Dashboard\HasCmsModel;

class FormTabsCollection extends CoreCollectionAbstract implements FormTabsCollectionInterface
{
    use HasCmsModel;

    /**
     * @inheritDoc
     */
    public function appendItem(FormTabInterface $formTab): void
    {
        $this->collection->add($formTab);
    }

    public function appendItems(array $formTabs): void
    {
        foreach ($formTabs as $formTab)
        {
            if($formTab instanceof FormTabInterface) $this->appendItem($formTab);
        }
    }

    /**
     * @inheritDoc
     */
    public function toArray()
    {
        $tabs = [];
        foreach ($this->collection as $item) {
            if (!$item instanceof FormTabInterface) continue;
            $tabs[] = [
                'key' => $item->getKey(),
                'title' => $item->getTitle(),
                'components' => $item->getComponentsCollection()->setCmsModel($this->cmsModel)->toArray()
            ];
        }
        return $tabs;
    }
}
