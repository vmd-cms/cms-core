<?php

namespace VmdCms\CoreCms\Collections;

use Illuminate\Support\Collection;
use VmdCms\CoreCms\Contracts\Collections\CollectionInterface;

abstract class CoreCollectionAbstract implements CollectionInterface
{
    /**
     * @var Collection
     */
    protected $collection;

    /**
     * CoreCollectionAbstract constructor.
     */
    public function __construct()
    {
        $this->collection = new Collection();
    }

    /**
     * @inheritDoc
     */
    public function getItems(): Collection
    {
        return $this->collection;
    }

    /**
     * @inheritDoc
     */
    public function getItemsArray(): array
    {
        $items = [];
        foreach ($this->collection as $item){
            $items[] = $item;
        }
        return $items;
    }

}
