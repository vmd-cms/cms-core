<?php

namespace VmdCms\CoreCms\Collections;

use VmdCms\CoreCms\Contracts\Collections\TableColumnsCollectionInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Components\TableColumnInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;

class TableColumnsCollection extends CoreCollectionAbstract implements TableColumnsCollectionInterface
{
    /**
     * @var ?CmsModelInterface
     */
    private $cmsModel;

    /**
     * @inheritDoc
     */
    public function appendItem(TableColumnInterface $component): void
    {
        $this->collection->add($component);
    }

    /**
     * @inheritDoc
     */
    public function prependItem(TableColumnInterface $component): void
    {
        $this->collection->prepend($component);
    }

    /**
     * @inheritDoc
     */
    public function getItem(string $field): ?TableColumnInterface
    {
        return $this->collection->where('field_name',$field)->first();
    }

    /**
     * @inheritDoc
     */
    public function toArray()
    {
        return $this->collection->map(function (TableColumnInterface $item){
            return $item->toArray();
        });
    }
}
