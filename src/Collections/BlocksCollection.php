<?php

namespace VmdCms\CoreCms\Collections;

use VmdCms\CoreCms\Contracts\Collections\BlocksCollectionInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Display\Blocks\BlockInterface;

class BlocksCollection extends CoreCollectionAbstract implements BlocksCollectionInterface
{
    /**
     * @inheritDoc
     */
    public function appendItem(BlockInterface $block): void
    {
        $this->collection->add($block);
    }

    /**
     * @inheritDoc
     */
    public function toArray()
    {
        $blocks = [];
        foreach ($this->collection as $item) {
            if (!$item instanceof BlockInterface) continue;
            $blocks[$item->getGroupKey()][] = $item->toArray();
        }
        return $blocks;
    }
}
