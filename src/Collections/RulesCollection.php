<?php

namespace VmdCms\CoreCms\Collections;

use VmdCms\CoreCms\Contracts\Dashboard\Validation\RuleInterface;
use VmdCms\CoreCms\Contracts\Collections\RulesCollectionInterface;

class RulesCollection extends CoreCollectionAbstract implements RulesCollectionInterface
{
    /**
     * @inheritDoc
     */
    public function toArray()
    {
        $arr = [];
        $this->collection->map(function (RuleInterface $item) use (&$arr){
            $arr = array_merge($arr,$item->toArray());
        });
        return $arr;
    }

    /**
     * @param RuleInterface $rule
     */
    public function append(RuleInterface $rule)
    {
        $this->collection->add($rule);
    }

    /**
     * @param string $key
     * @return bool
     */
    public function hasRule(string $key): bool
    {
        return !empty($this->collection->filter(function(RuleInterface $item) use($key) {
            return $item->getKey() == $key;
        })->first());
    }
}
