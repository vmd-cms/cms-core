<?php

namespace VmdCms\CoreCms\Collections;

use VmdCms\CoreCms\Contracts\Collections\FormButtonsCollectionInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Buttons\FormButtonInterface;

class FormButtonsCollection extends CoreCollectionAbstract implements FormButtonsCollectionInterface
{
    /**
     * @inheritDoc
     */
    public function appendItem(FormButtonInterface $button): void
    {
        $this->collection->add($button);
    }

    /**
     * @inheritDoc
     */
    public function toArray()
    {
        $buttons = [];
        foreach ($this->collection as $item) {
            if (!$item instanceof FormButtonInterface) continue;
            $buttons[$item->getGroupKey()][] = $item->toArray();
        }
        return $buttons;
    }
}
