<?php

namespace VmdCms\CoreCms\Collections;

use VmdCms\CoreCms\Contracts\Collections\AdminSectionCollectionInterface;
use VmdCms\CoreCms\Contracts\Sections\AdminSectionInterface;
use VmdCms\CoreCms\Exceptions\Sections\SectionNotFoundException;

class AdminSectionCollection extends CoreCollectionAbstract implements AdminSectionCollectionInterface
{
    /**
     * @inheritDoc
     */
    public function appendItem(AdminSectionInterface $adminSection): void
    {
        $this->collection->put($adminSection->getSectionSlug(), $adminSection);
    }

    /**
     * @inheritDoc
     */
    public function getByKey($key): AdminSectionInterface
    {
        if(!$item = $this->collection->get($key)) throw new SectionNotFoundException();
        return $item;
    }
}
