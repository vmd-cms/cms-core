<?php

namespace VmdCms\CoreCms\Collections;

use VmdCms\CoreCms\Contracts\Collections\CollectionPaginatedInterface;
use VmdCms\CoreCms\Exceptions\Api\Models\ApiParamsException;

abstract class CollectionPaginatedAbstract extends CoreCollectionAbstract implements CollectionPaginatedInterface
{
    /**
     * @var int
     */
    protected $page;

    /**
     * @var int
     */
    protected $totalPages;

    /**
     * @var int
     */
    protected $total;

    /**
     * @var bool
     */
    protected $hasMore;

    /**
     * CollectionPaginatedInterface constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->total = 0;
        $this->hasMore = false;
        $this->page = 1;
        $this->totalPages = 1;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @return bool
     */
    public function hasMore(): bool
    {
        return $this->hasMore;
    }

    /**
     * @param int $total
     * @return CollectionPaginatedInterface
     */
    public function setTotal(int $total): CollectionPaginatedInterface
    {
        $this->total = $total;
        return $this;
    }

    /**
     * @param bool $flag
     * @return CollectionPaginatedInterface
     */
    public function setHasMore(bool $flag): CollectionPaginatedInterface
    {
        $this->hasMore = $flag;
        return $this;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     * @return CollectionPaginatedInterface
     * @throws ApiParamsException
     */
    public function setPage(int $page): CollectionPaginatedInterface
    {
        if($page <= 0){
            throw new ApiParamsException('page value must be bigger than 0');
        }
        $this->page = $page;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalPages(): int
    {
        return $this->totalPages;
    }

    /**
     * @param int $total
     * @return CollectionPaginatedInterface
     * @throws ApiParamsException
     */
    public function setTotalPages(int $total): CollectionPaginatedInterface
    {
        if($total < 0){
            throw new ApiParamsException('Total pages value must be bigger or 0');
        }
        $this->totalPages = $total;
        return $this;
    }

}
