<?php

namespace VmdCms\CoreCms\Collections;

use VmdCms\CoreCms\Contracts\Collections\DependedComponentsCollectionInterface;
use VmdCms\CoreCms\Contracts\DTO\DependedComponentDtoInterface;

class DependedComponentCollection extends CoreCollectionAbstract implements DependedComponentsCollectionInterface
{
    /**
     * @var ?CmsModel
     */
    private $cmsModel;

    /**
     * @inheritDoc
     */
    public function appendItem(DependedComponentDtoInterface $component): void
    {
        $this->collection->add($component);
    }

    /**
     * @inheritDoc
     */
    public function toArray()
    {
        return $this->collection->map(function (DependedComponentDtoInterface $item){
            return $item->toArray();
        });
    }
}
