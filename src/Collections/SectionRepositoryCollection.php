<?php

namespace VmdCms\CoreCms\Collections;

use VmdCms\CoreCms\Contracts\Collections\SectionRepositoryCollectionInterface;
use VmdCms\CoreCms\Contracts\DTO\SectionRepositoryDtoInterface;

class SectionRepositoryCollection extends CoreCollectionAbstract implements SectionRepositoryCollectionInterface
{
    /**
     * @inheritDoc
     */
    public function appendItem(SectionRepositoryDtoInterface $sectionRepository): void
    {
        $this->collection->put($sectionRepository->getAdminSectionClass(), $sectionRepository);
    }

    /**
     * @inheritDoc
     */
    public function getByKey($key): SectionRepositoryDtoInterface
    {
        return $this->collection->get($key);
    }
}
