<?php

namespace VmdCms\CoreCms\Collections;

use VmdCms\CoreCms\Contracts\Collections\BreadCrumbsCollectionInterface;
use VmdCms\CoreCms\Contracts\DTO\BreadCrumbDtoInterface;
use VmdCms\CoreCms\DTO\Dashboard\BreadCrumbDto;

class BreadCrumbsCollection extends CoreCollectionAbstract implements BreadCrumbsCollectionInterface
{
    /**
     * @param BreadCrumbDtoInterface $item
     */
    public function appendItem(BreadCrumbDtoInterface $item): void
    {
        $this->collection->add($item);
    }

    /**
     * @param BreadCrumbDtoInterface $item
     */
    public function prependItem(BreadCrumbDtoInterface $item): void
    {
        $this->collection->prepend($item);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function toArray()
    {
        return $this->collection->map(function (BreadCrumbDto $item){
            return $item->toArray();
        });
    }
}
