<?php

namespace VmdCms\CoreCms\Collections;

use VmdCms\CoreCms\Contracts\Collections\ModeratorPermissionsCollectionInterface;
use VmdCms\CoreCms\Contracts\DTO\ModeratorPermissionDtoInterface;
use VmdCms\CoreCms\Exceptions\Moderators\ModeratorPermissionNotFound;

class ModeratorPermissionsCollection extends CoreCollectionAbstract implements ModeratorPermissionsCollectionInterface
{
    /**
     * @inheritDoc
     */
    public function appendItem(ModeratorPermissionDtoInterface $permissionDto): void
    {
        $this->collection->put($permissionDto->getCoreSectionId(), $permissionDto);
    }

    public function getByCoreSectionId($key): ModeratorPermissionDtoInterface
    {
        if(!$item = $this->collection->get($key)) throw new ModeratorPermissionNotFound();
        return $item;
    }
}
