<?php

namespace VmdCms\CoreCms\Collections;

use VmdCms\CoreCms\Contracts\Collections\FormComponentsCollectionInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormComponentInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Traits\Dashboard\HasCmsModel;
use VmdCms\CoreCms\Traits\Sections\AdminSectionSetFields;

class FormComponentsCollection extends CoreCollectionAbstract implements FormComponentsCollectionInterface
{
    use HasCmsModel,AdminSectionSetFields;

    /**
     * @inheritDoc
     */
    public function appendItem(FormComponentInterface $component): void
    {
        $this->collection->add($component);
    }

    /**
     * @inheritDoc
     */
    public function toArray()
    {
        return $this->collection->map(function (FormComponentInterface $item)
        {
            $cmsModel = $item->getCmsModel() instanceof CmsModelInterface ? $item->getCmsModel() : $this->cmsModel;
            return $this->setComponentValue($item,$cmsModel);
        });
    }
}
