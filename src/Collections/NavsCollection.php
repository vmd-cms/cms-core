<?php

namespace VmdCms\CoreCms\Collections;

use VmdCms\CoreCms\Contracts\Collections\NavsCollectionInterface;
use VmdCms\CoreCms\Contracts\DTO\NavDtoInterface;

class NavsCollection extends CoreCollectionAbstract implements NavsCollectionInterface
{
    /**
     * @inheritDoc
     */
    public function toArray()
    {
        $arr = [];
        $this->collection->map(function (NavDtoInterface $item) use (&$arr){
            $arr = array_merge($arr,$item->toArray());
        });
        return $arr;
    }

    /**
     * @param NavDtoInterface $nav
     */
    public function append(NavDtoInterface $nav)
    {
        $this->collection->add($nav);
    }
}
