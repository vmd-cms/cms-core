<?php

namespace VmdCms\CoreCms\Collections;

use VmdCms\CoreCms\Contracts\Collections\LinksCollectionInterface;
use VmdCms\CoreCms\Contracts\DTO\LinkDtoInterface;

class LinksCollection extends CoreCollectionAbstract implements LinksCollectionInterface
{
    /**
     * @inheritDoc
     */
    public function toArray()
    {
        $arr = [];
        $this->collection->map(function (LinkDtoInterface $item) use (&$arr){
            $arr = array_merge($arr,$item->toArray());
        });
        return $arr;
    }

    /**
     * @param LinkDtoInterface $link
     */
    public function append(LinkDtoInterface $link)
    {
        $this->collection->add($link);
    }
}
