<?php

namespace VmdCms\CoreCms\CoreModules\Languages\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Contracts\Models\StaticInstanceInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\ColumnEditable;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Models\CmsSection;
use VmdCms\CoreCms\Traits\Models\HasStaticInstance;

class CoreLanguage extends CmsSection implements StaticInstanceInterface
{
    use HasStaticInstance;

    /**
     * @var string
     */
    protected $slug = 'languages';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('translates');
    }

    public function display()
    {
        return Display::dataTable([
            Column::text('id','Id'),
            Column::text('key'),
            Column::text('symbol'),
            Column::text('title'),
            ColumnEditable::switch('active'),
            ColumnEditable::switch('default'),
        ])->setSearchable(true);
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return $this->edit(null);
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::switch('active'),
            FormComponent::switch('default'),
            FormComponent::input('key')->maxLength(5)->required()->unique(),
            FormComponent::input('symbol')->maxLength(32)->required()->unique(),
            FormComponent::image('icon'),
            FormComponent::input('title')->required(),
        ]);
    }

    public function getCmsModelClass(): string
    {
        return \VmdCms\CoreCms\CoreModules\Languages\Models\CoreLanguage::class;
    }
}
