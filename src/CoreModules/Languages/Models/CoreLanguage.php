<?php

namespace VmdCms\CoreCms\CoreModules\Languages\Models;

use VmdCms\CoreCms\CoreModules\CoreTranslates\Entity\CoreTranslatesEntity;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\HasImagePath;

class CoreLanguage extends CmsModel
{
    use HasImagePath;

    protected $fillable = ["key", "title", "icon", "default", "active"];

    public static function table(): string
    {
        return 'core_languages';
    }

    /**
     * @return string
     */
    public static function getPrimaryField(): string
    {
        return 'key';
    }

    /**
     * @return string|null
     */
    protected function getImageValue(): ?string
    {
        return $this->icon ?? null;
    }

    protected static function boot()
    {
        parent::boot();

        self::created(function ($model){
            (new CoreTranslatesEntity())->seedAllModulesCoreTranslates($model->key);
        });
    }
}
