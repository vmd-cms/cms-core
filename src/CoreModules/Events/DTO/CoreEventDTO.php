<?php

namespace VmdCms\CoreCms\CoreModules\Events\DTO;

use VmdCms\CoreCms\CoreModules\Events\Contracts\DTO\CoreEventDTOInterface;
use VmdCms\CoreCms\CoreModules\Events\Entity\Event;

class CoreEventDTO implements CoreEventDTOInterface
{
    /**
     * @var string
     */
    protected $slug;

    /**
     * @var array
     */
    protected $payload;

    /**
     * @var int|null
     */
    protected $moderatorId;

    /**
     * @var int|null
     */
    protected $userId;

    /**
     * @var string|null
     */
    protected $userSessionId;

    /**
     * @var mixed|null
     */
    protected $conditionValue;

    /**
     * @var int|null
     */
    protected $resourceId;

    /**
     * @var string|null
     */
    protected $resourceSlug;

    public function __construct(string $slug, array $payload = [])
    {
        $this->slug = $slug;
        $this->payload = $payload;
        $this->resourceId = $this->getPayloadValue(Event::PAYLOAD_RESOURCE_ID);
        $this->resourceSlug = $this->getPayloadValue(Event::PAYLOAD_RESOURCE_SLUG);
        $this->moderatorId = $this->getPayloadValue(Event::PAYLOAD_MODERATOR_ID);
        $this->userId = $this->getPayloadValue(Event::PAYLOAD_USER_ID);
        $this->userSessionId = $this->getPayloadValue(Event::PAYLOAD_USER_SESSION_ID);
        $this->conditionValue = $this->getPayloadValue(Event::PAYLOAD_CONDITION_VALUE);
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return array
     */
    public function getPayload(): array
    {
        return $this->payload;
    }

    /**
     * @param string $key
     * @return mixed|null
     */
    public function getPayloadValue(string $key)
    {
        $path = explode('.',$key);

        $value = $this->payload;
        for ($i = 0; $i < count($path);$i++){
            if(empty($value)) break;
            if(is_array($value)){
                $value = $value[$path[$i]] ?? null;
            }
            elseif (is_object($value)){
                $field = $path[$i];
                $value = $value->$field ?? null;
            }
            else $value = null;
        }
        return $value;
    }

    /**
     * @return int|null
     */
    public function getModeratorId(): ?int
    {
        return $this->moderatorId;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @return string|null
     */
    public function getUserSessionId(): ?string
    {
        return $this->userSessionId;
    }

    /**
     * @return mixed|null
     */
    public function getConditionValue()
    {
        return $this->conditionValue;
    }

    /**
     * @return int|null
     */
    public function getResourceId(): ?int
    {
        return $this->resourceId;
    }

    /**
     * @return string|null
     */
    public function getResourceSlug(): ?string
    {
        return $this->resourceSlug;
    }

}
