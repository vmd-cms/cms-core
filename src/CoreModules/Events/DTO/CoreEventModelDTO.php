<?php

namespace VmdCms\CoreCms\CoreModules\Events\DTO;

use Illuminate\Support\Str;
use function GuzzleHttp\Psr7\str;

class CoreEventModelDTO
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $slug;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var boolean
     */
    protected $private;

    public function __construct(string $slug,string $title = null){
        $this->slug = $slug;
        $this->title = empty($title) ? Str::ucfirst(str_replace('_',' ',$slug)) : $title;
        $this->private = false;
    }

    /**
     * @param int $id
     * @return CoreEventModelDTO
     */
    public function setId(int $id): CoreEventModelDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $slug
     * @return CoreEventModelDTO
     */
    public function setSlug(string $slug): CoreEventModelDTO
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @param string $title
     * @return CoreEventModelDTO
     */
    public function setTitle(string $title): CoreEventModelDTO
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @param string $type
     * @return CoreEventModelDTO
     */
    public function setType(string $type): CoreEventModelDTO
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param bool $private
     * @return CoreEventModelDTO
     */
    public function setPrivate(bool $private): CoreEventModelDTO
    {
        $this->private = $private;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return bool
     */
    public function isPrivate(): bool
    {
        return $this->private;
    }

}
