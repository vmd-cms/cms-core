<?php

namespace VmdCms\CoreCms\CoreModules\Events\Entity;

use VmdCms\CoreCms\CoreModules\Events\Collections\CoreEventModelDTOCollection;
use VmdCms\CoreCms\CoreModules\Events\DTO\CoreEventModelDTO;
use VmdCms\CoreCms\CoreModules\Events\Enums\CoreEventEnums;
use VmdCms\CoreCms\CoreModules\Events\Enums\CoreEventTypeEnums;
use VmdCms\CoreCms\CoreModules\Events\Models\CoreEvent;

class CoreEventsSetup
{
    /**
     * @var CoreEventsSetup
     */
    protected static $instance;

    protected $collection;

    protected function __construct(){
        $this->collection = new CoreEventModelDTOCollection();
    }

    /**
     * @return static
     */
    public static function getInstance():CoreEventsSetup
    {
        if(!self::$instance instanceof CoreEventsSetup){
            self::$instance = new static();
        }
        return self::$instance;
    }

    public function seed(){

        if(!count($this->collection->getItems())) return false;

        foreach ($this->collection->getItems() as $dto){
            if(!$dto instanceof CoreEventModelDTO) continue;
            CoreEvent::updateOrCreate([
                'slug' => $dto->getSlug()
            ],
            [
                'title' => $dto->getTitle(),
                'type' => $dto->getType(),
                'private' => $dto->isPrivate(),
            ]);
        }
    }

    /**
     * @param CoreEventModelDTO $eventDTO
     * @return CoreEventsSetup
     */
    public function appendEventDTO(CoreEventModelDTO $eventDTO): CoreEventsSetup{
       $this->collection->append($eventDTO);
       return $this;
    }

    public function seedCoreEvents(){
        $this->setCoreEvents();
        $this->seed();
    }

    protected function setCoreEvents() {
        $this->collection->appendArr([
            (new CoreEventModelDTO(CoreEventEnums::ADMIN_SIGN_IN))
                ->setType(CoreEventTypeEnums::SYSTEM),
            (new CoreEventModelDTO(CoreEventEnums::ADMIN_CREATE))
                ->setType(CoreEventTypeEnums::SYSTEM),
            (new CoreEventModelDTO(CoreEventEnums::ADMIN_VIEW))
                ->setType(CoreEventTypeEnums::SYSTEM),
            (new CoreEventModelDTO(CoreEventEnums::ADMIN_EDIT))
                ->setType(CoreEventTypeEnums::SYSTEM),
            (new CoreEventModelDTO(CoreEventEnums::ADMIN_DELETE))
                ->setType(CoreEventTypeEnums::SYSTEM),
        ]);
    }
}
