<?php

namespace VmdCms\CoreCms\CoreModules\Events\Entity;

use VmdCms\CoreCms\CoreModules\Events\DTO\CoreEventDTO;
use VmdCms\CoreCms\CoreModules\Events\Services\Events\CoreEvent;
use VmdCms\CoreCms\CoreModules\Moderators\Entity\AuthEntity;
use VmdCms\CoreCms\Services\Auth\UserSession;

class Event
{
    const PAYLOAD_MODERATOR_ID = 'moderator_id';
    const PAYLOAD_USER_ID = 'user_id';
    const PAYLOAD_USER_SESSION_ID = 'user_session_id';
    const PAYLOAD_CONDITION_VALUE = 'condition_value';
    const PAYLOAD_RESOURCE_ID = 'resource_id';
    const PAYLOAD_RESOURCE_SLUG = 'resource_slug';
    const PAYLOAD_CHANGES = 'changes';

    /**
     * @param string $slug
     * @param array $payload
     */
    public static function dispatch(string $slug,array $payload = []){
        if(!isset($payload[self::PAYLOAD_MODERATOR_ID])){
            $payload[self::PAYLOAD_MODERATOR_ID] = AuthEntity::getAuthModeratorId();
        }
        if(!isset($payload[self::PAYLOAD_USER_ID])){
            $payload[self::PAYLOAD_USER_ID] = AuthEntity::getAuthUserId();
        }
        if(!isset($payload[self::PAYLOAD_USER_SESSION_ID])){
            $payload[self::PAYLOAD_USER_SESSION_ID] = UserSession::getUserSessionId();
        }
        CoreEvent::dispatch(new CoreEventDTO($slug,$payload));
    }
}
