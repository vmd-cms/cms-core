<?php

namespace VmdCms\CoreCms\CoreModules\Events\Enums;

class CoreEventTypeEnums
{
    const NOTIFICATION = 'notification';
    const SYSTEM = 'system';
}
