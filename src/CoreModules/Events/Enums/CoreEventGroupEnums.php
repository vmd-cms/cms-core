<?php

namespace VmdCms\CoreCms\CoreModules\Events\Enums;

class CoreEventGroupEnums
{
    const ADMIN = 'admin';
    const USER = 'user';

    public static function groups(){
        return [
            self::ADMIN => self::ADMIN,
            self::USER => self::USER
        ];
    }
}
