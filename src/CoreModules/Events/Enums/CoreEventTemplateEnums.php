<?php

namespace VmdCms\CoreCms\CoreModules\Events\Enums;

class CoreEventTemplateEnums
{
    const TYPE_MAIL = 'mail';
    const TYPE_SMS = 'sms';

    public static function types(){
        return [
            self::TYPE_MAIL => self::TYPE_MAIL,
            self::TYPE_SMS => self::TYPE_SMS
        ];
    }
}
