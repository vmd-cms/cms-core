<?php

namespace VmdCms\CoreCms\CoreModules\Events\Enums;

class CoreEventDependencyEnums
{
    const CONDITION_ANY = 'any';
    const CONDITION_BOOL = 'bool';
    const CONDITION_MATCH = 'match';

    public static function conditions(){
        return [
           self::CONDITION_ANY => self::CONDITION_ANY,
           self::CONDITION_BOOL => self::CONDITION_BOOL,
           self::CONDITION_MATCH => self::CONDITION_MATCH,
        ];
    }
}
