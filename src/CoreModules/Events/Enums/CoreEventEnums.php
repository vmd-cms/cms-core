<?php

namespace VmdCms\CoreCms\CoreModules\Events\Enums;

class CoreEventEnums
{
    const ADMIN_SIGN_IN = 'admin_sign_in';
    const ADMIN_CREATE = 'admin.create';
    const ADMIN_VIEW = 'admin.view';
    const ADMIN_EDIT = 'admin.edit';
    const ADMIN_DELETE = 'admin.delete';
}
