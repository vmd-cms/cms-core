<?php

namespace VmdCms\CoreCms\CoreModules\Events\Services\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use VmdCms\CoreCms\CoreModules\Events\Contracts\DTO\CoreEventDTOInterface;
use VmdCms\CoreCms\CoreModules\Events\Services\Handlers\CoreEventHandler;

class CoreEventJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const QUEUE = 'core_event';

    /**
     * @var CoreEventDTOInterface
     */
    protected $coreEventDTO;

    /**
     * CoreEventJob constructor.
     * @param CoreEventDTOInterface $dto
     */
    public function __construct(CoreEventDTOInterface $dto)
    {
        $this->coreEventDTO = $dto;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        (new CoreEventHandler($this->coreEventDTO))->execute();
    }
}
