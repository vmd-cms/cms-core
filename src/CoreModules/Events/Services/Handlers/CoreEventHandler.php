<?php

namespace VmdCms\CoreCms\CoreModules\Events\Services\Handlers;

use VmdCms\CoreCms\CoreModules\Events\Contracts\DTO\CoreEventDTOInterface;
use VmdCms\CoreCms\CoreModules\Events\Contracts\Services\CoreEventHandlerInterface;
use VmdCms\CoreCms\CoreModules\Events\Enums\CoreEventDependencyEnums;
use VmdCms\CoreCms\CoreModules\Events\Enums\CoreEventTemplateEnums;
use VmdCms\CoreCms\CoreModules\Events\Enums\CoreEventTypeEnums;
use VmdCms\CoreCms\CoreModules\Events\Exceptions\CoreEventDependencyException;
use VmdCms\CoreCms\CoreModules\Events\Exceptions\CoreEventModelNotFoundException;
use VmdCms\CoreCms\CoreModules\Events\Models\CoreEvent;
use VmdCms\CoreCms\CoreModules\Events\Models\CoreEventDependency;
use VmdCms\CoreCms\CoreModules\Events\Models\CoreEventLog;
use VmdCms\CoreCms\Services\Config;
use VmdCms\CoreCms\Services\Logger;
use VmdCms\CoreCms\CoreModules\Notifications\Services\Mail\Handlers\MailServiceHandler;
use VmdCms\CoreCms\CoreModules\Notifications\Services\Mail\Jobs\MailServiceJob;

class CoreEventHandler implements CoreEventHandlerInterface
{
    /**
     * @var CoreEventDTOInterface
     */
    protected $coreEventDTO;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var CoreEventLog
     */
    protected $coreEventLog;

    /**
     * @var CoreEvent
     */
    protected $coreEventModel;

    /**
     * CoreEventHandler constructor.
     * @param CoreEventDTOInterface $dto
     */
    public function __construct(CoreEventDTOInterface $dto)
    {
        $this->coreEventDTO = $dto;
        $this->logger = new Logger('core_event_handler');
        $this->initializeCoreEventLog($this->coreEventDTO);
    }

    public function execute()
    {
        $this->logger->info('Start execute ' . $this->coreEventDTO->getSlug());
        try {
            $coreEventModel = $this->getCoreEventModel();
            $this->handleByCoreEventType($coreEventModel);
            $this->coreEventLog->save();
            $this->logger->info('Finish execute. CoreEvent #' . $coreEventModel->id
                . ' type:' . $coreEventModel->type . ' CoreEventLog # ' . $this->coreEventLog->id);
        }
        catch (\Exception $exception){
            $this->logger->error('Error execute CoreEventHandler');
            $this->logger->error($exception->getMessage());
            $this->coreEventLog->error = $exception->getMessage();
            $this->coreEventLog->save();
            $this->logger->info('Finish execute.');
        }
    }

    /**
     * @param CoreEventDTOInterface $dto
     */
    protected function initializeCoreEventLog(CoreEventDTOInterface $dto){
        $this->coreEventLog = new CoreEventLog();
        $this->coreEventLog->core_event_slug = $dto->getSlug();
        $this->coreEventLog->resource_id = $dto->getResourceId();
        $this->coreEventLog->resource_slug = $dto->getResourceSlug();
        $this->coreEventLog->moderator_id = $dto->getModeratorId();
        $this->coreEventLog->user_id = $dto->getUserId();
        $this->coreEventLog->user_session_id = $dto->getUserSessionId();
        $this->coreEventLog->payload = json_encode($dto->getPayload(),JSON_UNESCAPED_UNICODE);
    }

    /**
     * @return CoreEvent
     * @throws CoreEventModelNotFoundException
     */
    protected function getCoreEventModel(): CoreEvent
    {
        $model = CoreEvent::where('slug',$this->coreEventDTO->getSlug())
            ->with('activeDependencies')->first();
        if(!$model instanceof CoreEvent){
            throw new CoreEventModelNotFoundException();
        }
        $this->coreEventLog->core_event_id = $model->id;
        return $model;
    }

    /**
     * @param CoreEvent $coreEventModel
     * @throws CoreEventDependencyException
     */
    protected function handleByCoreEventType(CoreEvent $coreEventModel){
        switch ($coreEventModel->type){
            case CoreEventTypeEnums::NOTIFICATION: $this->handleDependencies($coreEventModel);
                break;
            default:;
        }
    }

    /**
     * @param CoreEvent $coreEventModel
     * @throws CoreEventDependencyException
     */
    protected function handleDependencies(CoreEvent $coreEventModel)
    {
        $dependencies = $coreEventModel->activeDependencies ?? null;
        if(!is_countable($dependencies) || !count($dependencies)){
            throw new CoreEventDependencyException('Not found', 404);
        }
        foreach ($dependencies as $dependency){
            try {
                $this->logger->info('Handle Dependency #'. $dependency->id);
                $this->handleDependency($dependency);
            }
            catch (\Exception $exception){
                $this->logger->error('Error Handle Dependency #'. $dependency->id);
                $this->logger->error($exception->getMessage());
                continue;
            }
        }
    }

    /**
     * @param CoreEventDependency $dependency
     * @throws CoreEventDependencyException
     * @throws CoreEventModelNotFoundException
     */
    protected function handleDependency(CoreEventDependency $dependency)
    {
        $this->checkDependencyCondition($dependency);

        switch ($dependency->type){
            case CoreEventTemplateEnums::TYPE_MAIL: $this->handleTypeMail($dependency);
            break;
            case CoreEventTemplateEnums::TYPE_SMS: $this->handleTypeSms($dependency);
            break;
            default: throw new CoreEventDependencyException('Wrong Type: ' . $dependency->type);
        }
    }

    /**
     * @param CoreEventDependency $dependency
     * @throws CoreEventDependencyException
     * @throws CoreEventModelNotFoundException
     */
    protected function handleTypeMail(CoreEventDependency $dependency)
    {
        if(Config::getInstance()->isUsedQueueJobs()){
            MailServiceJob::dispatch($dependency,$this->coreEventDTO);
        }
        else{
            (new MailServiceHandler($dependency,$this->coreEventDTO))->execute();
        }
    }

    /**
     * @param CoreEventDependency $dependency
     */
    protected function handleTypeSms(CoreEventDependency $dependency){
        $this->logger->info('SEND SMS HERE');
    }

    /**
     * @param CoreEventDependency $dependency
     * @throws CoreEventDependencyException
     */
    protected function checkDependencyCondition(CoreEventDependency $dependency){

        switch ($dependency->condition){
            case CoreEventDependencyEnums::CONDITION_BOOL: $this->checkPayloadBoolCondition();
            break;
            case CoreEventDependencyEnums::CONDITION_MATCH: $this->checkPayloadMatchCondition($dependency);
            break;
            case CoreEventDependencyEnums::CONDITION_ANY:;
            break;
            default: throw new CoreEventDependencyException(
                'Wrong Dependency #' . $dependency->id.  ' Condition type: ' . $dependency->condition);
        }
    }

    /**
     * @throws CoreEventDependencyException
     */
    protected function checkPayloadBoolCondition(){
        if(!$this->coreEventDTO->getPayloadValue('condition_value')){
            throw new CoreEventDependencyException('Wrong payload value. Condition Bool');
        }
    }

    /**
     * @param CoreEventDependency $dependency
     * @throws CoreEventDependencyException
     */
    protected function checkPayloadMatchCondition(CoreEventDependency $dependency)
    {
        if($this->coreEventDTO->getPayloadValue('condition_value') !== $dependency->condition_value){
            throw new CoreEventDependencyException(
                'Match condition not equals. Event condition_value:' .
                $this->coreEventDTO->getPayloadValue('condition_value') .
                ' !== dependency #' . $dependency->id . ' condition_value:' . $dependency->condition_value);
        }
    }

}
