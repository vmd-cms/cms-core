<?php

namespace VmdCms\CoreCms\CoreModules\Events\Services\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use VmdCms\CoreCms\CoreModules\Events\Contracts\DTO\CoreEventDTOInterface;

class CoreEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $coreEventDTO;
    public $payload;

    /**
     * CoreEvent constructor.
     * @param CoreEventDTOInterface $coreEventDTO
     */
    public function __construct(CoreEventDTOInterface $coreEventDTO)
    {
        $this->coreEventDTO = $coreEventDTO;
    }

}
