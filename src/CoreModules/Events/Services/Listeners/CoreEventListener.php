<?php

namespace VmdCms\CoreCms\CoreModules\Events\Services\Listeners;

use VmdCms\CoreCms\CoreModules\Events\Services\Events\CoreEvent;
use VmdCms\CoreCms\CoreModules\Events\Services\Handlers\CoreEventHandler;
use VmdCms\CoreCms\CoreModules\Events\Services\Jobs\CoreEventJob;
use VmdCms\CoreCms\Services\Config;

class CoreEventListener
{
    protected $useQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->useQueue = Config::getInstance()->isUsedQueueJobs();
    }

    /**
     * Handle the event.
     *
     * @param  CoreEvent $coreEvent
     * @return void
     */
    public function handle(CoreEvent $coreEvent)
    {
        if($this->useQueue){
            CoreEventJob::dispatch($coreEvent->coreEventDTO);
        }
        else{
            (new CoreEventHandler($coreEvent->coreEventDTO))->execute();
        }
    }
}
