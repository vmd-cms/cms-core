<?php

namespace VmdCms\CoreCms\CoreModules\Events\Exceptions;

use VmdCms\CoreCms\Exceptions\CoreException;

class CoreEventException extends CoreException
{

}
