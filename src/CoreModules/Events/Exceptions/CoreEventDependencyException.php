<?php

namespace VmdCms\CoreCms\CoreModules\Events\Exceptions;

use Throwable;

class CoreEventDependencyException extends CoreEventException
{
    public function __construct($messageOrException = '', $code = 409, Throwable $previous = null)
    {
        parent::__construct('CoreEventDependency Error. ' . $messageOrException, $code, $previous);
    }
}
