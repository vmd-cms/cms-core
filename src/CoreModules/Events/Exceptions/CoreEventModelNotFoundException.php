<?php

namespace VmdCms\CoreCms\CoreModules\Events\Exceptions;

use Throwable;

class CoreEventModelNotFoundException extends CoreEventException
{
    public function __construct($messageOrException = 'CoreEventModel Not Found', $code = 404, Throwable $previous = null)
    {
        parent::__construct($messageOrException, $code, $previous);
    }
}
