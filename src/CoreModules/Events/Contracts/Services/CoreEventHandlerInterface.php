<?php

namespace VmdCms\CoreCms\CoreModules\Events\Contracts\Services;

use VmdCms\CoreCms\CoreModules\Events\Contracts\DTO\CoreEventDTOInterface;

interface CoreEventHandlerInterface
{
    /**
     * CoreEventHandlerInterface constructor.
     * @param CoreEventDTOInterface $dto
     */
    public function __construct(CoreEventDTOInterface $dto);

    /**
     * @return void
     */
    public function execute();
}
