<?php

namespace VmdCms\CoreCms\CoreModules\Events\Contracts\DTO;

interface CoreEventDTOInterface
{
    /**
     * CoreEventDTOInterface constructor.
     * @param string $slug
     * @param array $payload
     */
    public function __construct(string $slug, array $payload = []);

    /**
     * @return string
     */
    public function getSlug(): string;

    /**
     * @return string|null
     */
    public function getResourceSlug(): ?string;

    /**
     * @return int|null
     */
    public function getResourceId(): ?int;

    /**
     * @return array
     */
    public function getPayload(): array;

    /**
     * @param string $key
     * @return mixed|null
     */
    public function getPayloadValue(string $key);

    /**
     * @return int|null
     */
    public function getModeratorId(): ?int;

    /**
     * @return int|null
     */
    public function getUserId(): ?int;

    /**
     * @return string|null
     */
    public function getUserSessionId(): ?string;

    /**
     * @return mixed|null
     */
    public function getConditionValue();

}
