<?php

namespace VmdCms\CoreCms\CoreModules\Events\Collections;

use VmdCms\CoreCms\Collections\CoreCollectionAbstract;
use VmdCms\CoreCms\CoreModules\Events\DTO\CoreEventModelDTO;

class CoreEventModelDTOCollection extends CoreCollectionAbstract
{
    public function append(CoreEventModelDTO $dto){
        $this->collection->put($dto->getSlug(),$dto);
    }

    public function appendArr(array $list){
        foreach ($list as $dto){
            if(!$dto instanceof CoreEventModelDTO) continue;
            $this->append($dto);
        }
    }
}
