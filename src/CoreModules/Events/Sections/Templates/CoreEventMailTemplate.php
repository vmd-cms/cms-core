<?php

namespace VmdCms\CoreCms\CoreModules\Events\Sections\Templates;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\ColumnEditable;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Models\CmsSection;

class CoreEventMailTemplate extends CmsSection
{
    /**
     * @var string
     */
    protected $slug = 'core_event_mail_templates';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('mail_templates');
    }

    public function display()
    {
        return Display::dataTable([
            Column::text('id','#')->searchable()->sortable(),
            Column::text('title',CoreLang::get('title_template'))->searchable()->sortable(),
            Column::text('subject',CoreLang::get('subject_mail'))->searchable()->sortable(),
            ColumnEditable::switch('active')
                ->sortable()->setWidth('200'),
        ])->setSearchable(true);
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return $this->edit(null);
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::switch('active'),
            FormComponent::input('title',CoreLang::get('title_template')),
            FormComponent::input('subject',CoreLang::get('subject_mail')),
            FormComponent::ckeditor('body','HTML'),
        ]);
    }

    public function getCmsModelClass(): string
    {
        return \VmdCms\CoreCms\CoreModules\Events\Models\Templates\CoreEventMailTemplate::class;
    }
}
