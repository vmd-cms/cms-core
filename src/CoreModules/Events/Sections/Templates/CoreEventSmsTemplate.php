<?php

namespace VmdCms\CoreCms\CoreModules\Events\Sections\Templates;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Models\CmsSection;

class CoreEventSmsTemplate extends CmsSection
{
    /**
     * @var string
     */
    protected $slug = 'core_event_sms_templates';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('sms_templates');
    }

    public function display()
    {
        return Display::dataTable([
            Column::text('id','#'),
            Column::text('title',CoreLang::get('title_template')),
            Column::text('active'),
        ])->setSearchable(true);
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return $this->edit(null);
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::switch('active'),
            FormComponent::input('title',CoreLang::get('title_template')),
            FormComponent::text('body',CoreLang::get('text')),
        ]);
    }

    public function getCmsModelClass(): string
    {
        return \VmdCms\CoreCms\CoreModules\Events\Models\Templates\CoreEventSmsTemplate::class;
    }
}
