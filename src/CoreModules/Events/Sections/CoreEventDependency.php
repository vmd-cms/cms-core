<?php

namespace VmdCms\CoreCms\CoreModules\Events\Sections;

use VmdCms\CoreCms\Collections\CoreCollectionAbstract;
use VmdCms\CoreCms\Collections\DependedComponentCollection;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\CoreModules\Events\Enums\CoreEventDependencyEnums;
use VmdCms\CoreCms\CoreModules\Events\Enums\CoreEventGroupEnums;
use VmdCms\CoreCms\CoreModules\Events\Enums\CoreEventTemplateEnums;
use VmdCms\CoreCms\CoreModules\Events\Enums\CoreEventTypeEnums;
use VmdCms\CoreCms\CoreModules\Events\Models\CoreEvent;
use VmdCms\CoreCms\CoreModules\Events\Models\Templates\CoreEventMailTemplate;
use VmdCms\CoreCms\CoreModules\Events\Models\Templates\CoreEventSmsTemplate;
use VmdCms\CoreCms\Dashboard\Forms\Components\DependedComponent;
use VmdCms\CoreCms\DTO\Dashboard\DependComponentDto;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\ColumnEditable;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Models\CmsSection;

class CoreEventDependency extends CmsSection
{
    /**
     * @var string
     */
    protected $slug = 'core_event_dependency';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('events');
    }

    public function display()
    {
        return Display::dataTable([
            Column::text('id','#')->searchable()->sortable(),
            Column::text('coreEvent.title', CoreLang::get('title'))
                ->setSearchableCallback(function ($query,$search){
                    $query->orWhereHas('coreEvent',function ($q) use ($search){
                        $q->where('title','like','%'.$search.'%');
                    });
                })
                ->setSortableCallback(function ($query, $sortType) {
                    $query->join('core_events', function ($join) {
                        $join->on('core_event_dependencies.core_event_id', '=', 'core_events.id');
                    });
                    $query->orderBy('core_events.title', $sortType)
                        ->selectRaw('core_event_dependencies.*');
                }),
            Column::text('coreEventTemplate.title',CoreLang::get('template'))
                ->setSearchableCallback(function ($query,$search){
                    $query->orWhereHas('coreEventTemplate',function ($q) use ($search){
                        $q->where('title','like','%'.$search.'%');
                    });
                })
                ->setSortableCallback(function ($query, $sortType) {
                    $query->join('core_event_templates', function ($join) {
                        $join->on('core_event_templates.id', '=', 'core_event_dependencies.core_event_template_id');
                    });
                    $query->orderBy('core_event_templates.title', $sortType)
                        ->selectRaw('core_event_dependencies.*');
                }),
            Column::text('type')->searchable()->sortable(),
            ColumnEditable::switch('active')
                ->sortable()->setWidth('200')->sortable(),
        ])->setSearchable(true);
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return $this->edit(null);
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {

        $templateDependedCollection = new DependedComponentCollection();
        $mailTemplates = new DependComponentDto(
            CoreEventTemplateEnums::TYPE_MAIL,
            FormComponent::select('',CoreLang::get('template'))
                ->setModelForOptions(CoreEventMailTemplate::class)
                ->setDisplayField('title')
                ->required()
        );
        $smsTemplates = new DependComponentDto(
            CoreEventTemplateEnums::TYPE_SMS,
            FormComponent::select('',CoreLang::get('template'))
                ->setModelForOptions(CoreEventSmsTemplate::class)
                ->setDisplayField('title')
                ->required()
        );
        $templateDependedCollection->appendItem($mailTemplates);
        $templateDependedCollection->appendItem($smsTemplates);
        $templateDependedComponent = new DependedComponent('core_event_template_id',$templateDependedCollection);

        $conditionDependedCollection = new DependedComponentCollection();
        $match = new DependComponentDto(
            CoreEventDependencyEnums::CONDITION_MATCH,
            FormComponent::input('','MATCH')->required()
        );
        $conditionDependedCollection->appendItem($match);
        $conditionDependedComponent = new DependedComponent('condition_value',$conditionDependedCollection);

        return Form::panel([
            FormComponent::switch('active'),
            FormComponent::select('core_event_id',CoreLang::get('event'))
                ->setModelForOptions(CoreEvent::class)
                ->setWhere(['type','=',CoreEventTypeEnums::NOTIFICATION])
                ->setDisplayField('title'),
            FormComponent::select('group')
                ->setEnumValues(CoreEventGroupEnums::groups()),
            FormComponent::dependedSelect('type')
                ->setEnumValues(CoreEventTemplateEnums::types())
                ->setDependedComponent($templateDependedComponent),
            FormComponent::dependedSelect('condition')
                ->setEnumValues(CoreEventDependencyEnums::conditions())
                ->setDependedComponent($conditionDependedComponent)
        ]);
    }

    public function getCmsModelClass(): string
    {
        return \VmdCms\CoreCms\CoreModules\Events\Models\CoreEventDependency::class;
    }
}
