<?php

namespace VmdCms\CoreCms\CoreModules\Events\Models\Templates;

use VmdCms\CoreCms\CoreModules\Events\Enums\CoreEventTemplateEnums;

class CoreEventMailTemplate extends CoreEventTemplate
{
    public static function getType(){
        return CoreEventTemplateEnums::TYPE_MAIL;
    }
}
