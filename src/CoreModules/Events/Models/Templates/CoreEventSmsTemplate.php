<?php

namespace VmdCms\CoreCms\CoreModules\Events\Models\Templates;

use VmdCms\CoreCms\CoreModules\Events\Enums\CoreEventTemplateEnums;

class CoreEventSmsTemplate extends CoreEventTemplate
{
    public static function getType(){
        return CoreEventTemplateEnums::TYPE_SMS;
    }
}
