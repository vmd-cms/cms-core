<?php

namespace VmdCms\CoreCms\CoreModules\Events\Models\Templates;

use Illuminate\Database\Eloquent\Builder;
use VmdCms\CoreCms\Models\CmsModel;

class CoreEventTemplate extends CmsModel
{
    public static function table(): string
    {
        return 'core_event_templates';
    }

    public static function getType(){
        return null;
    }

    protected static function boot()
    {
        parent::boot();

        if(static::getType()){
            static::addGlobalScope('type', function (Builder $builder) {
                $builder->where('type', static::getType());
            });
        }

        static::saving(function (CoreEventTemplate $model){
            $model->type = static::getType();
        });
    }
}
