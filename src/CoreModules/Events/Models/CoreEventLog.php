<?php

namespace VmdCms\CoreCms\CoreModules\Events\Models;

use VmdCms\CoreCms\Models\CmsModel;

class CoreEventLog extends CmsModel
{
    public static function table(): string
    {
        return 'core_event_logs';
    }
}
