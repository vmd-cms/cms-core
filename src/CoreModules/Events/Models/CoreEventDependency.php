<?php

namespace VmdCms\CoreCms\CoreModules\Events\Models;

use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\CoreModules\Events\Models\Templates\CoreEventTemplate;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Activable;

class CoreEventDependency extends CmsModel implements ActivableInterface
{
    use Activable;

    public static function table(): string
    {
        return 'core_event_dependencies';
    }

    public function coreEvent(){
        return $this->belongsTo(CoreEvent::class,'core_event_id','id');
    }

    public function coreEventTemplate(){
        return $this->belongsTo(CoreEventTemplate::class,'core_event_template_id','id');
    }
}
