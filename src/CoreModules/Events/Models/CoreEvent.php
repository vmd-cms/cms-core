<?php

namespace VmdCms\CoreCms\CoreModules\Events\Models;

use VmdCms\CoreCms\Models\CmsModel;

class CoreEvent extends CmsModel
{
    protected $fillable = ['slug', 'title', 'type', 'group', 'private'];

    public static function table(): string
    {
        return 'core_events';
    }

    public function activeDependencies(){
        return $this->hasMany(CoreEventDependency::class,'core_event_id','id')->with(['coreEventTemplate','coreEvent'])->active();
    }
}
