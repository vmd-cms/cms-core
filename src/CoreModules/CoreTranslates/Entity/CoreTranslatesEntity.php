<?php

namespace VmdCms\CoreCms\CoreModules\CoreTranslates\Entity;

use Illuminate\Support\Facades\Lang;
use VmdCms\CoreCms\CoreModules\Content\Entity\Languages;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Models\CoreTranslate;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Models\CoreTranslateInfo;
use VmdCms\CoreCms\CoreModules\Modules\Models\CoreModule;
use VmdCms\CoreCms\Exceptions\Files\FileNotExistException;
use VmdCms\CoreCms\Exceptions\Models\ModelNotFoundException;
use VmdCms\CoreCms\Models\CmsInfoModel;

class CoreTranslatesEntity
{
    public function seedAllModulesCoreTranslates(string $langKey)
    {
        $this->seedCoreTranslates($langKey);

        $modules = CoreModule::where('active',1)->get();
        if (is_countable($modules)){
            foreach ($modules as $module){
                $this->seedCoreTranslates($langKey,$module->slug);
            }
        }
    }

    public function seedCoreTranslates(string $langKey,string $namespace = "vmd_cms")
    {
        try {

            $languages = Languages::getInstance(true);
            if(!$languages->hasLanguage($langKey)){
                throw new ModelNotFoundException();
            }

            $currentLang = $languages->getCurrentLocale();
            $languages->setLocale($langKey);

            $coreTranslates = Lang::get($namespace .'::core');
            if(!is_array($coreTranslates)){
                throw new FileNotExistException();
            }

            foreach ($coreTranslates as $key=>$translate)
            {
                if(!$model = CoreTranslate::where('key',$key)->first()){
                    $model = new CoreTranslate([
                        'key' => $key,
                        'description' => $translate,
                        'default' => $translate,
                    ]);
                    $model->save();
                }

                if(!CoreTranslateInfo::where('core_translates_id',$model->id)->where('lang_key',$langKey)->first())
                {
                    $modelInfo = new CoreTranslateInfo();
                    $modelInfo->core_translates_id = $model->id;
                    $modelInfo->lang_key = $langKey;
                    $modelInfo->value = $translate;
                    $modelInfo->save();
                }

            }

            $languages->setLocale($currentLang);
        }
        catch (\Exception $e)
        {
        }

    }
}
