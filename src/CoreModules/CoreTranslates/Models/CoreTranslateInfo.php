<?php

namespace VmdCms\CoreCms\CoreModules\CoreTranslates\Models;

use VmdCms\CoreCms\Models\CmsInfoModel;

class CoreTranslateInfo extends CmsInfoModel
{
    protected $fillable = ['lang_key', 'core_translates_id', 'value'];

    public static function table(): string
    {
        return 'core_translates_info';
    }
}
