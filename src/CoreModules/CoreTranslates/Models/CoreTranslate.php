<?php

namespace VmdCms\CoreCms\CoreModules\CoreTranslates\Models;

use Illuminate\Database\Eloquent\Builder;
use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Activable;
use VmdCms\CoreCms\Traits\Models\HasInfo;

class CoreTranslate extends CmsModel implements HasInfoInterface, ActivableInterface
{
    use HasInfo,Activable;

    protected $fillable = ['key','description','default','active'];

    public static function table(): string
    {
        return 'core_translates';
    }

    protected function getInfoClass() : string
    {
        return self::class . 'Info';
    }


}
