<?php

namespace VmdCms\CoreCms\CoreModules\CoreTranslates\Services;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Lang;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Models\CoreTranslate;
use VmdCms\CoreCms\Exceptions\CoreException;
use VmdCms\CoreCms\Exceptions\Models\ModelNotFoundException;

final class CoreLang
{
    /**
     * @var array
     */
    protected static $translates;

    /**
     * @param string $key
     * @return string
     */
    public static function get(string $key, string $namespace = "vmd_cms"): string
    {
        try {
            self::setTranslates();

            $value = self::$translates[$key] ?? null;
        }catch (\Exception $exception){
            $value = null;
        }

        if(empty($value)){
            $value = Lang::get($namespace . '::core.' . $key);
        }

        return $value;
    }

    public static function getTranslates(): array
    {
        self::setTranslates();
        return self::$translates;
    }

    /**
     * @param bool $force
     */
    public static function setTranslates(bool $force = false)
    {
        if(!is_array(self::$translates) || $force){
            self::$translates = [];
            $coreTranslates = CoreTranslate::with('info')->get();
            if(is_countable($coreTranslates)){
                foreach ($coreTranslates as $translate){
                    self::$translates[$translate->key] = $translate->info->value ?? $translate->default ?? null;
                }
            }
        }
    }
}
