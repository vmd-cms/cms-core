<?php

namespace VmdCms\CoreCms\CoreModules\Content\Contracts;

use VmdCms\CoreCms\CoreModules\Content\Models\Blocks\Block;

interface BlockDtoInterface
{
    public function __construct(Block $block);

    /**
     * @return string
     */
    public function getBlockKey(): string;
}
