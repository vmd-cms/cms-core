<?php

namespace VmdCms\CoreCms\CoreModules\Content\Contracts;

use VmdCms\CoreCms\Contracts\Collections\CollectionInterface;

interface BlocksCollectionInterface extends CollectionInterface
{
    /**
     * @param BlockDtoInterface $blockDto
     */
    public function append(BlockDtoInterface $blockDto);

    /**
     * @param string $key
     * @return BlockDtoInterface|null
     */
    public function getItem(string $key): ?BlockDtoInterface;
}
