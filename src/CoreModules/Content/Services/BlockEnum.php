<?php

namespace VmdCms\CoreCms\CoreModules\Content\Services;

use VmdCms\CoreCms\Services\Enums;

class BlockEnum extends Enums
{
    const SLIDER_BLOCK = 'slider_block';
}
