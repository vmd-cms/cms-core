<?php

namespace VmdCms\CoreCms\CoreModules\Content\Services;

use App\Modules\Catalogs\Entity\CatalogNavs;
use App\Modules\Content\DTO\HeaderDTO;
use App\Modules\Content\Entity\Languages;
use App\Modules\Content\Entity\Translates;
use App\Modules\Prices\Entity\Currencies;
use VmdCms\CoreCms\CoreModules\Content\Entity\BlockEntity;
use VmdCms\CoreCms\CoreModules\Content\Collections\BlocksCollection;
use VmdCms\CoreCms\CoreModules\Content\Contracts\BlockDtoInterface;
use VmdCms\CoreCms\CoreModules\Content\Contracts\BlocksCollectionInterface;
use VmdCms\CoreCms\CoreModules\Seo\DTO\SeoDTO;

class DataShare
{
    /**
     * @var DataShare
     */
    protected static $instance;

    /**
     * @var BlocksCollectionInterface
     */
    public $blocks;

    /**
     * @var HeaderDTO
     */
    public $header;

    /**
     * @var Languages
     */
    public $languages;

    /**
     * @var Translates
     */
    public $translates;

    /**
     * @var Currencies
     */
    public $currencies;

    /**
     * @var CatalogNavs
     */
    public $catalogs;

    /**
     * @var SeoDTO
     */
    public $seo;

    /**
     * @var BlockEntity
     */
    protected $blockEntity;

    /**
     * DataShare constructor.
     * @param bool $relations
     */
    protected function __construct(bool $relations = true)
    {
        $this->translates = Translates::getInstance();
        $this->blocks = new BlocksCollection();
        $this->blockEntity = new BlockEntity();
        if(!$relations) return;
        $this->header = new HeaderDTO();
        $this->seo = new SeoDTO();
        $this->languages = Languages::getInstance();


        if(class_exists(Currencies::class)) $this->currencies = \App\Modules\Prices\Entity\Currencies::getInstance();
        if(class_exists(CatalogNavs::class)) $this->catalogs = CatalogNavs::getInstance();
    }

    public static function getInstance(bool $relations = true) : DataShare
    {
        if(!static::$instance instanceof DataShare)
        {
            static::$instance = new static($relations);
        }
        return static::$instance;
    }

    public function appendData($key, $value)
    {
        $this->$key = $value;
    }

    /**
     * @param string $key
     * @return string|null
     */
    public function getTranslate(string $key) : ?string
    {
        return $this->translates->getValue($key);
    }

    /**
     * @param string $key
     * @return BlockDtoInterface|null
     */
    public function getBlock(string $key): ?BlockDtoInterface
    {
        return $this->blocks->getItem($key);
    }

    /**
     * @param string $key
     * @param string $field
     * @return mixed|null
     */
    public function getBlockValue(string $key, string $field)
    {
        return $this->blocks->getItem($key)->$field ?? null;
    }

    public function appendBlockByClass(string $class){
        try {
            $block = $this->blockEntity->getBlockDTO($class);
            $this->blocks->append($block);
        }catch (\Exception $exception){}
    }
}
