<?php

namespace VmdCms\CoreCms\CoreModules\Content\Services;

class ContentReplacer
{
    public static function replacePatches(string $content = null): ?string
    {
        return !empty($content) ? str_replace('src="/storage/','src="' . config('app.url') . '/storage/',$content) : null;
    }

    public static function availableVarsStr(array $values): string
    {
        $result = 'Доступные переменные:</br>';
        foreach ($values as $key=>$text){
            $result .= $key . ' - ' . $text . '</br>';
        }
        return $result;
    }
}
