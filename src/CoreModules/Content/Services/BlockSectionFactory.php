<?php

namespace VmdCms\CoreCms\CoreModules\Content\Services;

use VmdCms\CoreCms\CoreModules\Content\Models\Blocks\Custom\SliderBlock;
use VmdCms\CoreCms\Services\BlockSectionFactoryAbstract;

class BlockSectionFactory extends BlockSectionFactoryAbstract
{
    protected static function getAssocKeySectionClasses(): array
    {
        return [
            SliderBlock::getModelKey() => \App\Modules\Content\Sections\Blocks\Custom\SliderBlock::class,
        ];
    }
}
