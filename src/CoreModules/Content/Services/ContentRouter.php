<?php

namespace VmdCms\CoreCms\CoreModules\Content\Services;

class ContentRouter
{
    const ROUTE_MAIN_PAGE = 'main-page';
    const ROUTE_INFO_PAGE = 'info-page';
    const ROUTE_BLOG_PAGE = 'blog-pages';
    const ROUTE_BLOGS_PAGE = 'blogs-pages';
    const ROUTE_BLOGS_LOAD_MORE = 'blog.load-more';

}
