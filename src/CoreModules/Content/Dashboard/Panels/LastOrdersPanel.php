<?php

namespace VmdCms\CoreCms\CoreModules\Content\Dashboard\Panels;

use VmdCms\CoreCms\Contracts\Dashboard\Display\Panels\SectionPanelInterface;
use VmdCms\CoreCms\Dashboard\Display\Panels\AbstractSectionPanel;
use VmdCms\CoreCms\Exceptions\CoreException;
use VmdCms\CoreCms\Exceptions\Modules\ModuleNotActiveException;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\Modules\Orders\Sections\Order;

class LastOrdersPanel extends AbstractSectionPanel
{
    public function __construct(array $data = [])
    {
        if (!class_exists(Order::class)) {
            throw new ModuleNotActiveException();
        }
        parent::__construct($data);
    }

    public function getCustomSectionPanel(): SectionPanelInterface
    {
        $orders = Display::dataTable([
            Column::text('id', '№ Заказа'),
            Column::text('customer_name', 'Покупатель'),
            Column::text('order_sum', 'Сумма заказа'),
            Column::chipText('status.info.title', 'Статус')
                ->setColorCallback(function ($model) {
                    return $model->status->color ?? null;
                }),
            Column::text('paymentType.info.title', 'Способ оплаты'),
            Column::text('deliveryType.info.title', 'Способ Доставки'),
            Column::date('created_at', 'Created at')->setFormat('d.m.Y H:i:s'),
        ])->orderDefault(function ($query) {
            $query->orderBy('created_at', 'desc');
        })->setItemsPerPage(10)
            ->setSearchable(false)
            ->setHideFooter(true)
            ->setHeaderHTML('<span>Последние 10 заказов</span>');

        $orders->setSection((new Order())->setCreatable(false));
        return $orders->getSectionPanel();
    }

    public function render()
    {
        $panel = $this->getCustomSectionPanel()->render();
        return view('vmd_cms::admin.views.sections.display.panels.col_12_panel', ['panel' => $panel])->render();
    }

    public function setAdditionalPanels(array $additionalPanels): SectionPanelInterface
    {
        return $this;
    }
}
