<?php

namespace VmdCms\CoreCms\CoreModules\Content\Dashboard\Panels;

use VmdCms\CoreCms\Contracts\Dashboard\Display\Panels\SectionPanelInterface;
use VmdCms\CoreCms\CoreModules\Content\DTO\Statistics\StatisticInfoTableDTO;
use VmdCms\CoreCms\CoreModules\Content\Entity\StatisticDashboardEntity;
use VmdCms\CoreCms\Dashboard\Display\Panels\AbstractSectionPanel;
use VmdCms\CoreCms\Exceptions\CoreException;
use VmdCms\CoreCms\Exceptions\Modules\ModuleNotActiveException;
use VmdCms\Modules\Orders\Sections\Order;

class StatisticInfoPanel extends AbstractSectionPanel
{
    public function __construct(array $data = [])
    {
        if(!class_exists(\App\Modules\Orders\Models\Order::class)){
            throw new ModuleNotActiveException();
        }
        parent::__construct($data);
    }

    public function getData(): array
    {
        return [
            'table' => (new StatisticDashboardEntity())->getStatisticInfoData()->toArray()
        ];
    }

    public function render()
    {
        return view('vmd_cms::admin.views.sections.display.panels.statistic_info', ['data' => $this->getData()])->render();
    }

}
