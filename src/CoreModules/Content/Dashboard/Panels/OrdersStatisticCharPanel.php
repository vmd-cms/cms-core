<?php

namespace VmdCms\CoreCms\CoreModules\Content\Dashboard\Panels;

use VmdCms\CoreCms\Contracts\Dashboard\Display\Panels\SectionPanelInterface;
use VmdCms\CoreCms\CoreModules\Content\Entity\StatisticDashboardEntity;
use VmdCms\CoreCms\Dashboard\Display\Panels\AbstractSectionPanel;
use VmdCms\CoreCms\Exceptions\CoreException;
use VmdCms\CoreCms\Exceptions\Modules\ModuleNotActiveException;
use VmdCms\Modules\Orders\Sections\Order;

class OrdersStatisticCharPanel extends AbstractSectionPanel
{
    public function __construct(array $data = [])
    {
        if(!class_exists(\App\Modules\Orders\Models\Order::class)){
            throw new ModuleNotActiveException();
        }
        parent::__construct($data);
    }

    public function getData(): array
    {
        return [
            'chart' => (new StatisticDashboardEntity())->getStatisticOrdersCharDTO()->toArray()
        ];
    }

    public function render()
    {
        return view('vmd_cms::admin.views.sections.display.panels.orders_statistic_char', ['data' => $this->getData()])->render();
    }
}
