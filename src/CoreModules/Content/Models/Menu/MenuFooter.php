<?php

namespace VmdCms\CoreCms\CoreModules\Content\Models\Menu;

class MenuFooter extends Menu
{
    const MODEL_KEY = 'footer';

    public static function getModelKey(){
        return self::MODEL_KEY;
    }
}
