<?php

namespace VmdCms\CoreCms\CoreModules\Content\Models\Menu;

use Illuminate\Database\Eloquent\Builder;
use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Contracts\Models\CloneableInterface;
use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;
use VmdCms\CoreCms\Contracts\Models\OrderableInterface;
use VmdCms\CoreCms\Contracts\Models\TreeableInterface;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Activable;
use VmdCms\CoreCms\Traits\Models\Cloneable;
use VmdCms\CoreCms\Traits\Models\HasInfo;
use VmdCms\CoreCms\Traits\Models\HasUrlPath;
use VmdCms\CoreCms\Traits\Models\Orderable;
use VmdCms\CoreCms\Traits\Models\Treeable;

class Menu extends CmsModel implements ActivableInterface, TreeableInterface, HasInfoInterface, OrderableInterface, CloneableInterface
{
    use Activable, Treeable, HasInfo, Orderable, HasUrlPath, Cloneable;

    public static function table(): string
    {
       return 'menu';
    }

    public static function getModelKey(){
        return null;
    }

    protected static function boot()
    {
        parent::boot();

        if(static::getModelKey()){
            static::addGlobalScope('key', function (Builder $builder) {
                $builder->where('key', static::getModelKey());
            });
        }

        static::saving(function (Menu $model){
            $model->key = static::getModelKey();
        });
    }

    private function getInfoClass() : string
    {
        return self::class . 'Info';
    }
}
