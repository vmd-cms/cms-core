<?php

namespace VmdCms\CoreCms\CoreModules\Content\Models\Menu;

class MenuHeader extends Menu
{
    const MODEL_KEY = 'header';

    public static function getModelKey(){
        return self::MODEL_KEY;
    }
}
