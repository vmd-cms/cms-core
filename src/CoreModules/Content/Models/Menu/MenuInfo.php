<?php

namespace VmdCms\CoreCms\CoreModules\Content\Models\Menu;

use VmdCms\CoreCms\Models\CmsInfoModel;

class MenuInfo extends CmsInfoModel
{
    public static function table(): string
    {
        return 'menu_info';
    }
}
