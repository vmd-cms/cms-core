<?php

namespace VmdCms\CoreCms\CoreModules\Content\Models\Blocks\Groups;

use VmdCms\CoreCms\CoreModules\Content\Models\Blocks\Block;

class GeneralGroupBlock extends Block
{
    public static function getModelGroup(): ?string
    {
        return 'general';
    }
}
