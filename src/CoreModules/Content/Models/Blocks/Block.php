<?php

namespace VmdCms\CoreCms\CoreModules\Content\Models\Blocks;

use Illuminate\Database\Eloquent\Builder;
use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Contracts\Models\CloneableInterface;
use VmdCms\CoreCms\Contracts\Models\CmsRelatedModelInterface;
use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;
use VmdCms\CoreCms\Contracts\Models\HasMediaDimensionsInterface;
use VmdCms\CoreCms\Contracts\Models\OrderableInterface;
use VmdCms\CoreCms\Contracts\Models\TreeableInterface;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Services\Files\StorageAdapter;
use VmdCms\CoreCms\Traits\Models\Activable;
use VmdCms\CoreCms\Traits\Models\Cloneable;
use VmdCms\CoreCms\Traits\Models\HasInfo;
use VmdCms\CoreCms\Traits\Models\HasMediaDimensions;
use VmdCms\CoreCms\Traits\Models\JsonData;
use VmdCms\CoreCms\Traits\Models\Orderable;
use VmdCms\CoreCms\Traits\Models\Treeable;

class Block extends CmsModel implements ActivableInterface, HasInfoInterface, OrderableInterface, TreeableInterface, CmsRelatedModelInterface, HasMediaDimensionsInterface
{
    use Activable, HasInfo, Orderable, Treeable, JsonData, HasMediaDimensions;

    public static function table(): string
    {
        return 'blocks';
    }

    public static function getModelKey(): ?string
    {
        return null;
    }

    public static function getModelGroup(): ?string
    {
        return null;
    }

    public static function getModelResource(): ?string
    {
        return null;
    }

    final public static function getModelTree()
    {
        return static::tree(true)->where('key',static::getModelKey())->first();
    }

    protected function getInfoClass() : string
    {
        return BlockInfo::class;
    }

    public function getMediaPathAttribute()
    {
        return StorageAdapter::getInstance()->getStoragePath($this->media);
    }

    public static function getForeignField(): string
    {
        return 'parent_id';
    }

    public static function getBaseModelClass(): string
    {
        return static::class;
    }

    protected static function boot()
    {
        parent::boot();

        if(static::getModelGroup()){
            static::addGlobalScope('group', function (Builder $builder) {
                $builder->where('group', static::getModelGroup());
            });
            static::saving(function (Block $model){
                $model->group = static::getModelGroup();
            });
        }

        if(static::getModelResource()){
            static::addGlobalScope('resource', function (Builder $builder) {
                $builder->where('resource', static::getModelResource());
            });
            static::saving(function (Block $model){
                $model->resource = static::getModelResource();
            });
        }

        if(static::getModelKey()){
            static::addGlobalScope('model_key', function (Builder $builder) {
                $builder->where('key', static::getModelKey());
            });
            static::saving(function (Block $model){
                $model->key = static::getModelKey();
            });
        }

        static::saved(static::getSavedCallback());
    }

    public function getValue($field){
        return $this->getDataField($field);
    }

    public function getInfoValue($field){
        return $this->getInfoDataField($field);
    }

}
