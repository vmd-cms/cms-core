<?php

namespace VmdCms\CoreCms\CoreModules\Content\Models\Blocks\Custom;

use VmdCms\CoreCms\CoreModules\Content\Models\Blocks\Groups\GeneralGroupBlock;
use VmdCms\CoreCms\CoreModules\Content\Services\BlockEnum;

class SliderBlock extends GeneralGroupBlock
{
    public static function getModelKey(): ?string
    {
        return BlockEnum::SLIDER_BLOCK;
    }

    public function getJsonDataFieldsArr() : array
    {
        return ['title'];
    }
}
