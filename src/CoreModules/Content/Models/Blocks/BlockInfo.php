<?php

namespace VmdCms\CoreCms\CoreModules\Content\Models\Blocks;

use VmdCms\CoreCms\Models\CmsInfoModel;

class BlockInfo extends CmsInfoModel
{
    public static function table(): string
    {
        return 'blocks_info';
    }
}
