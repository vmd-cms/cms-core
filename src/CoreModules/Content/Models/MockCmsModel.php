<?php

namespace VmdCms\CoreCms\CoreModules\Content\Models;

use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;

class MockCmsModel implements CmsModelInterface
{
    public static function getPrimaryField(): string
    {
        return '';
    }

    public static function find(string $param = null)
    {
        return null;
    }

    public static function save()
    {
        return true;
    }

    public static function create()
    {
        return true;
    }

    public function getPrimaryValue()
    {
        return '';
    }
}
