<?php

namespace VmdCms\CoreCms\CoreModules\Content\Models\Pages;

use Illuminate\Database\Eloquent\Builder;
use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;
use VmdCms\CoreCms\Contracts\Models\HasMediaDimensionsInterface;
use VmdCms\CoreCms\Contracts\Models\OrderableInterface;
use VmdCms\CoreCms\Contracts\Models\SeoableInterface;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Services\Files\StorageAdapter;
use VmdCms\CoreCms\Traits\Models\Activable;
use VmdCms\CoreCms\Traits\Models\HasInfo;
use VmdCms\CoreCms\Traits\Models\HasMediaDimensions;
use VmdCms\CoreCms\Traits\Models\HasUrlPath;
use VmdCms\CoreCms\Traits\Models\JsonData;
use VmdCms\CoreCms\Traits\Models\Orderable;
use VmdCms\CoreCms\Traits\Models\Seoable;

class Page extends CmsModel implements ActivableInterface, HasInfoInterface, OrderableInterface, HasMediaDimensionsInterface, SeoableInterface
{
    use Activable, HasInfo, Orderable, JsonData, Seoable, HasUrlPath, HasMediaDimensions;

    public static function table(): string
    {
        return 'pages';
    }

    public static function getModelKey(){
        return null;
    }

    protected function getInfoClass() : string
    {
        return PageInfo::class;
    }

    public static function getBaseModelClass(): string
    {
        return static::class;
    }

    protected static function boot()
    {
        parent::boot();
        if(static::getModelKey()){
            static::addGlobalScope('key', function (Builder $builder) {
                $builder->where('key', static::getModelKey());
            });
        }

        static::saving(function (Page $model){
            $model->key = static::getModelKey();
        });

        static::saved(function (Page $model) {

            $model->load('info');
            if(!$infoModel = $model->info ?? null)
            {
                $infoClass = $model->getInfoClass();
                $infoModel = new $infoClass();
                $infoModel->pages_id = $model->id;
            }
            $infoModel->data = json_encode($model->getInfoDataArrayAttr());
            $infoModel->save();
        });
    }

    public function getValue($field){
        return $this->getDataField($field);
    }

    /**
     * @return string
     */
    public function getPhotoPreviewPathAttribute()
    {
        return StorageAdapter::getInstance()->getStoragePath($this->photo_preview);
    }

    /**
     * @return string
     */
    public function getPhotoPathAttribute()
    {
        return StorageAdapter::getInstance()->getStoragePath($this->photo,false);
    }

    /**
     * @return string
     */
    protected function getUrlPrefix(): string
    {
        return 'page';
    }


}
