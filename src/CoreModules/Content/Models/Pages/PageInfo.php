<?php

namespace VmdCms\CoreCms\CoreModules\Content\Models\Pages;

use VmdCms\CoreCms\Models\CmsInfoModel;
use VmdCms\CoreCms\Traits\Models\JsonData;

class PageInfo extends CmsInfoModel
{
    use JsonData;

    public static function table(): string
    {
        return 'pages_info';
    }
}
