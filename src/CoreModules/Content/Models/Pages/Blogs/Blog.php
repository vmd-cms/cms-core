<?php

namespace VmdCms\CoreCms\CoreModules\Content\Models\Pages\Blogs;

use App\Modules\Content\Models\Pages\Page;

class Blog extends Page
{
    public static function getModelKey()
    {
        return 'blog';
    }

    /**
     * @return string
     */
    protected function getUrlPrefix(): string
    {
        return 'blog/';
    }
}
