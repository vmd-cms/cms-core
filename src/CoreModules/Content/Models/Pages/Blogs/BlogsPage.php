<?php

namespace VmdCms\CoreCms\CoreModules\Content\Models\Pages\Blogs;

use App\Modules\Content\Models\Pages\Page;

class BlogsPage extends Page
{
    public static function getModelKey()
    {
        return 'blogs_page';
    }
}
