<?php

namespace VmdCms\CoreCms\CoreModules\Content\Models\Pages\Custom;

use App\Modules\Content\Models\Pages\Page;

class MainPage extends Page
{
    public static function getModelKey()
    {
        return 'main_page';
    }
}
