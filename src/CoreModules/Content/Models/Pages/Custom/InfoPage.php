<?php

namespace VmdCms\CoreCms\CoreModules\Content\Models\Pages\Custom;

use App\Modules\Content\Models\Pages\Page;
use VmdCms\CoreCms\Contracts\Models\CloneableInterface;
use VmdCms\CoreCms\Traits\Models\Cloneable;

class InfoPage extends Page implements CloneableInterface
{
    use Cloneable;

    public static function getModelKey()
    {
        return 'info_page';
    }
}
