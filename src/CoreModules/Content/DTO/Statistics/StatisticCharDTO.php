<?php

namespace VmdCms\CoreCms\CoreModules\Content\DTO\Statistics;

use Illuminate\Contracts\Support\Arrayable;

class StatisticCharDTO implements Arrayable
{
    /**
     * @var array
     */
    protected $series;

    /**
     * @var array
     */
    protected $options;

    protected $categories;

    /**
     * @var string
     */
    protected $title;

    public function __construct(array $categories = [])
    {
        $this->categories = $categories;
        $this->setOptions();
        $this->series = [];
        $this->perPage = 10;
    }

    /**
     * @param string $title
     * @return StatisticCharDTO
     */
    public function setTitle(string $title): StatisticCharDTO
    {
        $this->title = $title;
        return $this;
    }

    public function appendItem(string $color,string $name,array $dataArray)
    {
        $this->series[] = [
            'color' => $color,
            'name' => $name,
            'data' => $dataArray,
        ];
    }

    protected function setOptions(){
        $this->options = [
            "legend" => [
                "position"=> "top",
                "verticalAlign"=> "top",
                "horizontalAlign"=> "left",
            ],
            "chart" => [
                "width"=> "100%",
                "type"=> "area"
            ],
            "dataLabels" => [
                "enabled" => false
            ],
            "stroke"=> [
                "curve"=> "smooth"
            ],
            "tooltip" => [
                "x" => [
                    "format" => "dd/MM/yy HH:mm"
                ]
            ],
            "xaxis" => [
                "type" => "datetime",
                "categories" => $this->categories
            ]
        ];
    }

    public function toArray()
    {
        return [
            'title' => $this->title,
            'series' => $this->series,
            'chartOptions' => $this->options
        ];
    }
}
