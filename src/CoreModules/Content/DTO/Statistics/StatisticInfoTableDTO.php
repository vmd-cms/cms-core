<?php

namespace VmdCms\CoreCms\CoreModules\Content\DTO\Statistics;

use Illuminate\Contracts\Support\Arrayable;

class StatisticInfoTableDTO implements Arrayable
{
    /**
     * @var array
     */
    protected $headers;

    /**
     * @var array
     */
    protected $items;

    /**
     * @var int
     */
    protected $perPage;

    /**
     * @var string
     */
    protected $title;

    public function __construct()
    {
        $this->setHeaders();
        $this->items = [];
        $this->perPage = 10;
    }

    /**
     * @param int $perPage
     * @return StatisticInfoTableDTO
     */
    public function setPerPage(int $perPage): StatisticInfoTableDTO
    {
        $this->perPage = $perPage;
        return $this;
    }

    /**
     * @param string $title
     * @return StatisticInfoTableDTO
     */
    public function setTitle(string $title): StatisticInfoTableDTO
    {
        $this->title = $title;
        return $this;
    }

    public function appendItem(string $color,string $info,string $total)
    {
        $this->items[] = [
            'color' => $color,
            'info' => $info,
            'total' => $total,
        ];
    }

    protected function setHeaders(){
        $this->headers = [
            [
                'text'=> 'color',
                'value'=>  'color',
                'align'=> 'center',
                'width'=> '50',
                'class'=> 'pl-5'
            ],
            [
                'text'=> 'info',
                'value'=>  'info',
                'align'=> 'start'
            ],
            [
                'text'=> 'total',
                'value'=>  'total',
                'align'=> 'end',
                'class'=> 'pr-5'
            ]
        ];
    }

    public function toArray()
    {
        return [
            'headers' => $this->headers,
            'items' => $this->items,
            'itemsPerPage' => $this->perPage,
            'title' => $this->title
        ];
    }
}
