<?php

namespace VmdCms\CoreCms\CoreModules\Content\DTO;

use Illuminate\Database\Eloquent\Collection;
use VmdCms\CoreCms\CoreModules\Content\Entity\Menus;
use App\Modules\Content\Entity\Settings;

class HeaderDTO
{
    /**
     * @var \Illuminate\Support\Collection
     */
    public $menu;

    /**
     * @var \Illuminate\Support\Collection
     */
    public $phones;

    /**
     * @var Collection
     */
    public $socials;

    /**
     * @var string
     */
    public $logo;

    public function __construct()
    {
        $this->menu = Menus::getInstance()->getHeader();

        $this->logo = Settings::getInstance()->getHeaderLogo();

        $this->phones = Settings::getInstance()->getPhones();

        $this->phones = Settings::getInstance()->getPhones();

        $this->socials = Settings::getInstance()->getSocials();
    }
}
