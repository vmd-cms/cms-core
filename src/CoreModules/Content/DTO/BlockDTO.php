<?php

namespace VmdCms\CoreCms\CoreModules\Content\DTO;

use VmdCms\CoreCms\CoreModules\Content\Contracts\BlockDtoInterface;
use VmdCms\CoreCms\CoreModules\Content\Models\Blocks\Block;

class BlockDTO implements BlockDtoInterface
{
    public $children;
    public $blockKey;
    public $mediaPath;
    public $body;

    public function __construct(Block $block)
    {
        $this->blockKey = $block::getModelKey();
        $this->mediaPath = $block->mediaPath;
        $this->body = $block->info->body ?? null;
        $this->appendAdditionalData($block);
        $this->appendChildren($block);

    }

    protected function appendAdditionalData(Block $block)
    {
        $additionalFields = $block->getJsonDataFieldsArr();
        if(is_array($additionalFields) && count($additionalFields))
        {
            foreach ($additionalFields as $field)
            {
                $this->$field = $block->getValue($field);
            }
        }

        $additionalInfoFields = $block->getJsonInfoDataFieldsArr();
        if(is_array($additionalInfoFields) && count($additionalInfoFields))
        {
            foreach ($additionalInfoFields as $field)
            {
                $this->$field = $block->getInfoValue($field);
            }
        }
    }

    protected function appendChildren(Block $block)
    {
        $this->children = [];
        if(!isset($block->children) || !count($block->children))
        {
            return;
        }
        foreach ($block->children as $child)
        {
            $this->children[] = new static($child);
        }
    }

    public function getBlockKey(): string
    {
        return $this->blockKey;
    }
}
