<?php

namespace VmdCms\CoreCms\CoreModules\Content\DTO;

use Illuminate\Support\Carbon;
use VmdCms\CoreCms\CoreModules\Content\Contracts\InfoPageDtoInterface;
use VmdCms\CoreCms\CoreModules\Content\Models\Pages\Page;

class InfoPageDTO implements InfoPageDtoInterface
{
    protected $title;
    protected $link;
    protected $photoPreview;
    protected $textPreview;

    protected $photo;
    protected $body;
    protected $publicAt;

    public function __construct(Page $model)
    {
        $this->title = $model->info->title ?? null;
        $this->link = $model->urlPath ?? null;
        $this->photoPreview = $model->photoPreviewPath ?? null;
        $this->photo = $model->PhotoPath ?? null;
        $this->textPreview = $model->info->preview ?? null;
        $this->body = $model->info->body ?? null;
        $this->publicAt = $model->public_at ?? null;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @return string|null
     */
    public function getPhotoPreview(): ?string
    {
        return $this->photoPreview;
    }

    /**
     * @return string|null
     */
    public function getTextPreview(): ?string
    {
        return $this->textPreview;
    }

    /**
     * @return string|null
     */
    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    /**
     * @return string|null
     */
    public function getBody(): ?string
    {
        return $this->body;
    }

    /**
     * @param string $format
     * @return string|null
     */
    public function getPublicAt(string $format = 'd.m.Y'): ?string
    {
        return $this->publicAt ? Carbon::parse($this->publicAt)->format($format) : null;
    }
}
