<?php

namespace VmdCms\CoreCms\CoreModules\Content\Initializers;;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use VmdCms\CoreCms\Exceptions\Services\StubBuilderException;
use VmdCms\CoreCms\Exceptions\Services\StubReplacerException;
use VmdCms\CoreCms\Initializers\AbstractModuleInitializer;

class ModuleInitializer extends AbstractModuleInitializer
{
    const SLUG = 'content';
    const ALIAS = 'Content';

    public function __construct()
    {
        parent::__construct();
        $this->stubBuilder->setPublishSeed(false);
        $this->stubBuilder->setPublishServices(true);
        $this->stubBuilder->setPublishDTO(true);
        $this->stubBuilder->setPublishEntity(true);
    }

    /**
     * @inheritDoc
     */
    public static function moduleAlias(): string
    {
        return self::ALIAS;
    }

    /**
     * @inheritDoc
     */
    public static function moduleSlug(): string
    {
        return self::SLUG;
    }

    /**
     * @throws FileNotFoundException
     * @throws StubBuilderException
     * @throws StubReplacerException
     */
    public function install()
    {
        $this->publish();
        $this->publishModuleClasses();
    }


}
