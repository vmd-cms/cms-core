<?php

namespace VmdCms\CoreCms\CoreModules\Content\Middleware;

use App\Modules\Content\Services\DataShare;
use Closure;

class DataShareMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        view()->share('data', DataShare::getInstance());
        return $next($request);
    }
}
