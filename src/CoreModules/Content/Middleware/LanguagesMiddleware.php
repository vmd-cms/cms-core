<?php

namespace VmdCms\CoreCms\CoreModules\Content\Middleware;

use Closure;
use VmdCms\CoreCms\CoreModules\Content\Entity\Languages;

class LanguagesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $prefix = $request->route()->getPrefix();

        $parts = explode('/',$prefix);
        $language = array_shift($parts);

        if ($language && substr($language, 0, 1) == '/') $language = substr_replace($language, '', 0, 1);

        Languages::getInstance()->setLocaleActive($language ?? app()->getLocale());

        return $next($request);
    }
}
