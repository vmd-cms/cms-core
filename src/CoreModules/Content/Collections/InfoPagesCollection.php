<?php

namespace VmdCms\CoreCms\CoreModules\Content\Collections;

use VmdCms\CoreCms\Collections\CoreCollectionAbstract;
use VmdCms\CoreCms\CoreModules\Content\Contracts\InfoPageDtoInterface;

class InfoPagesCollection extends CoreCollectionAbstract
{
    /**
     * @param InfoPageDtoInterface $dto
     */
    public function append(InfoPageDtoInterface $dto)
    {
        $this->collection->add($dto);
    }
}
