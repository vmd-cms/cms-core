<?php

namespace VmdCms\CoreCms\CoreModules\Content\Collections;

use VmdCms\CoreCms\Collections\CoreCollectionAbstract;
use VmdCms\CoreCms\CoreModules\Content\Contracts\BlockDtoInterface;
use VmdCms\CoreCms\CoreModules\Content\Contracts\BlocksCollectionInterface;

class BlocksCollection extends CoreCollectionAbstract implements BlocksCollectionInterface
{
    /**
     * @param BlockDtoInterface $blockDto
     */
    public function append(BlockDtoInterface $blockDto)
    {
        $this->collection->put($blockDto->getBlockKey(),$blockDto);
    }

    /**
     * @param string $key
     * @return BlockDtoInterface|null
     */
    public function getItem(string $key): ?BlockDtoInterface
    {
        return $this->collection->get($key);
    }
}
