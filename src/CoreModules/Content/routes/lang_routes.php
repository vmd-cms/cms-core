<?php

use Illuminate\Support\Facades\Route;
use App\Modules\Content\Services\ContentRouter;
use Illuminate\Routing\Router;

return function (){
    Route::group([
        'namespace' => "App\\Modules\\Content\\Controllers"
    ],function (Router $router){
        $router->get('', [
            'as'   => ContentRouter::ROUTE_MAIN_PAGE,
            'uses' => 'PageController@mainPage',
        ]);
        $router->get('blogs', [
            'as'   => ContentRouter::ROUTE_BLOGS_PAGE,
            'uses' => 'BlogController@blogsPage',
        ]);
        $router->post('blogs/load-more', [
            'as'   => ContentRouter::ROUTE_BLOGS_LOAD_MORE,
            'uses' => 'BlogController@blogsPageLoadMore',
        ])->withoutMiddleware([\VmdCms\CoreCms\CoreModules\Content\Middleware\DataShareMiddleware::class]);
        $router->get('blog/{url}', [
            'as'   => ContentRouter::ROUTE_BLOG_PAGE,
            'uses' => 'BlogController@blogPage',
        ]);
    });
};
