<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$data->metaTitle ?? env('APP_NAME') ?? ''}}</title>
    <meta name="description" content="{{$data->metaDescription ?? env('APP_NAME') ?? ''}}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="twitter:card" content="{{request()->url()}}">
    <meta name="twitter:site" content="{{request()->root()}}">
    <meta name="twitter:title" content="{{$data->metaTitle ?? env('APP_NAME') ?? ''}}">
    <meta name="twitter:description" content="{{$data->metaDescription ?? env('APP_NAME') ?? ''}}">
    <meta name="twitter:image:src" content="{{request()->root() . '/logo.png'}}">

    <meta property="og:locale" content="{{str_replace('_', '-', app()->getLocale()) }}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{$data->metaTitle ?? env('APP_NAME') ?? ''}}">
    <meta property="og:description" content="{{$data->metaDescription ?? env('APP_NAME') ?? ''}}">
    <meta property="og:url" content="{{request()->url()}}">
    <meta property="og:image" content="{{request()->root() . '/logo.png'}}">
    <meta property="og:site_name" content="{{env('APP_URL')}}">

    <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    @stack('header_injection')
</head>
<body>

@yield('header')
@yield('bread_crumbs')

<main>
    @yield('content')
    @section('seo_text')
        @if($data->metaText ?? null)
            <div class="container custom-container">
                <div class="ck-editor-block">{!! $data->metaText !!}</div>
            </div>
        @endif
    @show
</main>
@yield('footer')
@stack('footer_injection')

</body>
</html>

