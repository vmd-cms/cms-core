@extends('content::main',['meta_title' => $meta_title ?? '', 'meta_keywords' => $meta_keywords ?? '', 'meta_description' => $meta_description ?? ''])

@push('header_injection')

@endpush

@section('header')
    @include('content::layouts.components.header')
@endsection

@section('footer')
    @include('content::layouts.components.footer')
@endsection

@push('footer_injection')

@endpush
