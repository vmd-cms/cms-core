<?php

namespace VmdCms\CoreCms\CoreModules\Content\Entity;

use App\Modules\Content\Entity\Languages;
use App\Modules\Translates\Models\Translate;
use Illuminate\Support\Facades\DB;
use VmdCms\CoreCms\CoreModules\Translates\Services\GroupEnums;
use VmdCms\CoreCms\Services\Config;

class Translates
{
    /**
     * @var Translates
     */
    protected static $instance;

    /**
     * @var array
     */
    protected $translates;

    /**
     * @var bool
     */
    protected $isUsedGroups;

    protected function __construct(){
        $this->isUsedGroups = Config::getInstance()->isUsedGroupTranslates();
        $this->translates = $this->getTranslatesByGroups($this->isUsedGroups ? [null,GroupEnums::GENERAL] : []);
    }

    protected function getTranslatesByGroups(array $groups)
    {

        $query = DB::table('translates as base')
            ->select(['base.key','info.value'])
            ->join('translates_info as info',function ($join){
                $join->on('info.translates_id','=','base.id')
                    ->where('info.lang_key',Languages::getInstance()->getCurrentLocale());
            })
            ->where('base.active',true);
        if(is_array($groups) && count($groups)){
            $query->whereIn('base.group',$groups);
        }
        return $query->distinct('base.id')->get()->pluck('value','key');
    }

    /**
     * @return Translates
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    /**
     * @param array $groups
     * @return Translates
     */
    public function appendGroups(array $groups): Translates
    {
        if($this->isUsedGroups){
            $translates = $this->getTranslatesByGroups($groups);
            if(is_array($translates)) $this->translates = array_merge($this->translates,$translates);
        }
        return $this;
    }

    /**
     * @param string $key
     * @param string|null $default
     * @return string|null
     */
    public function getValue(string $key, string $default = null) : ?string
    {
        if(is_null($default)) {
            $default = $key;
        }
        return $this->translates[$key] ?? $default;
    }
}
