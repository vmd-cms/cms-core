<?php

namespace VmdCms\CoreCms\CoreModules\Content\Entity;

use App\Modules\Content\DTO\BlockDTO;
use App\Modules\Content\Models\Blocks\Block;
use App\Modules\Content\Models\Blocks\BlockInfo;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use VmdCms\CoreCms\Exceptions\Models\ClassNotFoundException;
use VmdCms\CoreCms\Exceptions\Models\ModelNotFoundException;

class BlockEntity
{
    protected function getBlocks(string $group, string $key = null){
        $query = DB::table(Block::table() . ' as model')->select([
            'model.id as id',
            'model.parent_id as parent_id',
            'model.group as group',
            'model.key as key',
            'model.resource_id as resource_id',
            'model.resource as resource',
            'model.media as media',
            'model.data as model_data',
            'info.body as body',
            'info.data as data',
            'info.media as info_media'
        ])->leftJoin(BlockInfo::table() . ' as info', function ($join){
            $join->on('model.id','=','info.blocks_id');
            $join->where('info.lang_key',Languages::getInstance()->getCurrentLocale());
        })
            ->where('model.active',true)
            ->where('model.group',$group);

        if(!empty($key)){
            $query->where(function ($q) use ($group,$key){
                $q->where('model.key',$key)->orWhereIn('model.parent_id',function ($sub) use ($group,$key){
                    $sub->select('id')
                        ->from(Block::table())
                        ->where('group',$group)
                        ->where('key',$key);
                });
            });
        }
        return $query->orderBy('model.order')->get();
    }

    /**
     * @param string $class
     * @return Block
     * @throws ClassNotFoundException
     * @throws ModelNotFoundException
     */
    public function getBlockModel(string $class){
        if(!class_exists($class)) {
            throw new ClassNotFoundException();
        }

        $blockModel = new $class();
        if(!$blockModel instanceof \VmdCms\CoreCms\CoreModules\Content\Models\Blocks\Block){
            throw new ModelNotFoundException();
        }

        $blocks = $this->getBlocks($blockModel::getModelGroup(),$blockModel::getModelKey());
        $block = $blocks->where('parent_id',null)->first();

        if(empty($block)){
            throw new ModelNotFoundException();
        }

        $blockModel->id = $block->id;
        $blockModel->parent_id = $block->parent_id;
        $blockModel->group = $block->group;
        $blockModel->key = $block->key;
        $blockModel->resource_id = $block->resource_id;
        $blockModel->resource = $block->resource;
        $blockModel->data = $block->model_data;
        $blockModel->media = $block->media;

        $blockInfo = new BlockInfo();
        $blockInfo->lang_key = Languages::getInstance()->getCurrentLocale();
        $blockInfo->blocks_id = $block->id;
        $blockInfo->body = $block->body;
        $blockInfo->data = $block->data;
        $blockInfo->media = $block->info_media;

        $blockModel->info = $blockInfo;

        $childrenCollection = new Collection();

        $children = $blocks->where('parent_id', $block->id);
        foreach ($children as $child){

            $childModel = new $class();
            $childModel->id = $child->id;
            $childModel->parent_id = $child->parent_id;
            $childModel->group = $child->group;
            $childModel->key = $child->key;
            $childModel->resource_id = $child->resource_id;
            $childModel->resource = $child->resource;
            $childModel->data = $child->model_data;
            $childModel->media = $child->media;
            $childModel->children = new Collection();

            $childInfo = new BlockInfo();
            $childInfo->lang_key = Languages::getInstance()->getCurrentLocale();
            $childInfo->blocks_id = $child->id;
            $childInfo->body = $child->body;
            $childInfo->data = $child->data;
            $childInfo->media = $child->info_media;
            $childModel->info = $childInfo;
            $childrenCollection->add($childModel);
        }

        $blockModel->children = $childrenCollection;

        return $blockModel;
    }

    /**
     * @param string $class
     * @return BlockDTO
     * @throws ClassNotFoundException
     * @throws ModelNotFoundException
     */
    public function getBlockDTO(string $class){

        $blockModel = $this->getBlockModel($class);
        return new BlockDTO($blockModel);
    }
}
