<?php

namespace VmdCms\CoreCms\CoreModules\Content\Entity;

use App\Modules\Content\Models\Blocks\Custom\SliderBlock;
use App\Modules\Content\Models\Pages\Custom\MainPage;
use App\Modules\Content\Services\DataShare;
use App\Modules\Products\Entity\ProductEntity;
use App\Modules\Products\Models\Components\ProductTranslate;
use VmdCms\CoreCms\CoreModules\Content\DTO\BlockDTO;
use VmdCms\CoreCms\Exceptions\Models\ModelNotFoundException;

class PageEntity
{
    /**
     * @throws ModelNotFoundException
     */
    public function shareMainPageData()
    {
        if(!$mainPage = MainPage::first()) throw new ModelNotFoundException();

        $dataShare = DataShare::getInstance();

        $dataShare->seo->setData($mainPage);

        if(class_exists(ProductTranslate::class)){
            $dataShare->translates->appendGroups([
                ProductTranslate::getModelGroup()
            ]);
        }

        if($block = SliderBlock::getModelTree()){
            $dataShare->blocks->append(new BlockDTO($block));
        }

        $dataShare->appendData('blogs',(new BlogEntity())->setMainPageOnly(true)->getCollection()->getItems());

        if (class_exists(ProductEntity::class)){
            $productEntity = new ProductEntity();
            $dataShare->appendData('watched',$productEntity->getWatchedItems()->getItems());
        }
    }

}
