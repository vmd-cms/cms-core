<?php

namespace VmdCms\CoreCms\CoreModules\Content\Entity;

use App\Modules\Content\Models\Menu\Menu;
use App\Modules\Content\Models\Menu\MenuInfo;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use VmdCms\CoreCms\Collections\CoreCollectionAbstract;
use VmdCms\CoreCms\Collections\NavsCollection;
use VmdCms\CoreCms\DTO\NavDTO;
use VmdCms\CoreCms\Services\Files\StorageAdapter;

class MenuEntity
{
    public function getMenu(){
        return DB::table(Menu::table() . ' as model')->select([
            'model.id as id',
            'model.parent_id as parent_id',
            'model.key as key',
            'model.resource as resource',
            'model.resource_id as resource_id',
            'model.url as url',
            'model.icon as icon',
            'info.title as info_title',
            'info.url as info_url',
        ])->leftJoin(MenuInfo::table() . ' as info', function ($join){
            $join->on('model.id','=','info.menu_id');
            $join->where('info.lang_key',Languages::getInstance()->getCurrentLocale());
        })
            ->where('model.active',true)->orderBy('model.order')->get();
    }

    /**
     * @param Collection $items
     * @return NavsCollection
     */
    public function getMenuNavCollection(Collection $items): NavsCollection
    {
        $collection = new NavsCollection();

        if(!is_countable($items) || !count($items)){
            return $collection;
        }
        $grouped = $items->groupBy('parent_id');
        $parents = $grouped->get(null);

        $arrAssoc = [];
        foreach ($parents as $parent){

            $navItem = (new NavDTO($parent->info_title,$parent->url))
                ->setIcon($parent->icon ? StorageAdapter::getInstance()->getStoragePath($parent->icon) : null);
            if($parent->resource && class_exists($parent->resource))
            {
                $resourceItems = $parent->resource::active()->order()->with('info')->get();
                $childrenCollection = new NavsCollection();
                foreach ($resourceItems as $item){
                    $childrenCollection->append((new NavDTO($item->info->title ?? '',$item->urlPath)));
                }
                $navItem->setChildren($childrenCollection);
            }
            $arrAssoc[$parent->id] = $navItem;
        }

        foreach ($grouped as $parentId => $group){
            if(empty($parentId)) continue;

            $childrenCollection = new NavsCollection();
            $parent = $arrAssoc[$parentId] ?? null;
            if(!$parent instanceof NavDTO){
                continue;
            }
            foreach ($group as $child){
                $childrenCollection->append((new NavDTO($child->info_title,$parent->link . '/'. $child->url))
                    ->setIcon($child->icon ? StorageAdapter::getInstance()->getStoragePath($child->icon) : null));
            }
            $parent->setChildren($childrenCollection);
        }

        foreach ($arrAssoc as $item){
            if($item instanceof NavDTO){
                $collection->append($item);
            }
        }
        return $collection;
    }
}
