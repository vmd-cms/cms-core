<?php

namespace VmdCms\CoreCms\CoreModules\Content\Entity;

use Illuminate\Database\Eloquent\Collection;
use VmdCms\CoreCms\Collections\NavsCollection;
use VmdCms\CoreCms\CoreModules\Settings\Models\GeneralGroup\Addresses;
use VmdCms\CoreCms\CoreModules\Settings\Models\GeneralGroup\Emails;
use VmdCms\CoreCms\CoreModules\Settings\Models\GeneralGroup\Logo;
use VmdCms\CoreCms\CoreModules\Settings\Models\GeneralGroup\Phones;
use VmdCms\CoreCms\CoreModules\Settings\Models\GeneralGroup\Socials;
use VmdCms\CoreCms\DTO\NavDTO;
use VmdCms\CoreCms\Services\Files\StorageAdapter;

class Settings
{
    /**
     * @var Settings
     */
    protected static $instance;

    /**
     * @var Collection
     */
    protected $settings;

    protected function __construct()
    {
        $this->settings = \VmdCms\CoreCms\CoreModules\Settings\Models\Settings::order()->active()->get();
    }

    /**
     * @return Settings
     */
    public static function getInstance()
    {
        if (!static::$instance) static::$instance = new static();
        return static::$instance;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getSocials(): \Illuminate\Support\Collection
    {
        $items = $this->getItemsByKey(Socials::KEY);
        $socials = new NavsCollection();
        $storageAdapter = StorageAdapter::getInstance();
        foreach ($items as $item)
        {
            $socials->append((new NavDTO($item->title,$item->value))
                ->setIcon($storageAdapter->getSvgContent($item->media))
            );
        }
        return $socials->getItems();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getPhones(): \Illuminate\Support\Collection
    {
        $items = $this->getItemsByKey(Phones::KEY);
        $phonesCollection = new NavsCollection();
        foreach ($items as $item)
        {
            $phonesCollection->append(new NavDTO($item->title, $item->value));
        }
        return $phonesCollection->getItems();
    }

    /**
     * @return Collection
     */
    public function getEmails(): Collection
    {
        return $this->getItemsByKey(Emails::KEY);
    }

    /**
     * @return Collection
     */
    public function getAddresses(): Collection
    {
        return $this->getItemsByKey(Addresses::KEY);
    }

    /**
     * @return string|null
     */
    public function getHeaderLogo(): ?string
    {
        $logo = $this->getItemsByKey(Logo::KEY)->where('value',Logo::HEADER)->first();
        $logo = $logo instanceof \VmdCms\CoreCms\CoreModules\Settings\Models\Settings ? $logo->media : null;
        return StorageAdapter::getInstance()->getStoragePath($logo);
    }

    /**
     * @return string|null
     */
    public function getFooterLogo(): ?string
    {
        $logo = $this->getItemsByKey(Logo::KEY)->where('value',Logo::FOOTER)->first();
        $logo = $logo instanceof \VmdCms\CoreCms\CoreModules\Settings\Models\Settings ? $logo->media : null;
        return StorageAdapter::getInstance()->getStoragePath($logo);
    }

    protected function getItemsByKey(string $key)
    {
        return $this->settings instanceof Collection ? $this->settings->where('key', $key) : new Collection();
    }
}
