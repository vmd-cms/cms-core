<?php

namespace VmdCms\CoreCms\CoreModules\Content\Entity;

use App\Modules\Content\Entity\MenuEntity;
use App\Modules\Content\Models\Menu\MenuFooter;
use App\Modules\Content\Models\Menu\MenuHeader;
use Illuminate\Database\Eloquent\Collection;

class Menus
{
    /**
     * @var Menus
     */
    protected static $instance;

    /**
     * @var Collection
     */
    protected $menu;

    /**
     * @var Collection
     */
    protected $header;

    /**
     * @var Collection
     */
    protected $footer;

    /**
     * @var \VmdCms\CoreCms\CoreModules\Content\Entity\MenuEntity
     */
    protected $menuEntity;


    protected function __construct()
    {
        $this->menuEntity = new MenuEntity();
        $this->menu = $this->menuEntity->getMenu();

        $this->header = $this->menuEntity->getMenuNavCollection($this->menu->where('key', MenuHeader::getModelKey()));
        $this->footer = $this->menuEntity->getMenuNavCollection($this->menu->where('key', MenuFooter::getModelKey()));
    }

    /**
     * @return Menus
     */
    public static function getInstance()
    {
        if (!self::$instance) self::$instance = new static();
        return self::$instance;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getFooter(): \Illuminate\Support\Collection
    {
        return $this->footer->getItems();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getHeader(): \Illuminate\Support\Collection
    {
        return $this->header->getItems();
    }

}
