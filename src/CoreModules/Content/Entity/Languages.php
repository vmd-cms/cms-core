<?php

namespace VmdCms\CoreCms\CoreModules\Content\Entity;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Schema;
use VmdCms\CoreCms\CoreModules\Languages\Models\CoreLanguage;
use VmdCms\CoreCms\Services\Config;
use VmdCms\CoreCms\Services\CoreRouter;

class Languages
{
    /**
     * @var Languages
     */
    protected static $instance;

    /**
     * @var Collection
     */
    protected $languages;

    /**
     * @var Collection
     */
    protected $languagesActive;


    /**
     * @var array
     */
    protected $locales;

    /**
     * @var string
     */
    protected $defaultLocale;

    /**
     * @var string
     */
    protected $defaultDashboardLocale;

    /**
     * @var CoreLanguage
     */
    protected $defaultLanguage;

    /**
     * @var CoreLanguage
     */
    protected $defaultDashboardLanguage;

    /**
     * @var string
     */
    protected $currentLocale;

    /**
     * @var string
     */
    protected $currentLocaleActive;

    /**
     * @var CoreLanguage
     */
    protected $currentLanguage;

    /**
     * @var CoreLanguage
     */
    protected $currentLanguageActive;

    protected $localePrefix = null;

    private function __construct(){
        if(!Schema::hasTable(CoreLanguage::table())) return;

        $languages = CoreLanguage::orderBy('order')->get();
        $this->languages = $languages ?? new \Illuminate\Support\Collection();
        $this->languagesActive = $languages ? $languages->where('active',1) : new \Illuminate\Support\Collection();
        $this->locales = $languages ? $languages->pluck('key')->toArray() : [];

        $this->defaultLanguage = $this->languages->where('default',1)->first();
        $this->defaultLocale = $this->defaultLanguage instanceof CoreLanguage ? $this->defaultLanguage->key : null;

        $dashboardBaseLang = Config::getInstance()->getDashboardBaseLang();
        $defaultDashboardLanguage = isset($dashboardBaseLang['key']) ? $this->languages->where('key',$dashboardBaseLang['key'])->first() : null;
        $this->defaultDashboardLanguage = $defaultDashboardLanguage instanceof CoreLanguage ? $defaultDashboardLanguage : $this->defaultLanguage;
        $this->defaultDashboardLocale = $this->defaultDashboardLanguage instanceof CoreLanguage ? $this->defaultDashboardLanguage->key : null;
    }

    /**
     * @param bool $forse
     * @return Languages|static
     */
    public static function getInstance(bool $forse = false)
    {
        if (!self::$instance || $forse) self::$instance = new static();
        return self::$instance;
    }

    public function getLocalePrefix(){
        if($this->localePrefix === null){
            $this->localePrefix = $this->getCurrentLocale() !== $this->getDefaultLocale() ? '/' . $this->getCurrentLocale() : '';
        }
        return  $this->localePrefix;
    }

    /**
     * @return CoreLanguage|null
     */
    public function getDefaultLanguage(): ?CoreLanguage
    {
        return $this->defaultLanguage;
    }

    /**
     * @return CoreLanguage|null
     */
    public function getDefaultDashboardLanguage(): ?CoreLanguage
    {
        return $this->defaultDashboardLanguage;
    }

    /**
     * @return null|string
     */
    public function getDefaultLocale(): ?string
    {
        return $this->defaultLocale;
    }

    /**
     * @return null|string
     */
    public function getDefaultDashboardLocale(): ?string
    {
        return $this->defaultDashboardLocale;
    }

    /**
     * @return null|Collection
     */
    public function getLanguages(): ?Collection
    {
        return $this->languages;
    }

    /**
     * @return array
     */
    public function getLocales(): array
    {
        return $this->locales;
    }

    /**
     * @param string $locale
     * @return bool
     */
    public function hasLanguage(string $locale): bool
    {
        return $this->languages->where('key',$locale)->first() ? true : false;
    }

    /**
     * @return null|Collection
     */
    public function getLanguagesActive(): ?Collection
    {
        return $this->languagesActive;
    }

    /**
     * @return CoreLanguage|null
     */
    public function getCurrentLanguageActive(): ?CoreLanguage
    {
        if(empty($this->currentLanguageActive)){
            $this->currentLanguageActive = $this->languagesActive->where('key',$this->getCurrentLocaleActive())->first();
        }
        return $this->currentLanguageActive;
    }

    /**
     * @return CoreLanguage|null
     */
    public function getCurrentLanguage(): ?CoreLanguage
    {
        if(empty($this->currentLanguage)){
            $this->currentLanguage = $this->languagesActive->where('key',$this->getCurrentLocale())->first();
        }
        return $this->currentLanguage;
    }

    /**
     * @return string
     */
    public function getCurrentLocaleActive(): string
    {
        if(empty($this->currentLocaleActive)){
            $this->currentLocaleActive = $this->isLocaleActive($this->getCurrentLocale()) ? $this->getCurrentLocale() : $this->getDefaultLocale();
        }
        return $this->currentLocaleActive;
    }

    /**
     * @return string
     */
    public function getCurrentLocale(): string
    {
        if(empty($this->currentLocale)){
            $this->currentLocale = CoreRouter::getInstance()->isAdminPanel() ? session()->get('locale',null) : app()->getLocale();
        }
        if(empty($this->currentLocale)){
            $this->currentLocale = CoreRouter::getInstance()->isAdminPanel() ? $this->getDefaultDashboardLocale() : $this->getDefaultLocale();
        }
        if(empty($this->currentLocale)){
            $language = $this->getLanguagesActive() ? $this->getLanguagesActive()->first() : null;
            $this->currentLocale = $language ? $language->key : null;
        }
        if(empty($this->currentLocale)){
            $this->currentLocale = app()->getLocale();
        }
        return $this->currentLocale;
    }

    public function setLocaleActive(string $locale){
        $this->currentLocaleActive = $this->setLocale($this->isLocaleActive($locale) ? $locale : $this->defaultLocale);
    }

    public function setLocale(string $locale){

        if($this->languages->where('key',$locale)->first()){
            session()->put('locale', $locale);
            app()->setLocale($locale);
            $this->currentLocale = $locale;
        }
    }

    protected function isLocaleActive($locale){
        return $this->languagesActive->where('key',$locale)->first() ? true : false;
    }

    public function refreshDashboardAppLocale()
    {
        if($sessionLocale = session()->get('locale',null))
        {
            app()->setLocale($sessionLocale);
            $this->currentLocale = $sessionLocale;
        }

    }
}
