<?php

namespace VmdCms\CoreCms\CoreModules\Content\Entity;

use App\Modules\Content\DTO\InfoPageDTO;
use App\Modules\Content\Models\Pages\Blogs\Blog;
use App\Modules\Content\Models\Pages\Blogs\BlogsPage;
use App\Modules\Content\Services\ContentRouter;
use App\Modules\Content\Services\DataShare;
use App\Modules\Products\Entity\ProductEntity;
use App\Modules\Products\Models\Components\ProductTranslate;
use VmdCms\CoreCms\DTO\Dashboard\BreadCrumbDto;
use VmdCms\CoreCms\Exceptions\Models\ModelNotFoundException;
use VmdCms\CoreCms\Services\BreadCrumbs;

class BlogPageEntity
{
    /**
     * @throws ModelNotFoundException
     */
    public function shareBlogsPageData()
    {
        if(!$page = BlogsPage::active()->first()){
            throw new ModelNotFoundException();
        }

        $dataShare = DataShare::getInstance();
        $dataShare->seo->setData($page);

        BreadCrumbs::getInstance()->prependItem(new BreadCrumbDto('',DataShare::getInstance()->getTranslate('blog')));
        $dataShare->appendData('breadCrumbs',BreadCrumbs::getInstance()->getItems());

        $dataShare->appendData('pageDTO',new InfoPageDTO($page));

        $blogEntity = new BlogEntity();
        $dataShare->appendData('blogs',$blogEntity->setLimit(12)->getCollection()->getItems());
        $dataShare->appendData('hasMore',$blogEntity->getHasMore());
    }

    /**
     * @param string $url
     * @throws ModelNotFoundException
     */
    public function shareBlogPageData(string $url)
    {
        if(!$page = (new Blog())->getModelByUrl($url)){
            throw new ModelNotFoundException();
        }

        $dataShare = DataShare::getInstance();
        $dataShare->seo->setData($page);

        $dataShare->translates->appendGroups([
            ProductTranslate::getModelGroup()
        ]);

        BreadCrumbs::getInstance()->prependItem(new BreadCrumbDto('',$page->info->title ?? null));
        BreadCrumbs::getInstance()->prependItem(new BreadCrumbDto(route(ContentRouter::ROUTE_BLOGS_PAGE,[],false),DataShare::getInstance()->getTranslate('blog')));
        $dataShare->appendData('breadCrumbs',BreadCrumbs::getInstance()->getItems());

        $dataShare->appendData('pageDTO',new InfoPageDTO($page));

        $productEntity = new ProductEntity();
        $dataShare->appendData('bestPrice',$productEntity->getBestPriceItems()->getItems());
        $dataShare->appendData('novetly',$productEntity->getNovetlyItems()->getItems());

        $dataShare->appendData('blogs',(new BlogEntity())->setExceptId($page->id)->getCollection()->getItems());
    }

    /**
     * @param int $page
     * @return array
     */
    public function getBlogsPageLoadMoreData(int $page){

        $dataShare = DataShare::getInstance(false);
        $infoPageEntity = new BlogEntity();
        $items = $infoPageEntity->setPage(++$page)->setLimit(12)->getCollection()->getItems();
        return [
            'content' => view('content::layouts.components.info_pages_item_cards',['blogs' => $items, 'data' => $dataShare])->render(),
            'hasMore' => $infoPageEntity->getHasMore(),
            'page' => $page
        ];

    }

}
