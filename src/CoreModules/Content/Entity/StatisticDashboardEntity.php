<?php

namespace VmdCms\CoreCms\CoreModules\Content\Entity;

use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use VmdCms\CoreCms\CoreModules\Content\DTO\Statistics\StatisticCharDTO;
use VmdCms\CoreCms\CoreModules\Content\DTO\Statistics\StatisticInfoTableDTO;
use VmdCms\Modules\Orders\Models\Order;
use VmdCms\Modules\Orders\Models\Params\OrderStatusParam;
use VmdCms\Modules\Users\Models\User;

class StatisticDashboardEntity
{
    /**
     * @return StatisticInfoTableDTO
     */
    public function getStatisticInfoData(): StatisticInfoTableDTO
    {
        $tableDTO = new StatisticInfoTableDTO();
        $tableDTO->setTitle('Краткий обзор');

        $orders = Order::with('items')->get();
        $orderTotalData = $this->getOrdersDataTotal($orders);
        $orderCurrentYearTotalData = $this->getOrdersDataTotal($orders->where('created_at','>',Carbon::now()->startOfYear()));
        $orderCurrentMonthTotalData = $this->getOrdersDataTotal($orders->where('created_at','>',Carbon::now()->startOfMonth()));
        $orderTodayTotalData = $this->getOrdersDataTotal($orders->where('created_at','>',Carbon::now()->startOfDay()));

        $orderNewStatus = OrderStatusParam::where('slug','new')->first();
        $orderNewTotalData = $this->getOrdersDataTotal($orders->where('status',$orderNewStatus->id ?? null));

        $users = User::all();

        $totalOrdersSum = $orderTotalData['total'] . " грн";
        $tableDTO->appendItem('#885AF8','Всего продано на сумму:',$totalOrdersSum);

        $totalYearOrdersSum = $orderCurrentYearTotalData['total'] . " грн";
        $tableDTO->appendItem('#885AF8','Всего продано в этом году на сумму:',$totalYearOrdersSum);

        $totalMonthOrdersSum = $orderCurrentMonthTotalData['total'] . " грн";
        $tableDTO->appendItem('#885AF8','Всего продано в этом месяце на сумму:',$totalMonthOrdersSum);

        $totalTodayOrdersSum = $orderTodayTotalData['total'] . " грн";
        $tableDTO->appendItem('#885AF8','Всего продано сегодня на сумму:',$totalTodayOrdersSum);

        $tableDTO->appendItem('#FFB946','Всего заказов:',$orderTotalData['count']);

        $tableDTO->appendItem('#FFB946','Заказы в этом году:',$orderCurrentYearTotalData['count']);

        $tableDTO->appendItem('#FFB946','Заказы в этом месяце:',$orderCurrentMonthTotalData['count']);

        $tableDTO->appendItem('#FFB946','Заказы сегодня:',$orderTodayTotalData['count']);

        $tableDTO->appendItem('#FFB946','Необработанные заказы:',$orderNewTotalData['count']);

        $countCustomers = count($users);
        $tableDTO->appendItem('#2ED47A','Всего покупателей:',$countCustomers);

        $countWaitingApprovedCustomers = count($users->where('confirmed',false));
        $tableDTO->appendItem('#F7685B','Покупателей, ожидающих подтверждения:',$countWaitingApprovedCustomers);

        return $tableDTO;
    }

    public function getStatisticOrdersCharDTO(): StatisticCharDTO
    {
        $orderGroups = $this->getOrdersCurrentMonth();
        $userGroups = $this->getUsersCurrentMonth();

        $categories = array_unique(array_merge(array_keys($orderGroups),array_keys($userGroups)));
        sort($categories);

        $char = new StatisticCharDTO($categories);
        $char->setTitle('Статистика');

        $ordersCount = $ordersTotal = $usersCount = [];
        foreach ($categories as $category){
            $ordersCount[] = isset($orderGroups[$category]) ? $orderGroups[$category]['count'] : 0;
            $ordersTotal[] = isset($orderGroups[$category]) ? ($orderGroups[$category]['total']/1000) : 0;
            $usersCount[] = isset($userGroups[$category]) ? $userGroups[$category]['count'] : 0;
        }

        $char->appendItem('#FFBB4B','Заказы',$ordersCount);
        $char->appendItem('#885AF8','Сумма заказов',$ordersTotal);
        $char->appendItem('#109CF1','Покупатель',$usersCount);

        return $char;
    }

    protected function getUsersCurrentMonth(){
        $monthStart = Carbon::now()->startOfMonth();
        $users = User::where('created_at','>',$monthStart)->get();
        $groups = $this->getUsersData($users);
        return $groups;
    }

    protected function getOrdersCurrentMonth()
    {
        $monthStart = Carbon::now()->startOfMonth();
        $orders = Order::where('created_at','>',$monthStart)->with('items')->get();
        $groups = $this->getOrdersDataGroupedByDate($orders);
        return $groups;
    }

    protected function getOrdersDataGroupedByDate(Collection $orders){
        $dateGroup = [];
        if(is_countable($orders)){
            foreach ($orders as $order){
                $orderDate = Carbon::parse($order->created_at)->format('Y-m-d');

                if(!isset($dateGroup[$orderDate])){
                    $dateGroup[$orderDate] = [
                        'total' => 0,
                        'count' => 0
                    ];
                }
                $dateGroup[$orderDate]['total'] = $order->orderTotal;
                $dateGroup[$orderDate]['count'] += 1;
            }
        }
        return $dateGroup;
    }

    protected function getOrdersDataTotal(Collection $orders){
        $result = [
            'total' => 0,
            'count' => 0
        ];
        if(is_countable($orders)){
            foreach ($orders as $order){
                $result['total'] += $order->orderTotal;
                $result['count'] += 1;
            }
        }
        return $result;
    }

    protected function getUsersData(Collection $users){
        $dateGroup = [];
        if(is_countable($users)){
            foreach ($users as $user){
                $orderDate = Carbon::parse($user->created_at)->format('Y-m-d');

                if(!isset($dateGroup[$orderDate])){
                    $dateGroup[$orderDate] = [
                        'count' => 0
                    ];
                }
                $dateGroup[$orderDate]['count'] += 1;
            }
        }
        return $dateGroup;
    }


}
