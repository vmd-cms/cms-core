<?php

namespace VmdCms\CoreCms\CoreModules\Content\Entity;

use App\Modules\Content\DTO\InfoPageDTO;
use App\Modules\Content\Models\Pages\Blogs\Blog;
use VmdCms\CoreCms\CoreModules\Content\Collections\InfoPagesCollection;

class BlogEntity
{
    protected $mainPageOnly;
    protected $exceptId;
    protected $limit;
    protected $page;
    protected $hasMore;

    public function __construct()
    {
        $this->mainPageOnly = false;
        $this->exceptId = null;
        $this->limit = 4;
        $this->page = 1;
        $this->hasMore = false;
    }

    /**
     * @param $mainPageOnly
     * @return $this
     */
    public function setMainPageOnly(bool $mainPageOnly): self
    {
        $this->mainPageOnly = $mainPageOnly;
        return $this;
    }

    /**
     * @param int $exceptId
     * @return $this
     */
    public function setExceptId(int $exceptId): self
    {
        $this->exceptId = $exceptId;
        return $this;
    }

    /**
     * @param int $limit
     * @return $this
     */
    public function setLimit(int $limit): self
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @param int $page
     * @return $this
     */
    public function setPage(int $page): self
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return bool
     */
    public function getHasMore(): bool
    {
        return $this->hasMore;
    }

    /**
     * @return InfoPagesCollection
     */
    public function getCollection(): InfoPagesCollection
    {
        $infoPageCollection = new InfoPagesCollection();
        $query = Blog::active()->orderBy('public_at', 'desc')->limit($this->limit + 1);
        if ($this->mainPageOnly) {
            $query->where('flag', true);
        }
        if (!empty($this->exceptId)) {
            $query->where('id', '!=', $this->exceptId);
        }
        if ($this->page > 1) {
            $query->skip(($this->page - 1) * $this->limit);
        }
        $infoPages = $query->with('info')->get();

        if (is_countable($infoPages) && count($infoPages)) {
            foreach ($infoPages as $index => $infoPage) {
                if ($this->hasMore = $index >= $this->limit) {
                    break;
                }
                $infoPageCollection->append(new InfoPageDTO($infoPage));
            }
        }
        return $infoPageCollection;
    }

}
