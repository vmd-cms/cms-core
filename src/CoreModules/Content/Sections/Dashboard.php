<?php

namespace VmdCms\CoreCms\CoreModules\Content\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Contracts\Models\StaticInstanceInterface;
use VmdCms\CoreCms\CoreModules\Content\Dashboard\Panels\LastOrdersPanel;
use VmdCms\CoreCms\CoreModules\Content\Dashboard\Panels\OrdersStatisticCharPanel;
use VmdCms\CoreCms\CoreModules\Content\Dashboard\Panels\StatisticInfoPanel;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Dashboard\Display\Panels\SectionPanel;
use VmdCms\CoreCms\Dashboard\Display\PanelsDisplayWrapper;
use VmdCms\CoreCms\Exceptions\CoreException;
use VmdCms\CoreCms\Exceptions\Modules\ModuleNotActiveException;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Models\CmsSection;
use VmdCms\CoreCms\Traits\Models\HasStaticInstance;

class Dashboard extends CmsSection implements StaticInstanceInterface
{
    use HasStaticInstance;

    /**
     * @var string
     */
    protected $slug = '';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return '';
    }

    public function getCmsModelClass(): ?string
    {
        return \VmdCms\CoreCms\CoreModules\Content\Models\MockCmsModel::class;
    }

    /**
     * @return DisplayInterface
     */
    public function display()
    {
        $display = new PanelsDisplayWrapper();

        $panels = [];

        try {
            $statistic = new SectionPanel();
            $statistic->setAdditionalPanels([(new OrdersStatisticCharPanel()),(new StatisticInfoPanel())]);
            $panels[] = $statistic;
        }catch (\Exception $exception){}

        try {
            if(!class_exists(\App\Modules\Orders\Models\Order::class)){
                throw new ModuleNotActiveException();
            }
            $panels[] = (new LastOrdersPanel());
        }catch (\Exception $exception){}

        $display->setSectionPanels($panels);
        return $display;
    }

    public function isCreatable(): bool
    {
        return false;
    }

    public function isViewable(CmsModel $model = null): bool
    {
        return false;
    }

    public function isDeletable(CmsModel $model = null): bool
    {
        return false;
    }

    public function isEditable(CmsModel $model = null): bool
    {
        return false;
    }
}
