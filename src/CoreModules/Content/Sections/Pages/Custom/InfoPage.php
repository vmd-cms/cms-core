<?php

namespace VmdCms\CoreCms\CoreModules\Content\Sections\Pages\Custom;

use App\Modules\Content\Sections\Pages\Page;
use VmdCms\CoreCms\Collections\FormTabsCollection;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Dashboard\Forms\Components\FormTab;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;

class InfoPage extends Page
{
    /**
     * @var string
     */
    protected $slug = 'info_page';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('info_pages');
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::input('info.title',CoreLang::get('title'))->required(),
            FormComponent::url('url',CoreLang::get('url'))->setDependedField('info.title')->unique(),
            FormComponent::switch('active'),
            FormComponent::switch('flag'),
            FormComponent::image('photo_preview',CoreLang::get('preview')),
        ]);
    }


    public function getCmsModelClass(): string
    {
        return \App\Modules\Content\Models\Pages\Custom\InfoPage::class;
    }
}
