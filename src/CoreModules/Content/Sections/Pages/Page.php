<?php

namespace VmdCms\CoreCms\CoreModules\Content\Sections\Pages;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\ColumnEditable;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Models\CmsSection;
use VmdCms\Modules\Users\Models\User;

class Page extends CmsSection
{
    /**
     * @var string
     */
    protected $slug = 'pages';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('pages');
    }

    public function display()
    {
        return Display::dataTable([
            Column::text('id','#'),
            Column::text('title')
            ->setSearchableCallback(function ($query, $search) {
                $query->where('title', 'like', '%' . $search . '%');
            })->setSortableCallback(function ($query, $sortType) {
                $query->orderBy('title', $sortType);
            }),
            ColumnEditable::switch('active'),
            Column::date('created_at')->setFormat('Y-m-d'),
        ])->setSearchable(true);
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return $this->edit(null);
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::input('title')->required(),
            FormComponent::switch('active'),
        ]);
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Content\Models\Pages\Page::class;
    }
}
