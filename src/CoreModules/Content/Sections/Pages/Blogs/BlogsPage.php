<?php

namespace VmdCms\CoreCms\CoreModules\Content\Sections\Pages\Blogs;

use App\Modules\Content\Sections\Pages\Page;
use VmdCms\CoreCms\Collections\FormTabsCollection;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Dashboard\Forms\Components\FormTab;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;

class BlogsPage extends Page
{
    /**
     * @var string
     */
    protected $slug = 'blogs_page';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('blogs_page');
    }

    public function display()
    {
        $model = \App\Modules\Content\Models\Pages\Blogs\BlogsPage::first();
        if(empty($model))
        {
            $model = new \App\Modules\Content\Models\Pages\Blogs\BlogsPage();
            $model->save();
        }

        return $this->renderEdit($model->id);
    }

    public function isCreatable(): bool
    {
        return false;
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        $tabs = new FormTabsCollection();
        $tabs->appendItems([
            new FormTab(CoreLang::get('main'),[
                FormComponent::input('info.title',CoreLang::get('title'))->required(),
                FormComponent::switch('active'),
            ]),
            new FormTab('SEO',[
                FormComponent::seo('seo',''),
            ]),
        ]);
        return Form::tabbed($tabs);
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Content\Models\Pages\Blogs\BlogsPage::class;
    }
}
