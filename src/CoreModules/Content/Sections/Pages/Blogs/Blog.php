<?php

namespace VmdCms\CoreCms\CoreModules\Content\Sections\Pages\Blogs;

use App\Modules\Content\Sections\Pages\Page;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\DTO\Dashboard\Components\CroppedDimensionDTO;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;

class Blog extends Page
{
    /**
     * @var string
     */
    protected $slug = 'blog';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('blog_page');
    }

    public function display()
    {
        return Display::dataTable([
            Column::text('info.title',CoreLang::get('title')),
            Column::text('flag', CoreLang::get('is_show_on_main_page')),
            Column::text('active'),
            Column::date('created_at')->setFormat('Y-m-d H:i'),
        ])->setSearchable(true);
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return Form::panel([
            FormComponent::input('info.title',CoreLang::get('title'))->required(),
            FormComponent::url('url',CoreLang::get('url'))->setDependedField('info.title')->required()->unique(),
            FormComponent::image('photo_preview',CoreLang::get('preview')),
        ]);
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::switch('flag',CoreLang::get('is_show_on_main_page')),
            FormComponent::switch('active'),
            FormComponent::image('photo_preview',CoreLang::get('preview'))->copyToWEBP()
                ->appendCroppedDimension((new CroppedDimensionDTO('400x300',400,300)))
                ->appendCroppedDimension((new CroppedDimensionDTO('1024x768',1024,768))),
            FormComponent::input('info.title',CoreLang::get('title'))->required(),
            FormComponent::url('url',CoreLang::get('url'))->setDependedField('info.title')->required()->unique(),
            FormComponent::image('photo',CoreLang::get('image'))
                ->copyToWEBP()
                ->appendCroppedDimension((new CroppedDimensionDTO('300x250',300,250)))
                ->appendCroppedDimension((new CroppedDimensionDTO('968x624',968,624))),
            FormComponent::ckeditor('info.body',CoreLang::get('content')),
        ]);
    }


    public function getCmsModelClass(): string
    {
        return \App\Modules\Content\Models\Pages\Blogs\Blog::class;
    }
}
