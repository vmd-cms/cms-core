<?php

namespace VmdCms\CoreCms\CoreModules\Content\Sections\Blocks;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Route;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Contracts\Sections\AdminSectionInterface;
use \App\Modules\Content\Services\BlockSectionFactory;
use \App\Modules\Content\Services\BlockEnum;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Exceptions\Sections\SectionCmsModelException;
use VmdCms\CoreCms\Exceptions\Sections\SectionNotFoundException;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Models\CmsSection;
use VmdCms\CoreCms\Services\CoreRouter;

class Block extends CmsSection
{
    /**
     * @var string
     */
    protected $slug = 'blocks';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('blocks');
    }

    public function display()
    {
        return Display::dataTable([
            Column::text('key'),
            Column::text('description'),
            Column::text('active'),
        ])->setSearchable(true)->setWhereQuery([['parent_id', '=', null]]);
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return Form::panel([
            FormComponent::select('key')->setEnumValues(BlockEnum::getEnums())->unique()->required(),
            FormComponent::switch('active'),
            FormComponent::input('description')
        ]);
    }

    /**
     * @param int|null $id
     * @return FormInterface
     * @throws SectionCmsModelException
     * @throws SectionNotFoundException
     */
    public function edit(?int $id) : FormInterface
    {
        $section = $this->getInheritSection();
        return $section->edit($id);
    }

    protected $inheritSection;

    /**
     * @return AdminSectionInterface
     * @throws SectionCmsModelException
     * @throws SectionNotFoundException
     */
    protected function getInheritSection()
    {
        if(Route::current()->getName() != CoreRouter::ROUTE_EDIT_GET &&
            Route::current()->getName() != CoreRouter::ROUTE_EDIT_PUT &&
            empty(request()->id))
        {
            throw new SectionCmsModelException();
        }

        if(!$this->inheritSection instanceof AdminSectionInterface)
        {
            $baseModelClass = $this->getBaseCmsModelClass();
            $model = $baseModelClass::find(request()->id);
            if(!$model instanceof \VmdCms\CoreCms\CoreModules\Content\Models\Blocks\Block) throw new ModelNotFoundException();
            $this->inheritSection =  (new BlockSectionFactory())->getBlockModel($model->key);
        }
        return $this->inheritSection;
    }

    /**
     * @return string
     */
    protected function getBaseCmsModelClass()
    {
        return \App\Modules\Content\Models\Blocks\Block::class;
    }

    public function getCmsModelClass(): string
    {
        try {
            return $this->getInheritSection()->getCmsModelClass();
        }
        catch (\Exception $exception)
        {
            return $this->getBaseCmsModelClass();
        }
    }
}
