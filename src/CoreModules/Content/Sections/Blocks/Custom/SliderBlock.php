<?php

namespace VmdCms\CoreCms\CoreModules\Content\Sections\Blocks\Custom;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\CoreModules\Content\Sections\Blocks\Groups\GeneralGroupBlock;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Facades\FormTableColumn;

class SliderBlock extends GeneralGroupBlock
{
    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::input('key')->setDisabled(true),
            FormComponent::switch('active'),
            FormComponent::input('description'),
            FormComponent::input('title'),
            FormComponent::ckeditor('info.body',CoreLang::get('content')),
            FormComponent::datatable('children', CoreLang::get('slides'))->setComponents([
                FormTableColumn::image('media',CoreLang::get('image'))->setHelpText('help text'),
                FormTableColumn::colorPicker('info.data',CoreLang::get('title')),
                FormTableColumn::switch('active'),
            ])->setIsDeletable(true)->setPaddingTop(100)->displayRow(),
        ]);
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Content\Models\Blocks\Custom\SliderBlock::class;
    }

}

