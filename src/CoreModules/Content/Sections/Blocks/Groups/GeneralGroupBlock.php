<?php

namespace VmdCms\CoreCms\CoreModules\Content\Sections\Blocks\Groups;

use VmdCms\CoreCms\CoreModules\Content\Sections\Blocks\Block;

class GeneralGroupBlock extends Block
{
    /**
     * @var string
     */
    protected $slug = 'general_blocks';

    /**
     * @return string
     */
    protected function getBaseCmsModelClass()
    {
        return \App\Modules\Content\Models\Blocks\Groups\GeneralGroupBlock::class;
    }
}
