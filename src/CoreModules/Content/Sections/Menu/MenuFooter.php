<?php

namespace VmdCms\CoreCms\CoreModules\Content\Sections\Menu;

use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;

class MenuFooter extends Menu
{
    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('menu_footer');
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Content\Models\Menu\MenuFooter::class;
    }
}
