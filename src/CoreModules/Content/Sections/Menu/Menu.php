<?php

namespace VmdCms\CoreCms\CoreModules\Content\Sections\Menu;

use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Dashboard\Display\Filters\FilterDisplay;
use VmdCms\CoreCms\Dashboard\Display\Filters\FilterTreeComponent;
use VmdCms\CoreCms\Facades\ColumnEditable;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Models\CmsSection;

class Menu extends CmsSection
{
    /**
     * @var string
     */
    protected $slug = 'menu';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('menu');
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return $this->edit(null);
    }

    /**
     * @return DisplayInterface
     */
    public function display()
    {
        $filter = (new FilterDisplay())
            ->appendFilterComponent((new FilterTreeComponent('info.title'))
                ->setHeadTitle($this->getTitle())
                ->setModelClass($this->getCmsModelClass())
                ->setFilterField('parent_id')
                ->setFilterSlug('parent_id')
            );
        return Display::dataTable([
            ColumnEditable::text('info.title',CoreLang::get('title'))->maxLength(255),
            ColumnEditable::text('url',CoreLang::get('url'))->unique()->maxLength(128),
            ColumnEditable::switch('active')->alignCenter(),
        ])->whereNull('parent_id')->setSearchable(true)->setFilter($filter)->orderable();
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::radio('active'),
            FormComponent::input('info.title',CoreLang::get('title'))->required(),
            FormComponent::url('url',CoreLang::get('url'))->setDependedField('info.title')->unique(),
            FormComponent::select('parent_id',CoreLang::get('parent'))
                ->setModelForOptions($this->getCmsModelClass())
                ->setDisplayField('info.title')
                ->setForeignField('id')
                ->setWhere(['parent_id','=',null])
                ->nullable()
        ])->setHeadTitle(CoreLang::get('editing_menu'), 'info.title');
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Content\Models\Menu\Menu::class;
    }
}
