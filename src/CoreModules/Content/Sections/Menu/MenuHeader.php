<?php

namespace VmdCms\CoreCms\CoreModules\Content\Sections\Menu;

use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;

class MenuHeader extends Menu
{
    /**
     * @var string
     */
    protected $slug = 'menu_header';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('menu_header');
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Content\Models\Menu\MenuHeader::class;
    }
}
