<?php

namespace VmdCms\CoreCms\CoreModules\Content\Controllers;

use App\Modules\Content\Entity\PageEntity;
use Illuminate\Http\Request;
use VmdCms\CoreCms\Controllers\CoreController;
use App\Modules\Content\Entity\BlogPageEntity;

class BlogController extends CoreController
{

    public function blogsPage(){
        (new BlogPageEntity())->shareBlogsPageData();
        return view("content::layouts.pages.blog.blogs_page")->render();
    }

    public function blogsPageLoadMore(Request $request){
        $request->validate([
            'page' => 'int|required'
        ]);
        $result = (new BlogPageEntity())->getBlogsPageLoadMoreData($request->get('page'));
        return json_encode($result);
    }

    public function blogPage($url){
        (new BlogPageEntity())->shareBlogPageData($url);
        return view("content::layouts.pages.blog.blog_page")->render();
    }
}
