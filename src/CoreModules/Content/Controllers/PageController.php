<?php

namespace VmdCms\CoreCms\CoreModules\Content\Controllers;

use App\Modules\Content\Entity\PageEntity;
use Illuminate\Http\Request;
use VmdCms\CoreCms\Controllers\CoreController;

class PageController extends CoreController
{
    public function mainPage(Request $request)
    {
        (new PageEntity())->shareMainPageData();
        return view("content::layouts.pages.main.main_page")->render();
    }
}
