<?php

namespace VmdCms\CoreCms\CoreModules\Notifications\Enums;

class NotificationStatusesEnums
{
    const STATUS_SEND = 'send';
    const STATUS_ERROR = 'error';
}
