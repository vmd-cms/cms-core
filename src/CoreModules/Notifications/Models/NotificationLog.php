<?php

namespace VmdCms\CoreCms\CoreModules\Notifications\Models;

use VmdCms\CoreCms\Models\CmsModel;

class NotificationLog extends CmsModel
{
    public static function table(): string
    {
        return 'notification_logs';
    }
}
