<?php

namespace VmdCms\CoreCms\CoreModules\Notifications\Services\Mail\Handlers;

use Illuminate\Support\Facades\Mail;
use VmdCms\CoreCms\CoreModules\Events\Contracts\DTO\CoreEventDTOInterface;
use VmdCms\CoreCms\CoreModules\Notifications\Entity\TemplateRender;
use VmdCms\CoreCms\CoreModules\Events\Enums\CoreEventGroupEnums;
use VmdCms\CoreCms\CoreModules\Events\Enums\CoreEventTemplateEnums;
use VmdCms\CoreCms\CoreModules\Events\Exceptions\CoreEventDependencyException;
use VmdCms\CoreCms\CoreModules\Events\Exceptions\CoreEventModelNotFoundException;
use VmdCms\CoreCms\CoreModules\Events\Models\CoreEvent;
use VmdCms\CoreCms\CoreModules\Events\Models\CoreEventDependency;
use VmdCms\CoreCms\CoreModules\Events\Models\Templates\CoreEventTemplate;
use VmdCms\CoreCms\CoreModules\Notifications\Enums\NotificationStatusesEnums;
use VmdCms\CoreCms\CoreModules\Notifications\Models\NotificationLog;
use VmdCms\CoreCms\CoreModules\Settings\Entity\SettingsEntity;
use VmdCms\CoreCms\Services\Logger;
use VmdCms\CoreCms\CoreModules\Notifications\Services\Mail\DTO\MailDTO;
use VmdCms\CoreCms\CoreModules\Notifications\Services\Mail\MailService;

class MailServiceHandler
{
    /**
     * @var CoreEventDependency
     */
    protected $dependency;

    /**
     * @var CoreEventDTOInterface
     */
    protected $coreEventDTO;

    /**
     * @var NotificationLog
     */
    protected $notificationLog;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * MailServiceHandler constructor.
     * @param CoreEventDependency $dependency
     * @param CoreEventDTOInterface $coreEventDTO
     */
    public function __construct(CoreEventDependency $dependency, CoreEventDTOInterface $coreEventDTO)
    {
        $this->dependency = $dependency;
        $this->coreEventDTO = $coreEventDTO;
        $this->logger = new Logger('mail_service_handler');
        $this->notificationLog = new NotificationLog();
        $this->notificationLog->core_event_id = $dependency->core_event_id;
        $this->notificationLog->core_event_dependency_id = $dependency->id;
        $this->notificationLog->core_event_template_id = $dependency->core_event_template_id;
        $this->notificationLog->type = $dependency->type;
        $this->notificationLog->group = $dependency->group;
    }


    public function execute()
    {
        try {
            $this->logger->info('Start execute MailServiceHandler event: '
                . $this->coreEventDTO->getSlug() . ' dependency #' . $this->dependency->id);
            $mailDTO = $this->getPreparedMailDTO();
            $this->notificationLog->recipients = json_encode($mailDTO->getEmails());
            $this->notificationLog->subject = $mailDTO->getSubject();
            $this->notificationLog->body = $mailDTO->getBody();
            foreach ($mailDTO->getEmails() as $recipient) {
                Mail::to($recipient)->send(new MailService($mailDTO));
            }
            $this->notificationLog->status = NotificationStatusesEnums::STATUS_SEND;
        }
        catch (\Exception $exception){
            $this->logger->error('Error execute MailServiceHandler');
            $this->logger->error($exception->getMessage());
            $this->notificationLog->error = $exception->getMessage();
            $this->notificationLog->status = NotificationStatusesEnums::STATUS_ERROR;
        }
        $this->notificationLog->save();
        $this->logger->info('Finish execute MailServiceHandler');
    }

    /**
     * @return MailDTO
     * @throws CoreEventDependencyException
     * @throws CoreEventModelNotFoundException
     */
    public function getPreparedMailDTO(){
        $coreEventModel = $this->dependency->coreEvent ?? null;
        if(!$coreEventModel instanceof CoreEvent){
            throw new CoreEventModelNotFoundException();
        }

        switch ($this->dependency->group){
            case CoreEventGroupEnums::ADMIN: $emails = SettingsEntity::getInstance()->getAdminEmails();
                break;
            case CoreEventGroupEnums::USER: $emails = $this->coreEventDTO->getPayloadValue('user_email');
                break;
            default: throw new CoreEventDependencyException('Wrong group:' . $this->dependency->group);
        }

        if((is_array($emails) && !count($emails)) || empty($emails)){
            throw new CoreEventDependencyException('Empty recipients emails. Group: ' . $this->dependency->group);
        }

        if(!is_array($emails)){
            $emails = array($emails);
        }

        $template = $this->dependency->coreEventTemplate ?? null;
        if(!$template instanceof CoreEventTemplate){
            throw new CoreEventDependencyException('Empty CoreEventTemplate');
        }

        if(!$template->active){
            throw new CoreEventDependencyException('CoreEventTemplate is not active');
        }

        if($template->type !== CoreEventTemplateEnums::TYPE_MAIL){
            throw new CoreEventDependencyException('Wrong CoreEventTemplate type:' . $template->type);
        }

        $templateRender = new TemplateRender($this->coreEventDTO);
        $subject = $templateRender->renderTemplate($template->subject);
        $body = $templateRender->renderTemplate($template->body);

        return new MailDTO($emails,$subject,$body);
    }


   /*
    protected function replaceVariables(string $template): string
    {
        preg_match_all("/\{[^\]]*\}/", $template, $matches);

        if(!is_countable($matches) || !count($matches)) return $template;

        $searchArr = $replaceArr = [];

        for ($i = 0; $i < count($matches); $i++){

            $search = $matches[$i][0] ?? null;
            if(empty($search)) continue;
            $searchArr[$i] = $search;
            $path = str_replace(['{','}'],['',''],$search);
            if(empty($path)) continue;
            $replaceArr[$i] = $this->coreEventDTO->getPayloadValue($path);;
        }

        return str_replace($searchArr,$replaceArr,$template);
    }
   */
}
