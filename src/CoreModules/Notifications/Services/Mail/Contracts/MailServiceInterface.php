<?php

namespace VmdCms\CoreCms\CoreModules\Notifications\Services\Mail\Contracts;

interface MailServiceInterface
{
    /**
     * MailServiceInterface constructor.
     * @param MailDTOInterface $dto
     */
    public function __construct(MailDTOInterface $dto);

}
