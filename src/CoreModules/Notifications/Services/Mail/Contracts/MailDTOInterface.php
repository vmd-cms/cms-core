<?php

namespace VmdCms\CoreCms\CoreModules\Notifications\Services\Mail\Contracts;

interface MailDTOInterface
{
    /**
     * MailDTOInterface constructor.
     * @param array $emails
     * @param string $subject
     * @param string $body
     */
    public function __construct(array $emails,string $subject,string $body);

    /**
     * @return array
     */
    public function getEmails(): array;

    /**
     * @return string
     */
    public function getSubject(): string;

    /**
     * @return string
     */
    public function getBody(): string;
}
