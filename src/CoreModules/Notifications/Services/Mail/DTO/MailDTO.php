<?php

namespace VmdCms\CoreCms\CoreModules\Notifications\Services\Mail\DTO;

use VmdCms\CoreCms\CoreModules\Notifications\Services\Mail\Contracts\MailDTOInterface;

class MailDTO implements MailDTOInterface
{
    /**
     * @var array
     */
    protected $emails;

    /**
     * @var string
     */
    protected $subject;

    /**
     * @var string
     */
    protected $body;
    /**
     * MailDTO constructor.
     * @param array $emails
     * @param string $subject
     * @param string $body
     */
    public function __construct(array $emails, string $subject, string $body)
    {
        $this->emails = $emails;
        $this->subject = $subject;
        $this->body = $body;
    }

    /**
     * @return array
     */
    public function getEmails(): array
    {
        return $this->emails;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }
}
