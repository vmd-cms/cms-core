<?php

namespace VmdCms\CoreCms\CoreModules\Notifications\Services\Mail\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use VmdCms\CoreCms\CoreModules\Events\Contracts\DTO\CoreEventDTOInterface;
use VmdCms\CoreCms\CoreModules\Events\Models\CoreEventDependency;
use VmdCms\CoreCms\CoreModules\Notifications\Services\Mail\Handlers\MailServiceHandler;

class MailServiceJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const QUEUE = 'services';

    protected $dependency;

    protected $coreEventDTO;

    /**
     * MailServiceJob constructor.
     * @param CoreEventDependency $dependency
     * @param CoreEventDTOInterface $coreEventDTO
     */
    public function __construct(CoreEventDependency $dependency, CoreEventDTOInterface $coreEventDTO)
    {
        $this->dependency = $dependency;
        $this->coreEventDTO = $coreEventDTO;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        (new MailServiceHandler($this->dependency,$this->coreEventDTO))->execute();
    }
}
