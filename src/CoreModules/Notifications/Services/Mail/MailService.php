<?php

namespace VmdCms\CoreCms\CoreModules\Notifications\Services\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use VmdCms\CoreCms\CoreModules\Notifications\Services\Mail\Contracts\MailDTOInterface;
use VmdCms\CoreCms\CoreModules\Notifications\Services\Mail\Contracts\MailServiceInterface;

class MailService extends Mailable implements MailServiceInterface
{
    use Queueable, SerializesModels;

    public $mailDTO;

    /**
     * MailService constructor.
     * @param MailDTOInterface $dto
     */
    public function __construct(MailDTOInterface $dto)
    {
        $this->mailDTO = $dto;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->mailDTO->getSubject())->html($this->mailDTO->getBody());
    }
}
