<?php

namespace VmdCms\CoreCms\CoreModules\Notifications\Entity;

use VmdCms\CoreCms\CoreModules\Events\Contracts\DTO\CoreEventDTOInterface;

class TemplateRender
{
    /**
     * @var CoreEventDTOInterface
     */
    protected $coreEventDTO;

    /**
     * TemplateRender constructor.
     * @param CoreEventDTOInterface $coreEventDTO
     */
    public function __construct(CoreEventDTOInterface $coreEventDTO){
        $this->coreEventDTO = $coreEventDTO;
    }

    /**
     * @param string $template
     * @return string
     */
    public function renderTemplate(string $template):string
    {
        $template = $this->renderStructures($template);
        $template = $this->renderVariables($template);
        return $template;
    }

    protected function renderStructures(string $template): string{
        return $this->renderControlStructures($template);
    }

    protected function renderControlStructures(string $template): string{

        preg_match_all("/{%if(.*?)endif%}/", $template, $matches);
        $controlStructures = $matches[0] ? $matches[0] : null;

        if(!is_countable($controlStructures) || !count($controlStructures)){
           return $template;
        }

        $_low_or_equals = '<=';
        $_bigger_or_equals = '>=';
        $_equals = '==';
        $_not_equals = '!=';
        $_low = '<';
        $_bigger = '>';


        for ($i=0; $i < count($controlStructures); $i++){

            $parts = explode('{%else%}',$controlStructures[$i]);

            $ifBodyStr = array_shift($parts);
            $ifConditionStr = substr($ifBodyStr,0,mb_strpos($controlStructures[$i],'%}')+3);
            $ifCondition = trim(substr($ifConditionStr,4,-3));
            $ifConditionBody = $this->renderVariables(str_replace($ifConditionStr,'',$ifBodyStr));

            if(strpos($ifCondition,$_low_or_equals) != false) $type = $_low_or_equals;
            elseif(strpos($ifCondition,$_bigger_or_equals) != false) $type = $_bigger_or_equals;
            elseif(strpos($ifCondition,$_equals) != false) $type = $_equals;
            elseif(strpos($ifCondition,$_not_equals) != false) $type = $_not_equals;
            elseif(strpos($ifCondition,$_low) != false) $type = $_low;
            elseif(strpos($ifCondition,$_bigger) != false) $type = $_bigger;
            else $type = null;

            if(empty($type)) continue;

            $variables = explode($type,$ifCondition);
            $firstVarStr = trim(head($variables));
            $secondVarStr = trim(last($variables));

            $firstVar = $this->renderVariables($firstVarStr);
            $secondVar = $this->renderVariables($secondVarStr);

            switch ($type){
                case $_bigger: $value = $firstVar > $secondVar;
                break;
                case $_bigger_or_equals: $value = $firstVar >= $secondVar;
                break;
                case $_low: $value = $firstVar < $secondVar;
                break;
                case $_low_or_equals: $value = $firstVar <= $secondVar;
                break;
                case $_equals: $value = $firstVar == $secondVar;
                break;
                case $_not_equals: $value = $firstVar != $secondVar;
                break;
                default: $value = false;
            }

            if($value){
                $template = str_replace($controlStructures[$i],$ifConditionBody,$template);
                continue;
            }


            $elseBodyStr = array_pop($parts);
            if(empty($elseBodyStr)){
                $template = str_replace($controlStructures[$i],'',$template);
                continue;
            }

            $elseBodyStr = str_replace('{%endif%}','',$elseBodyStr);
            $elseBody = $this->renderVariables($elseBodyStr);

            $template = str_replace($controlStructures[$i],$elseBody,$template);
        }
        return $template;
    }

    protected function renderVariables(string $template): string
    {
        preg_match_all("/\[(.*?)\]/", $template, $matches);
        $variables = $matches[0] ? $matches[0] : null;
        if(!is_countable($variables) || !count($variables)){
            return $template;
        }

        for ($i = 0; $i < count($variables); $i++)
        {
            $variablePath = str_replace(['[',']'],['',''],$variables[$i]);
            $variable = $this->coreEventDTO->getPayloadValue($variablePath);
            $template = str_replace($variables[$i],$variable,$template);
        }
        return $template;
    }
}
