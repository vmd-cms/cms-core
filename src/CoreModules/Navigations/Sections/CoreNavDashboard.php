<?php

namespace VmdCms\CoreCms\CoreModules\Navigations\Sections;

use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Traits\Models\HasStaticInstance;

class CoreNavDashboard extends CoreNav
{
    use HasStaticInstance;

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('navigation_dashboard');
    }

    /**
     * @var string
     */
    protected $slug = 'navigation-dashboard';

    public function getCmsModelClass(): string
    {
        return \VmdCms\CoreCms\CoreModules\Navigations\Models\CoreNavDashboard::class;
    }
}
