<?php

namespace VmdCms\CoreCms\CoreModules\Navigations\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Contracts\Models\StaticInstanceInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\CoreModules\Sections\Models\CoreSection;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Models\CmsSection;

abstract class CoreNav extends CmsSection implements StaticInstanceInterface
{
    public function display()
    {
        return Display::tree()->setDisplayField('title')->setMaxLevel(3);
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::switch('active'),
            FormComponent::select('parent_id',CoreLang::get('parent'))
               ->setModelForOptions($this->getCmsModelClass())
               ->setDisplayField('title'),
            FormComponent::select('section_id', CoreLang::get('section'))
                ->setModelForOptions(CoreSection::class)
                ->setDisplayField('title'),
            FormComponent::input('title','Base title')->required(),
            FormComponent::input('info.title',CoreLang::get('title'))->required(),
        ]);
    }

    public function create(): FormInterface
    {
        return $this->edit(null);
    }
}
