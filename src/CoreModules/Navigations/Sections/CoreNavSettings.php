<?php

namespace VmdCms\CoreCms\CoreModules\Navigations\Sections;

use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Traits\CrudPolicy\ProtectedCrudPolicy;
use VmdCms\CoreCms\Traits\Models\HasStaticInstance;

class CoreNavSettings extends CoreNav
{
    use HasStaticInstance,ProtectedCrudPolicy;

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('navigation_settings');
    }

    /**
     * @var string
     */
    protected $slug = 'navigation-settings';

    public function getCmsModelClass(): string
    {
        return \VmdCms\CoreCms\CoreModules\Navigations\Models\CoreNavSettings::class;
    }
}
