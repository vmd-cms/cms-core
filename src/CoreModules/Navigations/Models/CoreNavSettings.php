<?php

namespace VmdCms\CoreCms\CoreModules\Navigations\Models;

class CoreNavSettings extends CoreNav
{
    const ANCHOR_KEY = 'nav_settings';

    /**
     * @inheritDoc
     */
    public static function getAnchorKey(): string
    {
        return self::ANCHOR_KEY;
    }
}
