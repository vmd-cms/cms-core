<?php

namespace VmdCms\CoreCms\CoreModules\Navigations\Models;

use VmdCms\CoreCms\Models\CmsInfoModel;

class CoreNavInfo extends CmsInfoModel
{
    protected $fillable = ['lang_key', 'core_navs_id', 'title'];

    public static function table(): string
    {
        return 'core_navs_info';
    }
}
