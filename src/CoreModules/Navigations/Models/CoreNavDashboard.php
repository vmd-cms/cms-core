<?php

namespace VmdCms\CoreCms\CoreModules\Navigations\Models;

class CoreNavDashboard extends CoreNav
{
    const ANCHOR_KEY = 'nav_dashboard';

    /**
     * @inheritDoc
     */
    public static function getAnchorKey(): string
    {
        return self::ANCHOR_KEY;
    }
}
