<?php

namespace VmdCms\CoreCms\CoreModules\Navigations\Models;

use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;
use VmdCms\CoreCms\Contracts\Models\OrderableInterface;
use VmdCms\CoreCms\Contracts\Models\TreeableInterface;
use VmdCms\CoreCms\CoreModules\Sections\Models\CoreSection;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Activable;
use VmdCms\CoreCms\Traits\Models\HasInfo;
use VmdCms\CoreCms\Traits\Models\Orderable;
use VmdCms\CoreCms\Traits\Models\Treeable;
use Illuminate\Database\Eloquent\Builder;

class CoreNav extends CmsModel implements ActivableInterface,OrderableInterface,TreeableInterface,HasInfoInterface
{
    use Orderable,Treeable,Activable,HasInfo;

    protected $fillable = ['parent_id','section_id','slug','title','icon','order','active'];

    /**
     * @return null|string
     */
    public static function getAnchorKey() : ?string
    {
        return null;
    }

    protected static function boot()
    {
        parent::boot();

        if(static::getAnchorKey())
        {
            static::addGlobalScope('anchor', function (Builder $builder) {
                $builder->where('anchor', static::getAnchorKey());
            });
            static::saving(function (CoreNav $model){
                $model->anchor = static::getAnchorKey();
            });
        }
    }

    private function getInfoClass() : string
    {
        return self::class . 'Info';
    }


    public static function table(): string
    {
        return 'core_navs';
    }

    public function section()
    {
        return $this->hasOne(CoreSection::class,'id','section_id');
    }

    public function getTitleLangAttribute(){
        return $this->info->title ?? $this->attributes['title'];
    }
}
