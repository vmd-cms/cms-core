<?php

namespace VmdCms\CoreCms\CoreModules\Moderators\Sections;

use Illuminate\Support\Str;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Contracts\Models\StaticInstanceInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\CoreModules\Moderators\Entity\AuthEntity;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Models\CmsSection;
use VmdCms\CoreCms\Traits\Models\HasStaticInstance;

class Moderator extends CmsSection implements StaticInstanceInterface
{
    use HasStaticInstance;

    /**
     * @var string
     */
    protected $slug = 'moderators';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('moderators');
    }

    public function display()
    {
        return Display::dataTable([
            Column::text('full_name'),
            Column::text('email'),
            Column::text('role.title', CoreLang::get('title')),
            Column::date('created_at')->setFormat('Y-m-d'),
        ])->setSearchable(true);
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return $this->edit(null);
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        $model = \VmdCms\CoreCms\CoreModules\Moderators\Models\Moderator::find($id);
        if($model){
            $this->setCreatable(false);
            $this->title = 'Профиль:' . Str::ucfirst($model->full_name);
        }

        $components = [
            FormComponent::image('photo')
                ->setHelpText(CoreLang::get('recommended_dimensions') . '141px x 180px'),
            FormComponent::input('full_name')->addClass('max-width-475px', 'component')->required(),
            FormComponent::input('email')->email()->unique()->required(),
            FormComponent::input('phone'),
            FormComponent::input('position'),
            FormComponent::password('passwordHash',CoreLang::get('new_password')),
        ];

        if(parent::isEditable($model) && $this->sectionCheckPerms(ModeratorRole::class,'edit_perm')){
            $components[] =   FormComponent::select('moderator_role_id',CoreLang::get('access_rules'))
                ->setModelForOptions(\VmdCms\CoreCms\CoreModules\Moderators\Models\ModeratorRole::class)
                ->setDisplayField('title')
                ->setForeignField('id')
                ->addClass('max-width-150px', 'component');
        }

        return Form::panel($components)->hideFloatActionGroup();
    }

    public function getCmsModelClass(): string
    {
        return \VmdCms\CoreCms\CoreModules\Moderators\Models\Moderator::class;
    }

    public function isEditable(CmsModel $model = null): bool
    {
        $authModeratorId = AuthEntity::getAuthModeratorId();
        if($model && $authModeratorId && $authModeratorId == $model->id){
            return true;
        }
        return parent::isEditable($model);
    }
}
