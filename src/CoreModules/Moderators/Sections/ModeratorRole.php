<?php

namespace VmdCms\CoreCms\CoreModules\Moderators\Sections;

use VmdCms\CoreCms\Collections\FormTabsCollection;
use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Contracts\Models\StaticInstanceInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\CoreModules\Moderators\Entity\AuthEntity;
use VmdCms\CoreCms\CoreModules\Moderators\Models\ModeratorRolePermission;
use VmdCms\CoreCms\CoreModules\Sections\Models\CoreSection;
use VmdCms\CoreCms\Dashboard\Forms\Components\FormTab;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Facades\FormTableColumn;
use VmdCms\CoreCms\Models\CmsSection;
use VmdCms\CoreCms\Traits\Models\HasStaticInstance;

class ModeratorRole extends CmsSection implements StaticInstanceInterface
{
    use HasStaticInstance;

    /**
     * @var string
     */
    protected $slug = 'moderator-roles';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('moderator_roles');
    }

    public function display()
    {
        return Display::dataTable([
            Column::text('id','#'),
            Column::text('title'),
            Column::text('description'),
            Column::date('created_at')->setFormat('Y-m-d'),
        ])->setSearchable(true);
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return Form::panel([
            FormComponent::input('title')->required(),
            FormComponent::input('slug')->required()->unique(),
            FormComponent::text('description')
        ]);
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        $sections = CoreSection::get();
        $permissions = ModeratorRolePermission::where('moderator_role_id',$id)->get();

        $tabs = new FormTabsCollection();
        $tabs->appendItems([
            new FormTab(CoreLang::get('main'),[
                FormComponent::input('title')->required(),
                FormComponent::input('slug')->required(),
                FormComponent::text('description')
            ]),
            new FormTab(CoreLang::get('access_rules'),[
                FormComponent::view('vmd_cms::admin.views.sections.forms.components.custom.moderator_permissions')
                    ->setData(['sections' => $sections,'permissions' => $permissions])
                    ->setStoreCallback(function ($model){
                        $request = request();
                        $authModeratorId = AuthEntity::getAuthModeratorId();
                        $displayPerms = $request->get('display',[]);
                        $createPerms = $request->get('create',[]);
                        $viewPerms = $request->get('view',[]);
                        $editPerms = $request->get('edit',[]);
                        $deletePerms = $request->get('delete',[]);
                        $sectionIds = $request->get('section_id',[]);

                        foreach ($sectionIds as $key => $sectionId){

                            try {

                                ModeratorRolePermission::updateOrCreate([
                                    'moderator_role_id' => $model->id,
                                    'core_section_id' => $sectionId,
                                ],
                                    [
                                        'display_perm' => isset($displayPerms[$key]),
                                        'create_perm' => isset($createPerms[$key]),
                                        'view_perm' => isset($viewPerms[$key]),
                                        'edit_perm' => isset($editPerms[$key]),
                                        'delete_perm' => isset($deletePerms[$key]),
                                        'moderator_id' => $authModeratorId
                                    ]);

                            }catch (\Exception $exception){

                            }

                        }
                })->wide(),
            ]),
        ]);
        return Form::tabbed($tabs);
    }

    public function getCmsModelClass(): string
    {
        return \VmdCms\CoreCms\CoreModules\Moderators\Models\ModeratorRole::class;
    }
}
