<?php

namespace VmdCms\CoreCms\CoreModules\Moderators\Models;

use VmdCms\CoreCms\Contracts\Models\CmsRelatedModelInterface;
use VmdCms\CoreCms\Contracts\Models\OrderableInterface;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Orderable;

class ModeratorRolePermission extends CmsModel implements CmsRelatedModelInterface, OrderableInterface
{
    use Orderable;

    protected $fillable = ['moderator_role_id','core_section_id','display_perm','create_perm','view_perm','edit_perm','delete_perm','moderator_id'];

    public static function table(): string
    {
        return 'moderator_role_permission';
    }

    public static function getForeignField(): string
    {
        return 'moderator_role_id';
    }

    public static function getBaseModelClass(): string
    {
        return Moderator::class;
    }
}
