<?php

namespace VmdCms\CoreCms\CoreModules\Moderators\Models;

use VmdCms\CoreCms\Models\CmsModel;

class ModeratorRole extends CmsModel
{
    protected $fillable = ['title', 'slug', 'description', 'active'];

    public static function table(): string
    {
        return 'moderator_roles';
    }

    public function permissions()
    {
        return $this->hasMany(ModeratorRolePermission::class,'moderator_role_id','id')->order();
    }
}
