<?php

namespace VmdCms\CoreCms\CoreModules\Moderators\Models;

use Illuminate\Contracts\Auth\Authenticatable;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\HasImagePath;

class Moderator extends CmsModel implements Authenticatable
{
    use \Illuminate\Auth\Authenticatable, HasImagePath;

    protected $fillable = ['name','email','password', 'moderator_role_id', 'has_full_access', 'active'];

    public static function table(): string
    {
        return "moderators";
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->hasOne(ModeratorRole::class,'id','moderator_role_id')->with('permissions');
    }

    public function setPasswordHashAttribute($value)
    {
        if(!empty($value)) $this->attributes['password'] = $value;
    }

    public function getPasswordHashAttribute()
    {
        return null;
    }

    public function getFullNameAttribute()
    {
        return implode(' ',array_merge(
                [$this->attributes['first_name'] ?? ''],
                [$this->attributes['last_name'] ?? ''],
                [$this->attributes['father_name'] ?? '']
            )
        );
    }

    public function setFullNameAttribute($value = null)
    {
        $exploded = $value ? explode(' ',$value) : [];

        $this->attributes['first_name'] = count($exploded) ? array_shift($exploded) : null;
        $this->attributes['last_name'] = count($exploded) ? array_shift($exploded) : null;
        $this->attributes['father_name'] = count($exploded) ? implode(' ', $exploded) : null;
    }

}
