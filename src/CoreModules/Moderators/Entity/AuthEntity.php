<?php

namespace VmdCms\CoreCms\CoreModules\Moderators\Entity;

use VmdCms\CoreCms\CoreModules\Moderators\Models\Moderator;

class AuthEntity
{
    /**
     * @return int|null
     */
    public static function getAuthModeratorId(): ?int
    {
        return auth()->guard('moderator')->id();
    }

    /**
     * @return Moderator|null
     */
    public static function getAuthModerator(): ?Moderator
    {
        return auth()->guard('moderator')->user();
    }

    /**
     * @return int|null
     */
    public static function getAuthUserId(): ?int
    {
        return auth()->guard('user')->id();
    }
}
