<?php

namespace VmdCms\CoreCms\CoreModules\Modules\Models;

use VmdCms\CoreCms\CoreModules\Modules\Entities\InitializerEntity;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Activable;

class CoreModule extends CmsModel
{
    use Activable;

    protected $fillable = ['slug', 'title', 'module_initializer_class', 'active'];

    protected static function boot()
    {
        parent::boot();

        static::saving(function (CoreModule $model){
            $model->module_initializer_class = InitializerEntity::getInstance()->getClassBySlug($model->slug);
        });
    }

    public static function table(): string
    {
        return 'core_modules';
    }
}
