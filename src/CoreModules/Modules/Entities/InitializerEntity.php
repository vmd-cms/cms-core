<?php

namespace VmdCms\CoreCms\CoreModules\Modules\Entities;

use ReflectionClass;
use VmdCms\CoreCms\Contracts\Modules\ModuleInitializerInterface;
use VmdCms\CoreCms\Exceptions\Models\ClassNotFoundException;

class InitializerEntity
{

    private $initializers = array();

    /**
     * @var InitializerEntity
     */
    private static $instance;

    /**
     * ModuleEntity constructor.
     */
    private function __construct()
    {
        $this->initializers = $this->getModuleInitializerClasses();
    }

    /**
     * @return InitializerEntity
     */
    public static function getInstance()
    {
        if(!self::$instance) self::$instance = new self();
        return self::$instance;
    }

    /**
     * @return array
     */
    public function getSlugs()
    {
        return array_keys($this->initializers);
    }

    /**
     * @param string $slug
     * @return mixed
     * @throws ClassNotFoundException
     */
    public function getClassBySlug(string $slug)
    {
        if(!isset($this->initializers[$slug])) throw new ClassNotFoundException();
        return $this->initializers[$slug];
    }

    /**
     * @return array
     */
    private function getModuleInitializerClasses()
    {
        $classes = get_declared_classes();
        $initializers =[];
        foreach($classes as $class) {
            try {
                $reflect = new ReflectionClass($class);
                if(!$reflect->isAbstract() && $reflect->implementsInterface(ModuleInitializerInterface::class))
                    $initializers[$class::moduleSlug()] = $class;
            }
            catch (\Exception $exception){
                continue;
            }
        }
        return $initializers;
    }
}
