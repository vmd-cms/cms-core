<?php

namespace VmdCms\CoreCms\CoreModules\Modules\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Contracts\Models\StaticInstanceInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\CoreModules\Modules\Entities\InitializerEntity;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Models\CmsSection;
use VmdCms\CoreCms\Traits\CrudPolicy\ProtectedCrudPolicy;
use VmdCms\CoreCms\Traits\Models\HasStaticInstance;

class CoreModule extends CmsSection implements StaticInstanceInterface
{
    use HasStaticInstance,ProtectedCrudPolicy;

    /**
     * @var string
     */
    protected $slug = 'core-modules';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('modules');
    }

    /**
     * @return DisplayInterface
     */
    public function display()
    {
        return Display::dataTable([
            Column::text('slug'),
            Column::text('title'),
            Column::text('module_initializer_class', 'Module Initializer Class'),
            Column::text('active'),
            Column::date('created_at')->setFormat('Y-m-d'),
        ])->setSearchable(true);
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return $this->edit(null);
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::select('slug')->setEnumValues(InitializerEntity::getInstance()->getSlugs())->unique()->required(),
            FormComponent::input('module_initializer_class','Module Initializer Class')->unique()->required(),
            FormComponent::switch('active'),
        ]);
    }

    public function getCmsModelClass(): string
    {
        return \VmdCms\CoreCms\CoreModules\Modules\Models\CoreModule::class;
    }

}
