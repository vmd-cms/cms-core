<?php

namespace VmdCms\CoreCms\CoreModules\Settings\Entity;

use Illuminate\Support\Collection;
use VmdCms\CoreCms\CoreModules\Settings\Models\AdminGroup\AdminGroup;
use VmdCms\CoreCms\CoreModules\Settings\Models\AdminGroup\Emails;
use VmdCms\CoreCms\CoreModules\Settings\Models\GeneralGroup\GeneralGroup;
use VmdCms\CoreCms\CoreModules\Settings\Models\Settings;

class SettingsEntity
{
    protected $settings;

    protected static $instance;

    private function __construct(){
        $this->settings = Settings::active()->get();
    }

    public static function getInstance(){
        if(!static::$instance instanceof SettingsEntity){
            static::$instance = new SettingsEntity();
        }
        return static::$instance;
    }

    /**
     * @param string $key
     * @return Collection|null
     */
    public function getAdminSettings(string $key): ?Collection
    {
        return $this->settings->where('group',AdminGroup::GROUP)->where('key',$key);
    }

    /**
     * @param string $key
     * @return Collection|null
     */
    public function getGeneralSettings(string $key): ?Collection
    {
        return $this->settings->where('group',GeneralGroup::GROUP)->where('key',$key);
    }

    /**
     * @return array
     */
    public function getAdminEmails(): array
    {
        return $this->getValues(AdminGroup::GROUP,Emails::KEY);
    }

    /**
     * @return array
     */
    public function getAdminPhones(): array
    {
        return $this->getValues(AdminGroup::GROUP,Emails::KEY);
    }

    /**
     * @param string $group
     * @param string $key
     * @return array
     */
    public function getValues(string $group, string $key): array
    {
        $settings = $this->settings->where('group',$group)->where('key',$key);
        $values = [];
        if(is_countable($settings) && count($settings)){
            foreach ($settings as $item){
                $values[] = $item->value;
            }
        }
        return $values;
    }
}
