<?php

namespace VmdCms\CoreCms\CoreModules\Settings\Sections;

use App\Modules\Prices\Models\Currency;
use VmdCms\CoreCms\Collections\FormTabsCollection;
use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Contracts\Models\StaticInstanceInterface;
use VmdCms\CoreCms\CoreModules\Content\Models\MockCmsModel;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\CoreModules\Languages\Models\CoreLanguage;
use VmdCms\CoreCms\CoreModules\Settings\Models\GeneralGroup\Addresses;
use VmdCms\CoreCms\CoreModules\Settings\Models\GeneralGroup\Emails;
use VmdCms\CoreCms\CoreModules\Settings\Models\GeneralGroup\Logo;
use VmdCms\CoreCms\CoreModules\Settings\Models\GeneralGroup\Map;
use VmdCms\CoreCms\CoreModules\Settings\Models\GeneralGroup\Phones;
use VmdCms\CoreCms\CoreModules\Settings\Models\GeneralGroup\Socials;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormCancelButton;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormSaveButton;
use VmdCms\CoreCms\Dashboard\Forms\Components\FormTab;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Facades\FormTableColumn;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Models\CmsSection;
use VmdCms\CoreCms\Services\CoreRouter;
use VmdCms\CoreCms\Traits\Models\HasStaticInstance;

class Settings extends CmsSection implements StaticInstanceInterface
{
    use HasStaticInstance;

    /**
     * @var string
     */
    protected $slug = 'settings';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('settings');
    }

    /**
     * @return DisplayInterface
     */
    public function display()
    {
        $tabs = new FormTabsCollection();

        $tabItems[] = new FormTab(CoreLang::get('main'),[
                FormComponent::datatable('languages')->setComponents([
                    FormTableColumn::input('title'),
                    FormTableColumn::switch('default')->setDisabled(true)->alignCenter(),
                    FormTableColumn::switch('active')->setDisabled(true)->alignCenter(),
                ])->setIsCreatable(false)
                    ->setIsDeletable(false)
                    ->setCustomModelClass(CoreLanguage::class)
                    ->setPaddingBottom(50)
                    ->displayRow(),
                FormComponent::datatable('currency')->setComponents([
                    FormTableColumn::input('info.title',CoreLang::get('title'))->unique()->maxLength(5)->minLength(2),
                    FormTableColumn::input('info.symbol',CoreLang::get('symbol')),
                    FormTableColumn::input('rate')->float(),
                    FormTableColumn::switch('default'),
                    FormTableColumn::switch('active'),
                ])->setIsCreatable(false)
                    ->setIsDeletable(false)
                    ->setCustomModelClass(Currency::class)
                    ->setPaddingBottom(50)
                    ->displayRow(),
                FormComponent::datatable('phones',CoreLang::get('phone_numbers'))->setComponents([
                    FormTableColumn::input('title',CoreLang::get('text'))->maxLength(32),
                    FormTableColumn::input('value',CoreLang::get('link'))->minLength(6)->maxLength(15),
                    FormTableColumn::switch('active'),
                ])->setCustomModelClass(Phones::class)
                    ->setPaddingBottom(50)
                    ->displayRow(),
                FormComponent::datatable('emails')->setComponents([
                    FormTableColumn::input('value',CoreLang::get('address'))->email(),
                    FormTableColumn::switch('active'),
                ])->setCustomModelClass(Emails::class)
                    ->setPaddingBottom(50)
                    ->displayRow(),
                FormComponent::datatable('socials')->setComponents([
                    FormTableColumn::input('title'),
                    FormTableColumn::input('value',CoreLang::get('link')),
                    FormTableColumn::image('media',CoreLang::get('upload_icon')),
                    FormTableColumn::switch('active'),
                ])->setCustomModelClass(Socials::class)
                    ->setPaddingBottom(50)
                    ->displayRow(),
                FormComponent::datatable('addresses')->setComponents([
                    FormTableColumn::input('value'),
                    FormTableColumn::switch('active'),
                ])->setCustomModelClass(Addresses::class)
                    ->setPaddingBottom(50)
                    ->displayRow(),
                FormComponent::datatable('map')->setComponents([
                    FormTableColumn::input('value'),
                    FormTableColumn::switch('active'),
                ])->setCustomModelClass(Map::class)
                    ->setPaddingBottom(50)
                    ->displayRow(),
                FormComponent::datatable('logo')->setComponents([
                    FormTableColumn::select('value',CoreLang::get('location'))
                        ->setEnumValues(Logo::getAnchors())
                        ->unique()
                        ->required(),
                    FormTableColumn::image('media',CoreLang::get('upload_logo')),
                    FormTableColumn::switch('active'),
                ])->setCustomModelClass(Logo::class),
            ]);

        $tabs->appendItems($tabItems);

        return Form::tabbed($tabs)
            ->setSectionSlug($this->slug)
            ->setAction(CoreRouter::getDisplayRoute($this->slug))
            ->setMethod('POST')
            ->setButtons([
                new FormSaveButton(),
                new FormCancelButton()
            ]);
    }

    public function isCreatable(): bool
    {
        return false;
    }

    public function isViewable(CmsModel $model = null): bool
    {
        return false;
    }

    public function isDeletable(CmsModel $model = null): bool
    {
        return false;
    }

    public function getCmsModelClass(): string
    {
        return MockCmsModel::class;
    }
}
