<?php

namespace VmdCms\CoreCms\CoreModules\Settings\Models;

use VmdCms\CoreCms\Models\CmsInfoModel;

class SettingsInfo extends CmsInfoModel
{
    public static function table(): string
    {
        return 'settings_info';
    }
}
