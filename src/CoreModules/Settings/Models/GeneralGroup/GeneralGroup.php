<?php

namespace VmdCms\CoreCms\CoreModules\Settings\Models\GeneralGroup;

use VmdCms\CoreCms\CoreModules\Settings\Models\Settings;

class GeneralGroup extends Settings
{
    const GROUP = 'general';

}
