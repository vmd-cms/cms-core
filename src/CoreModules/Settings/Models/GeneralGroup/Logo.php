<?php

namespace VmdCms\CoreCms\CoreModules\Settings\Models\GeneralGroup;

class Logo extends GeneralGroup
{
    const KEY = 'logo';

    const HEADER = 'header';
    const FOOTER = 'footer';

    public static function getAnchors()
    {
        return [
            self::HEADER => self::HEADER,
            self::FOOTER => self::FOOTER
        ];
    }
}
