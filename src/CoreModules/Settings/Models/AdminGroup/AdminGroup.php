<?php

namespace VmdCms\CoreCms\CoreModules\Settings\Models\AdminGroup;

use VmdCms\CoreCms\CoreModules\Settings\Models\Settings;

class AdminGroup extends Settings
{
    const GROUP = 'admin';
}
