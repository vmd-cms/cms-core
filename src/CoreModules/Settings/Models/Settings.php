<?php

namespace VmdCms\CoreCms\CoreModules\Settings\Models;

use Illuminate\Database\Eloquent\Builder;
use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;
use VmdCms\CoreCms\Contracts\Models\OrderableInterface;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Activable;
use VmdCms\CoreCms\Traits\Models\HasInfo;
use VmdCms\CoreCms\Traits\Models\JsonData;
use VmdCms\CoreCms\Traits\Models\Orderable;

class Settings extends CmsModel implements OrderableInterface,ActivableInterface,HasInfoInterface
{
    use Orderable, Activable, JsonData, HasInfo;

    const GROUP = null;
    const KEY = null;

    public static function table(): string
    {
        return 'settings';
    }

    public static function getModelGroup(){
        return static::GROUP;
    }

    public static function getModelKey(){
        return static::KEY;
    }

    protected function getInfoClass(): string
    {
        return SettingsInfo::class;
    }

    protected static function boot()
    {
        parent::boot();

        if(static::getModelGroup()){

            static::addGlobalScope('group', function (Builder $builder) {
                $builder->where('group', static::getModelGroup());
            });

            static::saving(function (Settings $model){
                $model->group = static::getModelGroup();
            });
        }

        if(static::getModelKey()){
            static::addGlobalScope('key', function (Builder $builder) {
                $builder->where('key', static::getModelKey());
            });
            static::saving(function (Settings $model){
                $model->key = static::getModelKey();
            });
        }

    }

}
