<?php

namespace VmdCms\CoreCms\CoreModules\Settings\Initializers;;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use VmdCms\CoreCms\Exceptions\Services\StubBuilderException;
use VmdCms\CoreCms\Exceptions\Services\StubReplacerException;
use VmdCms\CoreCms\Initializers\AbstractModuleInitializer;

class ModuleInitializer extends AbstractModuleInitializer
{
    const SLUG = 'settings';
    const ALIAS = 'Settings';

    public function __construct()
    {
        parent::__construct();
        $this->stubBuilder->setPublishControllers(false);
        $this->stubBuilder->setPublishSeed(false);
    }

    /**
     * @inheritDoc
     */
    public static function moduleAlias(): string
    {
        return self::ALIAS;
    }

    /**
     * @inheritDoc
     */
    public static function moduleSlug(): string
    {
        return self::SLUG;
    }

    /**
     * @throws FileNotFoundException
     * @throws StubBuilderException
     * @throws StubReplacerException
     */
    public function install()
    {
        $this->publishModuleClasses();
    }
}
