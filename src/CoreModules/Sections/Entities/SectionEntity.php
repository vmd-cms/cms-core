<?php

namespace VmdCms\CoreCms\CoreModules\Sections\Entities;

use VmdCms\CoreCms\Contracts\Sections\AdminSectionInterface;
use ReflectionClass;

class SectionEntity
{
    public static function getAdminSectionsList()
    {
        $classes = get_declared_classes();
        $sections = array();
        foreach($classes as $class) {

            $reflect = new ReflectionClass($class);
            if($reflect->implementsInterface(AdminSectionInterface::class) && !$reflect->isAbstract())
                $sections[] = $class;

        }
        return $sections;
    }
}
