<?php

namespace VmdCms\CoreCms\CoreModules\Sections\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Contracts\Models\StaticInstanceInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\CoreModules\Modules\Models\CoreModule;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\ColumnEditable;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Models\CmsSection;
use VmdCms\CoreCms\Traits\CrudPolicy\ProtectedCrudPolicy;
use VmdCms\CoreCms\Traits\Models\HasStaticInstance;

class CoreSection extends CmsSection implements StaticInstanceInterface
{
    use HasStaticInstance,ProtectedCrudPolicy;

    /**
     * @var string
     */
    protected $slug = 'core-sections';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('sections');
    }

    public function display()
    {
        return Display::dataTable([
            Column::text('title'),
            Column::text('core_section_class', 'Admin Section Class'),
            ColumnEditable::switch('active'),
            Column::date('created_at')->setFormat('Y-m-d'),
        ])->setSearchable(true);
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return $this->edit(null);
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::switch('active'),
            FormComponent::input('title')->required(),
            FormComponent::select('module_id',CoreLang::get('module'))
                ->setDisplayField('slug')
                ->setModelForOptions(CoreModule::class),
            FormComponent::input('core_section_class','Admin Section Class')->unique()
        ]);
    }

    public function getCmsModelClass(): string
    {
        return \VmdCms\CoreCms\CoreModules\Sections\Models\CoreSection::class;
    }
}
