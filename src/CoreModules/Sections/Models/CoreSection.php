<?php

namespace VmdCms\CoreCms\CoreModules\Sections\Models;

use VmdCms\CoreCms\Contracts\Models\OrderableInterface;
use VmdCms\CoreCms\CoreModules\Modules\Models\CoreModule;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Activable;
use VmdCms\CoreCms\Traits\Models\Orderable;

class CoreSection extends CmsModel implements OrderableInterface
{
    use Orderable,Activable;

    protected $fillable = ['module_id','core_section_class','title','order','active'];

    public function coreModule()
    {
        return $this->belongsTo(CoreModule::class, 'module_id' , 'id');
    }

    public static function table(): string
    {
        return 'core_sections';
    }
}
