<?php

namespace VmdCms\CoreCms\CoreModules\Seo\Models;

use VmdCms\CoreCms\Models\CmsInfoModel;

class SeoInfo extends CmsInfoModel
{
    protected $fillable = ['seo_id'];

    public static function table(): string
    {
        return 'seo_info';
    }
}
