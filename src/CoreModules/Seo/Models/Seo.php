<?php

namespace VmdCms\CoreCms\CoreModules\Seo\Models;

use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Services\Files\StorageAdapter;
use VmdCms\CoreCms\Traits\Models\HasInfo;
use VmdCms\CoreCms\Traits\Models\JsonData;

class Seo extends CmsModel implements HasInfoInterface
{
    use HasInfo, JsonData;

    protected $fillable = ['resource','resource_id'];

    public static function table(): string
    {
        return "seo";
    }

    public function getMetaTitleAttribute(){
        return $this->info->meta_title ?? '';
    }

    public function getMetaDescriptionAttribute(){
        return $this->info->meta_description ?? '';
    }

    public function getMetaTextAttribute(){
        return $this->info->meta_text ?? '';
    }

    public function resource()
    {
        return $this->morphTo();
    }

    /**
     * @return string
     */
    public function getMetaPhotoPathAttribute()
    {
        return StorageAdapter::getInstance()->getAbsolutePath($this->meta_photo,false);
    }

}
