<?php

namespace VmdCms\CoreCms\CoreModules\Seo\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Contracts\Models\StaticInstanceInterface;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Models\CmsSection;
use VmdCms\CoreCms\Traits\Models\HasStaticInstance;

class Seo extends CmsSection implements StaticInstanceInterface
{
    use HasStaticInstance;

    /**
     * @var string
     */
    protected $slug = 'seo';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return "SEO";
    }

    public function display()
    {
        return Display::dataTable([
            Column::text('resource','Resource'),
            Column::text('url','Url'),
            Column::date('created_at')->setFormat('Y-m-d'),
        ])->setSearchable(true);
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return $this->edit(null);
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::input('resource')->required(),
            FormComponent::input('resource_id','Resource id')->required(),
            FormComponent::input('url','URL')->required(),
            FormComponent::image('meta_photo','Meta Photo'),
        ]);
    }

    public function getCmsModelClass(): string
    {
        return \VmdCms\CoreCms\CoreModules\Seo\Models\Seo::class;
    }
}
