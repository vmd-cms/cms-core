<?php

namespace VmdCms\CoreCms\CoreModules\Seo\DTO;

use VmdCms\CoreCms\Collections\CoreCollectionAbstract;

class BreadCrumbDTOCollection extends CoreCollectionAbstract
{
    /**
     * @param BreadCrumbDTO $dto
     */
    public function append(BreadCrumbDTO $dto)
    {
        $this->collection->add($dto);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $arr = [];
        $this->collection->map(function (BreadCrumbDTO $item) use (&$arr){
            $arr[] = $item->toArray();
        });
        return $arr;
    }
}
