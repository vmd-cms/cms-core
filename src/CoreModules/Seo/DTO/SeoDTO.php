<?php

namespace VmdCms\CoreCms\CoreModules\Seo\DTO;

use VmdCms\CoreCms\Contracts\DTO\SeoDTOInterface;
use VmdCms\CoreCms\Contracts\Models\SeoableInterface;
use VmdCms\CoreCms\CoreModules\Content\Services\ContentReplacer;
use VmdCms\CoreCms\Services\Files\StorageAdapter;

class SeoDTO implements SeoDTOInterface
{
    /**
     * @var string
     */
    public $metaTitle;

    /**
     * @var string
     */
    public $metaDescription;

    /**
     * @var string
     */
    public $metaText;

    /**
     * @var string
     */
    public $metaPhoto;

    /**
     * @var BreadCrumbDTOCollection
     */
    public $breadCrumbs;

    public function __construct()
    {
        $this->breadCrumbs = new BreadCrumbDTOCollection();
    }

    /**
     * @param BreadCrumbDTOCollection $collection
     * @return $this
     */
    public function setBreadcrumbs(BreadCrumbDTOCollection $collection): SeoDTOInterface
    {
        $this->breadCrumbs = $collection;
        return $this;
    }

    /**
     * @param BreadCrumbDTO $item
     * @return SeoDTOInterface
     */
    public function appendBreadcrumb(BreadCrumbDTO $item): SeoDTOInterface
    {
        $this->breadCrumbs->append($item);
        return $this;
    }

    /**
     * @param SeoableInterface $model
     * @return null|SeoDTOInterface
     */
    public function setData(SeoableInterface $model) : ?SeoDTOInterface
    {
        if(!$seo = $model->seo ?? null) return null;

        $this->metaTitle = $seo->info->meta_title ?? null;
        $this->metaDescription = $seo->info->meta_description ?? null;
        $this->metaText = ContentReplacer::replacePatches($seo->info->meta_text ?? null);
        $this->metaPhoto = StorageAdapter::getInstance()->getAbsolutePath($seo->meta_photo, false);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    /**
     * @return string|null
     */
    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    /**
     * @param string $metaTitle
     * @return SeoDTO
     */
    public function setMetaTitle(string $metaTitle): SeoDTO
    {
        $this->metaTitle = $metaTitle;
        return $this;
    }

    /**
     * @param string $metaDescription
     * @return SeoDTO
     */
    public function setMetaDescription(string $metaDescription): SeoDTO
    {
        $this->metaDescription = $metaDescription;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMetaText(): ?string
    {
        return $this->metaText;
    }

    /**
     * @param string|null $metaText
     * @return $this
     */
    public function setMetaText(string $metaText = null): SeoDTO
    {
        $this->metaText = $metaText;
        return $this;
    }

    /**
     * @param string|null $metaPhoto
     * @return $this
     */
    public function setMetaPhoto(string $metaPhoto = null): SeoDTO
    {
        $this->metaPhoto = $metaPhoto;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'meta_title' => $this->metaTitle,
            'meta_description' => $this->metaDescription,
            'meta_text' => $this->metaText,
            'meta_photo' => $this->metaPhoto,
            'breadcrumbs' => $this->breadCrumbs instanceof BreadCrumbDTOCollection ? $this->breadCrumbs->toArray() : []
        ];
    }

}
