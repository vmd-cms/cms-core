<?php

namespace VmdCms\CoreCms\CoreModules\Seo\DTO;

use Illuminate\Contracts\Support\Arrayable;

class BreadCrumbDTO implements Arrayable
{
    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $url;

    public function __construct(string $title = null, string $url = null)
    {
        $this->title = $title;
        $this->url = $url;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
          'title' => $this->title,
          'url' => $this->url
        ];
    }

}
