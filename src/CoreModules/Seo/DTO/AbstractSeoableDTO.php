<?php

namespace VmdCms\CoreCms\CoreModules\Seo\DTO;

use VmdCms\CoreCms\Contracts\DTO\SeoableDTOInterface;
use VmdCms\CoreCms\Contracts\DTO\SeoDTOInterface;

abstract class AbstractSeoableDTO implements SeoableDTOInterface
{
    /**
     * @var SeoDTOInterface
     */
    private $seoDTO;

    /**
     * @param SeoDTOInterface $seoDTO
     * @return SeoableDTOInterface
     */
    public function setSeoDTO(SeoDTOInterface $seoDTO): SeoableDTOInterface
    {
        $this->seoDTO = $seoDTO;
        return $this;
    }

    /**
     * @return SeoDTOInterface
     */
    public function getSeoDTO(): SeoDTOInterface
    {
        if(!$this->seoDTO instanceof SeoDTOInterface){
            $this->seoDTO = new SeoDTO();
        }
        return $this->seoDTO;
    }

}
