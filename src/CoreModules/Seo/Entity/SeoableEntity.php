<?php

namespace VmdCms\CoreCms\CoreModules\Seo\Entity;

use VmdCms\CoreCms\Contracts\Models\HasSeoTemplateInterface;
use VmdCms\CoreCms\Contracts\Models\SeoableInterface;
use VmdCms\CoreCms\CoreModules\Content\Entity\Languages;
use VmdCms\CoreCms\CoreModules\Content\Entity\Translates;
use VmdCms\CoreCms\CoreModules\Seo\DTO\BreadCrumbDTO;
use VmdCms\CoreCms\CoreModules\Seo\DTO\BreadCrumbDTOCollection;
use VmdCms\CoreCms\CoreModules\Seo\DTO\SeoDTO;
use VmdCms\CoreCms\Exceptions\Models\ClassNotFoundException;
use VmdCms\CoreCms\Models\CmsModel;

class SeoableEntity
{
    /**
     * @var array
     */
    protected $replaces;

    /**
     * @param array $replaces
     * @return SeoableEntity
     */
    public function setReplaces(array $replaces): SeoableEntity
    {
        $this->replaces = $replaces;
        return $this;
    }

    public function getBreadcrumbs(array $items = [], bool $fullPath = true): BreadCrumbDTOCollection
    {
        $breadCrumbs = new BreadCrumbDTOCollection();

        $url = Languages::getInstance()->getLocalePrefix();

        $breadCrumbs->append(new BreadCrumbDTO(Translates::getInstance()->getValue('main'),(!empty($url) ? $url : '/')));
        foreach ($items as $item){
            if(isset($item['url'])){
                $urlItem = $url . (substr($item['url'],0,1) !== '/' ? '/' . $item['url'] : $item['url']);
                if($fullPath){
                    $url = $urlItem;
                }
            }else{
                $urlItem = null;
            }
            $breadCrumbs->append(new BreadCrumbDTO($item['title'] ?? '',$urlItem));
        }
        return $breadCrumbs;
    }

    /**
     * @param string $pageClass
     * @return SeoDTO
     * @throws ClassNotFoundException
     */
    public function getPageSeoDTObyClass(string $pageClass): SeoDTO
    {
        if(!class_exists($pageClass)){
            throw  new ClassNotFoundException();
        }
        $page = $pageClass::where('active',true)->first();

        return $this->getPageSeoDTO($page);
    }

    /**
     * @param CmsModel|null $model
     * @return SeoDTO
     */
    public function getPageSeoDTO(CmsModel $model = null): SeoDTO
    {
        $dto =  new SeoDTO();
        if($model instanceof SeoableInterface){
            $dto->setData($model);
        }

        if($model instanceof HasSeoTemplateInterface)
        {
            $title = $model->info->title ?? $model->title ?? null;
            $templateModel = $model->getSeoTemplateModel();
            $templates = $templateModel::where('active',true)->with('info')->get();

            if(empty($dto->getMetaTitle())){
                $metaTitleTemplateModel = $templates->where('key',$templateModel::metaTitleKey())->first();
                $metaTitleTemplate = $metaTitleTemplateModel ? $metaTitleTemplateModel->getTemplate() : null;
                $dto->setMetaTitle(str_replace('[title]',$title,$metaTitleTemplate));
            }

            if(empty($dto->getMetaDescription())){
                $metaDescriptionTemplateModel = $templates->where('key',$templateModel::metaDescriptionKey())->first();
                $metaDescriptionTemplate = $metaDescriptionTemplateModel ? $metaDescriptionTemplateModel->getTemplate() : null;
                $dto->setMetaDescription(str_replace('[title]',$title,$metaDescriptionTemplate));
            }
        }

        if(is_array($this->replaces) && count($this->replaces)){

            $metaTitle = $dto->getMetaTitle();
            $metaDescription = $dto->getMetaDescription();
            $metaText = $dto->getMetaText();

            foreach ($this->replaces as $key=>$value){
                $metaTitle = str_replace($key, $value, $metaTitle);
                $metaDescription = str_replace($key, $value, $metaDescription);
                $metaText = str_replace($key, $value, $metaText);

            }

            $dto->setMetaTitle($metaTitle);
            $dto->setMetaDescription($metaDescription);
            $dto->setMetaText($metaText);
        }

        return $dto;
    }
}
