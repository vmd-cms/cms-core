<?php

namespace VmdCms\CoreCms\CoreModules\Translates\Models\Groups;

use VmdCms\CoreCms\CoreModules\Translates\Models\Translate;
use VmdCms\CoreCms\CoreModules\Translates\Services\GroupEnums;

class GeneralGroupTranslate extends Translate
{
    public static function getModelGroup(): ?string
    {
        return GroupEnums::GENERAL;
    }
}
