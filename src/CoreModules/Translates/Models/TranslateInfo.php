<?php

namespace VmdCms\CoreCms\CoreModules\Translates\Models;

use VmdCms\CoreCms\Models\CmsInfoModel;

class TranslateInfo extends CmsInfoModel
{
    protected $fillable = ['translates_id'];

    public static function table(): string
    {
        return 'translates_info';
    }
}
