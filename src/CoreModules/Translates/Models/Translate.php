<?php

namespace VmdCms\CoreCms\CoreModules\Translates\Models;

use Illuminate\Database\Eloquent\Builder;
use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;
use VmdCms\CoreCms\CoreModules\Content\Models\Menu\Menu;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Activable;
use VmdCms\CoreCms\Traits\Models\HasInfo;

class Translate extends CmsModel implements HasInfoInterface, ActivableInterface
{
    use HasInfo,Activable;

    public static function table(): string
    {
        return 'translates';
    }

    protected function getInfoClass() : string
    {
        return self::class . 'Info';
    }

    public static function getModelGroup(): ?string
    {
        return null;
    }

    protected static function boot()
    {
        parent::boot();

        if(static::getModelGroup()){
            static::addGlobalScope('group', function (Builder $builder) {
                $builder->where('group', static::getModelGroup());
            });
            static::saving(function (Translate $model){
                $model->group = static::getModelGroup();
            });
        }
    }

}
