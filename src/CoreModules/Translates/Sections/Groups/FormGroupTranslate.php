<?php

namespace VmdCms\CoreCms\CoreModules\Translates\Sections\Groups;

use VmdCms\CoreCms\CoreModules\Translates\Sections\AbstractTranslate;

class FormGroupTranslate extends AbstractTranslate
{
    /**
     * @var string
     */
    protected $slug = 'form_translates';

    public function getCmsModelClass(): string
    {
        return \App\Modules\Translates\Models\Groups\FormGroupTranslate::class;
    }
}
