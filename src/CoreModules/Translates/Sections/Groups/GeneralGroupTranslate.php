<?php

namespace VmdCms\CoreCms\CoreModules\Translates\Sections\Groups;

use VmdCms\CoreCms\CoreModules\Translates\Sections\AbstractTranslate;

class GeneralGroupTranslate extends AbstractTranslate
{
    /**
     * @var string
     */
    protected $slug = 'general_translates';

    public function getCmsModelClass(): string
    {
        return \App\Modules\Translates\Models\Groups\GeneralGroupTranslate::class;
    }
}
