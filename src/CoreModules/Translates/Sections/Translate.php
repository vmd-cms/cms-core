<?php

namespace VmdCms\CoreCms\CoreModules\Translates\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Exceptions\Sections\SectionNotAvailableException;

class Translate extends AbstractTranslate
{
    /**
     * @var string
     */
    protected $slug = 'translates';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('translates');
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        throw new SectionNotAvailableException();
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Translates\Models\Translate::class;
    }

    public function isCreatable(): bool
    {
        return false;
    }
}
