<?php

namespace VmdCms\CoreCms\CoreModules\Translates\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\ColumnEditable;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Models\CmsSection;

abstract class AbstractTranslate extends CmsSection
{
    /**
     * @var string
     */
    protected $slug = 'translates';

    public function display()
    {
        return Display::dataTable([
            Column::text('id','#')->setWidth(50)->alignCenter()->setSearchableCallback(function ($query, $search) {
                $query->orWhere('id', 'like', '%' . $search . '%');
            }),
            Column::text('key')->setSearchableCallback(function ($query, $search) {
                $query->orWhere('key', 'like', '%' . $search . '%');
            }),
            ColumnEditable::text('info.value',CoreLang::get('value'))->setSearchableCallback(function ($query,$search){
                $query->orWhereHas('info',function ($q) use ($search){
                    $q->where('value','like','%'.$search.'%');
                });
            }),
            Column::text('description')->setSearchableCallback(function ($query, $search) {
                $query->orWhere('description', 'like', '%' . $search . '%');
            }),
            Column::text('active'),
        ])->setSearchable(true);
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return $this->edit(null);
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::input('key')->unique()->maxLength(64)->required(),
            FormComponent::switch('active'),
            FormComponent::input('description'),
            FormComponent::text('info.value',CoreLang::get('value')),
        ]);
    }
}
