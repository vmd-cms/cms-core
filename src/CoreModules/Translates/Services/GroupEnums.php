<?php

namespace VmdCms\CoreCms\CoreModules\Translates\Services;

use VmdCms\CoreCms\Services\Enums;

class GroupEnums extends Enums
{
    const GENERAL = 'general';
    const FORM = 'form';
}
